# Events and handlers in the ODSS web app

1. 'viewselected'
    1. Fired by
        1. app.js
            1. When user selected a view from the dropdown, there is a handler in app.js that fires this event
    1. Listeners
        1. DataSourceController
            1. Set the repo base path on the data store proxy
        1. LayerController
            1. Calls 'removeAllLayers' on OpenLayer and Leaflet box controllers
            1. Sets view name on LayerTreeStore and reloads
        1. PlatformController
            1. Removes all tracks from the maps by called removeTrack for each track on the OpenLayers and Leaflet box controllers
            1. Clears all the current tracks from the TrackStore
            1. Sets the view name on the TrackStore and reloads
1. 'layerupdated'
    1. Fired by
        1. LayerViewSelectionWindowController
            1. Fired when the user closes the window where they edit what layers are on a view
        1. LayerController
            1. Fired when user saves a layer in the layer editor
    1. Listeners
        1. LayerController
            1. Just loads the LayerTreeStore
1. 'mapready'
    1. Fired by
        1. OpenLayersBoxController
            1. After base map is set
    1. Listeners
        1. LayerController
            1. Calls 'removeAllLayers' on OpenLayer and Leaflet box controllers
            1. Sets the view name on the LayerTreeStore proxy and calls the reload
        1. PlatformController
            1. Loops over the tracks in the TrackStore and calls removeTrack on OpenLayers and Leaflet box controllers
            1. Clears the tracks from the TrackStore
            1. Sets the view name on the PlatformStore and calls reload
1. 'bboxupdated'
    1. Fired by
        1. OpenLayersBoxController
            1. Fired when base map is set
            1. Fired when container moves and the values from the bouding box of the view are different from the one stores on the SpaceTime model
    1. Listeners: NONE!
1. 'move'
    1. Fired by: NONE
    1. Listeners: NONE
1. 'platformselected'
    1. Fired by:
        1. PlatformController
            1. When an item is appended to the platforms grid tree panel, the controller sets the check mark to on and then fires the event that the platform was selected (also sends the raw node along with it)
            1. When the user click on the check box next to the panel (also sends the raw node along with it)
    1. Listeners
        1. TrackController
            1. Grabs or creates the Track object from the Track store and adds a call to get the track data for that selected platform to the queue of track requests for the app to call.
1. 'platformdeselected'
    1. Fired by
        1. PlatformController
            1. Fired when a check box in the platform treed grid panel is unchecked (also sends the raw node along with it)
    1. Listeners
        1. TrackController
            1. Grabs the track from the track store
            1. Calls remove track on OpenLayers and Leaflet box controllers
            1. Removes track from the track store
1. 'platformViewUpdated'
    1. Fired by
        1. PlatformViewSelectionWindowController
            1. Fires when the platform list editor window is closed.
    1. Listeners
        1. PlatformController
            1. Iterates over all the tracks in the track store
            1. Calls remove track on the OpenLayers and Leaflet box controllers for each layer
            1. Sets the view name on the platform store proxy and reloads it.
1. 'timewindowupdated'
    1. Fired by
        1. SpaceTimeController:
            1. Fired when start field date changes
            1. Fired when number of hours field changes
            1. When the user clicks on the right or left step arrows
            1. When the user clicks on the play button it starts a interval timer that calls changes the start date time and then fires the event
    1. Listeners
        1. LayerController
            1. If the controls are in playback mode, it grabs the WMS layers from the OpenLayersBoxController and resets the time options so when they update from the server, they are requesting the correct time.
            1. ************ THIS NEEDS TO BE IMPLEMENTED IN THE LEAFLET BOX CONTROLLER ******
        1. TrackController
            1. Iterates over the tracks in the trackstore
            1. Adds a call to get the track data for that selected platform to the queue of track requests for the app to call.
