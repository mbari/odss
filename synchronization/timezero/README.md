# Timezero Tracking Sync

This directory contains a script that queries for all non-ais platforms in the tracking database and then creates files that can be read by the Timezero software so the platform positions can be plotted on the bridge of a ship.

## Running

To run this, you need to have Python installed (written in version 2.7.14) with they psycopg2 module installed.  You need to configure the connection parameters by defining some environment variables that contains the database connection information.  The variables are:

* TRACKING_DATABASE_HOST: This is the hostname where the tracking database is running.
* TRACKING_DATABASE_NAME: This is the name of the database to connect to (usually mbaritracking)
* TRACKING_DATABASE_USERNAME: The username for the connection to the database
* TRACKING_DATABASE_PASSWORD: The password to use to connect to the database
  
There is a shell script template in this directory (timezero-positions.sh.template) that can help you get it configured to run using a cronjob. Copy the file to a file named timezero-positions.sh and edit.  Add your particular environment variable values and change the WORKING_DIR to point to the location where the script is located.  You can then run the script by running:

```bash
./timezero-positions.sh
```

You can also run the script directly if you have the environment variables defined in the shell already by running the script from the command line using the following syntax:

python ./timezero-positions.py /directory/where/files/will/be/written XXX YYY

Where XXX is the number of minutes in the past to pull data for and YYY (optional) is how many data points from that time frame to write to the resulting timezero files.  Note the results files will be removed and recreated every time this script is run.
