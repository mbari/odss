# This script grabs the most recent positions of non-AIS platforms in the tracking DB
# and creates a file that can be read by Timezero software that is running on our
# ships

# import the os library
import os

# Import the system library
import sys

# Import the PostgreSQL library
import psycopg2

# Import utility to handle command line arguments
from sys import argv

# Import some date utilities
from datetime import datetime, timedelta

# I need the math library for lat/lon conversions
import math

# A flag to indicate if print statements are to be executed
debug = True

# Grab the current date and time
now = datetime.now()

# Grab the script name, the directory to write to, and how many minutes (or number of points) of data to grab
directory_name = argv[1]
minutes_of_data = argv[2]
if debug:
    print now.strftime('%m/%d/%y %H:%M:%S'), ": Starting timezero extraction..."
if debug:
    print "Will write files to", directory_name
if debug:
    print "Will query back", minutes_of_data, "minutes of data"

# If there is a third argument, grab it or default to minutes
num_points = 0
if (len(argv) > 3):
    num_points = int(argv[3])
    if debug:
        print "Will only write", num_points, "positions to the results file"

# Create a current date
query_date = datetime.now() - timedelta(minutes=int(minutes_of_data))

# Grab the environment variables for the Tracking DB connection
database_host = os.environ.get('TRACKING_DATABASE_HOST')
if database_host is None:
    sys.exit('Database host was not defined as the environment variable TRACKING_DATABASE_HOST, please fix')
database_name = os.environ.get('TRACKING_DATABASE_NAME')
if database_name is None:
    sys.exit('Database name was not defined as the environment variable TRACKING_DATABASE_NAME, please fix')
database_username = os.environ.get('TRACKING_DATABASE_USERNAME')
if database_username is None:
    sys.exit('Database username was not defined as the environment variable TRACKING_DATABASE_USERNAME, please fix')
database_password = os.environ.get('TRACKING_DATABASE_PASSWORD')
if database_password is None:
    sys.exit('Database password was not defined as the environment variable TRACKING_DATABASE_PASSWORD, please fix')

# Now create the connection URL
database_url = "host='" + database_host + "' dbname='" + database_name + "' user='" + database_username + \
               "' password='" + database_password + "'"
if debug:
    print "Will query data from", database_host, "as user", database_username

# Grab a connection
try:
    conn = psycopg2.connect(database_url)
except psycopg2.Error as e:
        print "Unable to connect!"
        print e.pgerror
        print e.diag.message_detail
        sys.exit(1)

# Grab a cursor
cur = conn.cursor()

# Create a dictionary to hold ID to name mappings
id_to_name = {}

# And create an array to hold if a file for a platform has been created
platforms_with_files = []

# Now try reading all the platforms that are non-AIS so we can create a dictionary of platform ID to name
if debug:
    print "Querying for all non-AIS platforms"
try:
    cur.execute(
        """select id, name from platform where platformtype_id <> (select id from platform_type where name = 'ais') order by id""")
except:
    sys.exit("Problem running query to find non-AIS platforms")

# Fill up the dictionary
rows = cur.fetchall()
for row in rows:
    id_to_name[row[0]] = row[1]
    if debug:
        print "Platform", row[1], "(id=", row[0], ")"

# Now we want to query for positions
query = """select platform_id, datetime AT TIME ZONE 'GMT', st_x(geom), st_y(geom) from position where platform_id in
(select id from platform where platformtype_id <> (select id from platform_type where name = 'ais')) and
datetime > '""" + query_date.strftime("%m/%d/%Y %I:%M:%S %p") + """' group by platform_id, datetime, st_x(geom), 
st_y(geom) order by platform_id, datetime"""
if debug:
    print query
try:
    cur.execute(query)
except:
    sys.exit("Problem running query to positions for non-AIS platforms")

# Create a dictionary that will hold all the locations of the various platforms
locations = {}

# Now loop over the positions and build the file
rows = cur.fetchall()
for row in rows:
    platform_id = str(row[0])
    timestamp = row[1]
    longitude = row[2]
    latitude = row[3]
    platform_name = id_to_name[row[0]]

    # Make sure the data points are in a real range
    if -180 <= longitude <= 180 and -90 <= latitude <= 90:

        # Make sure there is an entry in the dictionary for this platform
        if platform_name not in locations.keys():
            if debug:
                print "Platform", platform_name, "not in location dictionary yet"
            locations[platform_name] = []

        # Some the Timezero software does a very strange conversion using the decimal from the lat and lon
        # it essentially looks at the numbers after the decimal as decimal minutes.  So I have to do the conversion
        # from decimal degrees to minutes and then substitute that as the four digits after the decimal
        # Let's do the latitude first, split into before and after decimal parts
        (lat_dec_degrees, lat_degrees) = math.modf(latitude)

        # Now convert the decimal degrees to minutes and divide by 100 to move the decimal
        lat_minutes = lat_dec_degrees * 60 / 100

        # Now add back to the degrees
        lat_converted = lat_degrees + lat_minutes

        # Same for longitude
        lon_tuple = math.modf(longitude)
        (lon_dec_degrees, lon_degrees) = math.modf(longitude)

        # Now convert the decimal degrees to minutes and divide by 100 to move the decimal
        lon_minutes = lon_dec_degrees * 60 / 100

        # Now add back to the degrees
        lon_converted = lon_degrees + lon_minutes

        # Do a little formatting
        if lon_converted < 0:
            longitude_string = "%.4f" % abs(lon_converted) + "W"
        else:
            longitude_string = "%.4f" % lon_converted + "E"
        if lat_converted < 0:
            latitude_string = "%.4f" % abs(lat_converted) + "S"
        else:
            latitude_string = "%.4f" % lat_converted + "N"

        # OK, so we now have everything we need, let's write the data to the local dictionary but we first need to
        # check and see if we are limiting the number of points and if we are and it's full, pop off the first element
        if 0 < num_points <= len(locations[platform_name]):
            if debug:
                print "Array of locations for", platform_name, "is full will pop top element"
            locations[platform_name].pop(0)

        if debug:
            print "Adding", len(locations[platform_name]), "th location to", platform_name
        locations[platform_name].append("0;" + platform_name + ";" + latitude_string + ";" + longitude_string + ";" + timestamp.strftime('%m/%d/%y %H:%M:%S') + ";" + platform_name)


# Now the location dictionary is all set, we need to write it to files
if debug:
    print "All done loading dictionary"

# Iterate of the platform names
for platform_name_key, location_values in locations.items():
    if debug:
        print "Will write", len(location_values), "location(s) for platform ", platform_name_key

    # Open the platform file for writing
    try:
        posreport = open(directory_name + os.sep + platform_name_key, 'w')
    except IOError as e:
        print e

    # Write the header line
    posreport.write("POSREPORT\n")

    # Iterate over the array and write the items
    for location in location_values:
        posreport.write(location + "\n")

    # Now close the POSREPORT file
    posreport.close()
