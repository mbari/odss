#!/bin/bash
#
# Name: updateMapDefaultTime
#
# This script updates the mapserver map files that have a time
# dimension, with the most recently available times
# 
# This allows the ODSS to leave-off the time query and get the
# most recent data by default
#
# Usage: updateMapDefaultTime
#
# Copyright (c) MBARI 2012
# Author: D. Cline 
# Print usage
print_usage()
{
  echo "  "
  echo "  "
  echo -e "\033[1m USAGE:  updateMapDefaultTime \033[0m"
  echo "  "
}

if test $# -gt 1 
then print_usage
exit 1
fi

datasets=( 
 "http://localhost:8180/erddap/wms/erdRyanSST/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdROMSnh4/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdROMSnh4int/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdROMSchl/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdROMSmld/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdROMSno3/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdROMStemp/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdATssta1day/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdATssta3day/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdATssta8day/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdMWchla1day/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdMWchla3day/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdMWchla8day/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdMWchla/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdMWcflh/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdMWpar0/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdMWsstd/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdATssta/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdGAssta/request?service=WMS&request=GetCapabilities&version=1.3.0" 
 "http://localhost:8180/erddap/wms/erdQAwekm/request?service=WMS&request=GetCapabilities&version=1.3.0" 
)

maps=( 
 "/home/stoqsadm/deployed/mapfiles/sstdecloud/ryan2012Sep.layer.map"
 "/home/stoqsadm/deployed/mapfiles/roms/nh4.layer.map"
 "/home/stoqsadm/deployed/mapfiles/roms/nh4int.layer.map"
 "/home/stoqsadm/deployed/mapfiles/roms/chl.layer.map"
 "/home/stoqsadm/deployed/mapfiles/roms/mld.layer.map"
 "/home/stoqsadm/deployed/mapfiles/roms/no3.layer.map"
 "/home/stoqsadm/deployed/mapfiles/roms/sst.layer.map"
 "/home/stoqsadm/deployed/mapfiles/coastwatch/sst1day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/coastwatch/sst3day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/coastwatch/sst8day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/coastwatch/chl1day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/coastwatch/chl3day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/coastwatch/chl8day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/oceanwatch/chl1day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/oceanwatch/cflh.layer.map" 
 "/home/stoqsadm/deployed/mapfiles/oceanwatch/par0.layer.map" 
 "/home/stoqsadm/deployed/mapfiles/oceanwatch/sstd1day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/oceanwatch/sstat1day.layer.map"
 "/home/stoqsadm/deployed/mapfiles/oceanwatch/sstga1day.layer.map" 
 "/home/stoqsadm/deployed/mapfiles/oceanwatch/wekm.layer.map"
)

ttl=${#datasets[@]}
tmp=tmp.$RANDOM.xml

for (( i=0;i<$ttl;i++ ));
do
    dataset=${datasets[${i}]}
    map=${maps[${i}]}
    basefile=$(basename $map)
    rootdir=${map%/*}
    tmpmap=$rootdir/.$basefile
    wget -q -O $tmp $dataset
    if [ $? -ne 0 ]; then
	echo "Error retrieving $filename."
    else
	dateQuery=`fgrep ISO8601 $tmp | sed "s/.*default=\"//" | cut -f1 -d "\""`
	echo "----> $dateQuery found for map: $map <----"
	sed "s/default_start_time/$dateQuery/g" $tmpmap > $map
    fi 
    rm $tmp
done
exit 0
