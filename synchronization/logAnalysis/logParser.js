// Create a moment for date parsing and utilities
var moment = require('moment');

// The file system handle
var fs = require('fs');

// Grab the line reader from the library
var lineReader = require('line-reader');

// The necessary regular expresssions
var startRegExp = new RegExp("start:\\s+(\\S+\\s+\\S+\\s+\\d+\\s+\\d+:\\d+:\\d+\\s+\\S+\\s+\\d+)");
var endRegExp = new RegExp("end:\\s+(\\S+\\s+\\S+\\s+\\d+\\s+\\d+:\\d+:\\d+\\s+\\S+\\s+\\d+)");
var numFilesTransferredRegExp = new RegExp("Number of files transferred: (\\d+)");
var bytesSentRegExp = new RegExp("Total bytes sent: (\\d+)");
var bytesReceivedRegExp = new RegExp("Total bytes received: (\\d+)");
var startFileListRegExp = new RegExp("receiving incremental file list|sending incremental file list");
var endFileListRegExp = new RegExp("Number of files:");
var inFileList = false;
var fileNameRegExp = new RegExp("^(\\S+)");
var rsyncMessageRegExp = new RegExp("^rsync");

// Variables needed during log parsing
var startDate = null;
var bytesSent = 0;
var bytesReceived = 0;
var totalBytesTransferred = 0;
var numFilesTransferred = 0;
var filesTransferred = "<ol>";
var rsyncMessages = "<html><head><title>Rsync Messages</title><body><h1>Rsync Messages</h1><table border='1'>";

// Grab the file name to parse and the path for the results from the command line
var fileName = process.argv[2];
var resultsDir = process.argv[3];

// First check for results directory location
if (!fs.existsSync(resultsDir)) {
	console.log("Results directory " + resultsDir + " does not exist");
	process.exit(1);
}

// Now check for input file
if (!fs.existsSync(fileName)) {
	console.log("Log file " + fileName + " does not exist");
	process.exit(1);
}

// Copy the web content to the directory where the results will go
if (!fs.existsSync(resultsDir + '/index.html')) {
	fs.createReadStream('web/index.html').pipe(fs.createWriteStream(resultsDir + '/index.html'));
}
if (!fs.existsSync(resultsDir + "/js")) {
	fs.mkdirSync(resultsDir + "/js");
}
if (!fs.existsSync(resultsDir + '/js/highcharts.js')) {
	fs.createReadStream('web/js/highcharts.js').pipe(fs.createWriteStream(resultsDir + '/js/highcharts.js'));
}
if (!fs.existsSync(resultsDir + '/js/jquery-1.11.0.min.js')) {
	fs.createReadStream('web/js/jquery-1.11.0.min.js').pipe(fs.createWriteStream(resultsDir + '/js/jquery-1.11.0.min.js'));
}
// Check for directory structure
if (!fs.existsSync(resultsDir + "/results")){
	fs.mkdirSync(resultsDir + "/results");
}
if (!fs.existsSync(resultsDir + "/results/files")){
	fs.mkdirSync(resultsDir + "/results/files");
}
// Delete the current results file
if (fs.existsSync(resultsDir + "/results/results.csv")) {
	fs.unlinkSync(resultsDir + "/results/results.csv");
}
// For each line in the log file
fs.appendFileSync(resultsDir + "/results/results.csv", "Date, KB Sent, KB Received, Total KB Transferred, Number of Files Transferred, Minutes to Transfer\n");

lineReader.eachLine(fileName, function(line, last) {
	if (startRegExp.exec(line)){
		startDate = moment(startRegExp.exec(line)[1]);
	} else if (endRegExp.exec(line)){
		endDate = moment(endRegExp.exec(line)[1]);
		totalBytesTransferred = bytesSent + bytesReceived;

		// Write results to a CSV
		fs.appendFileSync(resultsDir + "/results/results.csv",startDate.toISOString() + "," + bytesSent/1000 + "," + bytesReceived/1000 + "," + 
			totalBytesTransferred/1000 + "," + numFilesTransferred + "," + endDate.diff(startDate,'minutes') + "\n");

		// Now write the files that were transferred to a file with unix timestamp as name.
		if (filesTransferred !== "<ol>"){
			filesTransferred += "</ol>";
			fs.writeFileSync(resultsDir + "/results/files/" + startDate.unix() + ".html",filesTransferred);
		}

		// Clear tracking variables
		bytesSent = 0;
		bytesReceived = 0;
		totalBytesTransferred = 0;
		numFilesTransferred = 0;
		filesTransferred = "<ol>";
	} else if (numFilesTransferredRegExp.exec(line)){
		numFilesTransferred += parseInt(numFilesTransferredRegExp.exec(line)[1]);
	} else if (bytesSentRegExp.exec(line)){
		bytesSent += parseInt(bytesSentRegExp.exec(line)[1]);
	} else if (bytesReceivedRegExp.exec(line)){
		bytesReceived += parseInt(bytesReceivedRegExp.exec(line)[1]);
	} else if (startFileListRegExp.exec(line)) {
		inFileList = true;
	} else if (endFileListRegExp.exec(line)){
		inFileList = false;
	} else if (inFileList && rsyncMessageRegExp.exec(line)){
		rsyncMessages += "<tr><td>" + startDate.toISOString() + "</td><td>" + line + "</td></tr>";
	} else if (inFileList && fileNameRegExp.exec(line)){
		filesTransferred += "<li>" + fileNameRegExp.exec(line)[1] + "</li>";
	}
}).then(function(){
	// Close the messages HTML
	rsyncMessages += "</table></body></html>";
	fs.writeFileSync(resultsDir + "/results/rsyncMessages.html",rsyncMessages);
});
