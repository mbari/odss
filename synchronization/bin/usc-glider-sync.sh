#!/bin/bash
# This script synchronizes data products between local and USC glider data server
#
# To avoid password input on each invocation of rsync, local machine's 
# public rsa key should be included in remote machine's .ssh/authorized_hosts 
# file (see https://blogs.oracle.com/jkini/entry/how_to_scp_scp_and)

if [ $# -ne 1 ]
then
  echo usage: `basename $0` deployment
  echo \'deployment\' has format YYYY_Mon \(e.g. \'2013_Mar\'\)
  exit $E_BADARGS
fi

DEPLOYMENT=$1

echo start: `date`

# First check whether script is already running - if so, then exit

if ps axco command | grep rsync
  then echo "rsync already running"
  exit 1
fi

OPTS='--timeout 30 -P -arzpu --copy-dirlinks --partial --partial-dir=partial-dir --stats'

# Synchronize with USC glider directory
rsync $OPTS mbariodss@usclab.usc.edu:GliderData/*  /data/canon/$DEPLOYMENT/usc_glider/

echo end: `date` \n\n
