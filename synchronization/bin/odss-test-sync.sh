#!/bin/bash
# This script synchronizes data products between beach.mbari.org and odss-test
#
# This script requires that the IP address to the remote host be resolvable; 
# hence should be executed from the ship (not from shore) 
#
# To avoid password input on each invocation of rsync, local machine's 
# public rsa key should be included in remote machine's .ssh/authorized_hosts 
# file (see https://blogs.oracle.com/jkini/entry/how_to_scp_scp_and)

#if [ $# -ne 2 ]
#then
#  echo usage: `basename $0` ship deployment
#  echo Arguments are agreed-upon between ship and shore: 
#  echo \'ship\' is ship subdirectory name \(e.g. \'wf\' or \'carson\'\)
#  echo \'deployment\' has format YYYY_Mon \(e.g. \'2013_Mar\'\)
#  exit $E_BADARGS
#fi

echo start: `date`
#SHIP=$1
#DEPLOYMENT=$2

# First check whether script is already running - if so, then exit

if ps axco command | grep rsync
  then echo "rsync already running"
  exit 1
fi

OPTS='--timeout 30 -P -arzpu --copy-dirlinks --partial --partial-dir=partial-dir --stats'

# Synchronize /data/canon; pull
rsync $OPTS stoqsadm@beach:/data/canon/2013_Mar/ /data/canon/2013_Mar

# Pull erddap configuration file from shore 
rsync $OPTS stoqsadm@beach:/data/erddap/content/erddap/datasets.xml /data/erddap/content/erddap/

# Pull products from shore 
rsync $OPTS stoqsadm@beach:/data/erddap/data/ /data/erddap/data

# Pull mapserver files
rsync $OPTS stoqsadm@beach:/data/mapserver/mapfiles/ /data/mapserver/mapfiles

# Pull tilecache config
rsync $OPTS stoqsadm@beach:/data/tilecache-2.11/tilecache.cfg /data/tilecache-2.11/

## Pull tomcat config file from shore
rsync $OPTS stoqsadm@beach:/opt/apache-tomcat6/webapps/catalogs/layers/layers.json /opt/apache-tomcat-odss/webapps/catalogs/layers/

echo end: `date` \n\n
