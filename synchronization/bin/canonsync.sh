#!/bin/bash
# This script synchronizes data products between WF and "beach" over vsat for
# the Fall CANON 2012 experiment.
#
# This script requires that the IP address to the remote host be resolvable; 
# hence should be executed from the ship (not from shore) 
#
# To avoid password input on each invocation of rsync, local machine's 
# public rsa key should be included in remote machine's .ssh/authorized_hosts 
# file (see https://blogs.oracle.com/jkini/entry/how_to_scp_scp_and)

if [ $# -ne 2 ]
then
  echo usage: `basename $0` ship deployment
  echo Arguments are agreed-upon between ship and shore: 
  echo \'ship\' is ship subdirectory name \(e.g. \'wf\' or \'carson\'\)
  echo \'deployment\' has format YYYY_Mon \(e.g. \'2013_Mar\'\)
  exit $E_BADARGS
fi

echo start: `date`
SHIP=$1
DEPLOYMENT=$2

# First check whether script is already running - if so, then exit

if ps axco command | grep rsync
  then echo "rsync already running"
  exit 1
fi

OPTS='--timeout 30 -P -arzpu --copy-dirlinks --partial --partial-dir=partial-dir --stats'

# Synchronize dropbox; pull
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/dropbox/ /data/canon/$DEPLOYMENT/dropbox

# Synchronize dropbox; push
rsync $OPTS /data/canon/$DEPLOYMENT/dropbox/ stoqsadm@beach:/data/canon/$DEPLOYMENT/dropbox


# Pull erddap configuration file from shore 
rsync $OPTS stoqsadm@beach:/data/erddap/content/erddap/datasets.xml /data/erddap/content/erddap/

# Pull products from shore 
rsync $OPTS stoqsadm@beach:/data/erddap/data/ /data/erddap/data

## Pull tomcat config file from shore
### rsync $OPTS stoqsadm@beach:/opt/apache-tomcat6/webapps/catalogs/layers/layers.json /opt/apache-tomcat-odss/webapps/catalogs/layers/

# Pull ESP data from shore 
mkdir -p /data/canon/$DEPLOYMENT/esp/instances/Bruce/data
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/esp/instances/Bruce/data/raw/ /data/canon/$DEPLOYMENT/esp/instances/Bruce/data/raw 
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/esp/instances/Bruce/data/processed/ /data/canon/$DEPLOYMENT/esp/instances/Bruce/data/processed
mkdir -p /data/canon/$DEPLOYMENT/esp/instances/Mack/data
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/esp/instances/Mack/data/raw/ /data/canon/$DEPLOYMENT/esp/instances/Mack/data/raw 
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/esp/instances/Mack/data/processed/ /data/canon/$DEPLOYMENT/esp/instances/Mack/data/processed


# Pull LRAUV products from shore 
mkdir -p /data/canon/$DEPLOYMENT/lrauv/instances
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/lrauv/instances/daphne/ /data/canon/$DEPLOYMENT/lrauv/instances/daphne
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/lrauv/instances/tethys/ /data/canon/$DEPLOYMENT/lrauv/instances/tethys

# Pull glider products from shore 
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/usc_glider/ /data/canon/$DEPLOYMENT/usc_glider
rsync $OPTS stoqsadm@beach:/data/canon/$DEPLOYMENT/waveglider/ /data/canon/$DEPLOYMENT/waveglider

# Push ship products to shore 
rsync $OPTS /data/canon/$DEPLOYMENT/$SHIP/ stoqsadm@beach:/data/canon/$DEPLOYMENT/$SHIP

# Push Dorado
rsync $OPTS /data/canon/$DEPLOYMENT/dorado/data/processed/ stoqsadm@beach:/data/canon/$DEPLOYMENT/dorado/data/processed

echo end: `date` \n\n
