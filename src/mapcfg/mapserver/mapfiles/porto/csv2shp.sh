#!/bin/bash
# 
# Convert .csv files to shape files for use as a layer
# 
set -x
dir=`pwd`
pushd $dir 
filename=$1

fbname=`basename "$filename" .csv`

cat > $fbname.vrt << EOF
<OGRVRTDataSource>
    <OGRVRTLayer name="$fbname">
        <SrcDataSource relativeToVRT="1">$dir/$fbname.csv</SrcDataSource>
        <GeometryType>wkbPoint</GeometryType>
	<SrcLayer>$fbname</SrcLayer>
        <LayerSRS>WGS84</LayerSRS>
        <GeometryField encoding="PointFromColumns" x="Longitude" y="Latitude"/>
    </OGRVRTLayer>
</OGRVRTDataSource>
 
EOF
 
ogr2ogr -f libkml $fbname.kml $fbname.vrt

if [ -e $fbname.dbf ]; then
    ogr2ogr -overwrite -F "ESRI Shapefile" $dir $fbname.vrt
else
    ogr2ogr -f "ESRI Shapefile" $dir $fbname.vrt
fi
echo "Done !"
