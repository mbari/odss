import csv
import sys
import shutil
import pdb

ifilename = sys.argv[1]
ofilename  = sys.argv[2]
stride = int(sys.argv[3])
ifile  = open(ifilename, "rb")
tfile  = open('tmp.csv', "wb")
reader = csv.reader(ifile)
writer = csv.writer(tfile, delimiter=',')
lastplatformid = 0
samplenum = 0
rownum = 0

for row in reader:
    # Save header row.
    if rownum == 0:
        header = row
        writer.writerow(header)
    else:
        colnum = 0
        platform = row[0]
        if platform != lastplatformid:
            print '%-8s: %s' % (header[colnum], platform)
            writer.writerow(row)
	    samplenum = 0 
	elif (samplenum%stride) == 0 :
            print '%-8s: %s' % (header[colnum], platform)
            writer.writerow(row)

        lastplatformid = platform
        samplenum += 1

    rownum += 1

ifile.close()
tfile.close()
shutil.move('tmp.csv',ofilename)

