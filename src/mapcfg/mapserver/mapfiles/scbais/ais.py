import csv
import sys
import shutil

ifile  = open(sys.argv[1], "rb")
ofile  = open('tmp.csv', "wb")
reader = csv.reader(ifile)
writer = csv.writer(ofile, delimiter=',')
lastplatformid = 0

rownum = 0
for row in reader:
    # Save header row.
    if rownum == 0:
        header = row
        writer.writerow(header)
    else:
        colnum = 0
        platform = row[0]
        if row[0] != lastplatformid:
            print '%-8s: %s' % (header[colnum], row[0])
            writer.writerow(row)
        
        lastplatformid = row[0]
            
    rownum += 1

ifile.close()
ofile.close()
shutil.copy('tmp.csv','ais.csv')

