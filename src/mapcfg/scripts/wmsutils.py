#!/usr/bin/env python

import tempfile
import os
import pdb
import datetime
import re
import sys, string
import urllib
import fileinput
import shutil
import fileinput
import socket
 
from datetime import datetime, timedelta  
from xml.dom import minidom 

mbariBaseUrl="http://"+socket.gethostname()+"/erddap/wms"
		
def match_capabilities(myid, dateQuery, format): 
 	print "Getting capabilities for "+myid   
	try: 
		dtCompareStr = dateQuery.strftime(format)

		(fd, tmpfile)=tempfile.mkstemp() 
		remote=mbariBaseUrl+"/"+myid+"/request?service=WMS&request=GetCapabilities&version=1.3.0" 

		# Get capabilities xml document
		a,response=urllib.urlretrieve(remote, tmpfile)  
		responseType=response['Content-Type'] 
	 
		if responseType != 'text/xml;charset=UTF-8' :
			raise Exception("cannot fetch capabilities from " + remote + ". Is the "+myid+" dataset defined in your ERDDAP server ? ")
			
		# Parse and get the time dimension 		
		xmlcapdoc=minidom.parse(tmpfile)
		dimensions=xmlcapdoc.getElementsByTagName('Dimension')     

		# Get all valid times until one matches per the format above
		for d in dimensions: 
			value = d.attributes['name'].value
			if value == "time" :  
				nodeValue = d.firstChild.nodeValue
				values = nodeValue.split(', ')
				for v in values:  
					dtCapabilities = datetime.strptime(v, "%Y-%m-%dT%H:%M:%SZ" )  
					dtCapabilitiesStr = dtCapabilities.strftime(format)
					print "Comparing matching date " + dtCompareStr + " with " + dtCapabilitiesStr + " "+ v
					if dtCapabilitiesStr == dtCompareStr :
						return True
					
				 
		
		os.remove(tmpfile)	
		print "no matching datetime found" 
		return False

	except Exception, e: 
		os.remove(tmpfile)	
		raise Exception("Error getting capabilities for " + myid + " %s " %e ) 
 
