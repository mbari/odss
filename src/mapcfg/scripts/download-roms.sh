#! /bin/bash    
cd $HOME/dev/mbari-dss/src/mapcfg/scripts/
source $HOME/dev/mbari-dss/venv-mbari-dss/bin/activate
export FER_LIBS=$HOME/dev/mbari-dss/venv-mbari-dss/lib
export LD_LIBRARY_PATH=$HOME/dev/mbari-dss/venv-mbari-dss/lib/python2.6/site-packages/pyferret/

# virtualenv is now active, which means your PATH has been modified.  
python download-roms.py
