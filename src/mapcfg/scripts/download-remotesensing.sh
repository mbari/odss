#! /bin/bash    
source $HOME/dev/mbari-dss/venv-mbari-dss/bin/activate
cd $HOME/dev/mbari-dss/src/mapcfg/scripts

# virtualenv is now active, which means your PATH has been modified.  
python download-remotesensing.py

# update the erddap server
curl "https://odss.mbari.org/erddap/setDatasetFlag.txt?datasetID=allDatasets&flagKey=2858425129"

