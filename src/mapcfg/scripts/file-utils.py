#!/usr/bin/env python

__author__ = "Danelle Cline"
__copyright__ = "Copyright 2014, MBARI"
__license__ = "MIT License"
__maintainer__ = "Danelle Cline"
__email__ = "dcline at mbari.org"
__status__ = "Development"
__doc__ = '''

This script is a simple utility to remove sorted files in a directory up to a particular file
This proved useful for managing files behind an ERDDAP server

@undocumented: __doc__ parser
@author: __author__
@status: __status__
@license: __license__
'''


import os
import argparse
import pdb

if __name__ == '__main__':
	parser = argparse.ArgumentParser()#description='Remove files up to particular file')
	parser.add_argument('--uptofile', dest='up_to_file', action='store', help='', default='.',required=True)
	parser.add_argument('--directory', dest='directory', action='store', help='', default='.',required=True)
	args = parser.parse_args()

	for filename in sorted(os.listdir(args.directory)):
    		if  filename == args.up_to_file:
			break	
    		else:
			print 'Removing ' + args.directory +  filename
			os.remove(args.directory + filename)
	
print 'Done!'
