#!/usr/bin/env python

__author__ = "Danelle Cline"
__copyright__ = "Copyright 2013, MBARI"
__license__ = "MIT License"
__maintainer__ = "Danelle Cline"
__email__ = "dcline at mbari.org"
__status__ = "Development"
__doc__ = '''


@undocumented: __doc__ parser
@author: __author__
@status: __status__
@license: __license__
'''
from datetime import datetime, timedelta 
import os
import pdb
import re
import sys, string
import netCDF4
import time
import tempfile
import shutil
import time as time

def replace_str(templatefile, tmpfile, templateStr, newStr): 
	o = open(tmpfile,"w") 
	for line in open(templatemapfile): 
		line = line.replace(templateStr,newStr)
		o.write(line) 
	o.close()
        
def ensure_dir(filename):
    d = os.path.dirname(filename)
    if not os.path.exists(d): 
        os.makedirs(d)

try:
    
    (fd, tmpfile)=tempfile.mkstemp()
    #url='http://dods.mbari.org/opendap/hyrax/data/ssdsdata/deployments/m1/201202/OS_M1_20120222hourly_CMSTV.nc?'
    url='http://dods.mbari.org/opendap/hyrax/data/ssdsdata/deployments/m1/201407/OS_M1_20140716hourly_CMSTV.nc?';
    dataset=netCDF4.Dataset(url)
    dtime=dataset.variables['hr_time_met'] #if need to switch to dods.mbari.org url, use these instead of 'time' variable
    timedim=dataset.dimensions['hr_time_met']
    #dtime=dataset.variables['time']
    #timedim=dataset.dimensions['time']
    secondsepoch=dtime[len(timedim)-1]
    t=time.gmtime(secondsepoch)
    print secondsepoch
    dt=datetime(t.tm_year,t.tm_mon,t.tm_mday,t.tm_hour,t.tm_min,t.tm_sec)
    defaultDateStr=dt.strftime('%Y%m%dT%H%M%SZ')
    mapfiles=['/data/mapserver/mapfiles/m1/currenthourly.layer.map','/data/mapserver/mapfiles/m1/windhourly.layer.map']
    for m in mapfiles:
        mapfilebase=os.path.basename(m) 
        mapfileroot=m.split(mapfilebase)[0]
        templatemapfile=mapfileroot+"/."+mapfilebase  
        print "Found default time " + defaultDateStr +" for " + m
        replace_str(templatemapfile, tmpfile, 'default_start_time', defaultDateStr)
        # now copy the map file 
	fsrc=open(tmpfile,'rt') 
	fdst=open(m,'wt') 
	shutil.copyfileobj(fsrc, fdst)
 
except Exception, e: 
	print "Error : %s" % e
finally:	
	os.remove(tmpfile)

print "Done !"
