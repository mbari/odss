#! /bin/bash    
cd $HOME/dev/mbari-dss/src/mapcfg/scripts/
source $HOME/dev/mbari-dss/venv-mbari-dss/bin/activate

# virtualenv is now active, which means your PATH has been modified.  
python updatemaps.py
