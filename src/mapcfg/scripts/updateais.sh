base=/data/mapserver/mapfiles/ais 
pushd $base
tmp=tmp.$RANDOM.xml
wget -q -O $tmp http://`hostname`/trackingdb/positionOfType/ais/last/10m/data.csv
if [ $? -ne 0 ]; then
  echo "Error retrieving $tmp." 
else
  python $base/ais.py $tmp ais.csv 10
  /usr/local/bin/ogr2ogr -overwrite -f "ESRI Shapefile" $base ais.csv 
  /usr/local/bin/ogr2ogr -overwrite -f "ESRI Shapefile" $base ais.vrt
fi
rm $tmp
popd
