#!/usr/bin/env python

__author__ = "Danelle Cline"
__copyright__ = "Copyright 2014, MBARI"
__license__ = "MIT License"
__maintainer__ = "Danelle Cline"
__email__ = "dcline at mbari.org"
__status__ = "Development"
__doc__ = '''

This script downloads ROMS model data from JPL, subsets it, and saves it to netCDF format. 

This is intended to be used to download remote sensing data which is in turn rsynced from 
shore to ship for use on boats with the ODSS software installed. 

Prerequisities:

python-2.6, numpy, netcdf4, and python-dateutil.
See README.txt for install

@undocumented: __doc__ parser
@author: __author__
@status: __status__
@license: __license__
'''
import subprocess
import tempfile
import os
import pdb
import datetime
import re
import sys, string
import urllib
import shutil
import netCDF4
import pyferret
import time
import re

from dap2file import dap2file
from wmsutils import match_capabilities
 
from collections import namedtuple  
from datetime import datetime, timedelta
from dateutil import tz
from time import mktime

def toepoch(dt, epoch=datetime(1970,1,1)):
    td = dt - epoch
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 1e6

def correct_time(filename,dtActual,nchunk=10,quiet=False): 
    ncfile = netCDF4.Dataset(filename,'r')
    fd, tmpfile = tempfile.mkstemp()
    ncfiletmp = netCDF4.Dataset(tmpfile,'w')
    
    # create dimensions. Check for unlimited dim.
    unlimdimname = False
    unlimdim = None
    
    # create global attributes.
    if not quiet: sys.stdout.write('copying global attributes ..\n')
    for attname in ncfile.ncattrs():
        setattr(ncfiletmp,attname,getattr(ncfile,attname))
        
    if not quiet: sys.stdout.write('copying dimensions ..\n')
    for dimname,dim in ncfile.dimensions.items():
        if dim.isunlimited():
            unlimdimname = dimname
            unlimdim = dim
            ncfiletmp.createDimension(dimname,None)
        else:
            ncfiletmp.createDimension(dimname,len(dim))
            
    # create variables.
    for varname,ncvar in ncfile.variables.items():
        if not quiet: sys.stdout.write('copying variable %s\n' % varname)
        # is there an unlimited dimension?
        if unlimdimname and unlimdimname in ncvar.dimensions:
            hasunlimdim = True
        else:
            hasunlimdim = False
        if hasattr(ncvar, '_FillValue'):
            FillValue = ncvar._FillValue
        else:
            FillValue = None 
        var = ncfiletmp.createVariable(varname,ncvar.dtype,ncvar.dimensions,fill_value=FillValue)
        # fill variable attributes.
	attdict = ncvar.__dict__
	if '_FillValue' in attdict: del attdict['_FillValue']
	var.setncatts(attdict) 
        # fill variables with data.
        if hasunlimdim: # has an unlim dim, loop over unlim dim index.
 
            # range to copy
            if nchunk:
                start = 0; stop = len(unlimdim); step = nchunk
                if step < 1: step = 1
                for n in range(start, stop, step):
                    nmax = n+nchunk
                    if nmax > len(unlimdim): nmax=len(unlimdim)
                    var[n:nmax] = ncvar[n:nmax]
                    print ncvar[n:nmax]
            else:
                var[0:len(unlimdim)] = ncvar[:]

            if varname == 'time' :
                var[0:len(unlimdim)] = toepoch(dtActual)
                
        else: # no unlim dim or 1-d variable, just copy all data at once.
            var[:] = ncvar[:]
        ncfiletmp.sync() # flush data to disk
    # close files and move temp corrected file to the original
    ncfiletmp.close()
    ncfile.close()
    print 'Moving '+tmpfile+' to '+filename
    shutil.move(tmpfile,filename)
    
def findWholeWord(w):
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search
 
def file_exists(filename):
    try:
        with open(filename) as f:
            return True
    except IOError:
        return False
 
def ensure_dir(filename):
    d = os.path.dirname(filename)
    if not os.path.exists(d): 
        os.makedirs(d)

# For the greater Monterey Bay area
#latMin=str(36.05)
#latMax=str(37.07375)
#lonMin=str(237.075) #longitude wrapped
#lonMax=str(238.7625)
latMin=str(35.8)
latMax=str(37.8)
lonMin=str(236.2) #longitude wrapped
lonMax=str(238.8)

dataset = namedtuple("dataset", "erddapid url prefix datadir stephours timestride maxtimeindex mindepth maxdepth regiondef minlon maxlon minlat maxlat") 

id0 = {'variables': 'temp, salt, u, v, zeta','erddapid': 'erdMRYROMSforecast', 'url': 'http://west.rssoffice.com:8080/thredds/dodsC/pacific/CA3km-forecast/CA/', 'prefix': 'ca_subCA_fcst_' , 'datadir':'/data/erddap/data/rssoffice/CA3km-forecast/', 'stephours': '6' ,'timestride': '1', 'maxtimeindex':'73','mindepth':'0','maxdepth':'10','regiondef':'True','minlon':lonMin,'maxlon':lonMax,'minlat':latMin,'maxlat':latMax}
id1 = {'variables': 'temp, salt, u, v, zeta','erddapid': 'erdMRYROMSnowcast', 'url': 'http://west.rssoffice.com:8080/thredds/dodsC/pacific/CA3km-nowcast/CA/', 'prefix': 'ca_subCA_das_' , 'datadir':'/data/erddap/data/rssoffice/CA3km-nowcast/', 'stephours': '6' ,'timestride': '1', 'maxtimeindex':'1','mindepth':'0','maxdepth':'100','regiondef':'True','minlon':lonMin,'maxlon':lonMax,'minlat':latMin,'maxlat':latMax}
id2 = {'variables': 'temp, salt, u, v, zeta','erddapid': 'erdMRYROMSnowcast_100m', 'url': 'http://west.rssoffice.com:8080/thredds/dodsC/pacific/CA3km-nowcast/CA/', 'prefix': 'ca_subCA_das_' , 'datadir':'/data/erddap/data/rssoffice/CA3km-nowcast-100m/', 'stephours': '6' ,'timestride': '1', 'maxtimeindex':'1','mindepth':'100','maxdepth':'100','regiondef':'True','minlon':lonMin,'maxlon':lonMax,'minlat':latMin,'maxlat':latMax}
id3 = {'variables': 's2, zz2, no3, nh4','erddapid': 'erdMRYROMSnowcast_bio', 'url': 'http://west.rssoffice.com:8080/thredds/dodsC/pacific/CA3km-nowcast/CA/', 'prefix': 'ca_subCA_bio_' , 'datadir':'/data/erddap/data/rssoffice/CA3km-nowcast-bio/', 'stephours': '6' ,'timestride': '1', 'maxtimeindex':'1','mindepth':'0','maxdepth':'50','regiondef':'True','minlon':lonMin,'maxlon':lonMax,'minlat':latMin,'maxlat':latMax}
id4 = {'variables': 'nh4','erddapid': 'erdMRYROMSnowcast_bio50m', 'url': 'http://west.rssoffice.com:8080/thredds/dodsC/pacific/CA3km-nowcast/CA/', 'prefix': 'ca_subCA_bio_' , 'datadir':'/data/erddap/data/rssoffice/CA3km-nowcast-bio-50m/', 'stephours': '6' ,'timestride': '1', 'maxtimeindex':'1','mindepth':'50','maxdepth':'50','regiondef':'True','minlon':lonMin,'maxlon':lonMax,'minlat':latMin,'maxlat':latMax}
id5 = {'variables': 'temp, salt, u, v, zeta','erddapid': 'erdSCBROMSnowcast', 'url': 'http://west.rssoffice.com:8080/thredds/dodsC/pacific/CA3km-nowcast/SCB/', 'prefix': 'ca_subSCB_das_' , 'datadir':'/data/erddap/data/ourocean/erdSCBROMSnowcast/', 'stephours': '6' ,'timestride': '1', 'maxtimeindex':'1','mindepth':'0','maxdepth':'50','regiondef':'False'}
id6 = {'variables': 'temp, salt, u, v, zeta','erddapid': 'erdSCBROMSforecast', 'url': 'http://west.rssoffice.com:8080/thredds/dodsC/pacific/CA3km-nowcast/SCB/', 'prefix': 'ca_subSCB_fcst_' , 'datadir':'/data/erddap/data/ourocean/erdSCBROMSforecast/', 'stephours': '6' ,'timestride': '1', 'maxtimeindex':'73','mindepth':'0','maxdepth':'50','regiondef':'False'} 
 
datalist = [id0,id1,id2,id3,id4,id5,id6]

try:
	pyferret.start()
	pyferret.run('cancel mode journal')
	pyferret.run('cancel mode upcase')
 	pyferret.run('SET MEMORY/SIZE=50')
	(fd, local)=tempfile.mkstemp()
	local+='.nc'
        
	for j in range(0,2):
		startHr=-24*j 
		dateQuery=datetime.utcnow() + timedelta(hours=startHr)  

 		for i in range(len(datalist)):   
			dataset=datalist[i]
                        myvars=dataset['variables']
			myid=dataset['erddapid']
			mydir=dataset['datadir']
			myurl=dataset['url']
			myprefix=dataset['prefix']
			mystride=int(dataset['timestride'])
			mymax=int(dataset['maxtimeindex'])
			mymindepth=int(dataset['mindepth'])
			mymaxdepth=int(dataset['maxdepth'])
			myregiondef=dataset['regiondef']
			if myregiondef == 'True' :
				myminlon=float(dataset['minlon'])
				mymaxlon=float(dataset['maxlon'])
				myminlat=float(dataset['minlat'])
				mymaxlat=float(dataset['maxlat'])
			mystep=int(dataset['stephours'])
			ensure_dir(mydir) 
			dateQueryStr=dateQuery.strftime('%Y%m%d')

			print myid
			print mydir
			print myurl
			r=range(3,24,mystep)
			for k in r :
				hours="%02d" % k
				url=myurl+myprefix+dateQueryStr+hours+'.nc'  
				filename=mydir+myprefix+dateQueryStr+hours+'.nc' 
				if file_exists(filename) == False :
					dds=url+'.dds'
					try : 
						print "Downloading from "+dds
                                		a,response=urllib.urlretrieve(dds, local)
                                		responseType=response['Content-Type'] 

                                		if responseType != 'text/plain' :
                                        		print "cannot download from " + dds + " bad response type "+responseType
                             			else: 
							infile = open(local, 'r')
							firstLine = infile.readline()
							if findWholeWord('Dataset')(firstLine):
								pyferret.run('use "%s" '%url)
								pyferret.run('show data/var')
 
								if myregiondef == 'False' :
									# subset depth and stride the times to reduce file size
									bb='z=%d:%d,L=1:%d:%d' % (mymindepth, mymaxdepth,mymax,mystride)
								else:
									# subset depth, stride the times, and bound lon/lat to reduce file size
									bb='z=%d:%d,L=1:%d:%d,x=%f:%f,y=%f:%f' % (mymindepth, mymaxdepth,mymax,mystride,myminlon,mymaxlon,myminlat,mymaxlat)

                                                                vsplit=myvars.split(', ')
                                                                varstr=''
                                                                for n in range(len(vsplit)) :
                                                                    varstr=varstr + ('%s[%s],' % (vsplit[n],bb))

                                                                varstr=varstr.rstrip(',')
                                                                pyferret.run('save/keep_axisnames/clobber/file="%s" %s' % (local,varstr) )

								proc=subprocess.Popen(['ncdump','-h', local], stdout = subprocess.PIPE)
								for line in proc.stdout:
									#find matching AX009, AX006, etc. renamed during the subsetting by ferret
									match = re.match(r'.*\sAX\d{3,}',line) 
									if match :
										a=match.group()

										#replace white space and tabs	
										axisname=' '.join(a.split())

										#rename axis name with time and remove depth_bnds dimension
										os.system('ncrename -h -O -v %s,time -d %s,time %s' %  (axisname, axisname, local))
                                                                            
                                                                		#Fix timecode
                                                                		if myid == 'erdSCBROMSnowcast' or myid == 'erdMRYROMSnowcast_bio' or  myid == 'erdMRYROMSnowcast' or myid == 'erdMRYROMSnowcast_100m' or myid == 'erdMRYROMSnowcast_bio50m':
                                                                    			print "----------------->correcting time for SCB/Monterey nowcast data" 
                                                                    			timeformat = "%Y%m%d%H"
                                                                    			dt = datetime.strptime(dateQueryStr+hours, timeformat)
                                                                    			correct_time(local,dt)

										os.system('ncks -C -O -x -v depth_bnds %s %s' %  (local, filename)) 
                                                                                break
					except Exception, e: 
						print " %s " %e
except Exception, e:
	print " %s " %e 
finally:
        if file_exists(local):
                os.remove(local)
