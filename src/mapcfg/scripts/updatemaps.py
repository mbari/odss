#!/usr/bin/env python

__author__ = "Danelle Cline"
__copyright__ = "Copyright 2013, MBARI"
__license__ = "MIT License"
__maintainer__ = "Danelle Cline"
__email__ = "dcline at mbari.org"
__status__ = "Development"
__doc__ = '''

This script updates the mapserver map files with
the latest time available for data defined in the
mapdatasets.xml file.

This is intended to be used on a shore computer
with the ODSS software installed. 

Prerequisities:

install python

install numpy and netcdf4.
On beach.mbari.org, netcdf4 needed to have install of hdf5 from www.hdfgroup.org site,
then set environment variable HDF5_DIR before installing

easy_install numpy
export HDF5_DIR=/home/stoqsadm/hdf5-1.8.9-linux-static/
easy_install netcdf4

@undocumented: __doc__ parser
@author: __author__
@status: __status__
@license: __license__
'''

import pdb
import tempfile
import os
import datetime
import re
import sys, string
import urllib
import fileinput
import shutil
import fileinput
import numpy as np
 
from datetime import datetime, timedelta
from xml.dom import minidom 

mbariBaseUrl="http://normandy.mbari.org/erddap/"

def toepoch(dt, epoch=datetime(1970,1,1)):
    td = dt - epoch
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 1e6

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

def replace_str(templatefile, tmpfile, templateStr, newStr): 
	o = open(tmpfile,"w") 
	for line in open(templatemapfile): 
		line = line.replace(templateStr,newStr)
		o.write(line) 
	o.close()

def ensure_dir(filename):
    d = os.path.dirname(filename)
    if not os.path.exists(d): 
        os.makedirs(d)

try:
  
	(fd, tmpfile)=tempfile.mkstemp()
	(fd, tmpfile2)=tempfile.mkstemp() 
	xmldoc = minidom.parse('mapdatasets.xml')
	itemlist = xmldoc.getElementsByTagName('dataset')
        
	for s in itemlist :
                offsethours=int(s.attributes['layeroffsethours'].value)
		myid=s.attributes['erddapid'].value 		
		mapfile=s.attributes['mapfile'].value 		
		timeformat=s.attributes['timeformat'].value
		minlegend=s.attributes['minlegend'].value
		maxlegend=s.attributes['maxlegend'].value
		legendprefix=s.attributes['legendprefix'].value 	
		datatype=s.attributes['datatype'].value	
		numdims=int(s.attributes['numdims'].value)	
		haslegend=str2bool(s.attributes['haslegend'].value)	
		remote=mbariBaseUrl+"wms/"+myid+"/request?service=WMS&request=GetCapabilities&version=1.3.0"
		ensure_dir(mapfile)

		try: 
			print "Getting capabilities for "+myid 
			a,response=urllib.urlretrieve(remote, tmpfile) 
			responseType=response['Content-Type']
			 
			if responseType != 'text/xml;charset=UTF-8' :
				raise Exception("cannot fetch capabilities from " + remote + ". Is the "+myid+" dataset defined in your ERDDAP server ? ")
			 
			# Parse and get the time dimension from the returned xml		
			xmlcapdoc=minidom.parse(tmpfile)
			dimensions=xmlcapdoc.getElementsByTagName('Dimension')  

                        depths=[0.0]
                        allDepths={}
                        
                        for d in dimensions:
				value = d.attributes['name'].value
                                #print value
                                if value == "depth" or value == "altitude" or value == "elevation":
                                  # Get all the depths
                                  allDepths=" ".join(t.nodeValue for t in d.childNodes if t.nodeType == t.TEXT_NODE)
                                  print allDepths
                                  
                                  if allDepths.count('/') > 0:
                                    # if split with / it's in the format start/stop/increment
                                    results=allDepths.split('/')
                                    begin=float(results[0])
                                    end=float(results[1])
                                    incr=-1*float(results[2])
                                    depths=np.arange(begin,end+incr,incr)
                                    
                                    # if non-uniformly spaced, in comma-separated list
                                  else :
                                      if allDepths.count(',') >=  0 :
                                        results=allDepths.split(',')
                                        depths=map(float,results)
                                    
                        # Get the max depth index
                        maxdindex=len(depths)
                        
			for d in dimensions:
				value = d.attributes['name'].value
				if value == "time" :

                                        # Get all the dates and find the index for the maximum date index
					allDates=" ".join(t.nodeValue for t in d.childNodes if t.nodeType == t.TEXT_NODE)
					maxtindex=allDates.count(',')

                                        # calculate offset and adjust date accordingly
					if offsethours != 0: 
						dates=allDates.split(',')	
						secondsList={} 
						dtList={} 
						# put dates into datetime list by seconds since UTC
						for i in range(0,maxtindex):
							dtList[i]=datetime.strptime(dates[i] , "%Y-%m-%dT%H:%M:%SZ" )
							secondsList[i]=toepoch(dtList[i])

						# find closest to now offset by hours
						d=datetime.utcnow() + timedelta(hours=offsethours)
						secondsTarget=toepoch(d)
						tindex=min(secondsList, key=lambda x:abs(float(secondsList[x])-float(secondsTarget)))
						dt=dtList[tindex]
					else:
						tindex=maxtindex
						defaultDateStr=d.attributes['default'].value
						dt=datetime.strptime( defaultDateStr, "%Y-%m-%dT%H:%M:%SZ" )

					defaultDateStr=dt.strftime(timeformat)
 
					# get the template map file, which should be the mapfile prepended with a dot, e.g.
					# /home/stoqsadm/deployed/mapfiles/oceanwatch/.chl1day.layer.map
					mapfilebase=os.path.basename(mapfile) 
					mapfileroot=mapfile.split(mapfilebase)[0]
					templatemapfile=mapfileroot+"/."+mapfilebase  
		
					print "Found default time " + defaultDateStr +" for " + mapfile
					replace_str(templatemapfile, tmpfile, 'default_start_time', defaultDateStr)
	
					# get the new legend image(s)
                                        dims={}
					if haslegend == True:
						print "Getting legend for "+myid
						numtypes=datatype.count(',')
                                                split=datatype.split(',')
						if numtypes > 0:
                                                  for k in range(0,maxdindex) :
							dim = ''
							for r in split:
								if numdims > 3:
                                                                  dim=''.join(r)+'['+str(tindex)+']['+str(k)+']'+'[]'*(numdims-2)+','+dim
								else:
                                                                  dim=''.join(r)+'['+str(tindex)+']'+'[]'*(numdims-1)+','+dim

							dims[k]=dim.rstrip(',')
						else:
                                                  for k in range(0,maxdindex):
                                                    dim=''
                                                    if numdims > 3:
                                                      dim=datatype+'['+str(tindex)+']['+str(k)+']'+'[]'*(numdims-2)
                                                    else: 
                                                      dim=datatype+'['+str(tindex)+']'+'[]'*(numdims-1)
                                                    dims[k]=dim

                                                for k in range(0,maxdindex):
                                                  remoteUrl=mbariBaseUrl+'griddap/'+myid+".png?"+dims[k]+'&.colorBar=|||'+str(minlegend)+'|'+str(maxlegend)+'&.legend=Only'
                                                  print "Remote url: " + remoteUrl
                                                  #fetch the png legend	
                                                  a,response=urllib.urlretrieve(remoteUrl, tmpfile2) 
                                                  responseType=response['Content-Type'] 
                                                  if responseType != 'image/png' :
                                                    print("cannot fetch legend " + remoteUrl + ". Is the "+myid+" dataset defined in your ERDDAP server ? ")
                                                  else:
                                                    legendfile='/data/mapserver/mapfiles/legend-icons/%s%s%.1f%s.png' % (myid,legendprefix,depths[k],datatype)
                                                    fsrc=open(tmpfile2,'rb')
                                                    fdst=open(legendfile,'wb')
                                                    shutil.copyfileobj(fsrc, fdst)
							
					# now copy the map file 
					fsrc=open(tmpfile,'rt')
					fdst=open(mapfile,'wt')
					shutil.copyfileobj(fsrc, fdst)
		except Exception, e: 
			print "Error getting capabilities for " + myid + " %s " %e
except Exception, e:
	print "Error getting capabilities %s " % e

finally:	
	os.remove(tmpfile)
	os.remove(tmpfile2)
