base=/data/mapserver/mapfiles/ais 
pushd $base
tmp=tmp.$RANDOM.xml
wget -q -O $tmp http://`hostname`/trackingdb/positionOfType/ais/last/24h/data.csv
if [ $? -ne 0 ]; then
  echo "Error retrieving $tmp." 
else
  python $base/ais.py $tmp aisdaily.csv 500
  /usr/local/bin/ogr2ogr -overwrite -f "ESRI Shapefile" $base aisdaily.csv 
  /usr/local/bin/ogr2ogr -overwrite -f "ESRI Shapefile" $base aisdaily.vrt
fi
rm $tmp
popd
