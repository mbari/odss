base=/data/mapserver/mapfiles/ais 
pushd $base
tmp=tmp.$RANDOM.xml
wget -q -O $tmp http:/`hostname`/trackingdb/positionOfType/ais/last/1h/data.csv
if [ $? -ne 0 ]; then
  echo "Error retrieving $tmp." 
else
  python $base/ais.py $tmp aishourly.csv 50 
  /usr/local/bin/ogr2ogr -overwrite -f "ESRI Shapefile" $base aishourly.csv 
  /usr/local/bin/ogr2ogr -overwrite -f "ESRI Shapefile" $base aishourly.vrt
fi
rm $tmp
popd
