#!/usr/bin/env python

__author__ = "Danelle Cline"
__copyright__ = "Copyright 2013, MBARI"
__license__ = "MIT License"
__maintainer__ = "Danelle Cline"
__email__ = "dcline at mbari.org"
__status__ = "Development"
__doc__ = '''

This script downloads remote sensing data as defined in
mapdatasets.xml and writes the data to netCDF format. 

latMin/latMax/lonMin/lonMax variables define the region 
that is downloaded.

This is intended to be used to download remote sensing data
which is in turn rsynced from shore to ship for use on
boats with the ODSS software installed. 

Prerequisities:

python-2.6, numpy, netcdf4, and python-dateutil.
See README.txt for install

@undocumented: __doc__ parser
@author: __author__
@status: __status__
@license: __license__
'''
import tempfile
from datetime import datetime
from dateutil import tz 
import os
import sys
import urllib
from urllib.request import urlretrieve
import shutil
import netCDF4
import time

from xml.dom import minidom
from time import mktime


def toepoch(dt, epoch=datetime(1970,1,1)):
    td = dt - epoch
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 1e6

def getTime(time):
    return getText(time.childNodes)

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

# def correct_time(filename,dtActual,nchunk=10,quiet=False):
#     ncfile = netCDF4.Dataset(filename,'r')
#     fd, tmpfile = tempfile.mkstemp()
#     ncfiletmp = netCDF4.Dataset(tmpfile,'w')
#     # create dimensions. Check for unlimited dim.
#     unlimdimname = False
#     unlimdim = None
#     # create global attributes.
#     if not quiet: sys.stdout.write('copying global attributes ..\n')
#     for attname in ncfile.ncattrs():
#         #change attribute time to correct time
#         if attname == 'time_coverage_start' or attname == 'time_coverage_end' :
#             setattr(ncfiletmp, attname, unicode(datetime.strftime('%Y-%m-%dT%H:%M:%SZ')))
#         else :
#             setattr(ncfiletmp,attname,getattr(ncfile,attname))
#     if not quiet: sys.stdout.write('copying dimensions ..\n')
#     for dimname,dim in ncfile.dimensions.items():
#         if dim.isunlimited():
#             unlimdimname = dimname
#             unlimdim = dim
#             ncfiletmp.createDimension(dimname,None)
#         else:
#             ncfiletmp.createDimension(dimname,len(dim))
#     # create variables.
#     for varname,ncvar in ncfile.variables.items():
#         if not quiet: sys.stdout.write('copying variable %s\n' % varname)
#         # is there an unlimited dimension?
#         if unlimdimname and unlimdimname in ncvar.dimensions:
#             hasunlimdim = True
#         else:
#             hasunlimdim = False
#         if hasattr(ncvar, '_FillValue'):
#             FillValue = ncvar._FillValue
#         else:
#             FillValue = None
#         var = ncfiletmp.createVariable(varname,ncvar.dtype,ncvar.dimensions,fill_value=FillValue)
#         # fill variable attributes.
#         attdict = ncvar.__dict__
#         if '_FillValue' in attdict: del attdict['_FillValue']
#         var.setncatts(attdict)
#         # fill variables with data.
#         if hasunlimdim: # has an unlim dim, loop over unlim dim index.
#             # range to copy
#             if nchunk:
#                 start = 0; stop = len(unlimdim); step = nchunk
#                 if step < 1: step = 1
#                 for n in range(start, stop, step):
#                     nmax = n+nchunk
#                     if nmax > len(unlimdim): nmax=len(unlimdim)
#                     var[n:nmax] = ncvar[n:nmax]
#             else:
#                 var[0:len(unlimdim)] = ncvar[:]
#         else: # no unlim dim or 1-d variable, just copy all data at once.
#             var[:] = ncvar[:]
#         ncfiletmp.sync() # flush data to disk
#     # close files and move temp corrected file to the original
#     ncfiletmp.close()
#     ncfile.close()
#     print('Moving '+tmpfile+' to '+filename)
#     shutil.move(tmpfile,filename)

def file_exists(filename):
    try:
        with open(filename) as f:
            return True
    except IOError:
        return False

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

def ensure_dir(filename):
    d = os.path.dirname(filename)
    if not os.path.exists(d): 
        os.makedirs(d)

# check the timecode consistency between what was queried and what was returned; if these are different 
# this means the query returned the closest time, which may be redundant and will 
# not time catalog correctly, so we don't use this
# def check_timecode(filename, dtQuery):
#     try:
#         data = netCDF4.Dataset(filename)
#
#         if hasattr(data, 'time_coverage_start') :
#             tc = data.time_coverage_start
#             dtFile = datetime.strptime(tc,'%Y-%m-%dT%H:%M:%SZ' )
#             targetDate = dtFile.strftime('%Y-%m-%d')
#             queryDate = dtQuery.strftime('%Y-%m-%d')
#
#             # ignore the hours; just check if dates match
#             if targetDate == queryDate :
#                 return True
#
#             print('Target date: ' + targetDate + ' does not  match query date: ' + queryDate)
#
#             # if time_coverage_start fails, check time variable
#             t = data.variables['time']
#             tDate = time.gmtime(float(t[0]))
#             dt = datetime.fromtimestamp(mktime(tDate))
#             targetDate = dt.strftime('%Y-%m-%d')
#
#             # if time variable is okay, then correct the netCDF time fields
#             if targetDate == queryDate :
#                 correct_time(filename,dt,nchunk=10,quiet=False)
#                 return True
#
#             return False
#         else:
#             return True
#
#     except Exception as e:
#         raise Exception('Exception: %s' % e)
 
# define the subset of California to grab
latMin=str(30.0)
latMax=str(38.0)
lonMin=str(232.0) #longitude wrapped for those that need it  
lonMax=str(243.0)
lonMin2=str(-125.0)
lonMax2=str(-117.0)
tmpfile=''
mbariBaseUrl="https://odss.mbari.org/erddap/griddap"

try:
    xmldoc = minidom.parse('mapdatasets.xml')
    (fd, tmpfile)=tempfile.mkstemp()
    itemlist = xmldoc.getElementsByTagName('dataset')

    for s in itemlist :
        download=str2bool(s.attributes['download'].value)

        if download == True :
            remoteBaseUrl=s.attributes['url'].value
            delayHours=int(s.attributes['startdelayhours'].value)
            incrHours=int(s.attributes['incrhours'].value)
            format=s.attributes['querytimeformat'].value
            srcid=s.attributes['erddapidsrc'].value
            myid=s.attributes['erddapid'].value
            datatype=s.attributes['datatype'].value
            outDir=s.attributes['outdir'].value
            numdims=int(s.attributes['numdims'].value)
            lonWrap=str2bool(s.attributes['lonwrap'].value)
            startHr=0
            lastTimeAvail=datetime.utcnow()
            lastTimeAvail=lastTimeAvail.replace(tzinfo=tz.gettz('UTC'))

            # if ERDDAP get the last available time for this dataset
            if 'erddap' in remoteBaseUrl:
                baseUrl=remoteBaseUrl.split('erddap')[0]
                remote=baseUrl+"/erddap/wms/"+srcid+"/request?service=WMS&request=GetCapabilities&version=1.3.0"
                print("Getting lastest available time for " + remote)

                try:

                    a,response=urlretrieve(remote, tmpfile)
                    responseType=response['Content-Type']
                    if responseType != 'application/xml;charset=UTF-8' :
                        print("cannot fetch capabilities from " + remote + ". Is the "+srcid+" dataset defined in the ERDDAP server ? ")
                    else:
                        # Parse and get the time dimension from the returned xml
                        xmlcapdoc=minidom.parse(tmpfile)
                        dimensions=xmlcapdoc.getElementsByTagName('Dimension')
                        for d in dimensions:
                            value = d.attributes['name'].value
                            if value == "time" :
                                # Get all the dates and find the index for the maximum date index
                                allDates=" ".join(t.nodeValue for t in d.childNodes if t.nodeType == t.TEXT_NODE)
                                dates = allDates.split(',')
                                dates_list = [datetime.strptime(date, "%Y-%m-%dT%H:%M:%SZ") for date in dates]
                                dates_list.sort()
                                utc = dates_list[-1]
                                lastTimeAvail = utc.replace(tzinfo=tz.gettz('UTC'))
                except Exception as e:
                    print("Error downloading: %s" % e)


            # if THREDDs get the last available time for this dataset
            if 'thredds' in remoteBaseUrl:

                # remove the ? and replace with /dataset.xml
                baseUrl=remoteBaseUrl[:-1]
                remote=baseUrl+'/dataset.xml'

                print("Getting latest available time for "+ remote)

                try:
                    a,response=urlretrieve(remote, tmpfile)
                    responseType=response['Content-Type']

                    if responseType.find('xml') > 0:
                        # Parse and get the time dimension from the returned xml
                        xmlcapdoc=minidom.parse(tmpfile)
                        tspan=xmlcapdoc.getElementsByTagName('TimeSpan')[0]
                        laststring = getTime(tspan.getElementsByTagName('end')[0])
                        utc = datetime.strptime(laststring,'%Y-%m-%dT%H:%M:%SZ')
                        lastTimeAvail = utc.replace(tzinfo=tz.gettz('UTC'))

                        print("Latest time available for " + remote + " is " + laststring)
                    else:
                        print("cannot download from " + remote + " bad response type "+responseType)

                except Exception as e:
                    print("Error downloading: %s" % e)

            # calculate offset and adjust date accordingly according to hourly increment
            dateQuery=lastTimeAvail
            dateQuery=dateQuery.replace(tzinfo=tz.gettz('UTC'))

            # format the query date according to format
            dateQueryStr=dateQuery.strftime(format)

            # remove the underscore, dash, T, and Z ISO identifiers and format filename to store netCDF to
            reps={'-':'', 'T':'_', ':':'', 'Z':''}
            start=replace_all(dateQueryStr, reps)
            filename=outDir+'/'+myid+'/'+myid+'_'+start+'.nc'

            # make sure output directory to write netCDF files exists
            ensure_dir(filename)

            # only download if file doesn't exist already and within the bounds of the available times
            print("Checking if file exists " + filename)

            # truncate the hours/minutes/seconds/microseconds to just compare the dates
            d1 = lastTimeAvail.replace(hour=0, minute=0, second=0, microsecond=0)
            d2 = dateQuery.replace(hour=0, minute=0, second=0, microsecond=0)

            if not file_exists(filename) and d1 >= d2 :
                print("File " + filename +" doesn't exist and " + lastTimeAvail.strftime(format) +" is >= " + dateQueryStr)
                # format the gridded query differently, depending on whether calling gridded THREDDS or ERDDAP servers
                if 'erddap' in remoteBaseUrl:
                    if lonWrap == True:
                        if numdims > 3:
                            remote=remoteBaseUrl+datatype+'[('+dateQueryStr+'):1:('+dateQueryStr+')][(0.0):1:(0.0)][('+latMin+'):1:('+latMax+')][('+lonMin+'):1:('+lonMax+')]'
                        else:
                            remote=remoteBaseUrl+datatype+'[('+dateQueryStr+'):1:('+dateQueryStr+')][('+latMin+'):1:('+latMax+')][('+lonMin+'):1:('+lonMax+')]'
                    else:
                        if numdims > 3:
                          remote=remoteBaseUrl+datatype+'[('+dateQueryStr+'):1:('+dateQueryStr+')][(0.0):1:(0.0)][('+latMin+'):1:('+latMax+')][('+lonMin2+'):1:('+lonMax2+')]'
                        else:
                          remote=remoteBaseUrl+datatype+'[('+dateQueryStr+')][('+latMax+'):('+latMin+')][('+lonMin2+'):('+lonMax2+')]'
                elif remoteBaseUrl.find('thredds') > 0:
                    if lonWrap == True:
                      remote=remoteBaseUrl+"var="+datatype+"&temporal=range&time_start="+dateQueryStr+"&time_end="+dateQueryStr+"&spatial=bb&north="+latMax+"&south="+latMin+"&east="+lonMax2+"&west="+lonMin2+"&horizStride=1&accept=netcdf&"
                    else:
                      remote=remoteBaseUrl+"var="+datatype+"&temporal=range&time_start="+dateQueryStr+"&time_end="+dateQueryStr+"&spatial=bb&north="+latMax+"&south="+latMin+"&east="+lonMax+"&west="+lonMin+"&horizStride=1&accept=netcdf&"
                else:
                    raise Exception("Error - unknown protocol found in "+remoteBaseUrl+" for id "+srcid+" should be THREDDS or ERDDAP based download")

                try:
                    print("Downloading from "+remote)
                    a,response=urlretrieve(remote, tmpfile)
                    responseType=response['Content-Type']

                    if responseType != 'application/x-netcdf' and responseType != 'application/x-download' :
                      print("cannot download from " + remote + " bad response type "+responseType)
                    else:
                      print("Moving " + tmpfile + "  to " + filename)
                      shutil.move(tmpfile,filename)
                      '''if check_timecode(tmpfile, dateQuery) :
                      	shutil.move(tmpfile,filename)
                      else:
                      	raise Exception("timecode doesn't match query date " + dateQueryStr + ".")'''

                except Exception as e:
                    print("Error downloading: %s" % e)

except Exception as e:
    print("Error : %s" % e)
finally:
    if file_exists(tmpfile):
        print('removing '+tmpfile)
        os.remove(tmpfile)

print("Done !")
