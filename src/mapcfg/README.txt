The following are configuration and download scripts for setting up 
Mapserver, Tilecache and ERDDAP for use with the ODSS web interface.

#########################################################################################
Requirements
####################################################################################
1. A second tomcat installation installed at /opt/apache-tomcat7 for use with ERDDAP
2. ERDDAP, Mapserver, and Tilecache installed and nominally working. 
If you installed the GUI, you should have a nominally working ERDDAP and Tilecache

####################################################################################
Installation
####################################################################################
# Install map cfg files
cp -Rf mapserver/mapfiles/* /data/mapserver/mapfiles

# Install tilecache cfg files
cp tilecache/tilecache.cfg /var/www/tilecache/

# Change the ownership of the tilecache to user running ODSS, e.g. odssadm 
sudo chown -Rf odssadm:odssadm /tmp/tilecache

# Install datasets
cp erddap/datasets.xml /data/erddap/content/erddap/datasets.xml

# Start erddap
sudo /sbin/service tomcat-erddap stop
sudo /sbin/service tomcat-erddap start

####################################################################################
The download scripts in the scripts directory are optional, but serve as an example of
how to fetch subsetted satellite images that can be cataloged in ERDDAP/THREDDS and 
displayed in ODSS.
These scripts require hdf4/5, pyferret, python-2.6, numpy, netcdf4.
####################################################################################
Suggest to install in a virtual python environment, e.g. 

# create virtual environment in $HOME/dev/mbari-dss directory
cd $HOME/dev/mbari-dss
virtualenv venc-mbari-dss

# install numpy and dateutil 
easy_install numpy python-dateutil

# download and install hdf4
wget http://www.hdfgroup.org/ftp/HDF/HDF_Current/src/hdf-4.2.10.tar.gz
tar -zxvf hdf-4.2.10.tar.gz 
cd hdf-4.2.10
./configure --prefix=$HOME/dev/mbari-dss/venv-mbari-dss/
make install

# download and install hdf5
wget http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.8.13.tar.gz
tar -zxvf hdf5-1.8.13.tar.gz 
cd hdf5-1.8.13
./configure --prefix=$HOME/dev/mbari-dss/venv-mbari-dss/ --enable-shared -enable-hl
make install

# download and install netcdf4
wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4.3.2.tar.gz
tar -zxvf netcdf-4.3.2.tar.gz
cd netcdf-4.3.2
LDFLAGS=-L$HOME/dev/mbari-dss/venv-mbari-dss/lib CPPFLAGS=-I$HOME/dev/mbari-dss/venv-mbari-dss/include ./configure --prefix=$HOME/dev/mbari-dss/venv-mbari-dss --enable-netcdf-4 --enable-dap --enable-shared
make install

export NETCDF4_DIR=$HOME/dev/mbari-dss/venv-mbari-dss/ 
export HDF5_DIR=$HOME/dev/mbari-dss/venv-mbari-dss/ 
easy_install netcdf4

# download and install pyferret directly into virtual environment
cd $HOME/dev/mbari-dss/venv-mbari-dss/
wget ftp://ftp.pmel.noaa.gov/ferret/pub/pyferret/pyferret_executables-1.0.2-RHEL6-64.tar.gz
tar -zxvf pyferret_executables-1.0.2-RHEL6-64.tar.gz

# install nco tools
wget http://nco.sourceforge.net/src/nco-4.4.5.tar.gz
tar -zxvf nco-4.4.5.tar.gz 
cd nco-4.4.5
LDFLAGS=-L$HOME/dev/mbari-dss/venv-mbari-dss/lib CPPFLAGS=-I$HOME/dev/mbari-dss/venv-mbari-dss/include ./configure --prefix=$HOME/dev/mbari-dss/venv-mbari-dss
make install

# Install crontab entries
# Use crontab -l > file to list current user's crontab to the file, and
# crontab file, to install new crontab.
crontab -l > tmp.entries
cat cronentries >> tmp.entries
crontab tmp.entries
rm tmp.entries

####################################################################################
Developer Notes
####################################################################################

