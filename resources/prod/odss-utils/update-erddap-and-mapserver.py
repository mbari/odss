#!/usr/bin/env python

__author__ = "Danelle Cline/Kevin Gomes"
__copyright__ = "Copyright 2021, MBARI"
__license__ = "MIT License"
__maintainer__ = "Kevin Gomes"
__email__ = "kgomes at mbari.org"
__status__ = "Development"
__doc__ = '''

The purpose of this script is to read a configuration file and then download remote datasets that are served through 
ERDDAP or THREDDS and subset them as they are downloaded. The purpose of this is to create smaller datasets that only
pertain to our area of interest so that we can sync these datasets out to the boats more easily. It does not make
sense to sync the entire global dataset when we most likely will only be operating in one area of interest.

This script combines the outstanding work from Danelle Cline to download and subset the data and then update the
mapserver configuration files so they accurately reflect what is in the ERDDAP/THREDDS server.

Configuration:
1. latMin/latMax/lonMin/lonMax variables define the region that is downloaded

Prerequisities:
1. python 3
2. numpy
3. netcdf4
4. python-dateutil.

@undocumented: __doc__ parser
@author: __author__
@status: __status__
@license: __license__
'''

# Import the necessary modules
import json
import stat
import tempfile
import sys
from datetime import datetime, timedelta
from dateutil import tz
import os
from urllib.request import urlretrieve
import shutil
from xml.dom import minidom
import numpy as np
import time

# To be documented
def file_exists(filename):
    try:
        with open(filename) as f:
            return True
    except IOError:
        return False


# To be documented
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text


# To be documented
def ensure_dir(filename):
    d = os.path.dirname(filename)
    if not os.path.exists(d):
        os.makedirs(d)


# Function to convert dates to epoch
def toepoch(dt, epoch=datetime(1970, 1, 1)):
    td = dt - epoch
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10 ** 6) / 1e6


# The function reads in a template file and creates a new file replacing one string with another
def replace_str(template_file, new_file, template_string, replacement_string):
    # Open the new file for writing
    o = open(new_file, "w")
    # Read in each line of template file
    for line in open(template_file):
        # Replace and strings matching template string
        line = line.replace(template_string, replacement_string)

        # Write to the new file
        o.write(line)

    # Close the new file
    o.close()


# Create a default configuration file
config_file_name = "mapdatasets.json"

# See if the config file was defined in the environment which would override the default
if "UEM_CONFIG_FILE" in os.environ:
    config_file_name = os.environ["UEM_CONFIG_FILE"]

# Try to open and read in the configuration file
try:
    config_file = open(config_file_name, 'rb')
    config = json.load(config_file)
except OSError:
    print("Could not open/read configuration file:", config_file_name)
    sys.exit()

# Read in the information from the config file
erddap_base_url = config['config']['erddapBaseUrl']
if not erddap_base_url.endswith("/"):
    erddap_base_url = erddap_base_url + "/"
legend_file_string = config['config']['legendFileString']
lat_min = str(config['config']['latMin'])
lat_max = str(config['config']['latMax'])
lon_min = str(config['config']['lonMin'])
lon_max = str(config['config']['lonMax'])
lon_min_2 = str(config['config']['lonMin2'])
lon_max_2 = str(config['config']['lonMax2'])
datasets = config['datasets']

# Define the empty name of a temp file where the data will be downloaded
temp_file = ''

try:
    # Create a temp file
    (fd, temp_file) = tempfile.mkstemp()

    # Iterate over the list of data sets
    for dataset in datasets:
        # Check to see if the download flag is set for the dataset
        if dataset['download']:
            # Read in the dataset config variables
            url = dataset['url'] if 'url' in dataset else ''
            start_delay_hours = dataset['startdelayhours'] if 'startdelayhours' in dataset else 0
            incr_hours = dataset['incrhours'] if 'incrhours' in dataset else 24
            query_time_format = dataset['querytimeformat'] if 'querytimeformat' in dataset else ''
            erddap_src_id = dataset['erddapidsrc'] if 'erddapidsrc' in dataset else ''
            erddap_id = dataset['erddapid'] if 'erddapid' in dataset else ''
            flag_key = dataset['flagKey'] if 'flagKey' in dataset else ''
            datatype = dataset['datatype'] if 'datatype' in dataset else ''
            output_directory = dataset['outdir'] if 'outdir' in dataset else '.'
            number_of_dimensions = dataset['numdims'] if 'numdims' in dataset else 0
            lon_wrap = dataset['lonwrap'] if 'lonwrap' in dataset else False
            offset_hours = dataset['layeroffsethours'] if 'layeroffsethours' in dataset else 0
            mapfile = dataset['mapfile'] if 'mapfile' in dataset else ''
            time_format = dataset['timeformat'] if 'timeformat' in dataset else ''
            min_legend = dataset['minlegend'] if 'minlegend' in dataset else 0.0
            max_legend = dataset['maxlegend'] if 'maxlegend' in dataset else 0.0
            legend_prefix = dataset['legendprefix'] if 'legendprefix' in dataset else ''
            has_legend = dataset['haslegend'] if 'haslegend' in dataset else False
            dataset_lat_min = str(dataset['latMin']) if 'latMin' in dataset else lat_min
            dataset_lat_max = str(dataset['latMax']) if 'latMax' in dataset else lat_max
            dataset_lon_min = str(dataset['lonMin']) if 'lonMin' in dataset else lon_min
            dataset_lon_max = str(dataset['lonMax']) if 'lonMax' in dataset else lon_max
            dataset_lon_min_2 = str(dataset['lonMin2']) if 'lonMin2' in dataset else lon_min_2
            dataset_lon_max_2 = str(dataset['lonMax2']) if 'lonMax2' in dataset else lon_max_2

            start_hour = 0
            last_time_available = datetime.utcnow()
            last_time_available = last_time_available.replace(tzinfo=tz.gettz('UTC'))

            # TODO kgomes - validate input fields

            # Check to see if the URL points to an ERDDAP server
            if 'erddap' in url:

                # Grab just the base part of the remote URL
                base_url = url.split('erddap')[0]

                # Use that to construct the URL to the GetCapabilities XML document
                if not base_url.endswith("/"):
                    base_url = base_url + "/"
                get_capabilities_url = base_url + "erddap/wms/" + erddap_src_id + "/request?service=WMS&request=GetCapabilities&version=1.3.0"
                print("Grabbing latest capabilities from " + get_capabilities_url)

                try:
                    # Make the URL call to grab the XML document and save to the temp file
                    a, response = urlretrieve(get_capabilities_url, temp_file)

                    # Grab the response type and make sure it was XML that was returned
                    response_type = response['Content-Type']
                    if response_type != 'application/xml;charset=UTF-8':
                        print("Cannot fetch capabilities from " + get_capabilities_url + ". Is the " +
                              erddap_src_id + " dataset defined in the remote ERDDAP server?")
                    else:

                        # Parse and get the time dimension from the returned xml
                        capabilities_xml = minidom.parse(temp_file)

                        # Grab all the dimension tags
                        dimensions = capabilities_xml.getElementsByTagName('Dimension')

                        # Iterate over the dimension tags
                        for d in dimensions:
                            # Grab the value from inside the Dimension tag
                            dimension_tag_value = d.attributes['name'].value

                            # Check to see if we have the time dimension
                            if dimension_tag_value == "time":
                                # Get all the dates
                                all_dates = " ".join(t.nodeValue for t in d.childNodes if t.nodeType == t.TEXT_NODE)

                                # Split them into an array
                                dates = all_dates.split(',')

                                # Convert them to dates (from strings) and sort them
                                dates_list = [datetime.strptime(date, "%Y-%m-%dT%H:%M:%SZ") for date in dates]
                                dates_list.sort()

                                # Grab the latest one and set that as the last time available from this dataset
                                utc = dates_list[-1]
                                last_time_available = utc.replace(tzinfo=tz.gettz('UTC'))

                except Exception as e:
                    print("Error working in GetCapabilities XML: %s" % e)

            # Check to see if the URL indicates it is a THREDDS dataset
            if 'thredds' in url:

                # remove the ? and replace with /dataset.xml
                thredds_base_url = url[:-1]
                thredds_dataset_description_xml_url = thredds_base_url + '/dataset.xml'

                print("Getting latest available time from THREDDS dataset at " + thredds_dataset_description_xml_url)

                try:
                    # Grab the XML document and save to the temp file
                    a, response = urlretrieve(thredds_dataset_description_xml_url, temp_file)

                    # Grab the response type
                    response_type = response['Content-Type']

                    # Does it contain the word xml (checking if it looks like XML)
                    if response_type.find('xml') > 0:

                        # Parse the GetCapabilities XML from the temp file
                        thredds_dataset_description_xml = minidom.parse(temp_file)

                        # Look for the first TimeSpan element
                        time_span_element = thredds_dataset_description_xml.getElementsByTagName('TimeSpan')[0]

                        # Grab the last timestamp
                        rc = []
                        for node in time_span_element.getElementsByTagName('end')[0].childNodes:
                            if node.nodeType == node.TEXT_NODE:
                                rc.append(node.data)
                        last_time_string = ''.join(rc)

                        # Convert it to a date
                        last_time_utc = datetime.strptime(last_time_string, '%Y-%m-%dT%H:%M:%SZ')

                        # And set that to the last time available from the data set
                        last_time_available = last_time_utc.replace(tzinfo=tz.gettz('UTC'))

                        print("Latest time available for " + thredds_dataset_description_xml_url +
                              " is " + last_time_string)
                    else:
                        print("Cannot download from " + thredds_dataset_description_xml_url +
                              " bad response type " + response_type)

                except Exception as e:
                    print("Error downloading THREDDS dataset.xml: %s" % e)

            # calculate offset and adjust date accordingly according to hourly increment
            date_query = last_time_available

            # format the query date according to format
            date_query_string = date_query.strftime(query_time_format)

            # remove the underscore, dash, T, and Z ISO identifiers and format filename to store netCDF to
            replacement_set = {'-': '', 'T': '_', ':': '', 'Z': ''}
            start_date = replace_all(date_query_string, replacement_set)
            filename = output_directory + '/' + erddap_id + '/' + erddap_id + '_' + start_date + '.nc'

            # make sure output directory to write netCDF files exists
            ensure_dir(filename)

            # truncate the hours/minutes/seconds/microseconds to just compare the dates
            d1 = last_time_available.replace(hour=0, minute=0, second=0, microsecond=0)
            d2 = date_query.replace(hour=0, minute=0, second=0, microsecond=0)

            # only download if file doesn't exist already and within the bounds of the available times
            print("Checking if file exists " + filename)
            if not file_exists(filename) and d1 >= d2:

                print("File " + filename + " doesn't exist and " +
                      last_time_available.strftime(query_time_format) + " is >= " + date_query_string)

                # format the gridded query differently, depending on whether calling gridded THREDDS or ERDDAP servers
                if 'erddap' in url:
                    if lon_wrap == True:
                        if number_of_dimensions > 3:
                            remote = url + datatype + '[(' + date_query_string + '):1:(' + date_query_string + ')][(0.0):1:(0.0)][(' + dataset_lat_min + '):1:(' + dataset_lat_max + ')][(' + dataset_lon_min + '):1:(' + dataset_lon_max + ')]'
                        else:
                            remote = url + datatype + '[(' + date_query_string + '):1:(' + date_query_string + ')][(' + dataset_lat_min + '):1:(' + dataset_lat_max + ')][(' + dataset_lon_min + '):1:(' + dataset_lon_max + ')]'
                    else:
                        if number_of_dimensions > 3:
                            remote = url + datatype + '[(' + date_query_string + '):1:(' + date_query_string + ')][(0.0):1:(0.0)][(' + dataset_lat_min + '):1:(' + dataset_lat_max + ')][(' + dataset_lon_min_2 + '):1:(' + dataset_lon_max_2 + ')]'
                        else:
                            remote = url + datatype + '[(' + date_query_string + ')][(' + dataset_lat_min + '):(' + dataset_lat_max + ')][(' + dataset_lon_min_2 + '):(' + dataset_lon_max_2 + ')]'
                elif url.find('thredds') > 0:
                    if lon_wrap == True:
                        remote = url + "var=" + datatype + "&temporal=range&time_start=" + date_query_string + "&time_end=" + date_query_string + "&spatial=bb&north=" + dataset_lat_max + "&south=" + dataset_lat_min + "&east=" + dataset_lon_max_2 + "&west=" + dataset_lon_min_2 + "&horizStride=1&accept=netcdf&"
                    else:
                        remote = url + "var=" + datatype + "&temporal=range&time_start=" + date_query_string + "&time_end=" + date_query_string + "&spatial=bb&north=" + dataset_lat_max + "&south=" + dataset_lat_min + "&east=" + dataset_lon_max + "&west=" + dataset_lon_min + "&horizStride=1&accept=netcdf&"
                else:
                    raise Exception(
                        "Error - unknown protocol found in " + url + " for id " + erddap_src_id + " should be THREDDS or ERDDAP based download")

                try:
                    print("Downloading from " + remote)

                    # Now download the file from the remote server to the temp file
                    a, response = urlretrieve(remote, temp_file)

                    # Grab the response type
                    response_type = response['Content-Type']

                    # Make sure it's what we expect
                    if response_type != 'application/x-netcdf' and response_type != 'application/x-download':
                        print("cannot download from " + remote + " bad response type " + response_type)
                    else:
                        print("Moving " + temp_file + "  to " + filename)
                        shutil.move(temp_file, filename)

                        # Change permissions on the file to make sure it can be read by the world
                        os.chmod(filename, 0o755)

                except Exception as e:
                    print("Error downloading data file subset: %s" % e)

                # Now that the file subset has been downloaded, we need to hit the ERDDAP server in front of it to make
                # sure the information in ERDDAP is up to date. Check to see if a flag key is defined
                if flag_key:
                    try:
                        # Construct the URL to tell ERDDAP to update information about the downloaded file
                        update_url = erddap_base_url + "/setDatasetFlag.txt?datasetID=" + erddap_id + "&flagKey=" + flag_key
                        print("Updating ERRDAP from " + update_url)

                        # Make the call
                        a, response = urlretrieve(update_url)

                    except Exception as e:
                        print("Error trying to trigger ERDDAP update using flag key: %s" % e)

            # Now we need to make sure the mapfile that corresponds to the ERDDAP download is up to date. First make
            # sure the parent directory exists
            ensure_dir(mapfile)

            # Next, let's get the information from the ERDDAP server and create an updated mapserver file
            try:
                # Now let's create the URL to the GetCapabilities document from ERDDAP server for the data set that we
                # have downloaded
                local_get_capabilities_xml_url = erddap_base_url + "wms/" + erddap_id + "/request?service=WMS&request=GetCapabilities&version=1.3.0"
                print("Getting capabilities from " + local_get_capabilities_xml_url)

                # Let's create a new temp file since the original may not exist
                (gc_fd, gc_temp_file) = tempfile.mkstemp()

                # Let's get the document and write it to the temp file
                a, response = urlretrieve(local_get_capabilities_xml_url, gc_temp_file)

                # Do a quick check on the response type to make sure it looks OK
                responseType = response['Content-Type']
                if responseType != 'application/xml;charset=UTF-8':
                    raise Exception(
                        "cannot fetch capabilities from " + local_get_capabilities_xml_url +
                        ". Is the " + erddap_id + " dataset defined in your ERDDAP server ? ")

                # Parse the GetCapabilities XML
                gc_xml_doc = minidom.parse(gc_temp_file)
                dimensions = gc_xml_doc.getElementsByTagName('Dimension')

                # Initialize an array to hold depths in data set
                depths = [0.0]
                all_depths = {}

                # Loop over all the dimensions found in the GetCapabilities XML
                for dimension in dimensions:
                    # Grab the name
                    dimension_name = dimension.attributes['name'].value
                    # Check to see if it's related to depth
                    if dimension_name == "depth" or dimension_name == "altitude" or dimension_name == "elevation":
                        # Get all the depths in one space separated string
                        all_depths = " ".join(t.nodeValue for t in dimension.childNodes if t.nodeType == t.TEXT_NODE)
                        print("Depths available: " + all_depths)

                        # If the depths string contains slashes, it defines a depth set by begin, end and increment.
                        # We can use those values to construct an array of depth values
                        if all_depths.count('/') > 0:
                            # if split with / it's in the format start/stop/increment
                            depths_set_description = all_depths.split('/')
                            begin = float(depths_set_description[0])
                            end = float(depths_set_description[1])
                            incr = -1 * float(depths_set_description[2])
                            depths = np.arange(begin, end + incr, incr)

                        else:
                            # If the all depths string has commas, it is a list of depths, break them up and convert
                            # them to floats to get the depth array
                            if all_depths.count(',') >= 0:
                                results = all_depths.split(',')
                                results_map = map(float, results)
                                depths = np.array(list(results_map))

                # Get the max depth index which should just be the length of the array
                max_depth_index = len(depths)

                # Now lets look for the time information in the GetCapabilities XML
                for dimension in dimensions:
                    # Grab the name
                    dimension_name = dimension.attributes['name'].value

                    # Check to see if it's named time
                    if dimension_name == "time":

                        # Get all the dates from the XML and combine them
                        all_dates = " ".join(t.nodeValue for t in dimension.childNodes if t.nodeType == t.TEXT_NODE)

                        # Find the index of the largest time value which should just be the number of entries that is
                        # found by counting the number of commas
                        max_time_index = all_dates.count(',')

                        # Check to see if offset in hours was defined in the configuration file
                        if offset_hours != 0:

                            # Split all the dates into an array of strings
                            dates = all_dates.split(',')

                            # Create a map of datetimes
                            datetime_list = {}

                            # And a list of epoch timestamps
                            epoch_timestamp_list = {}

                            # Loop over all the date strings
                            for i in range(0, max_time_index):
                                # Convert the string to a dateimte
                                datetime_list[i] = datetime.strptime(dates[i], "%Y-%m-%dT%H:%M:%SZ")

                                # Convert to epoch and store in the list
                                epoch_timestamp_list[i] = toepoch(datetime_list[i])

                            # find closest to now offset by hours
                            closest_to_now_offset = datetime.utcnow() + timedelta(hours=offset_hours)

                            # Convert it to epoch
                            seconds_target = toepoch(closest_to_now_offset)

                            # Find the index of the closest epoch timestamp
                            target_index = min(epoch_timestamp_list, key=lambda x: abs(
                                float(epoch_timestamp_list[x]) - float(seconds_target)))

                            # Now grab the datetime object at that same index
                            target_datetime = datetime_list[target_index]
                        else:
                            # If no offset was specified, just grab the last one
                            target_index = max_time_index

                            # Grab the default date from the XML
                            default_date_string = dimension.attributes['default'].value

                            # Convert it to a datetime object so we can get the extracted date time in a different format
                            target_datetime = datetime.strptime(default_date_string, "%Y-%m-%dT%H:%M:%SZ")

                        # Convert the date time to a string in the format defined in the config file
                        default_date_string = target_datetime.strftime(time_format)
                        print("Found default time " + default_date_string + " for " + mapfile)

                        # get the template map file, which should be the mapfile prepended with a dot, e.g.
                        # /home/stoqsadm/deployed/mapfiles/oceanwatch/.chl1day.layer.map
                        mapfile_base = os.path.basename(mapfile)
                        mapfile_root = mapfile.split(mapfile_base)[0]
                        template_mapfile = mapfile_root + "/." + mapfile_base

                        # Create a temp file for the new mapfile from the template
                        (fd, new_mapfile_tempfile) = tempfile.mkstemp()

                        # Call the function to create the mapfile from the template by replacing the date string
                        replace_str(template_mapfile, new_mapfile_tempfile, 'default_start_time', default_date_string)

                        # now copy the temporarily created mapfile to the final mapfile
                        fsrc = open(new_mapfile_tempfile, 'rt')
                        fdst = open(mapfile, 'wt')
                        shutil.copyfileobj(fsrc, fdst)

                        # Clean up the temp mapfile if it exists
                        if file_exists(new_mapfile_tempfile):
                            print('removing ' + new_mapfile_tempfile)
                            os.remove(new_mapfile_tempfile)

                        # Let's check to see if the config states there is a legend
                        if has_legend:
                            print("Creating legend(s) for " + erddap_id)

                            # Let's create a tempfile so we can work with it
                            (fd, legend_temp_file) = tempfile.mkstemp()

                            # Create a list of dimensions
                            dims = {}

                            # Count the number of data types in the config by counting commas in the names
                            number_of_datatypes = datatype.count(',')

                            # Split the string of data types into a list
                            data_type_list = datatype.split(',')

                            # If there are some data types define
                            if number_of_datatypes > 0:

                                # Let's iterate over the depths
                                for k in range(0, max_depth_index):
                                    # Create a placeholder
                                    dim = ''
                                    for r in data_type_list:
                                        if number_of_dimensions > 3:
                                            dim = ''.join(r) + '[' + str(target_index) + '][' + str(k) + ']' + '[]' * (
                                                    number_of_dimensions - 2) + ',' + dim
                                        else:
                                            dim = ''.join(r) + '[' + str(target_index) + ']' + '[]' * (
                                                    number_of_dimensions - 1) + ',' + dim

                                    dims[k] = dim.rstrip(',')

                            else:
                                for k in range(0, max_depth_index):
                                    dim = ''
                                    if number_of_dimensions > 3:
                                        dim = datatype + '[' + str(target_index) + '][' + str(k) + ']' + '[]' * (
                                                    number_of_dimensions - 2)
                                    else:
                                        dim = datatype + '[' + str(target_index) + ']' + '[]' * (
                                                    number_of_dimensions - 1)
                                    dims[k] = dim

                            for k in range(0, max_depth_index):
                                legend_url = erddap_base_url + 'griddap/' + erddap_id + ".png?" + dims[
                                    k] + '&.colorBar=|||' + str(
                                    min_legend) + '|' + str(max_legend) + '&.legend=Only'
                                print("Remote url: " + legend_url)
                                # fetch the png legend
                                a, response = urlretrieve(legend_url, legend_temp_file)
                                responseType = response['Content-Type']
                                if responseType != 'image/png':
                                    print(
                                        "cannot fetch legend " + legend_url + ". Is the " + erddap_id + " dataset defined in your ERDDAP server ? ")
                                else:
                                    legendfile = legend_file_string % (
                                        erddap_id, legend_prefix, depths[k], datatype)
                                    fsrc = open(legend_temp_file, 'rb')
                                    fdst = open(legendfile, 'wb')
                                    shutil.copyfileobj(fsrc, fdst)

                            # Clean up the temp GetCapabilities XML file if it's there
                            if file_exists(legend_temp_file):
                                print('removing ' + legend_temp_file)
                                os.remove(legend_temp_file)

                # Clean up the temp GetCapabilities XML file if it's there
                if file_exists(gc_temp_file):
                    print('removing ' + gc_temp_file)
                    os.remove(gc_temp_file)

            except Exception as e:
                print("Error trying to generate mapfile from GetCapabilities from local ERDDAP server: %s" % e)

        else:
            print("Data set download flag is set to false. Skipping download, but will check for other data type")
            if dataset['datatype'] == 'wcofs':
                print("Data set is WCOFS. Will generate mapfiles for WCOFS")
                # Make sure output directory exists
                directory = dataset['outdir'] + "wcofs/"
                if not os.path.exists(directory):
                    os.makedirs(directory)
                # Grab GMT year, month, day
                gmt = time.gmtime()
                year = time.strftime("%Y", gmt)
                month = time.strftime("%m", gmt)
                day = time.strftime("%d", gmt)
                hour = time.strftime("%H", gmt)

                # The new files are generated at 03Z, so if the current hour is less than 3, we need will just do nothing
                # and leave the existing files in place
                if int(hour) <= 3:
                    print("Current hour is less than 03Z. Will not generate new mapfiles")
                else:
                    print("Current hour is greater than 03Z. Will generate new mapfiles")

                    # Let's generate the nowcast files first
                    nowcast_mapfile = open(dataset['outdir'] + "wcofs/" + dataset['wms_name'] + ".layer.map", "w")
                    nowcast_mapfile.write("  LAYER\n")
                    nowcast_mapfile.write("    NAME '" + dataset['layer_name'] + "'\n")
                    nowcast_mapfile.write("    TYPE RASTER\n")
                    nowcast_mapfile.write("    STATUS ON\n")
                    nowcast_mapfile.write("    CONNECTION '" + dataset['thredds_url_base'] + year + "/" + 
                                        month + "/" + day + "/nos.wcofs.2ds.n0" + hour + "." + year + month + day + 
                                        ".t03z.nc?COLORSCALERANGE=" + str(dataset['color_range_min']) + "," + str(dataset['color_range_max']) + 
                                        "&NUMCOLORBANDS=250'\n")
                    nowcast_mapfile.write("    CONNECTIONTYPE WMS\n")
                    nowcast_mapfile.write("    PROJECTION\n")
                    nowcast_mapfile.write("      'init=epsg:4326'\n")
                    nowcast_mapfile.write("    END\n")
                    nowcast_mapfile.write("    METADATA\n")
                    nowcast_mapfile.write("      'wms_name' '" + dataset['wms_name'] + "'\n")
                    nowcast_mapfile.write("      'wms_title' '" + dataset['wms_name'] + "'\n")
                    nowcast_mapfile.write("      'wms_server_version' '1.3.0'\n")
                    nowcast_mapfile.write("      'wms_srs' 'EPSG:4326'\n")
                    nowcast_mapfile.write("      'wms_format' 'image/png'\n")
                    nowcast_mapfile.write("      'wms_transparent' 'true'\n")
                    nowcast_mapfile.write("    END\n")
                    nowcast_mapfile.write("  END\n")           
                    nowcast_mapfile.close()

                    # Now, let's write the forecast files
                    forecast_hours = [12, 24, 36, 48, 60, 72]
                    for forecast_hour in forecast_hours:
                        forecast_mapfile = open(dataset['outdir'] + "wcofs/" + dataset['wms_name'] + "_forecast_" + str(forecast_hour) + ".layer.map", "w")
                        forecast_mapfile.write("  LAYER\n")
                        forecast_mapfile.write("    NAME '" + dataset['layer_name'] + "_forecast_" + str(forecast_hour) + "'\n")
                        forecast_mapfile.write("    TYPE RASTER\n")
                        forecast_mapfile.write("    STATUS ON\n")
                        forecast_mapfile.write("    CONNECTION '" + dataset['thredds_url_base'] + year + "/" + 
                                            month + "/" + day + "/nos.wcofs.2ds.f0" + str(forecast_hour) + "." + year + month + day + 
                                            ".t03z.nc?COLORSCALERANGE=" + str(dataset['color_range_min']) + "," + str(dataset['color_range_max']) + 
                                            "&NUMCOLORBANDS=250'\n")
                        forecast_mapfile.write("    CONNECTIONTYPE WMS\n")
                        forecast_mapfile.write("    PROJECTION\n")
                        forecast_mapfile.write("      'init=epsg:4326'\n")
                        forecast_mapfile.write("    END\n")
                        forecast_mapfile.write("    METADATA\n")
                        forecast_mapfile.write("      'wms_name' '" + dataset['wms_name'] + "'\n")
                        forecast_mapfile.write("      'wms_title' '" + dataset['wms_name'] + "'\n")
                        forecast_mapfile.write("      'wms_server_version' '1.3.0'\n")
                        forecast_mapfile.write("      'wms_srs' 'EPSG:4326'\n")
                        forecast_mapfile.write("      'wms_format' 'image/png'\n")
                        forecast_mapfile.write("      'wms_transparent' 'true'\n")
                        forecast_mapfile.write("    END\n")
                        forecast_mapfile.write("  END\n")           
                        forecast_mapfile.close()

except Exception as e:
    print("Error : %s" % e)
finally:
    if file_exists(temp_file):
        print('removing ' + temp_file)
        os.remove(temp_file)

print("Done !")
