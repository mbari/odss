# Deployment Resources

## Requirements

1. Create a directory on the local host where all data will be stored for the ODSS. For this example I will use ```/odsshost/data``` as the path for the host data directory.
1. Copy the .env.template file to a file named .env and set the ODSS_BASEDIR_HOST variable to this path. For example:

        ODSS_BASEDIR_HOST=/odsshost/data

1. On the host, create an ```erddap/erddapData``` in the base directory created in step 1. In this example, that would end up being ```/odsshost/data/erddap/erddapData```
1. If you want to override the datasets.xml file (you likely will), you can copy the datasets.xml file from the ```erddap/v2.18``` directory into the host erddap directory (in this example that would be ```/odsshost/data/erddap```) and then uncomment the volume mount in the docker-compose.yml file that mounts that file into the container.
1. In the .env file, uncomment/edit all the ```ERDDAP_*``` environment variables to match your settings. See the documentation from the Axiom [Docker Image](https://github.com/axiom-data-science/docker-erddap) and the [ERDDAP setup documentation](https://coastwatch.pfeg.noaa.gov/erddap/download/setup.html) for more details.
1. Note that it is likely you will want to increase the memory for the ERDDAP server and that can be done by uncommenting and editing the ```ERDDAP_MIN_MEMORY``` and ```ERDDAP_MAX_MEMORY``` variables in the .env file
1. TODO kgomes: If you are running an Apache proxy with SSL in front, I think you will have to fill out the ```ERDDAP_baseHttpsUrl``` environment variable
1. Created a ```thredds``` directory in your base directory and create subdirectories for ```tomcat/logs``` directory, a ```logs``` directory, a ```content``` directory and a ```data``` directory. In this example, I would end up with the following directories ```/odsshost/data/thredds/tomcat/logs```,```/odsshost/data/thredds/logs```, ```/odsshost/data/thredds/content```, ```/odsshost/data/thredds/data```
1. Make sure Docker is installed on the host machine and the docker daemon is running.

## Docker Compose

To run everything all the supporting services, make sure you have taken the steps above in the requirements section and then run:

```docker-compose up -d```

## Customization

### ERDDAP

In order to customize the ERDDAP data sets, you will want to edit the ```erddap/datasets.xml``` file. You can find instructions on how to work with this file [here](https://coastwatch.pfeg.noaa.gov/erddap/download/setupDatasetsXml.html).

### THREDDS

The default installation of the THREDDS server let's the container just create everything in the ```thredds/content/thredds``` folder. In that folder are the files that need to be changed in order to register data sets with THREDDS. The only problem is, by default, they are written from the container image. So, in order to override them, you would want to copy the files you need to another location (maybe somewhere like a ```thredds/custom``` directory) and then mount those over the ones in the container in the docker-compose.yml file. Files you will likely do that with are the ```thredds/content/thredds/threddsConfig.xml```, ```thredds/content/thredds/catalog.xml```, and ```thredds/content/thredds/enhancedCatalog.xml```.
