#!/usr/bin/env bash

# Create the user account
mongo -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD admin --eval "db.getSiblingDB('odss').createUser({ user : '$MONGO_ODSS_USER', pwd : '$MONGO_ODSS_PASSWORD', roles : [{ role: 'readWrite', db: 'odss'}]});"

# Create the various collections
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('layers',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('platforms',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('prefs',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('samples',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('sessions',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('timeline_periods',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('timeline_prefs',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('timeline_tokens',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('users',{});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.createCollection('views',{});"

# Now insert some default documents
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.views.insert({name:'Public',repoBasePath:'public'});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.layers.insert({name:'VIIRSN 8 Day Chlorophyll',type:'wms',url:'https://coastwatch.pfeg.noaa.gov/erddap/wms/erdVH2018chla8day/request',params:{service:'WMS',version:'1.1.1',request:'GetMap',srs:'EPSG:4326',elevation:'0.0',transparent:'true',format:'image/png',layers:'erdVH2018chla8day:chla'},options:{timeformat: \"UTC:yyyy-mm-dd'T'00:00:00'Z'\",hoursbetween:24,minhoursback:72},\"views\":[{\"name\":\"Public\",\"folder\":\"Chlorophyll\"}]});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.layers.insert({name:'Monterey NOAA Nav Chart',type:'wms',url:'https://localhost/cgi-bin/mapserv?',params:{map:'/data/mapserver/mapfiles/odss.map',layers:'NOAANavigationalChart:Monterey',transparent:'true'},options:{timeformat: \"UTC:yyyy-mm-dd'T'00:00:00'Z'\"},\"views\":[{\"name\":\"Public\",\"folder\":\"Charts\"}]});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.layers.insert({name:'SCB NOAA Nav Chart',type:'wms',url:'https://localhost/cgi-bin/mapserv?',params:{map:'/data/mapserver/mapfiles/odss.map',layers:'NOAANavigationalChart:SouthernCaliforniaBight',transparent:'true'},options:{timeformat: \"UTC:yyyy-mm-dd'T'00:00:00'Z'\"},\"views\":[{\"name\":\"Public\",\"folder\":\"Charts\"}]});"
mongo -u $MONGO_ODSS_USER -p $MONGO_ODSS_PASSWORD odss --eval "db.layers.insert({name:'Channel Island NOAA Chart',type:'wms',url:'https://localhost/cgi-bin/mapserv?',params:{map:'/data/mapserver/mapfiles/odss.map',layers:'NOAANavigationalChart:18720Public',transparent:'true'},options:{timeformat: \"UTC:yyyy-mm-dd'T'00:00:00'Z'\"},\"views\":[{\"name\":\"Public\",\"folder\":\"Charts\"}]});"
