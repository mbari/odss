// Require the AMQP library
var amqp = require('amqp');

var connection = amqp.createConnection(
    {
        host: 'localhost',
        port: '5672',
        login: 'odssuser',
        password: 'odsspassword',
        connectionTimeout: 0,
        authMechanism: 'AMQPLAIN',
        vhost: 'trackingvhost',
        ssl: { enabled: false }
    }
);

// The array of data points for the M
var mPoints = [
    [36.7500, -122.55],
    [36.85,-122.55],
    [36.75, -122.50],
    [36.85, -122.45],
    [36.75, -122.45],
    [36.85, -122.45],
    [36.75, -122.50],
    [36.85,-122.55]
];

// The array of data points for the B
var bPoints = [
    [36.7500, -122.4],
    [36.8500, -122.4],
    [36.8500, -122.35],
    [36.825, -122.30],
    [36.80, -122.35],
    [36.80, -122.4],
    [36.80, -122.35],
    [36.775, -122.3],
    [36.75, -122.35]
];

// The array of data points for the A
var aPoints = [
    [36.7500, -122.25],
    [36.85,-122.2],
    [36.75,-122.15],
    [36.805,-122.1775],
    [36.805,-122.2225]
];

// The array of data points for the R
var rPoints = [
    [36.7500, -122.100],
    [36.8500, -122.100],
    [36.8500, -122.05],
    [36.825, -122.0],
    [36.80, -122.05],
    [36.80, -122.1],
    [36.80, -122.05],
    [36.75, -122],
    [36.80, -122.05],
    [36.80, -122.1]
];

// The array of data points for the I
var iPoints = [
    [36.8500, -121.8500],
    [36.8500, -121.9500],
    [36.8500, -121.9000],
    [36.7500, -121.9000],
    [36.7500, -121.8500],
    [36.7500, -121.9500],
    [36.7500, -121.9000],
    [36.8500, -121.9000]
];

// Initilialize the loop counter
var loopCounter = 0;

// Wait for connection to become established.
connection.on('ready', function () {
    console.log("Connection ready ... ");

    // Grab the exchange
    exchange = connection.exchange('ships', {
        type: 'fanout',
        durable: true,
        autoDelete: false,
        confirm: true
    });

    // Handle the open event
    exchange.on('open', function () {
        console.log("Exchange open");
        setInterval(function () {
            // See which loop we are in
            console.log('Loop: ' + loopCounter);
            console.log('mIndex: ' + parseInt(loopCounter % mPoints.length));
            console.log('bIndex: ' + parseInt(loopCounter % bPoints.length));
            console.log('aIndex: ' + parseInt(loopCounter % aPoints.length));
            console.log('rIndex: ' + parseInt(loopCounter % rPoints.length));
            console.log('iIndex: ' + parseInt(loopCounter % iPoints.length));

            // Grab the current time stamp
            const now = new Date()
            const secondsSinceEpoch = Math.round(now.getTime() / 1000)

            // Send the lat lon
            var shipMessage = 'ship,testship,' + secondsSinceEpoch + ',' + mPoints[parseInt(loopCounter % mPoints.length)][0] + ',' + mPoints[parseInt(loopCounter % mPoints.length)][1] + ',sendTestAMQPMessages,,,' + now.toISOString();
            var auvMessage = 'auv,testauv,' + secondsSinceEpoch + ',' + bPoints[parseInt(loopCounter % bPoints.length)][0] + ',' + bPoints[parseInt(loopCounter % bPoints.length)][1] + ',sendTestAMQPMessages,,,' + now.toISOString();
            var drifterMessage = 'drifter,testdrifter,' + secondsSinceEpoch + ',' + aPoints[parseInt(loopCounter % aPoints.length)][0] + ',' + aPoints[parseInt(loopCounter % aPoints.length)][1] + ',sendTestAMQPMessages,,,' + now.toISOString();
            var mooringMessage = 'mooring,testmooring,' + secondsSinceEpoch + ',' + rPoints[parseInt(loopCounter % rPoints.length)][0] + ',' + rPoints[parseInt(loopCounter % rPoints.length)][1] + ',sendTestAMQPMessages,,,' + now.toISOString();
            var gliderMessage = 'glider,testglider,' + secondsSinceEpoch + ',' + iPoints[parseInt(loopCounter % iPoints.length)][0] + ',' + iPoints[parseInt(loopCounter % iPoints.length)][1] + ',sendTestAMQPMessages,,,' + now.toISOString();

            // console.log('Will publish all messages');
            exchange.publish('', shipMessage, {}, function (err) {
                if (err) {
                    console.log('Error publishing message!');
                    console.log(err);
                } else {
                    exchange.publish('', auvMessage, {}, function (err) {
                        if (err) {
                            console.log('Error publishing message!');
                            console.log(err);
                        } else {
                            exchange.publish('', drifterMessage, {}, function (err) {
                                if (err) {
                                    console.log('Error publishing message!');
                                    console.log(err);
                                } else {
                                    exchange.publish('', mooringMessage, {}, function (err) {
                                        if (err) {
                                            console.log('Error publishing message!');
                                            console.log(err);
                                        } else {
                                            exchange.publish('', gliderMessage, {}, function (err) {
                                                if (err) {
                                                    console.log('Error publishing message!');
                                                    console.log(err);
                                                } else {
                                                    console.log("published OK");
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });

            // Bump the loop counter
            loopCounter += 1;
        }, 5000)
    });
});

// Handle an error
connection.on('error', function (error) {
    console.log("Error on connection", error);
});