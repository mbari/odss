#!/bin/bash
set -e

echo "Creating $TRACKING_DB_USER user and mbaritracking database"
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE USER $TRACKING_DB_USER WITH PASSWORD '$TRACKING_DB_PASSWORD';
  CREATE DATABASE mbaritracking with OWNER=$TRACKING_DB_USER TEMPLATE=template_postgis;
  \c mbaritracking
  GRANT ALL ON ALL TABLES IN SCHEMA PUBLIC TO $TRACKING_DB_USER;
EOSQL

echo "all done"