# ODSS Utilities

This directory contains scripts that help with the management of data for the ODSS. The best way to use these are to build a Docker image using the Dockerfile in this directory. To do this simply run ```docker build .``` in this directory.

## Running the Utilities

Each section below describes a utility that runs inside the container and how to configure and work with it.

### Update ERDDAP and Mapserver

There is a python script inside the container that is set to run every 15 minutes. It looks for a file named ```mapdatasets.json``` in the root directory (```/```) inside the container. Based on the information inside that JSON file, it will reach out to different data servers (ERDDAP and THREDDS) and download subsets of those data files and store them wherever it is configured in the ```mapdatasets.json``` file. It is assumed that those locations are identified in the datasets.xml file of an ERDDAP somewhere. The script then reaches out to the ERDDAP server which serves those subsetted data (configured in the mapdatasets.json file) and then creates mapserver files and legend files that can be used by the ODSS. After building the ODSS utilities Docker image, it is usually run in the following manner:

```docker run -d -v /path/to/host/erddap/data:/data/erddap/data -v /path/to/host/mapserver:/data/mapserver -v /path/to/host/utils/logs:/var/log -v /path/to/host/mapdatasets.json:/mapdatasets.json --name odss-utils mbari/odss-utils```

where ```/path/to/host/erddap/data:/data/erddap/data``` is the mapping of the host directory to the /data/erddap/data directory inside the container. This allows you to map a directory from the host where you are likely serving files from ERDDAP. Basically, you want to mount a directory inside the container where the script can write to and then serve data from that host directory through ERDDAP. You would use the configurationin the mapdatasets.json file to tell the script inside the container where to write the downloaded files to. So, for example, if you wanted to have the scripts download subsets of the three day Cholorphyll data from Coastwatch, you would create an entry in the ```datasets``` array in the mapdatasets.json file that might look like:

        {
        "minlegend": "",
        "maxlegend": "",
        "layeroffsethours": 0,
        "incrhours": 24,
        "legendprefix": "",
        "haslegend": true,
        "numdims": 4,
        "download": true,
        "outdir": "/data/erddap/data/coastwatch",
        "url": "http://coastwatch.pfeg.noaa.gov/erddap/griddap/erdMWchla3day_LonPM180.nc?",
        "mapfile": "/data/mapserver/mapfiles/coastwatch/chl3day.layer.map",
        "lonwrap": false,
        "datatype": "chlorophyll",
        "erddapid": "erdMWchla3day",
        "erddapidsrc": "erdMWchla3day_LonPM180",
        "flagKey": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "startdelayhours": 72,
        "timeformat": "%Y-%m-%dT:%H:%M:%SZ",
        "querytimeformat": "%Y-%m-%dT12:00:00Z"
        },
 
where the ```outdir``` property tells the script where to download the files and remember, this is relative to the container. So, in this case, the script will write the files to ```/data/erddap/data/coastwatch``` inside the container. If you mount a host directory to ```/data/erddap/data``` it will then write those files to that host directory. You can then serve that host directory through an ERDDAP server.

The same goes for the ```mapfile``` property. The script reads information back from the ERDDAP server and creates mapfiles that is writes to that host location. Like with ERDDAP, you can then use those mapfiles from a different mapserver to serve up the WMS maps from ERDDAP.

Note the ```flagKey``` property needs to come from the ERDDAP server as it's generated after the dataset has been registered with ERDDAP.