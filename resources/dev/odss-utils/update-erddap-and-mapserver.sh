#!/usr/bin/bash

# Change to the root home directory
cd /

# Run the update erddap and mapserver python script
/usr/bin/python3 update-erddap-and-mapserver.py
