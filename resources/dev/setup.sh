#!/bin/bash
# This file sets up the directory structure with configuration files for the
# ODSS application and it's components (for use with Docker compose)

# First, read the environment variables from the .env file
source .env
echo "Starting setup of the ODSS repository structure in directory ${ODSS_BASEDIR_HOST}"

# Just as a sanity check, let's make sure the host directory exists
if [ ! -d ${ODSS_BASEDIR_HOST} ]; then
    echo "The host directory ${ODSS_BASEDIR_HOST} does not exist, please create and try again"
    exit
fi

#######################################################
# The Apache HTTPD service files
#######################################################
# The first step of the setup is to create the HTTPD directory on the host
echo "Setting up the HTTPD server"
if [ ! -d ${ODSS_BASEDIR_HOST}/httpd ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/httpd"
    mkdir ${ODSS_BASEDIR_HOST}/httpd
else
    echo "${ODSS_BASEDIR_HOST}/httpd already exists"
fi

# Now we need to generate the SSL certificate files if they don't exist
if [ ! -f ${ODSS_BASEDIR_HOST}/httpd/server.crt ]; then
    echo "SSL Certificates not generated, please answer the following"
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ${ODSS_BASEDIR_HOST}/httpd/server.key -out ${ODSS_BASEDIR_HOST}/httpd/server.crt
else
    echo "SSL server.crt exists in ${ODSS_BASEDIR_HOST}/httpd, will not overwrite"
fi

# Copy over the custom httpd.conf file
if [ ! -f ${ODSS_BASEDIR_HOST}/httpd/httpd.conf ]; then
    echo "httpd.conf file not copied over, will do so"
    cp ./httpd/httpd.conf ${ODSS_BASEDIR_HOST}/httpd/httpd.conf
else
    echo "${ODSS_BASEDIR_HOST}/httpd/httpd.conf exists, will not overwrite"
fi

#######################################################
# RabbitMQ server directories and files
#######################################################
# Create the base directory for the RabbitMQ data files and logs
if [ ! -d ${ODSS_BASEDIR_HOST}/rabbitmq ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/rabbitmq"
    mkdir ${ODSS_BASEDIR_HOST}/rabbitmq
else
    echo "${ODSS_BASEDIR_HOST}/rabbitmq already exists"
fi

# Create the data directory itself
if [ ! -d ${ODSS_BASEDIR_HOST}/rabbitmq/data ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/rabbitmq/data"
    mkdir ${ODSS_BASEDIR_HOST}/rabbitmq/data
else
    echo "${ODSS_BASEDIR_HOST}/rabbitmq/data already exists"
fi

# Now create the logs directory
if [ ! -d ${ODSS_BASEDIR_HOST}/rabbitmq/logs ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/rabbitmq/logs"
    mkdir ${ODSS_BASEDIR_HOST}/rabbitmq/logs
else
    echo "${ODSS_BASEDIR_HOST}/rabbitmq/logs already exists"
fi

#######################################################
# ERDDAP directories
#######################################################
if [ ! -d ${ODSS_BASEDIR_HOST}/erddap ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/erddap"
    mkdir ${ODSS_BASEDIR_HOST}/erddap
else
    echo "${ODSS_BASEDIR_HOST}/erddap already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/erddap/erddapData ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/erddap/erddapData"
    mkdir ${ODSS_BASEDIR_HOST}/erddap/erddapData
else
    echo "${ODSS_BASEDIR_HOST}/erddap/erddapData already exists"
fi
if [ ! -f ${ODSS_BASEDIR_HOST}/erddap/datasets.xml ]; then
    echo "ERDDAP datasets.xml file not copied over, will do so"
    cp ./erddap/v${ERDDAP_VERSION}/datasets.xml ${ODSS_BASEDIR_HOST}/erddap/datasets.xml
else
    echo "${ODSS_BASEDIR_HOST}/erddap/datasets.xml exists, will not overwrite"
fi

#######################################################
# THREDDS server directories
#######################################################
if [ ! -d ${ODSS_BASEDIR_HOST}/thredds ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/thredds"
    mkdir ${ODSS_BASEDIR_HOST}/thredds
else
    echo "${ODSS_BASEDIR_HOST}/thredds already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/thredds/content ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/thredds/content"
    mkdir ${ODSS_BASEDIR_HOST}/thredds/content
    mkdir ${ODSS_BASEDIR_HOST}/thredds/content/thredds
    cp ./thredds/catalog.xml ${ODSS_BASEDIR_HOST}/thredds/content/thredds/catalog.xml
else
    echo "${ODSS_BASEDIR_HOST}/thredds/content already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/thredds/data ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/thredds/data"
    mkdir ${ODSS_BASEDIR_HOST}/thredds/data
    cp ./thredds/Dorado389_2020_286_00_286_00.nc ${ODSS_BASEDIR_HOST}/thredds/data/Dorado389_2020_286_00_286_00.nc
else
    echo "${ODSS_BASEDIR_HOST}/thredds/data already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/thredds/logs ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/thredds/logs"
    mkdir ${ODSS_BASEDIR_HOST}/thredds/logs
else
    echo "${ODSS_BASEDIR_HOST}/thredds/logs already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/thredds/tomcat ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/thredds/tomcat"
    mkdir ${ODSS_BASEDIR_HOST}/thredds/tomcat
else
    echo "${ODSS_BASEDIR_HOST}/thredds/tomcat already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/thredds/tomcat/logs ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/thredds/tomcat/logs"
    mkdir ${ODSS_BASEDIR_HOST}/thredds/tomcat/logs
else
    echo "${ODSS_BASEDIR_HOST}/thredds/tomcat/logs already exists"
fi

#######################################################
# Mapserver configuration
#######################################################
if [ ! -d ${ODSS_BASEDIR_HOST}/mapserver ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mapserver"
    mkdir ${ODSS_BASEDIR_HOST}/mapserver
else
    echo "${ODSS_BASEDIR_HOST}/mapserver already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/mapserver/logs ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mapserver/logs"
    mkdir ${ODSS_BASEDIR_HOST}/mapserver/logs
else
    echo "${ODSS_BASEDIR_HOST}/mapserver/logs already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/mapserver/mapfiles ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mapserver/mapfiles"
    mkdir ${ODSS_BASEDIR_HOST}/mapserver/mapfiles
else
    echo "${ODSS_BASEDIR_HOST}/mapserver/mapfiles already exists"
fi
if [ ! -f ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/odss.map ]; then
    echo "Copying odss.map to ${ODSS_BASEDIR_HOST}/mapserver/mapfiles"
    cp ./mapserver/odss.map ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/odss.map
else
    echo "${ODSS_BASEDIR_HOST}/mapserver/mapfiles/odss.map already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/charts ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/charts"
    mkdir ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/charts
    cp ./mapserver/mapfiles/charts/* ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/charts/
else
    echo "${ODSS_BASEDIR_HOST}/mapserver/mapfiles/charts already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/stations ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/stations"
    mkdir ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/stations
    cp ./mapserver/mapfiles/stations/* ${ODSS_BASEDIR_HOST}/mapserver/mapfiles/stations/
else
    echo "${ODSS_BASEDIR_HOST}/mapserver/mapfiles/stations already exists"
fi

#######################################################
# MongoDB directory for data files
#######################################################
if [ ! -d ${ODSS_BASEDIR_HOST}/mongodb ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mongodb"
    mkdir ${ODSS_BASEDIR_HOST}/mongodb
else
    echo "${ODSS_BASEDIR_HOST}/mongodb already exists"
fi

#######################################################
# PostGIS and tracking client directories and files
#######################################################
if [ ! -d ${ODSS_BASEDIR_HOST}/mbaritracking ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mbaritracking"
    mkdir ${ODSS_BASEDIR_HOST}/mbaritracking
else
    echo "${ODSS_BASEDIR_HOST}/mbaritracking already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/mbaritracking/postgis ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mbaritracking/postgis"
    mkdir ${ODSS_BASEDIR_HOST}/mbaritracking/postgis
else
    echo "${ODSS_BASEDIR_HOST}/mbaritracking/postgis already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/mbaritracking/client ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mbaritracking/client"
    mkdir ${ODSS_BASEDIR_HOST}/mbaritracking/client
else
    echo "${ODSS_BASEDIR_HOST}/mbaritracking/client already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/mbaritracking/client/logs ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/mbaritracking/client/logs"
    mkdir ${ODSS_BASEDIR_HOST}/mbaritracking/client/logs
else
    echo "${ODSS_BASEDIR_HOST}/mbaritracking/client/logs already exists"
fi
# Make sure the database init script is executable so it will be in the container
chmod a+x ./trackingdb/tracking-init.sh

#######################################################
# Create the directories and example config files for the utils
#######################################################
if [ ! -d ${ODSS_BASEDIR_HOST}/utils ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/utils"
    mkdir ${ODSS_BASEDIR_HOST}/utils
else
    echo "${ODSS_BASEDIR_HOST}/utils already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/utils/logs ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/utils/logs"
    mkdir ${ODSS_BASEDIR_HOST}/utils/logs
else
    echo "${ODSS_BASEDIR_HOST}/utils/logs already exists"
fi
if [ ! -f ${ODSS_BASEDIR_HOST}/utils/mapdatasets.json ]; then
    echo "Copying mapdatasets.json to ${ODSS_BASEDIR_HOST}/utils"
    cp ./odss-utils/mapdatasets.json ${ODSS_BASEDIR_HOST}/utils/mapdatasets.json
else
    echo "${ODSS_BASEDIR_HOST}/utils/mapdatasets.json already exists"
fi

###########################################
# Create the repository directory structure
###########################################
if [ ! -d ${ODSS_BASEDIR_HOST}/repo ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/repo"
    mkdir ${ODSS_BASEDIR_HOST}/repo
else
    echo "${ODSS_BASEDIR_HOST}/repo already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/repo/activities ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/repo/activities"
    mkdir ${ODSS_BASEDIR_HOST}/repo/activities
else
    echo "${ODSS_BASEDIR_HOST}/repo/activities already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/repo/public ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/repo/public"
    mkdir ${ODSS_BASEDIR_HOST}/repo/public
    echo "This is the default file repository for the ODSS" >> ${ODSS_BASEDIR_HOST}/repo/public/README.txt
else
    echo "${ODSS_BASEDIR_HOST}/repo/public already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/repo/platforms ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/repo/platforms"
    mkdir ${ODSS_BASEDIR_HOST}/repo/platforms
else
    echo "${ODSS_BASEDIR_HOST}/repo/platforms already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/repo/routine-products ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/repo/routine-products"
    mkdir ${ODSS_BASEDIR_HOST}/repo/routine-products
else
    echo "${ODSS_BASEDIR_HOST}/repo/routine-products already exists"
fi

###########################################
# The ODSS Server
###########################################
if [ ! -d ${ODSS_BASEDIR_HOST}/server ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/server"
    mkdir ${ODSS_BASEDIR_HOST}/server
else
    echo "${ODSS_BASEDIR_HOST}/server already exists"
fi
if [ ! -d ${ODSS_BASEDIR_HOST}/server/logs ]; then
    echo "Creating ${ODSS_BASEDIR_HOST}/server/logs"
    mkdir ${ODSS_BASEDIR_HOST}/server/logs
else
    echo "${ODSS_BASEDIR_HOST}/server/logs already exists"
fi
