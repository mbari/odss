# This is the Dockerfile to build the image for the server API and web application for the ODSS

# Start with NodeJS version 14 image
FROM node:14

# Add the application code
COPY webapp/server/models/* /opt/odss/models/
COPY webapp/server/platim/ /opt/odss/platim/
COPY webapp/server/routes/* /opt/odss/routes/
COPY webapp/server/web/ /opt/odss/web/
COPY webapp/server/app.js /opt/odss/app.js
COPY webapp/server/AppServer.js /opt/odss/AppServer.js
COPY webapp/server/config.js.docker /opt/odss/config.js
COPY webapp/server/Datastore.js /opt/odss/Datastore.js
COPY webapp/server/package-lock.json /opt/odss/package-lock.json
COPY webapp/server/package.json /opt/odss/package.json

# Define the working directory
WORKDIR /opt/odss

# Run the NPM installation
RUN npm install

# Now define the entrypoint command
ENTRYPOINT ["node","app.js"]