# Baselayers Build

This directory contains the configuration files that the Ant build file (build.xml) uses to deploy alternate base layers to the apache server.  The default target for the Ant build is 'deploy'.  There should be a .properties file in the ../servers directory that has a name that you use in the build call to specify which properties to load before the build.  If you have a server named 'myserver', there should be a file named '../servers/myserver/myserver.properties' and then you would call Ant passing in this as a property named 'server'.  For example, you might run:

ant -Dserver=myserver deploy

Here are the targets and what they do:

* deploy:
  * Check to see if the gebco files are already deployed in the Apache html directory
  * If not, it unzips the gebco_noaa.zip file to the apache html directory
* undeploy:
  * deletes the gebco files from the apache html directory

Kevin Gomes (1/22/2014)
