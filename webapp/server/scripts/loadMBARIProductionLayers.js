// This is a script that will load the default layers for the ODSS installation
// into the mongodb on localhost
db = connect("localhost:27017/odss");
db.auth("odssadm", "theAcc0unTWp0w3r!");

// Clear all the layers
db.layers.remove();
db.getLastError();

// Create objects and insert for each of the layers
layer1 = {
    "_ver": 1,
    "name": "Chlorophyll-a, MODIS 1-day Composite",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMWchla1day:chlorophyll&RULE=erdMWchla1day:chlorophyll",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMWchla1day:chlorophyll",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "Chlorophyll"
        }
    ]
};
db.layers.insert(layer1);

layer2 = {
    "_ver": 1,
    "id": 4,
    "name": "Chlorophyll-a, MODIS 3-day Composite",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMWchla3day:chlorophyll&RULE=erdMWchla3day:chlorophyll",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMWchla3day:chlorophyll",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "Chlorophyll"
        }
    ]
};
db.layers.insert(layer2);

layer3 = {
    "_ver": 1,
    "id": 5,
    "name": "Chlorophyll-a, MODIS 8-day Composite",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMWchla8day:chlorophyll&RULE=erdMWchla8day:chlorophyll",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMWchla8day:chlorophyll",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "Chlorophyll"
        }
    ]
};
db.layers.insert(layer3);
layer4 = {
    "_ver": 1,
    "id": 6,
    "name": "Chlorophyll-a, MODIS Experimental",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMWchla:MWchla&RULE=erdMWchla:MWchla",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMWchla:MWchla",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "Chlorophyll"
        }
    ]
};
db.layers.insert(layer4);
layer5 = {
    "_ver": 1,
    "id": 7,
    "name": "SST 1 Day Composite",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdATssta1day:sst&RULE=erdATssta1day:sst",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdATssta1day:sst",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "SST"
        }
    ]
};
db.layers.insert(layer5);
layer6 = {
    "_ver": 1,
    "id": 8,
    "name": "SST 3 Day Composite",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdATssta3day:sst&RULE=erdATssta3day:sst",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdATssta3day:sst",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "SST"
        }
    ]
};
db.layers.insert(layer6);
layer7 = {
    "_ver": 1,
    "id": 9,
    "name": "SST 8 Day Composite",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdATssta8day:sst&RULE=erdATssta8day:sst",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdATssta8day:sst",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "SST"
        }
    ]
};
db.layers.insert(layer7);
layer8 = {
    "_ver": 1,
    "name": "SST Aqua MODIS",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMWsstd:MWsstd&RULE=erdMWsstd:MWsstd",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMWsstd:MWsstd",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer8);
layer9 = {
    "_ver": 1,
    "name": "SST NOAA GOES",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdGAssta:GAssta&RULE=erdGAssta:GAssta",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdGAssta:GAssta",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer9);
layer10 = {
    "_ver": 1,
    "name": "Fluorescence, Aqua MODIS",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMWcflh:MWcflh&RULE=erdMWcflh:MWcflh",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMWcflh:MWcflh",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer10);
layer11 = {
    "_ver": 1,
    "name": "Ekman Upwelling, METOP ASCAT",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdQAwekm:QAwekm&RULE=erdQAwekm:QAwekm",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdQAwekm:QAwekm",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer11);
layer12 = {
    "_ver": 1,
    "name": "Photosynthetically Available Radiation, MODIS",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMWpar0:MWpar0&RULE=erdMWpar0:MWpar0",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMWpar0:MWpar0",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer12);
layer13 = {
    "_ver": 1,
    "name": "M1 100m Hourly Avg Current",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdM1HourlyAvg:current,erdM1HourlyAvg:current:legend,erdM1HourlyAvg:current:legend_label",
        "depth": "100.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh3000'Z'",
        "hoursbetween": 1,
        "minhoursback": 1
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer13);
layer14 = {
    "_ver": 1,
    "name": "M1 20m Hourly Avg Current",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdM1HourlyAvg:current,erdM1HourlyAvg:current:legend,erdM1HourlyAvg:current:legend_label",
        "depth": "20.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh3000'Z'",
        "hoursbetween": 1,
        "minhoursback": 1
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer14);
layer15 = {
    "_ver": 1,
    "name": "M1 Hourly Avg Surface Wind",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdM1HourlyAvg:wind,erdM1HourlyAvg:wind:legend,erdM1HourlyAvg:wind:legend_label",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh3000'Z'",
        "hoursbetween": 1,
        "minhoursback": 1
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer15);
layer16 = {
    "_ver": 1,
    "name": "ROMS Surface Salinity Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast:salt&RULE=erdMRYROMSnowcast:salt",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast:salt",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer16);
layer17 = {
    "_ver": 1,
    "name": "ROMS 100m Salinity Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast:salt:100m&RULE=erdMRYROMSnowcast:salt:100m",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast:salt:100m",
        "depth": "100.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer17);
layer18 = {
    "_ver": 1,
    "name": "ROMS Surface Temperature Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast:temp&RULE=erdMRYROMSnowcast:temp",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast:temp",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer18);
layer19 = {
    "_ver": 1,
    "name": "ROMS 100m Temperature Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast:temp:100m&RULE=erdMRYROMSnowcast:temp:100m",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast:temp:100m",
        "depth": "100.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png",
        "colorBarMaximum": "12.0",
        "colorBarMaximum": "8.0"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer19);
layer20 = {
    "_ver": 1,
    "name": "ROMS Surface Current Nowcast",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast:current&RULE=erdMRYROMSnowcast:current",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast:current",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer20);
layer21 = {
    "_ver": 1,
    "name": "ROMS 100m Current Nowcast",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast:current:100m&RULE=erdMRYROMSnowcast:current:100m",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast:current:100m",
        "depth": "100.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer21);
layer22 = {
    "_ver": 1,
    "name": "ROMS Diatom Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast_bio:s2&RULE=erdMRYROMSnowcast_bio:s2",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast_bio:s2",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer22);
layer23 = {
    "_ver": 1,
    "name": "ROMS Meso Zooplankton Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast_bio:zz2&RULE=erdMRYROMSnowcast_bio:zz2",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast_bio:zz2",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer23);
layer24 = {
    "_ver": 1,
    "name": "ROMS Nitrate Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast_bio:no3&RULE=erdMRYROMSnowcast_bio:no3",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast_bio:no3",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer24);
layer25 = {
    "_ver": 1,
    "name": "ROMS Ammonium Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast_bio:nh4&RULE=erdMRYROMSnowcast_bio:nh4",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast_bio:nh4",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer25);
layer26 = {
    "_ver": 1,
    "name": "ROMS 50m Ammonium Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMSnowcast_bio:nh4:50m&RULE=erdMRYROMSnowcast_bio:nh4:50m",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMSnowcast_bio:nh4:50m",
        "depth": "50.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer26);
layer27 = {
    "_ver": 1,
    "name": "ROMS Surface Salinity 24hr Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS24hrforecast:salt&RULE=erdMRYROMS24hrforecast:salt",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS24hrforecast:salt",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer27);
layer28 = {
    "_ver": 1,
    "name": "ROMS Surface Salinity 48hr Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS48hrforecast:salt&RULE=erdMRYROMS48hrforecast:salt",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS48hrforecast:salt",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer28);
layer29 = {
    "_ver": 1,
    "name": "ROMS Surface Salinity 72hr Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS72hrforecast:salt&RULE=erdMRYROMS72hrforecast:salt",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS72hrforecast:salt",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer29);
layer30 = {
    "_ver": 1,
    "name": "ROMS Surface Temperature 24hr Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS24hrforecast:temp&RULE=erdMRYROMS24hrforecast:temp",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS24hrforecast:temp",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer30);
layer31 = {
    "_ver": 1,
    "name": "ROMS Surface Temperature 48hr Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS48hrforecast:temp&RULE=erdMRYROMS48hrforecast:temp",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS48hrforecast:temp",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer31);
layer32 = {
    "_ver": 1,
    "name": "ROMS Surface Temperature 72hr Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS72hrforecast:temp&RULE=erdMRYROMS72hrforecast:temp",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS72hrforecast:temp",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer32);
layer33 = {
    "_ver": 1,
    "name": "Compact Model 24 hr Current Forecast ",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdCM24hrCurrentForecast&RULE=erdCM24hrCurrentForecast",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdCM24hrCurrentForecast",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 1,
        "minhoursback": 1
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer33);
layer34 = {
    "_ver": 1,
    "name": "HF Radar Surface Current COCMP OMA  ",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYCurrentOMA&RULE=erdMRYCurrentOMA",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYCurrentOMA,erdMRYCurrentOMA:legend,erdSCBCurrentOMA:legend,erdMRYCurrentOMA:legend_label",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 1,
        "minhoursback": 1
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer34);
layer35 = {
    "_ver": 1,
    "name": "HF Radar Surface Current Realtime 2km Hourly",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYhfrnet2km&RULE=erdMRYhfrnet2km",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYhfrnet2km",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 1,
        "minhoursback": 1
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer35);
layer36 = {
    "_ver": 1,
    "name": "ROMS Surface Current 24 hr Forecast ",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS24hrforecast:current&RULE=erdMRYROMS24hrforecast:current",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS24hrforecast:current",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer36);
layer37 = {
    "_ver": 1,
    "name": "ROMS Surface Current 48 hr Forecast",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS48hrforecast:current&RULE=erdMRYROMS48hrforecast:current",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS48hrforecast:current",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer37);
layer38 = {
    "_ver": 1,
    "name": "ROMS Surface Current 72hr Forecast",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMRYROMS72hrforecast:current&RULE=erdMRYROMS72hrforecast:current",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMRYROMS72hrforecast:current",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer38);
layer39 = {
    "_ver": 1,
    "name": "Station M",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "stationm",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer39);
layer40 = {
    "_ver": 1,
    "name": "Line 67",
    "type": "wms",
    "url": "/tilecache/tilecache.cgi?",
    "params": {
        "map": "/data/mapserver/mapfiles/proxy.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "line67",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer40);
layer41 = {
    "_ver": 1,
    "name": "MARS node",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "layers": "marsnode",
        "transparent": "true"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer41);
layer42 = {
    "_ver": 1,
    "name": "Monterey NOAA Nav Chart",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "params": {
        "map": "/home/stoqsadm/deployed/mapfiles/proxy.map",
        "layers": "NOAANavigationalChart:Monterey",
        "transparent": "true"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'"
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer42);
layer43 = {
    "_ver": 1,
    "name": "AIS 10-minute Marine Traffic",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "layers": "ais",
        "transparent": "true"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'"
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer43);
layer44 = {
    "_ver": 1,
    "name": "AIS Hourly Marine Traffic",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "layers": "ais:hourly",
        "transparent": "true"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'"
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer44);
layer45 = {
    "_ver": 1,
    "name": "Monterey MODIS GNB Bloom Prediction",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdMWbloomGNBMonterey:labels&RULE=erdMWbloomGNBMonterey:labels",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMWbloomGNBMonterey:labels",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'120000'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer45);
layer46 = {
    "_ver": 1,
    "name": "SCB Shipping Lane Risk Map",
    "type": "wms",
    "url": "/tilecache/tilecache.cgi?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "scbshippinglanes",
        "singleTile": "true",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer46);
layer47 = {
    "_ver": 1,
    "name": "SCB NOAA Nav Chart",
    "type": "wms",
    "url": "/tilecache/tilecache.cgi?",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "layers": "NOAANavigationalChart:SouthernCaliforniaBight",
        "transparent": "true",
        "singleTile": "true"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer47);
layer48 = {
    "_ver": 1,
    "name": "SCB ROMS Surface Salinity Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdSCBROMSnowcast:salt&RULE=erdSCBROMSnowcast:salt",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdSCBROMSnowcast:salt",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer48);
layer49 = {
    "_ver": 1,
    "name": "SCB ROMS Surface Temperature Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdSCBROMSnowcast:temp&RULE=erdSCBROMSnowcast:temp",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdSCBROMSnowcast:temp",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer49);
layer50 = {
    "_ver": 1,
    "name": "SCB ROMS Surface Current Nowcast",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdSCBROMSnowcast:current&RULE=erdSCBROMSnowcast:current",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdSCBROMSnowcast:current",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer50);
layer51 = {
    "_ver": 1,
    "name": "SCB ROMS Surface Sea Surface Height Nowcast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdSCBROMSnowcastSSH:zeta&RULE=erdSCBROMSnowcastSSH:zeta",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdSCBROMSnowcastSSH:zeta",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 6,
        "minhoursback": 6
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer51);
layer52 = {
    "_ver": 1,
    "name": "SCB ROMS Surface Salinity Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdSCBROMSforecast:salt&RULE=erdSCBROMSforecast:salt",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdSCBROMSforecast:salt",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer52);
layer53 = {
    "_ver": 1,
    "name": "SCB ROMS Surface Temperature Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdSCBROMSforecast:temp&RULE=erdSCBROMSforecast:temp",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdSCBROMSforecast:temp",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyy-mm-dd'T'00:00:00'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer53);
layer54 = {
    "_ver": 1,
    "name": "SCB ROMS Surface Current Forecast",
    "type": "wms",
    "url": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?",
    "legendurl": "http://odss-test.shore.mbari.org/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odssvector.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdSCBROMSforecast:current&RULE=erdSCBROMSforecast:current",
    "params": {
        "map": "/data/mapserver/mapfiles/odssvector.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdSCBROMSforecast:current",
        "depth": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer54);
layer55 = {
    "_ver": 1,
    "name": "SCB ROMS Surface Sea Surface Height Forecast",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "/cgi-bin/mapserv?MAP=/data/mapserver/mapfiles/odss.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getlegendgraphic&FORMAT=image%2Fpng&LAYER=erdSCBROMSforecastSSH:zeta&RULE=erdSCBROMSforecastSSH:zeta",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdSCBROMSforecastSSH:zeta",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
        "timeformat": "UTC:yyyymmdd'T'hh0000'Z'",
        "hoursbetween": 3,
        "minhoursback": 3
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer55);
layer56 = {
    "_ver": 1,
    "name": "ECOHAB 2014 Spring ADCP Planned Location",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "ecohab2013Spring:adcp",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer56);
layer57 = {
    "_ver": 1,
    "name": "ECOHAB 2014 Spring Wirewalker Planned Location",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "ecohab2013Spring:wirewalker",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer57);
layer58 = {
    "_ver": 1,
    "name": "ECOHAB 2014 Spring CTD Planned Stations",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "ecohab2013Spring:ctd",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer58);
layer59 = {
    "_ver": 1,
    "name": "ECOHAB 2014 Fall ESP Locations",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "ecohab2013Fall:esp",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer59);
layer60 = {
    "_ver": 1,
    "name": "ECOHAB 2014 Spring ESP Locations",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "ecohab2013Spring:esp",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer60);
layer61 = {
    "_ver": 1,
    "name": "ECOHAB 2014 Spring LRAUV Planned Survey",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "ecohab2013Spring:lrauv",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer61);
layer62 = {
    "_ver": 1,
    "name": "ECOHAB 2014 Spring USC Glider 1 Planned Box Pattern",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "ecohab2013Spring:uscgliders_box",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer62);
layer63 = {
    "_ver": 1,
    "name": "ECOHAB 2014 Spring USC Glider 2 Planned Track Line",
    "type": "wms",
    "url": "/cgi-bin/mapserv?",
    "legendurl": "",
    "params": {
        "map": "/data/mapserver/mapfiles/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "ecohab2013Spring:uscgliders_line",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "options": {
    },
    "views": [
        {
            "name": "Public",
            "folder": ""
        }
    ]
};
db.layers.insert(layer63)