// This is a script that will load the default layers for the ODSS installation
// into the mongodb on localhost
db = connect("localhost:27017/odss");
db.auth("odssadm", "theAcc0unTWp0w3r!");

// Clear all the layers
db.layers.remove();
db.getLastError();

// Create objects for each of the layers
ssta5day = {
    "name": "SST, Blended, Global, EXPERIMENTAL (5 Day Composite)",
    "type": "wms",
    "url": "/tilecache/tilecache.cgi?",
    "legendurl": "/erddap/griddap/erdBAssta5day.png?sst[3879][0][][]&.legend=Only",
    "layerParams": {
        "map": repoBasePath + "/mapserver/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdBAssta5day:sst",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "layerOptions": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "SST"
        },
        {
            "name": "2013 September",
            "folder": "SST"
        }
    ]

}
db.layers.insert(ssta5day);

erdMHchla8day = {
    "name": "Chlorophyll-a, Aqua MODIS, NPP, Global, Science Quality (8 Day Composite)",
    "type": "wms",
    "url": "/tilecache/tilecache.cgi?",
    "legendurl": "/erddap/griddap/erdMHchla8day.png?chlorophyll[507][0][][]&.legend=Only",
    "layerParams": {
        "map": repoBasePath + "/mapserver/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdMHchla8day:chlorophyll",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "layerOptions": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "Chlorophyll"
        },
        {
            "name": "2013 September",
            "folder": "Chlorophyll"
        }
    ]

}
db.layers.insert(erdMHchla8day);

erdTAssh1day = {
    "name": "Sea Surface Height, Aviso, Global, Science Quality (1 Day Composite)",
    "type": "wms",
    "url": "/tilecache/tilecache.cgi?",
    "legendurl": "/erddap/griddap/erdTAssh1day.png?ssh[1253][0][][]&.legend=Only",
    "layerParams": {
        "map": repoBasePath + "/mapserver/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "erdTAssh1day:ssh",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "layerOptions": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "Sea Surface Height"
        },
        {
            "name": "2013 September",
            "folder": "Sea Surface Height"
        }
    ]

}
db.layers.insert(erdTAssh1day);

etopo180 = {
    "name": "Topography, ETOPO1, 0.0166667 degrees, Global (longitude -180 to 180), (Ice Sheet Surface)",
    "type": "wms",
    "url": "/tilecache/tilecache.cgi?",
    "legendurl": "/erddap/griddap/etopo180.png?altitude[][]&.legend=Only",
    "layerParams": {
        "map": repoBasePath + "/mapserver/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "etopo180:altitude",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "layerOptions": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "Topography"
        },
        {
            "name": "2013 September",
            "folder": "Topography"
        }
    ]

}
db.layers.insert(etopo180);

etopo360 = {
    "name": "Topography, ETOPO1, 0.0166667 degrees, Global (longitude 0 to 360), (Ice Sheet Surface)",
    "type": "wms",
    "url": "/tilecache/tilecache.cgi?",
    "legendurl": "/erddap/griddap/etopo360.png?altitude[][]&.legend=Only",
    "layerParams": {
        "map": repoBasePath + "/mapserver/odss.map",
        "service": "WMS",
        "version": "1.1.1",
        "request": "GetMap",
        "srs": "EPSG:4326",
        "exceptions": "application/vnd.ogc.se_iniimage",
        "layers": "etopo360:altitude",
        "elevation": "0.0",
        "transparent": "true",
        "bgcolor": "0x808080",
        "format": "image/png"
    },
    "layerOptions": {
        "timeformat": "UTC:yyyy-mm-dd'T'12:00:00'Z'",
        "hoursbetween": 24,
        "minhoursback": 24
    },
    "views": [
        {
            "name": "Public",
            "folder": "Topography"
        },
        {
            "name": "2013 September",
            "folder": "Topography"
        }
    ]

}
db.layers.insert(etopo360);
