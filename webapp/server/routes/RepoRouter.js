/**
 * This class can be used by a router to attach a handler to deal with requests from the
 * ODSS' data pane. It basically returns a file listing for a folder that is opened in
 * the data pane.  A handy thing to remember, is there are different segments of paths
 * used in this class.  For example, there should be a directory on the system running
 * this code which will server as the root of the data repository browser that is the
 * data pane.  Also, when 'Views' are created in the ODSS, there is a 'repoBasePath'
 * that links a views' data pane with a directory on the server.  This is relative to
 * the root directory.  So, for example, if the root directory is specified as:
 * 
 * /data/root/dir/for/odss
 * 
 * and in a View in the ODSS, the repoBase Path is specified as:
 * 
 * activity/canon
 * 
 * the path that will be linked to the View will be:
 * 
 * /data/root/dir/for/odss/activity/canon
 * 
 * and that directories contents will be show in the data pane.  In this code, I
 * refer to the root directory as 'repoRootDir'. Also, there is a 'repoRootDirUrl'
 * which is the base portion of the URL that points to the root directory and is
 * normally '/data'.
 */

// Import the logging library
var log4js = require('log4js');

// Any dependencies
var path = require('path');
var fs = require('fs');

// The module for helping to detect mime types
var mime = require('mime-types');

// The repository router object
function RepoRouter(repoRootDir, repoRootDirUrl) {

    // Grab reference to this object for callbacks
    var me = this;

    // Grab logger defined in app.js
    me.logger = log4js.getLogger('app');

    // Handle to repo base dir and repo base Url
    this.repoRootDir = repoRootDir;
    this.repoRootDirUrl = repoRootDirUrl;

    // This is a function that converts a node ID to a full file path
    this.convertIdToFile = function (nodeId, repoBasePath) {
        me.logger.debug('Converting NodeID to file path');

        // Set the start path equal to the repo base path
        var nodeFilePath = repoBasePath;
        me.logger.debug('Starting with ' + nodeFilePath);
        me.logger.debug('And NodeID ' + nodeId);

        // First split the node ID up by the delimeter, which is a period
        if (nodeId) {
            var nodeIDPathSegments = nodeId.split('.');
            // Now loop over the segments
            for (var i = 0; i < nodeIDPathSegments.length; i++) {
                me.logger.debug('Working with segment ' + nodeIDPathSegments[i]);
                if (fs.existsSync(nodeFilePath)) {
                    // Since the previous path exists, we will add a path separator and
                    // the next segment because it is most likely a directory
                    nodeFilePath += path.sep + nodeIDPathSegments[i];
                } else {
                    // Since the file does not exist, it is probably an incomplete file name
                    // based on the fact that we split it by '.', so reconstruct with a '.'
                    // instead of a path separator
                    nodeFilePath += '.' + nodeIDPathSegments[i];
                }
            }
        }

        // Return the path
        return nodeFilePath;
    }

    // A function to convert a file to a node ID
    this.convertFileToId = function (fullFilePath, viewRootPath) {
        // Remove the base path first
        var relativeFilePath = fullFilePath.substring(viewRootPath.length + 1);

        // Replace '/' with '.'
        var regexp = /\//g;
        var dotPath = relativeFilePath.replace(regexp, '.');

        // Remove leading '.'
        if (dotPath.startsWith('.')) dotPath = dotPath.substring(1);

        // Now return it
        return dotPath;
    }

    // This is the method that handles the request for data sources at a certain
    // node location in a repository with the given repo base path
    this.handleGetDataSource = function (req, res) {
        me.logger.debug('Repo request received:');
        me.logger.debug(req.query);

        // The array of DataSources to return
        var dataSourceArray = [];

        // We need to first build the local file path to the view's root repo directory
        // by starting with the entire root repo directory
        var viewRootPath = repoRootDir;

        // If there is a repoBasePath param, parse it an add it to the file path and URL
        if (req.query.repoBasePath && req.query.repoBasePath.length > 0) {
            me.logger.debug('repoBasePath=' + req.query.repoBasePath);

            // Make a copy of the param
            var repoBasePathParam = req.query.repoBasePath;

            // Remove any whitespace
            repoBasePathParam = repoBasePathParam.trim();

            // Remove any starting or trailing slashes
            if (repoBasePathParam.startsWith('/')) repoBasePathParam = repoBasePathParam.substring(1);
            if (repoBasePathParam.endsWith('/')) repoBasePathParam = repoBasePathParam.substring(0, repoBasePathParam.length - 1);
            me.logger.debug('repoBasePathParam=' + repoBasePathParam);

            // Now add to the local base path to get the local repo path
            viewRootPath += path.sep + repoBasePathParam;

        }
        me.logger.debug('viewRootPath=' + viewRootPath);

        // Grab the node ID parameter
        var nodeId = req.query.node;
        me.logger.debug("Node: " + nodeId);

        // Now use that to grab a pointer to the local file
        var nodeFilePath = me.convertIdToFile(nodeId, viewRootPath);
        me.logger.debug('nodeFilePath=' + nodeFilePath);

        // Now see if the file path exists and if it is a directory
        if (fs.existsSync(nodeFilePath) && fs.statSync(nodeFilePath).isDirectory()) {
            // Grab a listing of the files
            var dirList = fs.readdirSync(nodeFilePath);
            dirList.sort();

            // Now create two arrays, one for files and one for directories so we can list directories first
            var directories = [];
            var files = [];
            for (var j = 0; j < dirList.length; j++) {
                // Create the full path to the item
                var fullItemPath = nodeFilePath + path.sep + dirList[j];

                // Now make sure it exists
                if (fs.existsSync(fullItemPath)) {
                    // Now check to see if it's a directory
                    if (fs.statSync(fullItemPath).isDirectory()) {
                        directories.push(fullItemPath);
                    } else {
                        files.push(fullItemPath);
                    }
                } else {
                    me.logger.warn('I should have an item at path: ' + fullItemPath + ', but it does not seem to exist');
                }
            }

            // Now add the directories to the response
            for (var k = 0; k < directories.length; k++) {
                // Grab the full item path
                var fullDirPath = directories[k];

                // Grab the stats
                let stats = fs.statSync(fullDirPath);

                // Convert the directory to a node ID
                let nodeID = me.convertFileToId(fullDirPath, viewRootPath);

                // Create the URL
                var fullDirUrl = me.repoRootDirUrl + '/' + fullDirPath.substring(me.repoRootDir.length + 1);

                // Now add the item to the reponse object
                dataSourceArray.push(
                    {
                        "id": nodeID,
                        "name": path.basename(fullDirPath),
                        "leaf": false,
                        "url": fullDirUrl,
                        "sizeInBytes": stats.size
                    }
                )
            }

            // Now add the files to the response
            for (var l = 0; l < files.length; l++) {
                // Grab the full item path
                var fullFilePath = files[l];

                // Grab the stats
                let stats = fs.statSync(fullFilePath);

                // Convert the directory to a node ID
                let nodeID = me.convertFileToId(fullFilePath, viewRootPath);

                // Create the URL
                var fullFileUrl = me.repoRootDirUrl + '/' + fullFilePath.substring(me.repoRootDir.length + 1);

                // Grab the modified time
                var modDate = stats.mtime;

                // Now add the item to the reponse object
                dataSourceArray.push(
                    {
                        "id": nodeID,
                        "name": path.basename(fullFileUrl),
                        "leaf": true,
                        "type": "file",
                        "endDate": modDate.toUTCString(),
                        "url": fullFileUrl,
                        "mimeType": mime.lookup(fullFileUrl),
                        "sizeInBytes": stats.size
                    }
                )
            }
        }

        // Send the response
        res.send(JSON.stringify(dataSourceArray));
    };
}

// The factory method for constructing the RepoRouter
exports.createRepoRouter = function (repoRootDir, repoRootDirUrl) {
    // Create the new RepoRouter
    return new RepoRouter(repoRootDir, repoRootDirUrl);
};