// Import the logging module
var log4js = require('log4js');

/*
 * This class manages all the routes that are related to the User objects.  It needs
 * an instance of the User class to interact with the data store.
 */
function UserRouter(userModel) {

    // Grab a reference to this object for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');

    // The User model
    this.User = userModel;

    // A method to retrieve the User model
    this.getUser = function () {
        return me.User;
    };

    // This is the method that is used to retrieve users
    this.find = function (req, res) {
        // Check to see if this is a request for a single instance
        if (req.params.id) {
            me.User.findById(req.params.id, function (err, doc) {
                res.json(doc);
            });
        } else {
            // Look for login type and login ID
            if (req.params.loginType && req.params.loginID) {
                // Check which login type it is
                if (req.params.loginType === 'google') {
                    me.User.findOne({'google.identifier': req.params.loginID }, function (err, user) {
                        me.logger.debug("User that was found: " + user);
                        res.json(user);
                    });

                } else if (req.params.loginType === 'openid') {
                    me.User.findOne({'openid.identifier': req.params.loginID}, function (err, user) {
                        me.logger.debug("User that was found: " + user);
                        res.json(user);
                    });
                }
            } else {
                // Check to see if the current (i.e. session user is specified)
                if (req.query.current) {
                    me.logger.debug("findCurrent->request= ", req);
                    me.logger.debug("User from request is ", req.user);
                    res.json(req.user);
                } else {
                    // Check to see if the session properties were requested
                    if (req.query.sessionProperties) {
                        // Grab the current session ODSS properties
                        res.json(req.session.odssProperties);
                    } else {
                        // Return all the users
                        me.User.find({}, function (err, doc) {
                            res.json(doc);
                        });
                    }
                }
            }
        }

    };

    // The method to insert a new user
    this.insert = function (req, res) {
        // Remove any IDs
        delete req.body._id;

        // Now create a new user from the body of the request
        var newUser = new me.User(req.body);

        // Save it
        newUser.save(function (err, savedUser) {
            if (err) {
                // TODO kgomes send an error response
                me.logger.error(err);
            } else {
                // Since we were successful, set the proper response
                res.send(201,
                    {
                        success: true,
                        data: [
                            savedUser
                        ]
                    }
                );
            }
        });
    };

    // The method to update an existing user
    this.update = function (req, res) {

    };

    // The method to remove a user
    this.delete = function (req, res) {

    };
}

// The factory method for constructing the UserRouter
exports.createUserRouter = function (userModel) {
    // Create the new UserRouter
    return new UserRouter(userModel);
};