/**
 * This class provides all the routes to respond to requests relative to views
 */

// Import the logging library
var log4js = require('log4js');

// The view router object
function ViewRouter(viewModel) {

    // Grab reference to this object for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');

    // Grab the View model and services
    this.View = viewModel;

    // This is the method to find views
    this.find = function (req, res, next) {
        // Check to see if this is a request for a single instance
        if (req.params.id) {
            me.View.findById(req.params.id, function (err, view) {
                if (err) {
                    // TODO kgomes do something
                } else {
                    res.json(view);
                }
            });
        } else if (req.query.name) {
            me.View.findOne({name: req.query.name}, function (err, view) {
                if (err) {
                    return next(err)
                } else if (view) {
                    res.json(view);
                } else {
                    res.status(404).json({error: "no such view", name: req.query.name});
                }
            });
        } else {
            // Check to see if the call only wanted names
            if (req.query.namesOnly && req.query.namesOnly === 'true') {
                me.View.find({}, 'name', function (err, viewNameArray) {
                    if (err) {
                        // TODO kgomes do something
                    } else {
                        // Return all the views sorted by name
                        viewNameArray.sort(function (a, b) {
                            var keyA = a.name.toLowerCase(),
                                keyB = b.name.toLowerCase();
                            // Compare the 2 names
                            if (keyA < keyB) return -1;
                            if (keyA > keyB) return 1;
                            return 0;
                        });
                        res.json(viewNameArray);
                    }
                });
            } else {
                me.View.find({}, function (err, viewArray) {
                    if (err) {
                        // TODO kgomes do something
                    } else {
                        // Return all the views sorted by name
                        viewArray.sort(function (a, b) {
                            var keyA = a.name.toLowerCase(),
                                keyB = b.name.toLowerCase();
                            // Compare the 2 names
                            if (keyA < keyB) return -1;
                            if (keyA > keyB) return 1;
                            return 0;
                        });
                        // Now send it
                        res.json(viewArray);
                    }
                });
            }
        }

    };

    // Now the method to create a new view
    this.insert = function (req, res) {
        // Remove any id's in case
        delete req.body._id;

        // Now create the new view using the body of the request
        var newView = new me.View(req.body);

        // Save it
        newView.save(function (err, savedView) {
            if (err) {
                // TODO kgomes send an error response
                me.logger.error(err);
            } else {
                // Since we were successful, set the proper response
                res.send(201,
                    {
                        success: true,
                        data: [
                            savedView
                        ]
                    }
                );
            }
        });
    };

    // This is the method to update a view
    this.update = function (req, res) {

    };

    // And the method to remove a view
    this.delete = function (req, res) {

    };
}

// The factory method for constructing the ViewRouter
exports.createViewRouter = function (viewModel) {
    // Create the new ViewRouter
    return new ViewRouter(viewModel);
};
