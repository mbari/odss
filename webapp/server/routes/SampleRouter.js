/**
 * This class provides all the routes to respond to requests relative to samples
 */

// Import the logging module
var log4js = require('log4js');

// The sample router object
function SampleRouter(sampleModel) {

    // Grab reference to this object for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');

    // The valid list of query params
    this.validQueryParams = [
        'project', 'cruise', 'platform', 'organization', 'contactName', 'contactEmail', 'cast', 'stationName', 'owner',
        'bottle', 'depth', 'analysis', 'plannedStartDate', 'plannedEndDate', 'acquired', 'actualStartDate', 'actualEndDate',
        'location', 'howPreserved', 'whereStored', 'whereDataResides', 'modEpochSeconds', 'modByName', 'modByEmail', 'views'
    ];

    // This will be the model that will be used in the router
    this.Sample = sampleModel;

    // This method handles find requests
    this.find = function (req, res) {
        // Check to see if this is a request for a single instance
        if (req.params.id) {
            me.Sample.findById(req.params.id, function (err, sample) {
                if (err) {
                    // TODO kgomes do something
                } else {
                    // Check to see if the user wants the samples in CSV
                    if (req.query.asCSV && req.query.asCSV === 'true') {
                        me.sendSampleArrayAsCSV([sample], res);
                    } else {
                        res.json(sample);
                    }
                }
            });
        } else {
            // Check to see if the query contains the alternate primary keys of cruise, castNumber,
            // stationName, owner, bottle and analysis.  If it does, try to find the matching sample
            if (req.query.cruise && req.query.cruise !== '' &&
                req.query.cast && req.query.cast !== '' &&
                req.query.stationName && req.query.stationName !== '' &&
                req.query.owner && req.query.owner !== '' &&
                req.query.bottle && req.query.bottle !== '' &&
                req.query.analysis && req.query.analysis !== '') {

                // Run a query
                me.Sample.findOne({
                    'cruise': req.query.cruise,
                    'cast': req.query.cast,
                    'stationName': req.query.stationName,
                    'owner': req.query.owner,
                    'bottle': req.query.bottle,
                    'analysis': req.query.analysis
                }, function (err, sample) {
                    if (err) {
                        // TODO kgomes do something
                    } else {
                        // Check to see if the user wants the samples in CSV
                        if (req.query.asCSV && req.query.asCSV === 'true') {
                            me.sendSampleArrayAsCSV([sample], res);
                        } else {
                            res.json(sample);
                        }
                    }
                });
            } else {
                // Create an object that can be used for querying based on parameters
                var paramObject = {};

                // Loop over params and see if should be added to the query or not
                for (var param in req.query) {
                    if (me.validQueryParams.indexOf(param) >= 0) {
                        paramObject[param] = req.query[param];
                    }
                }

                // Return all the layers
                me.Sample.find(paramObject).sort('-actualStartDate').exec(function (err, samplesArray) {
                    if (err) {
                        // TODO kgomes do something
                    } else {
                        if (req.query.asCSV && req.query.asCSV === 'true') {
                            me.sendSampleArrayAsCSV(samplesArray, res);
                        } else {
                            res.json(samplesArray);
                        }
                    }
                });
            }
        }
    };

    // This is a helper method to send an array of samples to a response object in CSV format
    this.sendSampleArrayAsCSV = function (samplesArray, res) {
        // Set the content type
        res.set('Content-Type', 'text/plain');

        var csv = "ID, Parent Sample ID, Date (UTC), Project, Cruise, Platform, Station, Cast, " +
            "Bottle Number, How Preserved, Where Stored, Data Location, Longitude, Latitude, Depth, " +
            "Contact Name, Contact Organization, Contact Email\n";
        // Now iterate over the array to build the CSV format
        if (samplesArray && samplesArray.length > 0) {
            for (var i = 0; i < samplesArray.length; i++) {
                csv += samplesArray[i]._id + "," + samplesArray[i].parentSampleIDFK + ",";
                csv += samplesArray[i].actualStartDate.toISOString() + "," + samplesArray[i].project + ",";
                csv += samplesArray[i].cruise + "," + samplesArray[i].platform + ",";
                csv += samplesArray[i].stationName + "," + samplesArray[i].cast + ",";
                csv += samplesArray[i].bottle + "," + samplesArray[i].howPreserved + ",";
                csv += samplesArray[i].whereStored + "," + samplesArray[i].whereDataResides + ",";
                csv += samplesArray[i].location.longitude + "," + samplesArray[i].location.latitude + ",";
                csv += samplesArray[i].location.depth + "," + samplesArray[i].contactName + ",";
                csv += samplesArray[i].organization + "," + samplesArray[i].contactEmail;
                csv += "\n";
            }
        }
        res.send(csv);
    };

    // This method handles insert requests
    this.insert = function (req, res) {
        // Run the method to persist the sample (request body)
        me.Sample.persistSampleJSON(req.body, function (err, sample) {
            // Check for error
            if (err) {
                // Send the error message
                me.logger.error("Error trying to insert new sample", err);
                res.send('500', err);
            } else {
                me.logger.debug("After insert, sample to return", sample);
                // Since we were successful, set the proper response
                res.send(201,
                    {
                        success: true,
                        data: [
                            sample
                        ]
                    }
                );
            }
        });
    };

    // This method handles update requests
    this.update = function (req, res) {
        me.logger.debug("Going to update sample:", req.body);
        // Run the method to persist the sample (request body)
        me.Sample.persistSampleJSON(req.body, function (err, sample) {
            // Check for error
            if (err) {
                // Send the error message
                me.logger.error("Error trying to update a sample", err);
                res.send('500', err);
            } else {
                // Since we were successful, set the proper response
                res.send(200, sample);
            }
        });
    };

    // This method handles delete requests
    this.delete = function (req, res) {

    };
}

// The factory method for constructing the SampleRouter
exports.createSampleRouter = function (sampleModel) {
    // Create the new SampleRouter
    return new SampleRouter(sampleModel);
};