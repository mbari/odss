/**
 * This class provides a wrapper service for the Google Elevation API
 */

// Import the logging module
var log4js = require('log4js');

// Grab the HTTP request library
var request = require('request');

// The router object
function ElevationRouter() {
    // Grab reference to this object for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');

    // This is the method to find views
    this.find = function (req, res) {
        me.logger.debug("Elevation service called: ");
        me.logger.debug("Latitude = " + req.query.latitude);
        me.logger.debug("Longitude = " + req.query.longitude);

        // Build the response object
        var response = {
            "method": "GET"
        };

        // Make sure that a location is specified (latitude and longitude)
        if (typeof(req.query.latitude) != 'undefined' && typeof(req.query.longitude) !== 'undefined') {
            // Convert latitude and longitude to numbers
            var latNumber = Number(req.query.latitude);
            var lonNumber = Number(req.query.longitude);

            response.params = {
                "latitude": latNumber,
                "longitude": lonNumber
            };

            // OK, so a quick sanity check on the numbers
            if (latNumber <= 90 && latNumber >= -90) {
                if (lonNumber <= 180 && lonNumber >= -180) {

                    // OK, call the google service
                    request('https://maps.googleapis.com/maps/api/elevation/json?locations=' + latNumber + ',' +
                        lonNumber + '&key=AIzaSyCt1jXNPQYMKry2pnQqp6wRf9xw23ciqd8', function (error, resp, body) {
                        // Check for error
                        if (!error && resp.statusCode == 200) {
                            // Parse the body
                            var parsedBody = JSON.parse(body);

                            // Send back the data
                            response.data = {
                                "kind": "elevation",
                                "elevation": parsedBody.results[0].elevation,
                                "location": parsedBody.results[0].location,
                                "resolution":parsedBody.results[0].resolution
                            };
                            res.json(response);
                        } else {
                            response.error = {
                                "code": 500,
                                "message": "unknown error"
                            };
                            if (resp) {
                                response.error = {
                                    "code": resp.statusCode,
                                    "message": resp
                                };
                            }
                            res.json(response);
                        }
                    });
                } else {
                    response.error = {
                        "code": 400,
                        "message": "Longitude is outside of normal bounds (-180->180)"
                    };
                    res.json(response);
                }
            } else {
                response.error = {
                    "code": 400,
                    "message": "Latitude is outside of normal bounds (-90->90)"
                };
                res.json(response);
            }
        } else {
            me.logger.warn('No latitude or longitude specified');
            // Add an error message
            response.error = {
                "code": 400,
                "message": "Latitude and Longitude not specified as parameters (?longitude=xxxx&latitude=yyyyyy)"
            };
            res.json(response);
        }
    };
}

// The factory method for constructing the ElevationRouter
exports.createElevationRouter = function () {
    // Create the new ElevationRouter
    return new ElevationRouter();
};