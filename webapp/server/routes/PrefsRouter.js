/**
 * Provides the user-preferences related routes.
 */

// Import logging module
var log4js = require('log4js');


function PrefsRouter(PrefsModel) {

    // Grab reference to self for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');

    // gets the preferences of a given user
    me.find = function (req, res, next) {
        var uid = req.params.uid;
        PrefsModel.findOne({uid: uid}, function (err, userPrefs) {
            if (err) {
                next(err);
            } else {
                res.json(userPrefs);
            }
        });
    };

    // sets the preferences for a user
    me.insert = function (req, res, next) {
        var data = req.body;
        me.logger.debug("creating new preferences:", data);
        if (!data.uid) {
            var error = "uid required";
            res.status(400).json({error: error});
            return;
        }

        // TODO: validate uid (must exist in users collection)

        PrefsModel.create(data, function(err, created) {
            if (err) {
                return next(err);
            }
            else {
                res.send(201, {
                    success: true,
                    data: created
                });
            }
        });
    };

    // updates the preferences for a user
    me.update = function (req, res, next) {
        var uid = req.params.uid;
        var data = req.body;
        me.logger.debug("updating preferences: uid=", uid, data);
        PrefsModel.findOneAndUpdate({uid: uid}, data, function(err, updated) {
            if (err) {
                return next(err);
            }
            else {
                res.send(updated);
            }
        });
    };

    // deletes the preferences for a user
    me.delete = function (req, res, next) {
        var uid = req.params.uid;
        me.logger.debug("deleting preferences: uid=", uid);
        PrefsModel.findOneAndRemove({uid: uid}, function(err) {
            if (err) {
                return next(err);
            }
            else {
                res.json({prefsDeleted: uid});
            }
        });
    };
}

exports.createPrefsRouter = function (prefsModel) {
    return new PrefsRouter(prefsModel);
};
