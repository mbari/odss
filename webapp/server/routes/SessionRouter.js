// Import the logging library
var log4js = require("log4js");

/*
 * This class manages all the routes that are related to user sessions and their properties.
 */
function SessionRouter(esriApiKey) {
    // Grab a reference to this object for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger("app");

    // Grab the ESRI API Key to the web client can get it
    me.esriApiKey = esriApiKey;

    // This is the method to get the current user's session properties
    this.find = function (req, res) {
        if (req.query.sessionProperties) {
            // Add the API Key to the session properties
            req.session.odssProperties.esriApiKey = me.esriApiKey;
            // Grab and return the current session ODSS properties
            res.json(req.session.odssProperties);
        } else {
            res.json({
                esriApiKey: me.esriApiKey,
            });
        }
    };

    // The method to insert a new session property
    this.insert = function (req, res) {
        // This
    };

    // The method to update an existing property
    this.update = function (req, res) {};

    // The method to remove a property
    this.delete = function (req, res) {};
}

// The factory method for constructing the SessionRouter
exports.createSessionRouter = function (esriApiKey) {
    // Create the new SessionRouter
    return new SessionRouter(esriApiKey);
};
