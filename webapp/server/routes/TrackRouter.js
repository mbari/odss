/**
 * This Object provides all the routes to respond to requests for track geometry
 */

// Import logging module
var log4js = require('log4js');

// The Moment library
var moment = require('moment');

// The view router object
function TrackRouter(datastore) {

    // Grab a reference for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('TrackRouter');

    // Grab the DataStore
    this.datastore = datastore;

    // The find method
    this.find = function (req, res) {
        me.logger.debug("Call to find tracks");
        // The response object
        var responseObject = {
            status: 'success'
        };

        // Call the method to validate the parameters
        me.validateParams(req, function (err, result) {
            // Check for errors in parameters and if so, return the errors. Otherwise, try to get the track
            if (result.status === 'fail') {
                // Attach errors
                responseObject.message = "Call failed, see errors";
                responseObject.errors = result.errors;
                res.status(500).json(responseObject);
            } else {
                // Make the call to get the track
                me.datastore.getTrack(result.parameters.platformID, result.parameters.startDate,
                    result.parameters.endDate, result.parameters.lastNumberOfFixes, result.parameters.returnSRS,
                    result.parameters.ulCorner, result.parameters.lrCorner, result.parameters.sortOrder,
                    result.parameters.returnFormat, result.parameters.numberOfPoints, result.parameters.taperAccuracy,
                    function (errLocal, track) {
                        // Check for an error
                        if (errLocal) {
                            responseObject.status = 'fail';
                            responseObject.messsage = errLocal.message;
                            res.status(500).json(responseObject);
                        } else {
                            // What we do here depends on the return format requested
                            if (result.parameters.returnFormat === 'csv' || result.parameters.returnFormat === 'CSV') {
                                res.set('Content-Type', 'text/csv');
                                res.send(track.data);
                            } else if (result.parameters.returnFormat === 'html' || result.parameters.returnFormat === 'HTML') {
                                res.set('Content-Type', 'text/html');
                                res.send(track.data);
                            } else if (result.parameters.returnFormat === 'kml' || result.parameters.returnFormat === 'KML' ||
                                result.parameters.returnFormat === 'kmltrack' || result.parameters.returnFormat === 'KMLTrack') {
                                res.set('Content-Type', 'text/xml');
                                res.send(track.data);
                            } else {
                                res.set('Content-Type', 'application/json');
                                // Add the track as data to the return message
                                res.json(track);
                            }
                        }
                    });
            }
        });
    };

    this.insert = function (req, res) {
        me.logger.debug("Insert called, but not supported");
        res.send(200);
    };

    // This is the method to update a platform
    this.update = function (req, res) {
        me.logger.debug("Update called, but not supported");
        res.send(200);
    };

    this.delete = function (req, res) {
        me.logger.debug("Delete called, but not supported");
        res.send(200);
    };

    // This is a helper function to parse and validate all the incoming parameters
    this.validateParams = function (req, callback) {

        // The message object that will be returned
        var returnMessage = {
            status: 'success'
        };

        // The array to hold any errors
        var errors = [];

        // Grab all the parameters from the query
        var parameters = {
            platformID: req.query.platformID,
            startDate: req.query.startDate,
            endDate: req.query.endDate,
            lastNumberOfFixes: req.query.lastNumberOfFixes,
            returnSRS: req.query.returnSRS,
            ulCorner: req.query.ulCorner,
            lrCorner: req.query.lrCorner,
            sortOrder: req.query.sortOrder,
            returnFormat: req.query.returnFormat,
            numberOfPoints: req.query.numberOfPoints,
            taperAccuracy: req.query.taperAccuracy
        };

        // Check for a platformID
        if (typeof parameters.platformID === 'undefined' || parameters.platformID === null ||
            parameters.platformID === '') {
            // Mark as failed
            returnMessage.status = 'fail';
            // Add error message
            errors.push('No platformID specified');
        }

        // If start date is specified, convert it to a date
        if (typeof parameters.startDate !== 'undefined' && parameters.startDate !== null &&
            parameters.startDate !== '') {
            // Do the conversion
            parameters.startDate = moment(parameters.startDate);

            // Check to see if the conversion was valid
            if (!parameters.startDate.isValid()) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add error
                errors.push('Start date could not be parsed correctly');
            }
        }

        // If end date is specified, convert it to a date
        if (typeof parameters.endDate !== 'undefined' && parameters.endDate !== null &&
            parameters.endDate !== '') {
            // Do the conversion
            parameters.endDate = moment(parameters.endDate);
            // Check to see if the conversion was valid
            if (!parameters.endDate.isValid()) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add error
                errors.push('End date could not be parsed correctly');
            }
        }

        // If both start and end dates are specified, make sure start is before end
        if (parameters.startDate && parameters.endDate && parameters.startDate.isValid() &&
            parameters.endDate.isValid()) {
            if (parameters.endDate.isBefore(parameters.startDate)) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add error
                errors.push('End date is before start date');
            }
        }

        // If the lastNumberOfFixes is specified, convert it to a number
        if (typeof parameters.lastNumberOfFixes !== 'undefined' && parameters.lastNumberOfFixes !== null &&
            parameters.lastNumberOfFixes !== '') {
            // Try to convert it
            parameters.lastNumberOfFixes = parseInt(parameters.lastNumberOfFixes);

            // Check the result
            if (isNaN(parameters.lastNumberOfFixes)) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add error
                errors.push('Could not convert parameter lastNumberOfFixes to an integer');
            }
        }

        // Now let's check the requested return SRS and set default if none defined
        if (typeof parameters.returnSRS === 'undefined' || parameters.returnSRS === null ||
            parameters.returnSRS === '') {
            // None was specified, so set it too google
            parameters.returnSRS = 3857;
        } else {
            // OK, one was specified, let's try to convert it to a number
            // First look for EPSG designation in case the user put that in front
            if (parameters.returnSRS.indexOf('EPSG:') === 0 || parameters.returnSRS.indexOf('epsg:') === 0) {
                parameters.returnSRS = Number(parameters.returnSRS.substring(5));
            } else {
                parameters.returnSRS = Number(parameters.returnSRS);
            }

            // Make sure it is a number
            if (isNaN(parameters.returnSRS)) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The SRS specified ' + req.query.returnSRS + ' could not be converted to a number');
            }
        }

        // Now let's try to deal with the upper left geospatial corner
        if (typeof parameters.ulCorner !== 'undefined' && parameters.ulCorner !== null &&
            parameters.ulCorner !== '') {
            // Check to see if there is a comma in the string
            if (parameters.ulCorner.indexOf(',') < 0) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The string to specify the upper left of the search box did not have a comma in it, ' +
                    'it should be of the form longitude,latitude');
            }
            // Split the string by a comma
            var ulLonLatArray = parameters.ulCorner.split(',');

            // Make sure there are two elements
            if (ulLonLatArray.length !== 2) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The string to specify the upper left of the search box was found to have ' +
                    ulLonLatArray.length + ' term(s) in it and there should be 2, longitude,latitude');
            }

            // TODO kgomes since JavaScript will happily convert strings to numbers up to non-numeric digits,
            // TODO we need to check to make sure these strings only match numberic formats.

            // Parse the strings to floats
            var ulLongitude = parseFloat(ulLonLatArray[0]);
            parameters.ulCornerLongitude = ulLongitude;
            var ulLatitude = parseFloat(ulLonLatArray[1]);
            parameters.ulCornerLatitude = ulLatitude;

            // Verify they are numbers
            if (isNaN(ulLongitude)) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The longitude for the upper left corner did not parse to a number');
            }
            if (isNaN(ulLatitude)) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The latitude for the upper left corner did not parse to a number');
            }
        }

        // And same for lower right corner
        if (typeof parameters.lrCorner !== 'undefined' && parameters.lrCorner !== null &&
            parameters.lrCorner !== '') {
            // Check to see if there is a comma in the string
            if (parameters.lrCorner.indexOf(',') < 0) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The string to specify the lower right of the search box did not have a comma in it, ' +
                    'it should be of the form longitude,latitude');
            }
            // Split the string by a comma
            var lrLonLatArray = parameters.lrCorner.split(',');

            // Make sure there are two elements
            if (lrLonLatArray.length !== 2) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The string to specify the lower right of the search box was found to have ' +
                    lrLonLatArray.length + ' term(s) in it and there should be 2, longitude,latitude');
            }

            // TODO kgomes since JavaScript will happily convert strings to numbers up to non-numeric digits,
            // TODO we need to check to make sure these strings only match numeric formats.

            // Parse the strings to floats
            var lrLongitude = parseFloat(lrLonLatArray[0]);
            parameters.lrCornerLongitude = lrLongitude;
            var lrLatitude = parseFloat(lrLonLatArray[1]);
            parameters.lrCornerLatitude = lrLatitude;

            // Verify they are numbers
            if (isNaN(lrLongitude)) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The longitude for the lower right corner did not parse to a number');
            }
            if (isNaN(lrLatitude)) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The latitude for the lower right corner did not parse to a number');
            }
        }

        // Look at the sort order and make sure if it exists it is 'ASC' or 'DESC'
        if (typeof parameters.sortOrder !== 'undefined' && parameters.sortOrder !== null && parameters.sortOrder !== '') {
            // Check to make sure it's acceptable
            if (parameters.sortOrder !== 'ASC' && parameters.sortOrder !== 'asc' &&
                parameters.sortOrder !== 'DESC' && parameters.sortOrder !== 'desc') {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The sortOrder was specified, but is not ASC or DESC');
            }
        }

        // Check return format (if specified) or choose 'json' as default
        if (typeof parameters.returnFormat !== 'undefined' && parameters.returnFormat !== null &&
            parameters.returnFormat !== '') {
            // Make sure it's a valid choice
            if (parameters.returnFormat !== 'html' && parameters.returnFormat !== 'HTML' &&
                parameters.returnFormat !== 'csv' && parameters.returnFormat !== 'CSV' &&
                parameters.returnFormat !== 'json' && parameters.returnFormat !== 'JSON' &&
                parameters.returnFormat !== 'kml' && parameters.returnFormat !== 'KML' &&
                parameters.returnFormat !== 'kmltrack' && parameters.returnFormat !== 'KMLTrack') {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The returnFormat was specified, but is not HTML, CSV, JSON, KML or KMLTrack');
            }
        } else {
            // Since nothing was specified, choose 'json'
            parameters.returnFormat = 'json';
        }

        // If number of points is specified, make sure we can convert to a number
        if (typeof parameters.numberOfPoints !== 'undefined' && parameters.numberOfPoints !== null &&
            parameters.numberOfPoints !== '') {
            // TODO since JavaScript will happily convert strings to numbers up to non-numeric digits,
            // TODO we need to check to make sure these strings only match numeric formats.

            // Try to convert to integer
            var numberOfPointsInteger = parseInt(parameters.numberOfPoints);

            // Check to see how that worked out
            if (isNaN(numberOfPointsInteger)) {
                // Mark as failed
                returnMessage.status = 'fail';
                // Add the error
                errors.push('The numberOfPoints specified ' + parameters.numberOfPoints +
                    ' could not be converted to an integer');
            } else {
                parameters.numberOfPoints = numberOfPointsInteger;
            }
        }

        // If taperAccuracy is chosen, make sure we can figure out if it true of false
        if (typeof parameters.taperAccuracy !== 'undefined' && parameters.taperAccuracy !== null &&
            parameters.taperAccuracy !== '') {
            if (parameters.taperAccuracy === 'true' || parameters.taperAccuracy === 'TRUE') {
                parameters.taperAccuracy = true;
            } else {
                parameters.taperAccuracy = false;
            }
        } else {
            // Choose a default of false
            parameters.taperAccuracy = false;
        }

        // Attach any errors
        returnMessage.errors = errors;

        // Attach the parameters to the response
        returnMessage.parameters = parameters;

        // Now callback
        if (callback) {
            callback(null, returnMessage);
        }
    };
}

// The factory method for constructing the TrackRouter
exports.createTrackRouter = function (datastore) {
    // Create the new TrackRouter
    return new TrackRouter(datastore);
};