/**
 * This class provides all the routes to respond to requests relative to layers
 */

// Import the logging module
var log4js = require('log4js');

// The layer router object
function LayerRouter(layerModel) {

    // Grab reference to self for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');

    // Grab the Layer model
    var Layer = layerModel;

    // The function to find layers
    this.find = function (req, res) {
        // Check to see if this is a request for a single instance
        if (req.params.id) {
            Layer.findById(req.params.id, function (err, layer) {
                if (err) {
                    // TODO kgomes do something
                } else {
                    res.json(layer);
                }
            });
        } else {
            // Check to see if a view name is specified
            if (req.query.viewName && req.query.viewName !== '') {
                Layer.find({'views.name': req.query.viewName}, function (err, layersArray) {
                    if (err) {
                        // TODO kgomes do something
                    } else {
                        // Check to see if the result is to be in a tree format
                        if (req.query.treeView && req.query.treeView === 'true') {
                            res.json(me.convertLayersToTreeView(layersArray, req.query.viewName, req.query.groupByFolder));
                        } else {
                            res.json(layersArray);
                        }
                    }
                });
            } else {
                // Return all the layers
                Layer.find({}, function (err, layers) {
                    if (err) {
                        // TODO kgomes do something
                    } else {
                        if (req.query.treeView) {
                            res.json(me.convertLayersToTreeView(layers, null, null));
                        } else {
                            // Sort them by layer name
                            layers.sort(function (a, b) {
                                var keyA = a.name.toLowerCase(),
                                    keyB = b.name.toLowerCase();
                                // Compare the 2 names
                                if (keyA < keyB) return -1;
                                if (keyA > keyB) return 1;
                                return 0;
                            });

                            // Now send them back to the caller
                            res.json(layers);
                        }
                    }
                });
            }
        }
    };

    // This method handles insert requests
    this.insert = function (req, res) {

        // Run the method to persist the layer (request body)
        Layer.persistLayerJSON(req.body, function (err, layer) {
            // Check for error
            if (err) {
                // Send the error message
                me.logger.error("Error trying to insert new layer", err);
                res.send('500', err);
            } else {
                me.logger.debug("After insert, layer to return", layer);
                // Since we were successful, set the proper response
                res.send(201,
                    {
                        success: true,
                        data: [
                            layer
                        ]
                    }
                );
            }
        });
    };

    // This method handles update requests
    this.update = function (req, res) {
        // The first thing to do is check to see if this is a call to add or
        // remove a view name from the layer.
        if (req.query.addOrUpdateViewName && req.query.addOrUpdateViewName !== '') {
            me.logger.info("Trying to add view " + req.query.addOrUpdateViewName + " with folder " + req.query.folderName);
            if (req.query.folderName && req.query.folderName !== '') {
                Layer.addOrUpdateViewName(req.params.id, req.query.addOrUpdateViewName, req.query.folderName,
                    function (err, updatedLayer) {
                        if (err) {
                            me.logger.error("Error trying to add/update view name to/on a layer", err);
                            res.send('500', err);
                        } else {
                            me.logger.info("View added");
                            res.send(200, updatedLayer);
                        }
                    });
            }
        } else if (req.query.removeViewName && req.query.removeViewName !== '') {
            me.logger.info("Trying to remove view " + req.query.removeViewName);
            Layer.removeViewName(req.params.id, req.query.removeViewName, function (err, updatedLayer) {
                if (err) {
                    me.logger.error("Error trying to remove view name from layer:", err);
                    res.send('500', err);
                } else {
                    me.logger.info("View removed");
                    res.send(200, updatedLayer);
                }
            });
        } else {
            // Run the method to persist the layer (request body)
            Layer.persistLayerJSON(req.body, function (err, layer) {
                // Check for error
                if (err) {
                    // Send the error message
                    me.logger.error("Error trying to update a layer", err);
                    res.send('500', err);
                } else {
                    // Since we were successful, set the proper response
                    res.send(200, layer);
                }
            });
        }
    };

    // The delete method
    this.delete = function (req, res) {

    };
    /* *********************************
     * Some helper methods
     * ********************************* */

    // This is a method that will take an array of layers and organize them in a tree view
    this.convertLayersToTreeView = function (layersArray, viewName, groupByFolder) {

        // Sort the layers by name
        layersArray.sort(function (a, b) {
            var keyA = a.name.toLowerCase(),
                keyB = b.name.toLowerCase();
            // Compare the 2 names
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });

        // This is the tree object that will be returned
        var layersTree = {};

        // Create the root of the tree where the objects will go.
        layersTree.layers = [];

        // Check to see if the groupByFolder parameter is specified
        if (groupByFolder && groupByFolder === 'true') {
            // Loop over the sorted array of layers and process them one by one
            for (var i = 0; i < layersArray.length; i++) {
                // For the current layer, loop over all the views listed on the layer
                for (var j = 0; j < layersArray[i].views.length; j++) {
                    // Check to see if the view name for the current view matches
                    // what was specified coming in
                    if (layersArray[i].views[j].name === viewName) {
                        // OK, so we know we have the view object we are looking for,
                        // first make sure there is a folder specified along with the
                        // view name
                        if (layersArray[i].views[j].folder) {
                            // First check to see if the folder node has already been
                            // created from a previous layer
                            var layerAdded = false;
                            for (var k = 0; k < layersTree.layers.length; k++) {
                                if (layersTree.layers[k].name === layersArray[i].views[j].folder) {
                                    layersTree.layers[k].layers.push(layersArray[i]);
                                    layerAdded = true;
                                }
                            }
                            if (!layerAdded) {
                                layersTree.layers.push({
                                    text: layersArray[i].views[j].folder,
                                    name: layersArray[i].views[j].folder,
                                    expanded: true,
                                    layers: [layersArray[i]]
                                });
                            }
                        }
                    }
                }
            }
        } else {
            // Now add them to the array
            for (var layerIndex = 0; layerIndex < layersArray.length; layerIndex++) {
                layersTree.layers.push(layersArray[layerIndex]);
            }
        }

        // Return the results
        return layersTree;
    };
}

// The factory method for constructing the LayerRouter
exports.createLayerRouter = function (layerModel) {
    // Create the new LayerRouter
    return new LayerRouter(layerModel);
};