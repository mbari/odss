/**
 * This Object provides all the routes to respond to requests relative to platforms
 */

// Import the logging module
var log4js = require('log4js');

// The view router object
function PlatformRouter(platformModel) {

    // Grab reference to self for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');

    // The Platform model
    var Platform = platformModel;

    // The find method
    this.find = function (req, res, next) {
        if (req.params.id) {
            //
            // Single or multiple instance cases. (Multiple instance case
            // initially introduced for platform timeline editor purposes.)
            //
            var ids = req.params.id.split(',');

            // Check to see if this is a request for a single instance
            if (ids.length === 1) {
                Platform.findById(req.params.id, function (err, doc) {
                    res.json(doc);
                });
            }
            else {
                // Multiple instance case.
                Platform.find({'_id': {'$in': ids}}, function (err, platforms) {
                    if (err) {
                        return next(err);
                    }
                    res.json(platforms);
                });
            }
        } else if (req.query.name) {
            // NOTE: assuming that platform names do not contain commas.
            // TODO: verify this is the case in general OR change logic here to
            // accept multiple names.
            var names = req.query.name.split(',');
            Platform.find({'name': {'$in': names}}, function (err, platforms) {
                if (err) {
                    return next(err);
                }
                res.json(platforms);
            });
        } else {
            // So this request is to grab more than one.  The first thing to do is see if all the
            // platforms are requested, or only those with a specific view
            if (req.query.viewName && req.query.viewName !== '') {
                // Get the platforms that are associated with the given view name
                Platform.find({'views': req.query.viewName}, function (err, platforms) {
                    if (err) {
                        // TODO kgomes, do something here
                    } else {
                        me.renderPlatformArray(platforms, req, res);
                    }
                });

            } else {
                // Get all the platforms
                Platform.find({}, function (err, platforms) {
                    if (err) {
                        // TODO kgomes, do something here
                    } else {
                        me.renderPlatformArray(platforms, req, res);
                    }
                });
            }
        }
    };

    this.insert = function (req, res) {
    };

    // This is the method to update a platform
    this.update = function (req, res) {
        me.logger.info("Update request for platform with ID " + req.params.id);

        // The first thing to do is check to see if this is a call to add or
        // remove a view name from the platform.
        if (req.query.addViewName && req.query.addViewName !== '') {
            me.logger.info("Trying to add view " + req.query.addViewName);
            Platform.addViewName(req.params.id, req.query.addViewName, function (err, updatedPlatform) {
                me.logger.info("View added");
            });
        } else if (req.query.removeViewName && req.query.removeViewName !== '') {
            me.logger.info("Trying to remove view " + req.query.removeViewName);
            Platform.removeViewName(req.params.id, req.query.removeViewName, function (err, updatedPlatform) {
                me.logger.info("View removed");
            });
        }
        // Since we were successful, set the proper response
        res.send(200);
    };

    this.delete = function (req, res) {
    };

    /* *********************************
     * Some helper methods
     * ********************************* */

    // This is a helper method that takes in an array of platforms and the original request from
    // the client, and then renders that array to the response object appropriately.
    this.renderPlatformArray = function (platformArray, req, res) {
        // The first thing to check for is to see if a tree view was requested
        if (req.query.treeView && req.query.treeView === 'true') {
            // Now we need to check to see if the user wanted them just as a flat tree
            // or if the tree is to be grouped by type.
            if (req.query.groupBy && req.query.groupBy === 'type') {
                res.json(this.convertToTreeAndGroupByType(platformArray));
            } else {
                res.json(this.convertToTree(platformArray));
            }
        } else {
            // This means just a sorted view of the array was requested
            platformArray.sort(function (a, b) {
                var keyA = a.name.toLowerCase(),
                    keyB = b.name.toLowerCase();
                // Compare the 2 names
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });

            res.json(platformArray);
        }
    };

    // This is a method that takes an array of platforms converts them to a tree representation
    this.convertToTree = function (platformArray) {
        // The first thing to do is create an empty object to return
        var platformTreeToReturn = {};

        // Add the platforms array that will be filled
        platformTreeToReturn.platforms = [];

        // Make sure we got something in the incoming array
        if (platformArray && platformArray.length > 0) {

            // Sort it by name first
            platformArray.sort(function (a, b) {
                var keyA = a.name.toLowerCase(),
                    keyB = b.name.toLowerCase();
                // Compare the 2 names
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });

            // Now loop over the platforms and push platforms in the correct group
            platformArray.forEach(function (platform) {
                platformTreeToReturn.platforms.push(platform);
            });
        }

        // Now return the tree
        return platformTreeToReturn;
    };


    // This is a method that takes an array of platforms and converts them to a tree
    // that is grouped by their types
    this.convertToTreeAndGroupByType = function (platformArray) {
        // Create the sort by name function
        var sortByNamePropertyFunction = function (a, b) {
            var keyA = a.name.toLowerCase(),
                keyB = b.name.toLowerCase();
            // Compare the 2 names
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        };

        // The first thing to do is create an empty object to return
        var platformTreeToReturn = {};

        // Add the platforms array that will be filled
        platformTreeToReturn.platforms = [];

        // Now make a pass over the platforms and grab the various group types
        var types = [];
        for (var p = 0; p < platformArray.length; p++) {
            if (types.indexOf(platformArray[p].typeName) === -1) {
                types.push(platformArray[p].typeName);
            }
        }
        // Sort it
        types.sort();

        // Now push them onto the platforms array
        for (var t = 0; t < types.length; t++) {
            platformTreeToReturn.platforms.push({
                'text': types[t] + 's',
                'name': types[t] + 's',
                'expanded': true,
                'platforms': []
            });
        }
        // Now loop over the platforms and push platforms in the correct group
        for (var i = 0; i < platformArray.length; i++) {
            for (var j = 0; j < platformTreeToReturn.platforms.length; j++) {
                if (platformTreeToReturn.platforms[j].name === platformArray[i].typeName + 's') {
                    platformTreeToReturn.platforms[j].platforms.push(platformArray[i]);
                    break;
                }
            }
        }

        // Now sort the platforms under the type groupings
        for (var pt = 0; pt < platformTreeToReturn.platforms.length; pt++) {
            platformTreeToReturn.platforms[pt].platforms.sort(sortByNamePropertyFunction);
        }

        // Now return the tree
        return platformTreeToReturn;
    };
}

// The factory method for constructing the PlatformRouter
exports.createPlatformRouter = function (platformModel) {
    // Create the new PlatformRouter
    return new PlatformRouter(platformModel);
};
