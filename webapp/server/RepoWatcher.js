/*
 This is a Node application that monitors the ODSS repository and then creates an RSS file in the root
 of that directory that details out a running log of the changed files
 */
// Read in the configuration options
var odssCfg = require('./config.js');

// Configure the logger to use a file appender
var log4js = require('log4js');
log4js.configure({
    appenders: {
        RepoWatcher: {
            type: "file",
            filename: './logs/RepoWatcher.log'
        },
        out: {
            type: 'stdout'
        }
    },
    categories: { 
        default: { 
            appenders: ["RepoWatcher", "out"], 
            level: "info" 
        } 
    }
});
var logger = log4js.getLogger('RepoWatcher');

// Grab the file library
var fs = require('fs');

// Require the chokidar library for watching the repository
var chokidar = require('chokidar');

// The Slack client library
var Slack = require('node-slackr');

// The slack client
var slack;

// The channel that slack message will go to
var slackChannel;

// The icon to use in Slack
var slackIconURL;

// This is an array that keeps track of the messages to send to Slack
var messagesToSend = [];

// Grab the command line arguments
var directoryToMonitor = process.argv[2];
var directoryBaseUrl = process.argv[3];
logger.info("Will monitor directory " + directoryToMonitor);

// Check for command line args
if (!directoryToMonitor || !directoryBaseUrl) {
    console.log("ERROR: You need the two command line arguments: 'directoryToMonitor' 'directoryBaseUrl'");
    process.exit();
}

// Check for configuration options
if (odssCfg.repoWatcherOptions) {
    // Check for logging level
    if (odssCfg.repoWatcherOptions.loggerLevel) {
        logger.setLevel(odssCfg.repoWatcherOptions.loggerLevel);
    }

    // Check if there is a Slack web hook URL
    if (odssCfg.repoWatcherOptions.slackWebHookURL) {
        slack = new Slack(odssCfg.repoWatcherOptions.slackWebHookURL);
        logger.info("Will send slack messages to WebHook " + odssCfg.repoWatcherOptions.slackWebHookURL);
    }

    // Check for channel
    if (odssCfg.repoWatcherOptions.slackChannel) {
        slackChannel = odssCfg.repoWatcherOptions.slackChannel;
        logger.info("And the channel will be " + slackChannel);
    }

    // Check for icon URL
    if (odssCfg.repoWatcherOptions.slackIconURL) {
        slackIconURL = odssCfg.repoWatcherOptions.slackIconURL;
        logger.info("And the icon URL will be " + slackIconURL);
    }
}

// The function to send a message to Slack
var sendToSlack = function (messageToSend) {
    logger.debug("Will send message:", messageToSend);
    // Send the message
    if (slack && slackChannel) {
        logger.debug("Sending ...");
        slack.notify(messageToSend, function (err, result) {
            if (err) {
                logger.error("Error trying to send to slack: ", err);
            } else {
                logger.debug("Send to slack looks OK", result);
            }
        });
    }
};

// Watch directory specified
chokidar.watch(directoryToMonitor, {
    ignored: [
        /[\/\\]\./,
        '**/Thumbs.db'
    ],
    usePolling: true,
    interval: 5000,
    ignoreInitial: true
}).on('all', function (event, filepath) {
    logger.debug(event, filepath);

    // Look for an error first
    if (event === 'error') {
        logger.error("Error in file watcher for file path " + filepath);
    } else {

        // If the event is an addDir or unlinkDir, we will skip
        if (event !== 'addDir' && event !== 'unlinkDir') {
            // Grab the current date
            var now = new Date;

            // Grab just the filename relative to the directory
            var linkUrl = encodeURI(directoryBaseUrl + filepath.substring(directoryToMonitor.length));

            // Check to see if this is a deletion which is a bit different than the others
            if (event === 'unlink') {
                // Create the message to send
                var messageToSend = {
                    text: '*File delete: ' +
                    filepath.substring(filepath.lastIndexOf('/') + 1) + '*\n' +
                    '_Path_: ' + filepath.substring(directoryToMonitor.length) + '\n' +
                    '_At_: ' + now.toLocaleDateString() + ' ' + now.toLocaleTimeString(),
                    channel: slackChannel,
                    username: "ODSS",
                    icon_url: slackIconURL
                };

                // Add it to the queue
                logger.debug("Message to send", messageToSend);

                messagesToSend.push(messageToSend);
            } else {

                // Get the stats of the file
                fs.stat(filepath, function (err, stats) {
                    logger.debug("Stats:", stats);

                    // Create the message to send
                    var messageToSend = {
                        text: '*File ' + event + ': ' +
                        filepath.substring(filepath.lastIndexOf('/') + 1) + '*\n' +
                        '_Path_: ' + filepath.substring(directoryToMonitor.length) + '\n' +
                        '_At_: ' + now.toLocaleDateString() + ' ' + now.toLocaleTimeString() +
                        '\n_Size_: ' + stats.size / 1000 + ' KBytes\n_Link_: ' + linkUrl,
                        channel: slackChannel,
                        username: "ODSS",
                        icon_url: slackIconURL
                    };

                    // Add it to the queue
                    logger.debug("Message to send", messageToSend);

                    messagesToSend.push(messageToSend);
                });
            }

        } else {
            logger.debug("Is a directory so we will ignore");
        }
    }
});

// This is the timer that will look at the queue of objects that are changes and write them to the RSS file
setInterval(function () {
    // Check the queue
    if (messagesToSend.length > 0) {
        var messageToSend = messagesToSend.shift();
        sendToSlack(messageToSend);
    }
}, 5000);
