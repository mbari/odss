/**
 * This is the main server that is the starting point for the ODSS node.js server
 * Author: Kevin Gomes
 * Date: August 26, 2013
 */

// Import the configuration file
var odssCfg = require('./config.js');

// Configure the logger
var log4js = require('log4js');
log4js.configure({
    appenders: {
        app: {
            type: "dateFile",
            filename: odssCfg.logFileLocation + '/app.log',
            daysToKeep: 30,
            layout: {
                type: 'pattern', pattern: '%d [%p] %f{1}:%l %m'
            }
        },
        out: {
            type: 'stdout',
            layout: {
                type: 'pattern', pattern: '%d %p %c %f{1}:%l %m'
            }
        }
    },
    categories: {
        default: {
            appenders: ["app", "out"],
            level: odssCfg.appOptions.loggerLevel,
            enableCallStack: true
        }
    }
});
var logger = log4js.getLogger('app');

// Log start up
logger.info("Starting up the ODSS Node.js server at base URL " + odssCfg.appServerOptions.hostBaseUrl);
logger.info("Will use MongoDB " + odssCfg.dataStoreOptions.mongodbName + " on server " + odssCfg.dataStoreOptions.mongodbHost);

// Create the object used to interface to the data stores
var datastore = require('./Datastore').createDatastore(odssCfg.dataStoreOptions);

// Now create the application server
require('./AppServer').createAppServer(odssCfg.repoRootDir, odssCfg.repoRootDirUrl, datastore, odssCfg.appServerOptions);

// Now start up an interval to extract the platforms from the tracking DB and update them in Mongo
setInterval(function () {
    var platformPersistHandler = function (err, platform) {
        if (err) {
            logger.error("Error caught trying to insert or update platform:", err);
        } else {
            logger.debug("Platform " + platform.name + " updated successfully");
        }
    };

    try {
        // Grab all the platforms from the tracking database
        datastore.getTrackingPlatforms(function (err, platformArray) {
            if (err) {
                logger.warn("Error trying to get platforms from tracking DB");
                logger.warn(err);
            } else {
                // OK, make sure we got something
                if (platformArray && platformArray.length > 0) {
                    logger.debug("Got " + platformArray.length + " platforms from the tracking DB.");
                    // Grab the Platform model from the data store
                    var Platform = datastore.getPlatform();
                    for (var i = 0; i < platformArray.length; i++) {
                        Platform.persistPlatformJSON(platformArray[i], platformPersistHandler);
                    }
                }
            }
        });
    } catch (error) {
        logger.error('Error caught trying to get platforms from tracking DB');
        logger.error(error);
    }
}, odssCfg.appOptions.trackingUpdateInterval);
