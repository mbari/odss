/*
 * Controller for tokens.
 *
 * @author Carlos Rueda
 */

// Module dependencies
var logger   = require('log4js').getLogger('tokens');
var _        = require('lodash');
var mongoose = require('mongoose');

// models used by this controller
var Token        = mongoose.model('Token');
var GeneralInfo  = mongoose.model('GeneralInfo');
var Platform     = mongoose.model('Platform');

/**
 * Exposed operations
 */
module.exports = {
    findAll:        findAll,
    create:         create,
    getById:        getById,
    updateById:     updateById,
    deleteById:     deleteById,
    getGeneralInfo: getGeneralInfo
};

/**
 * Gets all tokens or those satisfying conditions according to parameters.
 *
 * @method  findAll
 */
function findAll(req, res, next) {
    logger.debug("getting tokens: params=", req.params, "query=", req.query);

    // note: "start" field used to refer to the proper tokens in the collection.
    var query = {start: {'$exists': true}};

    if (req.query.platform_name) {
        // TODO: assuming that platform names do not contain commas.
        var platform_names = req.query.platform_name.split(',');
        query.platform_name = platform_names.length === 1 ? platform_names[0] : {'$in': platform_names};
    }

    logger.debug("getting tokens: query=", query);

    Token.find(query, function(err, docs) {
        if (err) {
            return next(err);
        }
        else {
            res.send(docs);
        }
    });
}

function isValidId(id) {
    // mongoose 3.8.14 has mongoose.Types.ObjectId.isValid, but I'm not sure
    // if that method is available in current ODSS's mongoose version. So,
    // do the following pre-check before calling that method
    if (mongoose.Types && mongoose.Types.ObjectId && mongoose.Types.ObjectId.isValid) {
        return mongoose.Types.ObjectId.isValid(id);
    }
    else {
        return /^[0-9a-fA-F]{24}$/.test(id);
    }
}

function invalidId(id, req, res, next) {
    if (!isValidId(id)) {
        res.status(404).json({error: "invalid id", id: id});
        next('invalid id="' +id+ '"');
        return true;
    }
    return false;
}

function getById(req, res, next) {
    var token_id = req.params.token_id;
    if (invalidId(token_id, req, res, next)) {
        return;
    }
    logger.debug("getting token by id:", token_id);
    Token.find({start: {'$exists' :true}, _id: token_id}, function(err, docs) {
        if (err) {
            return next(err);
        }
        else {
            res.send(docs);
        }
    });
}

/*
 Responds with something like:
    { "lastUpdated": {
         "on": "2014-08-21T21:58:47.392Z",
         "by": "<user-id>"
    }}
 based on the token with maximum "_updated.on" value, if any.
*/
function getGeneralInfo(req, res, next) {
    logger.debug("getting general info");
    var query = {'generalInfo': {'$exists': true}};
    GeneralInfo.findOne(query, function(err, docs) {
        if (err) {
            return next(err);
        }
        //logger.debug("getGeneralInfo: docs", docs);
        if (docs && docs.generalInfo) {
            var generalInfo = docs.generalInfo;
            res.json(generalInfo);
            return;
        }

        ////////////////////////////////////////////////////////////
        // initialize GeneralInfo with most recently updated token:
        Token.findOne({_updated: {"$exists": true}})
        .sort("-_updated.on")
        .exec(function(err, docs) {
            if (err) {
                return next(err);
            }
            if (!docs || !docs._updated) {
                res.status(404).json({error: "not available information"});
                return;
            }
            var lastUpdated = docs._updated;

            var obj = {
                generalInfo: {
                    lastUpdated: {
                        on: lastUpdated.on,
                        by: lastUpdated.by || ""
                    }
                }
            };
            logger.debug("initializing GeneralInfo with", obj);

            GeneralInfo.create(obj, function(err, created) {
                if (err) {
                    logger.debug("couldn't create GeneralInfo object", err);
                    return next(err);
                }
                else {
                    logger.debug("GeneralInfo created", created);
                    res.json(obj.generalInfo);
                }
            });
        });
    });
}

function updateGeneralInfo(updatedOn, updatedBy) {
    var generalInfo = {
        lastUpdated: {
            on: updatedOn,
            by: updatedBy || ""
        }
    };

    var query = {'generalInfo': {'$exists': true}};
    var obj   = {'generalInfo': generalInfo};
    GeneralInfo.update(query, obj, {'upsert': true}, function(err) {
        if (err) {
            logger.error('error updating GeneralInfo', err);
        }
        else {
            logger.debug('GeneralInfo updated', obj);
        }
    });
}

function setUpdated(data) {
    var updatedOn = new Date();
    if (data._updated) {
        data._updated.on = updatedOn;
    }
    else {
        data._updated = {on: updatedOn};
    }
}

/**
 * Creates a token.
 *
 * @method  create
 */
function create(req, res, next){
    var data = req.body;
    setUpdated(data);
    logger.debug("creating token:", data);
    Token.create(data, function(err, created) {
        if (err) {
            return next(err);
        }
        else {
            updateGeneralInfo(data._updated.on, data._updated.by);
            res.send(created);
        }
    });
}

/**
 * Updates a token.
 */
function updateById(req, res, next){
    var token_id = req.params.token_id;
    if (invalidId(token_id, req, res, next)) {
        return;
    }

    var data = req.body;
    setUpdated(data);

    logger.debug("updating token: token_id=", token_id, data);
    Token.findByIdAndUpdate(token_id, data, function(err, updated) {
        if (err) {
            return next(err);
        }
        else {
            updateGeneralInfo(data._updated.on, data._updated.by);
            res.send(updated);
        }
    });
}

/**
 * Deletes a token.
 */
function deleteById(req, res, next){
    var token_id = req.params.token_id;
    if (invalidId(token_id, req, res, next)) {
        return;
    }

    var updatedOn = new Date();
    var updatedBy = ""; // TODO

    logger.debug("deleting token: token_id=", token_id);
    Token.findByIdAndRemove(token_id, function(err) {
        if (err) {
            return next(err);
        }
        else {
            updateGeneralInfo(updatedOn, updatedBy);
            res.json({tokenDeleted: token_id});
        }
    });
}
