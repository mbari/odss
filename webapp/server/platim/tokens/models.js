/*
 * Models associated with tokens.
 *
 * @author Carlos Rueda
 */

/**
 * Module dependencies
 */

var mongoose  = require('mongoose');

/**
 * `Token` model.
 *
 * @class Token
 */
var Token = mongoose.model('Token', new mongoose.Schema({
    platform_id:  String,  // TODO remove this one
    platform_name:  String,
    start:        String,
    end:          String,
    state:        String,
    description:  String,
    ttype:        {type: String, default: "ttdeployment", enum: ['ttdeployment', 'ttmission'] },
    geometry:     {},
    _updated:     { on: Date, by: {type: String, default: "" } }
}, {
    collection: "timeline_tokens"
}));

var GeneralInfo = mongoose.model('GeneralInfo', new mongoose.Schema({
    generalInfo: {
        lastUpdated: { on: Date, by: {type: String, default: "" } }
    }
}, {
    collection: "timeline_tokens"
}));


/**
 * Expose model `Token`.
 */

module.exports = {
    Token:       Token,
    GeneralInfo: GeneralInfo
};
