/*
 * The Planning Editor supporting backend REST service.
 * This module provides the setup operation for this service.
 *
 * @author Carlos Rueda
 */

var logger    = require('log4js').getLogger("platim");

/**
 * Expose function `setup`.  See AppServer.js
 */
module.exports = {
    setup: setup
};


/**
 * Sets up the Planning Editor supporting backend REST service.
 * Should be called after the setup of mongoose and the Platform model.
 *
 * @param app          The express application object.
 * @param routePrefix  Prefix for the routes, by default '/'.
 */
function setup(app, routePrefix) {

    routePrefix = routePrefix || '/';

    logger.debug("ODSS Platim service setup: using routePrefix=", routePrefix);

    // models:
    require('./tokens/models');

    // controllers:
    var tokens   = require('./tokens');

    // setup routes:

    app.get( routePrefix + 'tokens',                   tokens.findAll);
    app.get( routePrefix + 'tokens/info',              tokens.getGeneralInfo);
    app.get( routePrefix + 'tokens/:token_id',         tokens.getById);
    app.post(routePrefix + 'tokens',                   tokens.create);
    app.put( routePrefix + 'tokens/:token_id',         tokens.updateById);
    app.del( routePrefix + 'tokens/:token_id',         tokens.deleteById);
}
