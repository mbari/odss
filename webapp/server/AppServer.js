/**
 * This class provides the application/web server functionality for the ODSS.
 * Author: Kevin Gomes
 * Date: August 26, 2013
 */

// First load any dependencies
var log4js = require("log4js");
var express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");
var cookieParser = require("cookie-parser");
var session = require("express-session");
var serveIndex = require("serve-index");
var serveStatic = require("serve-static");
var MongoStore = require("connect-mongo")(session);
var http = require("http");
var path = require("path");
var favicon = require("serve-favicon");

// This is the function that serves as the constructor for the Application Server
function AppServer(repoRootDir, repoRootDirUrl, datastore, opts) {
    // Grab the logger defined in app.js
    this.logger = log4js.getLogger("app");

    // Grab a reference to this object for callback
    var me = this;

    // Hold references to the data repo base directory and the base URL
    this.repoRootDir = repoRootDir;
    this.repoRootDirUrl = repoRootDirUrl;

    // Hold the reference to the data store
    this.datastore = datastore;

    // The reference to the web server
    this.webServer = null;

    // A method to get the handle to the web server
    this.getWebServer = function () {
        return this.webServer;
    };

    // Hold the base of the URL that the server will respond to
    this.hostBaseUrl = "http://localhost/";
    if (opts.hostBaseUrl) {
        this.hostBaseUrl = opts.hostBaseUrl;
    }

    // The port to listen to
    this.port = 3000;
    if (opts.port) {
        this.port = opts.port;
    }

    // Create the router to handle elevation requests
    this.elevationRouter =
        require("./routes/ElevationRouter").createElevationRouter();

    // Create the LayerRouter
    this.layerRouter = require("./routes/LayerRouter").createLayerRouter(
        this.datastore.getLayer()
    );

    // Create the PlatformRouter
    this.platformRouter =
        require("./routes/PlatformRouter").createPlatformRouter(
            this.datastore.getPlatform()
        );

    // Create the PrefsRouter
    this.prefsRouter = require("./routes/PrefsRouter").createPrefsRouter(
        this.datastore.getPrefs()
    );

    // Create the RepoRouter
    this.repoRouter = require("./routes/RepoRouter").createRepoRouter(
        repoRootDir,
        repoRootDirUrl
    );

    // Create the SessionRouter
    this.sessionRouter = require("./routes/SessionRouter").createSessionRouter(
        opts.esriApiKey
    );

    // Create the TrackRouter
    this.trackRouter = require("./routes/TrackRouter").createTrackRouter(
        this.datastore
    );

    // Create the ViewRouter
    this.viewRouter = require("./routes/ViewRouter").createViewRouter(
        this.datastore.getView()
    );

    // Create the Express web/application server
    this.app = express();

    // Assign the delegate to all routes
    this.app.use(cors());

    // Set the port that server is working with to that from the environment or just
    // use the default port of 3000
    this.app.set("port", process.env.PORT || this.port);

    // If the cookie parser secret is specified in the opts, set it
    this.app.use(cookieParser(opts.expressCookieParserSecret));

    // Enable sessions in Express server and tied to the MongoDB
    this.app.use(
        session({
            secret: opts.expressSessionSecret,
            resave: false,
            saveUninitialized: false,
            store: new MongoStore({
                mongooseConnection: me.datastore.getDB(),
            }),
        })
    );

    // Configure the request parser
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());

    // Define the static web content directory (client application)
    this.app.use(express.static(path.join(__dirname, "web")));
    this.app.use(
        favicon(
            path.join(
                __dirname,
                "web",
                "odss",
                "resources",
                "images",
                "MBARI_logo5in.png"
            )
        )
    );

    // Serve the files from the repo directory
    this.app.use("/odss/data", serveIndex(repoRootDir, { icons: true }));
    this.app.use("/odss/data", serveStatic(repoRootDir));

    // Session properties
    this.app.get("/odss/session", this.sessionRouter.find);

    // All the routes for user preferences
    this.app.get("/odss/prefs/:uid", this.prefsRouter.find);
    this.app.post("/odss/prefs", this.prefsRouter.insert);
    this.app.put("/odss/prefs/:uid", this.prefsRouter.update);
    this.app.delete("/odss/prefs/:uid", this.prefsRouter.delete);

    // All the routes for views
    this.app.get("/odss/views/:id", this.viewRouter.find);
    this.app.get("/odss/views", this.viewRouter.find);
    this.app.post("/odss/views", this.viewRouter.insert);
    this.app.put("/odss/views/:id", this.viewRouter.update);
    this.app.delete("/odss/views/:id", this.viewRouter.delete);

    // All the routes for platforms
    this.app.get("/odss/platforms/:id", this.platformRouter.find);
    this.app.get("/odss/platforms", this.platformRouter.find);
    this.app.post("/odss/platforms", this.platformRouter.insert);
    this.app.put("/odss/platforms/:id", this.platformRouter.update);
    this.app.delete("/odss/platforms/:id", this.platformRouter.delete);

    // All the routes for layers
    this.app.get("/odss/layers/:id", this.layerRouter.find);
    this.app.get("/odss/layers", this.layerRouter.find);
    this.app.post("/odss/layers", this.layerRouter.insert);
    this.app.put("/odss/layers/:id", this.layerRouter.update);
    this.app.delete("/odss/layers/:id", this.layerRouter.delete);

    // The elevation service routes
    this.app.get("/odss/elevation", this.elevationRouter.find);

    // All the routes for tracks
    this.app.get("/odss/tracks", this.trackRouter.find);

    // The route for DataSource requests
    this.app.get(
        "/odss/services/catalog/datasource",
        this.repoRouter.handleGetDataSource
    );

    // set up platform timeline editor service:
    require("./platim").setup(this.app, "/odss/platim/");

    // Fire up the application server
    this.webServer = http
        .createServer(this.app)
        .listen(this.app.get("port"), function () {
            me.logger.info(
                "Express server listening on port " + me.app.get("port")
            );
        });
}

// The factory method for constructing the server
exports.createAppServer = function (
    repoRootDir,
    repoRootDirUrl,
    datastore,
    opts
) {
    // Create the new AppServer
    return new AppServer(repoRootDir, repoRootDirUrl, datastore, opts);
};
