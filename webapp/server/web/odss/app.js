/*
 * File: app.js
 *
 * This is the main application loading file for the ODSS application.
 * It is the first file to be loaded in the browser.
 */
// @require @packageOverrides
Ext.Loader.setConfig({
    enabled: true
});

// Create the Ext application
Ext.application({

    requires: [
        'Odss.view.DataSourceTreeGrid',
        'Odss.view.LayerFormWindow',
        'Odss.view.LayerTreePanel',
        'Odss.view.LayerViewSelectionWindow',
        'Odss.view.LeafletBox',
        'Odss.view.MissionControlPanel',
        'Odss.view.OdssViewport',
        'Odss.view.PlatformsGridTreePanel',
        'Odss.view.PlatformViewSelectionWindow'
    ],
    models: [
        'DataSource',
        'Layer',
        'Platform',
        'SpaceTime',
        'View'
    ],
    stores: [
        'DataSourceStore',
        'LayerStore',
        'LayerTreeStore',
        'PlatformTreeStore',
        'PlatformTreeStoreFull',
        'TrackStore',
        'ViewStore'
    ],
    views: [
        'DataSourceTreeGrid',
        'LayerFormWindow',
        'LayerTreePanel',
        'LayerViewSelectionWindow',
        'LeafletBox',
        'MissionControlPanel',
        'OdssViewport',
        'PlatformsGridTreePanel',
        'PlatformViewSelectionWindow'
    ],
    controllers: [
        'AjaxController',
        'DataSourceController',
        'LayerController',
        'LayerViewSelectionWindowController',
        'LeafletBoxController',
        'PlatformController',
        'PlatformViewSelectionWindowController',
        'SpaceTimeController',
        'TrackController',
        'ViewController'
    ],
    name: 'Odss',

    // This is a template method that is called when the application boots. It is called before the 
    // Application's launch function is executed so gives a hook point to run any code before your 
    // Viewport is created
    init: function () {
        // Let's first load any information that is stored in the user's cookie for this application
        this.odssConfig = Ext.decode(Ext.util.Cookies.get('odss-config'));

        // Make sure we have some default configurations
        if (!this.odssConfig) {
            this.odssConfig = {};
        }

        // If no odssConfig was found, load a default
        if (!this.odssConfig['mostRecentLocationAndZoom']) {
            this.odssConfig['mostRecentLocationAndZoom'] = {
                'lat': 36.78407,
                'lon': -122.33933,
                'zoom': 10
            }
        }
        if (!this.odssConfig['homeLocationAndZoom']) {
            this.odssConfig['homeLocationAndZoom'] = {
                'lat': 36.78407,
                'lon': -122.33933,
                'zoom': 10
            }
        }
        if (!this.odssConfig['lastView']) {
            this.odssConfig['lastView'] = {
                'name': 'Public',
                'repoBasePath': 'public'
            }
        }
    },

    // This is the function that is called after the page has completely loaded
    launch: function () {
        Ext.create('Odss.view.OdssViewport');
        // Create a reference to the application itself
        Odss.app = this;

        // Initialize the Quicktips framework
        Ext.QuickTips.init();

        // Apply some configuration to the QuickTip manager
        Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
            maxWidth: 600,
            minWidth: 100
        });

    },

    // This method returns a JSON object that has the keys 'lat' and lon' that represent where the
    // user saved their 'home' location to.
    getMostRecentLocationAndZoom: function () {
        if (this.odssConfig && this.odssConfig['mostRecentLocationAndZoom']) {
            return this.odssConfig['mostRecentLocationAndZoom'];
        }
    },

    // This method sets the most recent location and zoom levels both in the application and in
    // a cookie
    setMostRecentLocationAndZoom: function (lat, lon, zoom) {
        // Update the current config object
        this.odssConfig['mostRecentLocationAndZoom']['lat'] = lat;
        this.odssConfig['mostRecentLocationAndZoom']['lon'] = lon;
        this.odssConfig['mostRecentLocationAndZoom']['zoom'] = zoom;

        // Now persist that to the cookie
        Ext.util.Cookies.set('odss-config', Ext.encode(this.odssConfig));
    },

    // This method returns a JSON object that has the keys 'lat' and lon' that represent where the
    // user saved their 'home' location to.
    getHomeLocationAndZoom: function () {
        if (this.odssConfig && this.odssConfig['homeLocationAndZoom']) {
            return this.odssConfig['homeLocationAndZoom'];
        }
    },

    // This method sets the user's home location and zoom levels both in the application and in
    // a cookie
    setHomeLocationAndZoom: function (lat, lon, zoom) {
        // Update the current config object
        this.odssConfig['homeLocationAndZoom']['lat'] = lat;
        this.odssConfig['homeLocationAndZoom']['lon'] = lon;
        this.odssConfig['homeLocationAndZoom']['zoom'] = zoom;

        // Now persist that to the cookie
        Ext.util.Cookies.set('odss-config', Ext.encode(this.odssConfig));
    },

    // This method returns a JSON object that has the name of the user's last view they were
    // using as well as the base path of the repo for the data pane
    getLastView: function () {
        if (this.odssConfig && this.odssConfig['lastView']) {
            return this.odssConfig['lastView'];
        }
    },

    // This method sets the user's most recent view name and repo base path both in
    // the application and in a cookie
    setLastView: function (name, repoBasePath) {
        // Set the name and repo path
        this.odssConfig['lastView']['name'] = name;
        this.odssConfig['lastView']['repoBasePath'] = repoBasePath;

        // Now persist that to the cookie
        Ext.util.Cookies.set('odss-config', Ext.encode(this.odssConfig));

        // Fire the event that the view was selected
        this.fireEvent("viewselected", this, name);
    },

    // This is a utility function for convert dates to an ISO formatted string
    convertDateToISO: function (dateToConvert) {
        // Define a function to pad numbers less than 10
        function pad(n) {
            return n < 10 ? '0' + n : n;
        }

        // Create the string form to return
        if (dateToConvert !== null) {
            var isoString = dateToConvert.getUTCFullYear();
            isoString += '-' + pad(dateToConvert.getUTCMonth() + 1);
            isoString += '-' + pad(dateToConvert.getUTCDate());
            isoString += 'T' + pad(dateToConvert.getUTCHours());
            isoString += ':' + pad(dateToConvert.getUTCMinutes());
            isoString += ':' + pad(dateToConvert.getUTCSeconds()) + 'Z';

            // Now return the date in ISO form
            return isoString;
        }
    }
});
