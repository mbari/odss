/*
 * File: app/model/SpaceTime.js
 *
 * This is the model used by the SpaceTimeController
 */

Ext.define('Odss.model.SpaceTime', {
    extend: 'Ext.data.Model',
    alias: 'model.spacetime',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            defaultValue: false,
            name: 'playbackMode',
            type: 'boolean'
        },
        {
            defaultValue: 'new Date((new Date()).getTime()-(1000*60*60*24))',
            name: 'timeWindowStartDatetime',
            persist: false,
            type: 'date'
        },
        {
            defaultValue: 'new Date()',
            name: 'timeWindowEndDatetime',
            persist: false
        }
    ],

    getStartDateInISO: function() {

        // Define a function to pad numbers less than 10
        function pad(n){
            return n < 10 ? '0' + n : n;
        }

        // Create the string form to return
        var isoString = this.timeWindowStartDatetime.getUTCFullYear();
        isoString += '-' + pad(this.timeWindowStartDatetime.getUTCMonth() + 1);
        isoString += '-' + pad(this.timeWindowStartDatetime.getUTCDate());
        isoString += 'T' + pad(this.timeWindowStartDatetime.getUTCHours());
        isoString += ':' + pad(this.timeWindowStartDatetime.getUTCMinutes());
        isoString += ':' + pad(this.timeWindowStartDatetime.getUTCSeconds()) + 'Z';

        // Now return the date in ISO form
        return isoString;

    },

    getEndDateInISO: function() {

        // Define a function to pad numbers less than 10
        function pad(n){
            return n < 10 ? '0' + n : n;
        }

        // Build the iso string to return
        var isoString = this.timeWindowEndDatetime.getUTCFullYear();
        isoString += '-' + pad(this.timeWindowEndDatetime.getUTCMonth() + 1 );
        isoString += '-' + pad(this.timeWindowEndDatetime.getUTCDate());
        isoString += 'T' + pad(this.timeWindowEndDatetime.getUTCHours());
        isoString += ':' + pad(this.timeWindowEndDatetime.getUTCMinutes());
        isoString += ':' + pad(this.timeWindowEndDatetime.getUTCSeconds()) + 'Z';

        // Now return the date in ISO form
        return isoString;

    }

});