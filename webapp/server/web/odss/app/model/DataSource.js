/*
 * File: app/model/DataSource.js
 *
 * This is the data model to represent a data source (i.e. repo)
 */
Ext.define('Odss.model.DataSource', {
    extend: 'Ext.data.Model',
    alias: 'model.datasource',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id',
            type: 'string'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'text',
            type: 'string'
        },
        {
            name: 'type',
            type: 'string'
        },
        {
            name: 'boundingPolygon',
            type: 'string'
        },
        {
            name: 'startDate',
            type: 'date'
        },
        {
            name: 'endDate',
            type: 'date'
        },
        {
            name: 'url',
            type: 'string'
        },
        {
            name: 'mimeType',
            type: 'string'
        },
        {
            name: 'sizeInBytes',
            type: 'int'
        },
        {
            name: 'params',
            type: 'auto'
        },
        {
            name: 'responsetype',
            type: 'string'
        },
        {
            name: 'color',
            type: 'string'
        }
    ]
});