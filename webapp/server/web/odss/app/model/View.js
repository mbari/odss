/*
 * File: app/model/View.js
 *
 * This is the model for a View in the ODSS
 */

Ext.define('Odss.model.View', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    idProperty: '_id',

    fields: [
        {
            name: '_id',
            type: 'string'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'repoBasePath',
            type: 'string'
        }
    ]
});