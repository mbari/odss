/*
 * File: app/model/Track.js
 *
 * This is the model for a platform track in the ODSS
 */

Ext.define('Odss.model.Track', {
    extend: 'Ext.data.Model',
    alias: 'model.track',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
        {
            name: 'platformIdFK',
            type: 'int'
        },
        {
            name: 'platformNameFK',
            type: 'string'
        },
        {
            name: 'platformAbbreviationFK'
        },
        {
            name: 'platformType',
            type: 'string'
        },
        {
            name: 'latestTimestamp',
            type: 'number'
        },
        {
            name: 'trackColor',
            type: 'string'
        },
        {
            name: 'trackGeometry',
            type: 'auto'
        }
    ]
});