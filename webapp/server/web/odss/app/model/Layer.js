/*
 * File: app/model/Layer.js
 *
 * This is the model for a layer in the ODSS
 */
Ext.define('Odss.model.Layer', {
    extend: 'Ext.data.Model',
    alias: 'model.layer',

    requires: [
        'Ext.data.Field'
    ],

    idProperty: '_id',

    fields: [
        {
            name: '_id',
            type: 'string'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'type',
            type: 'string'
        },
        {
            name: 'url',
            type: 'string'
        },
        {
            name: 'legendUrl',
            type: 'string'
        },
        {
            name: 'params',
            type: 'auto'
        },
        {
            name: 'options',
            type: 'auto'
        },
        {
            name: 'folder',
            type: 'string'
        },
        {
            name: 'views',
            type: 'auto'
        }
    ]
});