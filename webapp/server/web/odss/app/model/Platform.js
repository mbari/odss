/*
 * File: app/model/Platform.js
 *
 * This is the model for a Platform in the ODSS
 */
Ext.define('Odss.model.Platform', {
    extend: 'Ext.data.Model',
    alias: 'model.platform',

    requires: [
        'Ext.data.Field'
    ],

    idProperty: '_id',

    fields: [
        {
            name: '_id',
            type: 'string'
        },
        {
            name: 'abbreviation',
            type: 'string'
        },
        {
            name: 'color',
            type: 'string'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'typeName',
            type: 'string'
        },
        {
            name: 'iconUrl',
            type: 'string'
        },
        {
            name: 'iconHeight',
            type: 'int'
        },
        {
            name: 'iconWidth',
            type: 'int'
        },
        {
            name: 'description',
            type: 'string'
        },
        {
            name: 'views',
            type: 'auto'
        }
    ]
});