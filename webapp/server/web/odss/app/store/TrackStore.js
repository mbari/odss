/*
 * File: app/store/TrackStore.js
 *
 * This is a store for handling tracks
 */
Ext.define('Odss.store.TrackStore', {
    extend: 'Ext.data.Store',

    requires: [
        'Odss.model.Track',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Array'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Odss.model.Track',
            storeId: 'TrackStore',
            proxy: {
                type: 'ajax',
                reader: {
                    type: 'array'
                }
            }
        }, cfg)]);
    }
});