/*
 * File: app/store/LayerStore.js
 *
 * This is the store for layers
 */
Ext.define('Odss.store.LayerStore', {
    extend: 'Ext.data.Store',

    requires: [
        'Odss.model.Layer',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            model: 'Odss.model.Layer',
            storeId: 'LayerStore',
            proxy: {
                type: 'rest',
                url: '/odss/layers',
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});