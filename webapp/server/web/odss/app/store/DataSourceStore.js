/*
 * File: app/store/DataSourceStore.js
 *
 * This is the store for the DataSource (i.e. repo)
 */
Ext.define('Odss.store.DataSourceStore', {
    extend: 'Ext.data.TreeStore',

    requires: [
        'Odss.model.DataSource',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoSync: true,
            model: 'Odss.model.DataSource',
            storeId: 'DataSourceStore',
            root: {
                text: 'Data',
                id: '',
                expanded: true
            },
            proxy: {
                type: 'ajax',
                extraParams: {
                    repoBasePath: 'public'
                },
                url: 'services/catalog/datasource',
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});