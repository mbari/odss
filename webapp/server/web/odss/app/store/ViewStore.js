/*
 * File: app/store/ViewStore.js
 *
 * This is the store for Views
 */
Ext.define('Odss.store.ViewStore', {
    extend: 'Ext.data.Store',

    requires: [
        'Odss.model.View',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
            model: 'Odss.model.View',
            storeId: 'MyJsonStore',
            proxy: {
                type: 'rest',
                url: '/odss/views'
            }
        }, cfg)]);
    }
});