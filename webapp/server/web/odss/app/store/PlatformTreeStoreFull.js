/*
 * File: app/store/PlatformTreeStoreFull.js
 *
 * This is the store for platform (in a fully detailed tree format)
 */
Ext.define("Odss.store.PlatformTreeStoreFull", {
  extend: "Ext.data.TreeStore",

  requires: [
    "Odss.model.Platform",
    "Ext.data.proxy.Ajax",
    "Ext.data.reader.Json",
  ],

  constructor: function (cfg) {
    var me = this;
    cfg = cfg || {};
    me.callParent([
      Ext.apply(
        {
          model: "Odss.model.Platform",
          storeId: "MyTreeStore",
          proxy: {
            type: "ajax",
            extraParams: {
              treeView: true,
              groupBy: "type",
            },
            url: "/odss/platforms",
            reader: {
              type: "json",
              root: "platforms",
            },
          },
        },
        cfg
      ),
    ]);
  },
});
