/*
 * File: app/store/PlatformTreeStore.js
 *
 * This is the store (in tree form) for platforms
 */
Ext.define('Odss.store.PlatformTreeStore', {
    extend: 'Ext.data.TreeStore',

    requires: [
        'Odss.model.Platform',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            model: 'Odss.model.Platform',
            storeId: 'PlatformTreeStore',
            root: {
                text: 'Menu',
                id: 'src',
                expanded: true,
                loaded: true
            },
            proxy: {
                type: 'ajax',
                batchActions: false,
                extraParams: {
                    viewName: 'Public',
                    treeView: true,
                    groupBy: 'type'
                },
                url: '/odss/platforms',
                reader: {
                    type: 'json',
                    root: 'platforms'
                }
            }
        }, cfg)]);
    }
});