/*
 * File: app/store/LayerTreeStore.js
 *
 * This is the store (in tree format) for layers
 */
Ext.define('Odss.store.LayerTreeStore', {
    extend: 'Ext.data.TreeStore',

    requires: [
        'Odss.model.Layer',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'Odss.model.Layer',
            storeId: 'LayerTreeStore',
            root: {
                text: 'Menu',
                id: 'src',
                expanded: true,
                loaded: true
            },
            proxy: {
                type: 'ajax',
                extraParams: {
                    viewName: 'Public',
                    treeView: true,
                    groupByFolder: true
                },
                url: '/odss/layers',
                reader: {
                    type: 'json',
                    root: 'layers'
                }
            }
        }, cfg)]);
    }
});