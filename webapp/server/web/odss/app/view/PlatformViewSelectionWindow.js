/*
 * File: app/view/PlatformViewSelectionWindow.js
 *
 * This is the window that opens when the use wants to edit the
 * platforms that are in a view
 */
Ext.define('Odss.view.PlatformViewSelectionWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.tree.Panel',
        'Ext.tree.View'
    ],

    height: 371,
    id: 'platform-view-selection-window',
    width: 394,
    autoScroll: true,
    autoDestroy: false,
    closeAction: 'hide',
    title: 'My Window',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'treepanel',
                    id: 'platform-view-selection-window-tree-panel',
                    header: false,
                    title: 'All Platforms',
                    forceFit: true,
                    hideHeaders: true,
                    store: 'PlatformTreeStoreFull',
                    rootVisible: false,
                    viewConfig: {
                        id: 'platform-view-selection-tree-view'
                    }
                }
            ]
        });

        me.callParent(arguments);
    }

});