/*
 * File: app/view/LayerFormWindow.js
 *
 * This is the form that the user can use to edit/add a layer to the ODSS
 */
Ext.define('Odss.view.LayerFormWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.layerformwindow',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Hidden',
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Checkbox',
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Fill',
        'Ext.button.Button'
    ],

    height: 676,
    id: 'layer-form-window',
    width: 460,
    title: 'Layer Properties',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    height: 645,
                    id: 'layer-form',
                    width: 448,
                    bodyPadding: 10,
                    header: false,
                    title: 'Layer Properties',
                    items: [
                        {
                            xtype: 'hiddenfield',
                            anchor: '100%',
                            id: 'layer-id-hidden-field',
                            fieldLabel: 'Label',
                            name: 'id'
                        },
                        {
                            xtype: 'fieldset',
                            id: 'layer-properties-field-set',
                            title: 'Layer',
                            items: [
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-name-text-field',
                                    fieldLabel: 'Name',
                                    labelWidth: 80,
                                    name: 'name'
                                },
                                {
                                    xtype: 'combobox',
                                    anchor: '100%',
                                    id: 'layer-type-combo-box',
                                    fieldLabel: 'Type',
                                    labelWidth: 80,
                                    name: 'type'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-url-text-field',
                                    fieldLabel: 'URL',
                                    labelWidth: 80,
                                    name: 'url'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-legend-url-text-field',
                                    fieldLabel: 'Legend URL',
                                    labelWidth: 80,
                                    name: 'legendUrl'
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            id: 'layer-parameters-field-set',
                            title: 'Parameters',
                            items: [
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-parameters-map-text-field',
                                    fieldLabel: 'Map',
                                    labelWidth: 80,
                                    name: 'map'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-parameters-service-text-box',
                                    fieldLabel: 'Service',
                                    labelWidth: 80,
                                    name: 'service'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-parameters-version-text-field',
                                    fieldLabel: 'Version',
                                    labelWidth: 80,
                                    name: 'version'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-parameters-request-text-field',
                                    fieldLabel: 'Request',
                                    labelWidth: 80,
                                    name: 'request'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-parameters-srs-text-field',
                                    fieldLabel: 'SRS',
                                    labelWidth: 80,
                                    name: 'srs'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-parameters-layer-text-field',
                                    fieldLabel: 'Layers',
                                    labelWidth: 80,
                                    name: 'layers'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-parameters-elevation-text-field',
                                    fieldLabel: 'Elevation',
                                    labelWidth: 80,
                                    name: 'elevation'
                                },
                                {
                                    xtype: 'checkboxfield',
                                    anchor: '100%',
                                    id: 'layer-parameters-transparent-checkbox',
                                    fieldLabel: 'Transparent',
                                    labelWidth: 80,
                                    name: 'transparent',
                                    boxLabel: ''
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            id: 'layer-options-field-set',
                            title: 'Options',
                            items: [
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-options-timeformat-text-field',
                                    fieldLabel: 'Time Format',
                                    labelWidth: 90,
                                    name: 'timeFormat'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-options-hoursbetween-text-field',
                                    fieldLabel: 'Hours Between',
                                    labelWidth: 90,
                                    name: 'hoursBetween'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-options-minhoursback-text-field',
                                    fieldLabel: 'Min Hours Back',
                                    labelWidth: 90,
                                    name: 'minHoursBack'
                                },
                                {
                                    xtype: 'checkboxfield',
                                    anchor: '100%',
                                    id: 'layer-options-public-checkbox',
                                    fieldLabel: 'Public?',
                                    labelWidth: 90,
                                    name: 'public'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    id: 'layer-options-public-folder-text-field',
                                    fieldLabel: 'Public Folder',
                                    labelWidth: 90,
                                    name: 'publicFolder'
                                }
                            ]
                        }
                    ],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'bottom',
                            items: [
                                {
                                    xtype: 'tbfill'
                                },
                                {
                                    xtype: 'button',
                                    id: 'layer-save-button',
                                    text: 'Save'
                                },
                                {
                                    xtype: 'button',
                                    id: 'layer-cancel-button',
                                    text: 'Cancel'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});