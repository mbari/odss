/*
 * File: app/view/OdssViewport.js
 *
 * This is the main viewport for the ODSS where everything else is added
 */
Ext.define("Odss.view.OdssViewport", {
  extend: "Ext.container.Viewport",

  requires: [
    "Odss.view.LeafletBox",
    "Odss.view.MissionControlPanel",
    "Odss.view.OpenLayersBox",
    "Odss.view.SimpleIFrame",
    "Ext.tab.Panel",
    "Ext.tab.Tab",
    "Ext.toolbar.Toolbar",
    "Ext.form.Label",
    "Ext.form.field.ComboBox",
    "Ext.toolbar.Fill",
    "Ext.toolbar.Separator",
    "Ext.form.field.Date",
    "Ext.form.field.Number",
    "Ext.grid.Panel",
  ],

  id: "odss-viewport",
  layout: "fit",

  initComponent: function () {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "tabpanel",
          id: "maintabpanel",
          activeTab: 0,
          items: [
            {
              xtype: "panel",
              defaults: {
                collapsible: true,
                split: true,
              },
              layout: "border",
              title: "Situational Awareness",
              dockedItems: [
                {
                  xtype: "toolbar",
                  dock: "top",
                  id: "odss-toolbar",
                  items: [
                    {
                      xtype: "combobox",
                      id: "odss-view-combo-box",
                      fieldLabel: "Select View",
                      labelAlign: "right",
                      labelWidth: 70,
                      displayField: "name",
                      store: "ViewStore",
                      valueField: "name",
                    },
                    {
                      xtype: "tbfill",
                      id: "odss-toolbar-fill",
                    },
                    {
                      xtype: "label",
                      cls: "liveStatusLabel",
                      id: "live-status-label",
                      width: 80,
                      text: "Mode: Live",
                    },
                    {
                      xtype: "button",
                      id: "live-status-button",
                      icon: "resources/images/OK.png",
                      text: "",
                      tooltip: "",
                    },
                    {
                      xtype: "tbseparator",
                      itemId: "odss-toolbar-sep-1",
                    },
                    {
                      xtype: "datefield",
                      id: "start-date-field",
                      width: 155,
                      fieldLabel: "Start",
                      labelAlign: "right",
                      labelWidth: 30,
                      size: 15,
                      format: "m/d/y H:i",
                    },
                    {
                      xtype: "numberfield",
                      id: "hours-number-field",
                      width: 100,
                      fieldLabel: "Plot(hrs)",
                      labelAlign: "right",
                      labelWidth: 45,
                      value: 24,
                      size: 8,
                      allowDecimals: false,
                    },
                    {
                      xtype: "numberfield",
                      id: "time-slider-number-field",
                      width: 100,
                      fieldLabel: "Step(hrs)",
                      labelAlign: "right",
                      labelWidth: 45,
                      value: 2,
                      size: 4,
                    },
                    {
                      xtype: "button",
                      id: "time-left-arrow-button",
                      icon: "resources/images/GoBack.png",
                      tooltip: "<i>Click to step back in time</i>",
                    },
                    {
                      xtype: "button",
                      id: "time-play-stop-button",
                      icon: "resources/images/Go.png",
                      text: "",
                      tooltip:
                        "<i>Click to start play back from start date</li>",
                    },
                    {
                      xtype: "button",
                      id: "time-right-arrow-button",
                      icon: "resources/images/GoForward.png",
                      tooltip: "<i>Click to step forward in time</i>",
                    },
                    {
                      xtype: "tbseparator",
                      id: "odss-toolbar-sep-2",
                    },
                    {
                      xtype: "button",
                      id: "go-home-button",
                      icon: "resources/images/Home.png",
                      tooltip: "<i>Click to go to home location</i>",
                    },
                    {
                      xtype: "button",
                      id: "set-home-button",
                      icon: "resources/images/BluePin.png",
                      tooltip:
                        "<i>Click to set current view as home location</i>",
                    },
                  ],
                },
              ],
              items: [
                {
                  xtype: "missioncontrolpanel",
                  id: "mission-control-panel",
                  minWidth: 100,
                  width: 300,
                  collapsed: true,
                  collapsible: true,
                  collapseMode: "header",
                  title: "Data",
                  region: "west",
                },
                {
                  xtype: "panel",
                  region: "center",
                  layout: "fit",
                  id: "odss-map-panel",
                  header: false,
                  items: [
                    {
                      xtype: "leafletbox",
                      id: "leaflet-box",
                    },
                  ],
                },
              ],
            },
            {
              xtype: "simpleiframe",
              src: "/odss-planning/",
              title: "Planning",
            },
            {
              xtype: "simpleiframe",
              src: "https://mbari.slack.com/messages/canon",
              title: "Collaboration",
            },
            {
              xtype: "simpleiframe",
              src: "https://stoqs.mbari.org",
              title: "STOQS",
            },
            {
              xtype: "simpleiframe",
              src: "https://docs.mbari.org/odss/",
              title: "Help",
            },
          ],
          listeners: {
            beforetabchange: {
              fn: me.onMaintabpanelBeforeTabChange,
              scope: me,
            },
          },
        },
      ],
    });

    me.callParent(arguments);
  },

  onMaintabpanelBeforeTabChange: function (tabPanel, newCard, oldCard, eOpts) {
    // Check to see if the tab was STOQS, if so, open new window to STOQS and return false
    if (newCard.title === "STOQS") {
      window.open("https://stoqs.mbari.org");
      return false;
    } else if (newCard.title == "Collaboration") {
      window.open("https://mbari.slack.com/messages/canon");
      return false;
    } else {
      return true;
    }
  },
});
