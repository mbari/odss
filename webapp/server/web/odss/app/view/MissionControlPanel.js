/*
 * File: app/view/MissionControlPanel.js
 *
 * This is the panel that holds the controls on the left side of the ODSS
 */
Ext.define('Odss.view.MissionControlPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.missioncontrolpanel',

    requires: [
        'Odss.view.PlatformsGridTreePanel',
        'Odss.view.LayerTreePanel',
        'Odss.view.DataSourceTreeGrid',
        'Ext.tree.Panel',
        'Ext.grid.Panel'
    ],

    autoScroll: true,
    layout: 'accordion',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'platformsgridtreepanel',
                    id: 'platformsgridtreepanel'
                },
                {
                    xtype: 'layerstreepanel',
                    id: 'layerstreepanel',
                    forceFit: true
                },
                {
                    xtype: 'datasourcetreegrid',
                    id: 'datasourcetreegrid',
                    rootVisible: false
                }
            ]
        });

        me.callParent(arguments);
    }

});