/*
 * File: app/view/DataSourceTreeGrid.js
 *
 * This is the data source grid view in the ODSS
 */
Ext.define('Odss.view.DataSourceTreeGrid', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.datasourcetreegrid',

    requires: [
        'Ext.tree.View',
        'Ext.tree.Column',
        'Ext.grid.column.Date',
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Fill',
        'Ext.button.Button'
    ],

    height: 250,
    width: 400,
    title: 'Data',
    hideHeaders: true,
    store: 'DataSourceStore',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            viewConfig: {

            },
            columns: [
                {
                    xtype: 'treecolumn',
                    dataIndex: 'name',
                    text: 'File',
                    flex: 1
                },
                {
                    xtype: 'datecolumn',
                    dataIndex: 'endDate',
                    text: 'Date',
                    format: 'm/d/y H:i'
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            id: 'refresh-datasource-button',
                            icon: 'resources/images/reload-16.png',
                            tooltip: 'Click to refresh view'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});