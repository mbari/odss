/*
 * File: app/view/LayerTreePanel.js
 *
 * This is the tree panel that shows all the layers
 */
Ext.define('Odss.view.LayerTreePanel', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.layerstreepanel',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.tree.View',
        'Ext.tree.Column'
    ],

    title: 'Layers',
    hideHeaders: true,
    store: 'LayerTreeStore',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'layer-tree-panel-toolbar',
                    items: [
                        {
                            xtype: 'button',
                            id: 'layer-tree-panel-edit-button',
                            icon: 'resources/images/edit-sm.png',
                            tooltip: 'Click to add/remove layers or organize into folders'
                        }
                        // {
                        //     xtype: 'button',
                        //     id: 'layer-tree-panel-add-button',
                        //     icon: 'resources/images/add-item.png',
                        //     text: ''
                        // }
                    ]
                }
            ],
            viewConfig: {
                id: 'layer-tree-panel-view',
                rootVisible: false
            },
            columns: [
                {
                    xtype: 'treecolumn',
                    dataIndex: 'name',
                    text: 'Name'
                }
            ]
        });

        me.callParent(arguments);
    }

});