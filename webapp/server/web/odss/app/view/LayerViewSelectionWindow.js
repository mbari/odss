/*
 * File: app/view/LayerViewSelectionWindow.js
 *
 * This is the window that opens and displays the form to edit the layers that are
 * available on a specific View
 */
Ext.define('Odss.view.LayerViewSelectionWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.form.field.Text',
        'Ext.grid.View',
        'Ext.grid.plugin.CellEditing'
    ],

    height: 400,
    id: 'layer-view-selection-window',
    width: 600,
    autoScroll: true,
    autoDestroy: false,
    closeAction: 'hide',
    title: 'Layer Selection',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    id: 'layer-view-selection-grid-panel',
                    autoScroll: true,
                    forceFit: true,
                    store: 'LayerStore',
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            width: 450,
                            dataIndex: 'name',
                            text: 'Name'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'folder',
                            text: 'Folder',
                            editor: {
                                xtype: 'textfield'
                            }
                        }
                    ],
                    viewConfig: {
                        id: 'layer-view-selection-grid-panel-view',
                        markDirty: false
                    },
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 1
                        })
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});