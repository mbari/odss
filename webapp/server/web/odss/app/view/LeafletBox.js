/*
 * File: app/view/OpenLayersBox.js
 *
 * This is a container that will hold the leaflet map
 */
Ext.define('Odss.view.LeafletBox', {
    extend: 'Ext.container.Container',
    alias: 'widget.leafletbox',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);
    }

});