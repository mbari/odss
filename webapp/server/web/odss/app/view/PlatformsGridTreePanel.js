/*
 * File: app/view/PlatformsGridTreePanel.js
 *
 * This is the panel where you can select/deselect platforms for display
 * on the map
 */
Ext.define('Odss.view.PlatformsGridTreePanel', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.platformsgridtreepanel',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.tree.View',
        'Ext.tree.Column',
        'Ext.grid.column.Action'
    ],

    title: 'Platforms',
    forceFit: true,
    hideHeaders: true,
    store: 'PlatformTreeStore',
    rootVisible: false,

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'platforms-grid-tree-panel-toolbar',
                    items: [
                        {
                            xtype: 'button',
                            id: 'platform-grid-tree-panel-toolbar-edit-button',
                            icon: 'resources/images/edit-sm.png',
                            tooltip: 'Click to add/remove platforms from the view'
                        },
                        {
                            xtype: 'button',
                            id: 'platform-grid-tree-panel-toolbar-select-all-button',
                            iconCls: 'far fa-check-square',
                            tooltip: 'Click to add all platforms to the map'
                        },
                        {
                            xtype: 'button',
                            id: 'platform-grid-tree-panel-toolbar-deselect-all-button',
                            iconCls: 'far fa-square',
                            tooltip: 'Click to remove all platforms from the map'
                        }
                    ]
                }
            ],
            viewConfig: {

            },
            columns: [
                {
                    xtype: 'treecolumn',
                    draggable: false,
                    id: 'platforms-grid-tree-panel-text-column',
                    dataIndex: 'text',
                    flex: 5,
                    hideable: false
                },
                {
                    xtype: 'actioncolumn',
                    id: 'platform-grid-tree-panel-track-data-action-column',
                    align: 'center',
                    flex: 1,
                    items: [
                        {
                            handler: function (view, rowIndex, colIndex, item, e, record, row) {
                                window.open("tracks?platformID=" + record.get('_id') + "&returnSRS=4326&lastNumberOfFixes=5&returnFormat=html");

                            },
                            icon: 'resources/images/122-stats.png',
                            tooltip: 'Click for last 5 positions'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});