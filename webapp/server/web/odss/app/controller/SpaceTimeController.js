/*
 * File: app/controller/SpaceTimeController.js
 *
 * This is the controller for handling logic for the time toolbar
 */
Ext.define('Odss.controller.SpaceTimeController', {
    extend: 'Ext.app.Controller',

    // Set the default mode to live
    playing: 'false',

    // Grab some references to various components
    refs: [
        {
            ref: 'odssToolbar',
            selector: '#odss-toolbar'
        },
        {
            ref: 'liveStatusLabel',
            selector: '#live-status-label'
        },
        {
            ref: 'liveStatusButton',
            selector: '#live-status-button'
        },
        {
            ref: 'startDateField',
            selector: '#start-date-field'
        },
        {
            ref: 'hoursNumberField',
            selector: '#hours-number-field'
        },
        {
            ref: 'timeSliderNumberField',
            selector: '#time-slider-number-field'
        },
        {
            ref: 'openLayersBox',
            selector: '#open-layers-box'
        },
        {
            ref: 'timeRightArrowButton',
            selector: '#time-right-arrow-button'
        }
    ],

    // This function is called when the controller is first initialized
    init: function(application) {
        // Link handlers to components
        this.control({
            "#live-status-button": {
                click: this.onModeButtonClick
            },
            "#start-date-field": {
                select: this.onDatefieldSelect,
                change: this.onStartDateFieldChange
            },
            "#hours-number-field": {
                change: this.onHoursNumberfieldChange
            },
            "#time-right-arrow-button": {
                click: this.onTimeRightArrowButtonClicked
            },
            "#time-left-arrow-button": {
                click: this.onTimeLeftArrowButtonClicked
            },
            "#time-play-stop-button": {
                click: this.onTimePlayStopButtonClick
            }
        });
    },

    // This method is called after the controller is initialized
    onLaunch: function() {
        // Initialize the Time model
        Odss.model.SpaceTime.timeWindowStartDatetime = new Date();
        Odss.model.SpaceTime.timeWindowStartDatetime.addHours(-24);
        Odss.model.SpaceTime.timeWindowEndDatetime = new Date();

        // I actually have to initiate the playback mode to true because
        // the timers are not on by default.  They will be started after
        // the application launches
        Odss.model.SpaceTime.playbackMode = true;

        // Create a setting that will specify how many point to plot for tracks
        this.numPoints = 100;

        // Set the playback flag to false
        this.playing = false;

        // Clear the intervalid
        this.intervalId = 0;

        // Change the mode to live
        this.changeMode('live');

    },

    // This method is called when the user clicks on the button to change modes
    onModeButtonClick: function(button, e, eOpts) {
        // First switch the mode to live
        this.changeMode('live');
    },

    // This method is called when the date field is changed
    onDatefieldSelect: function(field, value, eOpts) {
        // Since the user selected something, change to playback mode
        this.changeMode('playback');
    },

    // This method is called when the value in the start date field is changed
    onStartDateFieldChange: function(field, newValue, oldValue, eOpts) {
        // Update the start time in the Time model
        Odss.model.SpaceTime.timeWindowStartDatetime = this.getStartDateField().value;

        // Calculate the new end date using the start date and hours to plot fields
        var currentStartDate = this.getStartDateField().value;

        // Create a config from the date in the start time picker
        var startDateConfig = {
            year: currentStartDate.getFullYear(),
            month: currentStartDate.getMonth(),
            day: currentStartDate.getDate(),
            hour: currentStartDate.getHours(),
            minute: currentStartDate.getMinutes(),
            second: currentStartDate.getSeconds(),
            millisecond: currentStartDate.getMilliseconds()
        };

        // Create a Datejs object
        var newEndDate = new Date.today().set(startDateConfig);

        // Create the number of seconds based on the input
        var numberOfSeconds = this.getHoursNumberField().value * 60 * 60;

        // Adjust by seconds
        newEndDate.addSeconds(numberOfSeconds);

        // Set the new end date
        Odss.model.SpaceTime.timeWindowEndDatetime = newEndDate;

        // Fire an event that states the times were changed
        this.application.fireEvent("timewindowupdated", this);
    },

    // This method is called when the number of hours field value is changed
    onHoursNumberfieldChange: function(field, newValue, oldValue, eOpts) {
        // We need to update the end time of the time model, but the way we do this depends on the
        // mode we are in. Check for live mode
        if (Odss.model.SpaceTime.playbackMode === false){
            // In live mode, we actually need to adjust the start time, not the
            // end time.

            // Start by grabbing the current start date
            var currentStartDate = Odss.model.SpaceTime.timeWindowStartDatetime;

            // Create a Date object
            var newStartDate = new Date();

            // Create the number of seconds based on the input
            var numberOfSeconds = this.getHoursNumberField().value * 60 * 60;

            // Adjust by seconds
            newStartDate.addSeconds(-1 * numberOfSeconds);

            // Set the new start date
            Odss.model.SpaceTime.timeWindowStartDatetime = newStartDate;

            // Fire the event that the times changed
            this.application.fireEvent("timewindowupdated", this);

        } else {

            // Start by grabbing the current start date
            var currentStartDate = Odss.model.SpaceTime.timeWindowStartDatetime;

            // Create a config from the date in the start time picker
            var startDateConfig = {
                year: currentStartDate.getFullYear(),
                month: currentStartDate.getMonth(),
                day: currentStartDate.getDate(),
                hour: currentStartDate.getHours(),
                minute: currentStartDate.getMinutes(),
                second: currentStartDate.getSeconds(),
                millisecond: currentStartDate.getMilliseconds()
            };

            // Create a Date object
            var newEndDate = new Date.today().set(startDateConfig);

            // Create the number of seconds based on the input
            var numberOfSeconds = this.getHoursNumberField().value * 60 * 60;

            // Adjust by seconds
            newEndDate.addSeconds(numberOfSeconds);

            // Set the new end date
            Odss.model.SpaceTime.timeWindowEndDatetime = newEndDate;

            // Fire the event that the times changed
            this.application.fireEvent("timewindowupdated", this);
        }
    },

    // This is called when the user clicks on the step time forward button
    onTimeRightArrowButtonClicked: function(button, e, eOpts) {
        // First switch the mode to playback
        this.changeMode('playback');

        // When the user clicks on the right arrow, they simply want to move
        // the visible time window over by the hours specified in between the
        // arrows. Grab the number of hours, convert to seconds and move start date field
        var numHours = this.getTimeSliderNumberField().value;
        var numSeconds = numHours * 60 * 60;

        this.moveStartDate(numSeconds, true);
    },

    // This is called when the user clicks on the step time backward button
    onTimeLeftArrowButtonClicked: function(button, e, eOpts) {
        // First switch the mode to playback
        this.changeMode('playback');

        // When the user clicks on the left arrow, they simply want to move
        // the visible time window over by the hours specified in between the
        // arrows. Grab the number of hours, convert to seconds and move start
        // date field
        var numHours = this.getTimeSliderNumberField().value;
        var numSeconds = numHours * 60 * 60;
        this.moveStartDate(numSeconds, false);
    },

    // This method is called when the user clicks on the stop playback button
    onTimePlayStopButtonClick: function(button, e, eOpts) {
        // First switch the mode to playback
        this.changeMode('playback');

        if (this.playing !== null && this.playing === true) {
            this.playing = false;
            clearInterval ( this.intervalId );
            // Change icon
            button.setIcon('resources/images/Go.png');
            button.setTooltip('<i>Click to start playback</i>');
        } else {
            var localController = this;
            this.playing = true;
            this.intervalId = setInterval ( function() {

                // Grab the end date from the model
                var endDate = Odss.model.SpaceTime.timeWindowEndDatetime;

                // Grab the current date and time
                var now = new Date();

                // If the end date is past now, stop the playback
                if (endDate > now) {
                    clearInterval(localController.intervalId);
                    button.setIcon('resources/images/Go.png');
                    button.setTooltip('<i>Click to start playback</i>');
                    return;
                }

                // Grab the number of hours from the slider field
                var numHours = localController.getTimeSliderNumberField().value;

                // Calculate number of seconds
                var numSeconds = numHours * 60 * 60;

                // Adjust the start date which will fire a replot of tracks
                localController.moveStartDate(numSeconds, true);
            }, 10000 );
            button.setIcon('resources/images/Stop.png');
            button.setTooltip('<i>Click to stop playback</i>');
        }
    },

    // This method is called when the mode is change to/from playback and live
    changeMode: function(mode) {
        // This function switches the operating mode.  If the mode
        // is currently different from the requested mode, this
        // function will handle the appropriate changes.  Otherwise
        // it is already in that mode and nothing will happen.

        // Check current mode
        if (Odss.model.SpaceTime.playbackMode && typeof mode !== 'undefined' && mode === 'live') {
            // The GUI should switch modes from playback to live

            // Now I need to set the date in the start date field to the
            // current date - the number of hours to plot
            var newStartDate = new Date();

            // Adjust the start date by that
            newStartDate.addHours(-1 * this.getHoursNumberField().getValue());

            // Set the new start date
            this.getStartDateField().setValue(newStartDate);

            // Change the model flag to indicate live mode
            Odss.model.SpaceTime.playbackMode = false;

            // Change the mode label
            this.getLiveStatusLabel().setText("Mode: Live");

            // Change the icon
            this.getLiveStatusButton().setIcon("resources/images/OK.png");
            this.getLiveStatusButton().setTooltip("");

            // Fire off a task to update the time window every so often
            var localController = this;
            this.liveIntervalId = setInterval ( function() {
                // Create a new start date based on current time
                var newStartDate = new Date();

                // Adjust the start date by the number of hours back specified
                newStartDate.addHours(-1 * localController.getHoursNumberField().getValue());

                // Set the new start date
                localController.getStartDateField().setValue(newStartDate);
            }, 60000 );

        } else if (!Odss.model.SpaceTime.playbackMode && typeof mode !== 'undefined' && mode === 'playback') {
            // The GUI should switch to playback from live
            clearInterval ( this.liveIntervalId );

            // Set the model flag to playback mode
            Odss.model.SpaceTime.playbackMode = true;

            // Change the mode label
            this.getLiveStatusLabel().setText("Mode: Playback");

            // Change the icon
            this.getLiveStatusButton().setIcon("resources/images/NoCircle.png");
            this.getLiveStatusButton().setTooltip("<i>Click to return to Live mode</i>");
        }
    },

    // This method is called to move the start date of the time window
    moveStartDate: function(seconds, forward) {
        // Create the number of seconds based on the input
        var numberOfSeconds = seconds;
        if (forward !== true) {
            numberOfSeconds = -1 * numberOfSeconds;
        }
        // Slide the start and end dates on the time model
        if (Odss.model.SpaceTime.timeWindowStartDatetime !== null) {
            Odss.model.SpaceTime.timeWindowStartDatetime.addSeconds(numberOfSeconds);
        }
        if (Odss.model.SpaceTime.timeWindowEndDatetime !== null) {
            Odss.model.SpaceTime.timeWindowEndDatetime.addSeconds(numberOfSeconds);
        }

        // Now update the UI start date
        this.getStartDateField().setValue(Odss.model.SpaceTime.timeWindowStartDatetime);

        // Fire and event that the time model was updated
        this.application.fireEvent('timewindowupdated', this);
    }
});
