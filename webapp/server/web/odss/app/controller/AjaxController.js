/*
 * File: app/controller/AjaxController.js
 *
 * This class contains code that allows the application to make calls for
 * tracking data one request at a time. If we don't do something like this,
 * it looks like some kind of DOS attack on the ODSS server. Other parts
 * of the applications can add requests to a queue that are then handled
 * one at a time.
 */
Ext.define('Odss.controller.AjaxController', {
    extend: 'Ext.app.Controller',

    // This function is called when the controller is initialized
    init: function(application) {
        // Setup the array where the requests will be queued
        this.queuedRequests = [];

        // A flag to indicate the AjaxController is processing something
        this.inProcess = false;
    },

    // This is called after the controller is initialized
    onLaunch: function() {
        // Fire off the event to process any requests
        this.processNextRequest();
    },

    // This method takes in a request for an AJAX call and adds it to
    // a queue so that things can be processed one at a time to prevent
    // hammering any APIs with requests
    addRequestToQueue: function(reqMethod, reqUrl, reqParams, reqScope) {
        // Verify the method is correct
        if (null === reqMethod ||
            typeof reqMethod == 'undefined' ||
            (reqMethod.toLowerCase() != "get" &&
            reqMethod.toLowerCase() != "post")) {

            // Just return as the request would not be recognized
            return;
        }

        // Create an object to hold the request information
        var request = {
            method: reqMethod,
            url: reqUrl,
            params: reqParams,
            scope: reqScope
        };

        // If the request is for tracking data, and there is a request already
        // for the same ID, just ignore this one.
        if (request.url === 'tracks') {
            for (var i = 0; i < this.queuedRequests.length; i++) {
                if (this.queuedRequests[i].url === 'tracks' &&
                this.queuedRequests[i].params.platformID === request.params.platformID) {
                    // Ignore the request
                    return;
                }
            }
        }

        // Now add it
        this.queuedRequests.push(request);

        // Now fire the processor
        this.processNextRequest();

    },

    // This method takes an AJAX request off the queue and runs it
    processNextRequest: function() {
        // If Ajax is already, processing, just ignore
        if (this.inProcess) {
            return;
        }

        // Pick the request at index zero and if there is one, try to process it
        if (this.queuedRequests.length > 0) {
            // Set the processing flag
            this.inProcess = true;

            // Grab the scope from here as I will need it later
            var ajaxControllerScope = this;

            // Pull the request off the array
            var requestToProcess = ajaxControllerScope.queuedRequests.shift();

            // Process it
            Ext.Ajax.request({
                method: requestToProcess.method,
                url: requestToProcess.url,
                params: requestToProcess.params,
                scope: requestToProcess.scope,
                timeout: 10000,
                success: function (response, options) {
                    this.successHandler(response,options);
                    ajaxControllerScope.inProcess = false;
                    ajaxControllerScope.processNextRequest();
                },
                failure: function (response, options) {
                    console.log("AJAX request failed. Response:");
                    console.log(response);
                    console.log("Params:");
                    console.log(requestToProcess.params);
                    this.failureHandler(response, options);
                    ajaxControllerScope.inProcess = false;
                    ajaxControllerScope.processNextRequest();
                }
            });
        }
    }
});
