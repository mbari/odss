/*
 * File: app/controller/PlatformController.js
 *
 * This is the controller that handles all the activity in the
 * platforms window
 */
Ext.define('Odss.controller.PlatformController', {
    extend: 'Ext.app.Controller',

    // Grab some references
    refs: [
        {
            ref: 'platformsGridTreePanel',
            selector: 'platformsgridtreepanel'
        }
    ],

    // This is the method that is called when the component is initialized
    init: function (application) {
        // Link handlers to components on view
        this.control({
            "platformsgridtreepanel": {
                itemappend: this.onTreepanelItemAppend,
                checkchange: this.onTreepanelCheckChange,
                itemclick: this.onItemClickHandler
            },
            "#platform-grid-tree-panel-toolbar-select-all-button": {
                click: this.handlePlatformGridTreePanelToolbarSelectAllButtonClick
            },
            "#platform-grid-tree-panel-toolbar-deselect-all-button": {
                click: this.handlePlatformGridTreePanelToolbarDeselectAllButtonClick
            }
        });

        // Subscribe to the event that states the map component is ready.  We need
        // to respond to this so that we can wait to load the platform store until
        // the map is ready.
        application.on('mapready', this.handleMapReady, this);

        // Subscribe to view selections
        application.on('viewselected', this.handleViewSelection, this);

        // Subscribe to platform view updates
        application.on('platformViewUpdated', this.handlePlatformViewUpdated, this);
    },

    // This method is called when a platform is added to the tree
    onTreepanelItemAppend: function (nodeinterface, node, index, eOpts) {
        // First just set the text based on the name of the node
        node.set('text', node.raw.name);

        // If the node that's being appended is a leaf node, then we can
        // assume it's one of our Platform model instances that's been "dressed
        // up" as a node
        if (node.raw._id && node.raw._id != 'root') {
            // Turn on the check
            node.set('checked', true);

            // Set that it is a leaf
            node.set('leaf', true);

            // Write the text as the name and the abbreviation
            node.set('text', node.raw.name + " (" + node.raw.abbreviation + ")");
            // The node is a Platform model instance with NodeInterface
            // properties and methods added. We want to customize those
            // node properties to control how it appears in the TreePanel.

            // Initialize style sheet if it doesn't exist
            if (typeof this.styleText === "undefined") this.styleText = "";

            // Create the local style text
            this.styleText += ".platform-" + node.raw._id + "-tree-node {\n" +
                "width:12px;\n" +
                "height:12px;\n" +
                "background-color:" + node.raw.color + ";\n" +
                "background-image:url('');" +
                "}\n";

            // Remove the previous stylesheet
            Ext.util.CSS.removeStyleSheet("platform-tree-node-styles");

            // Create a new style sheet with all the styles stacked up
            Ext.util.CSS.createStyleSheet(this.styleText, "platform-tree-node-styles");

            // Change the icon's class to show the color of the track
            node.set('iconCls', 'x-tree-node-icon platform-' + node.raw._id + "-tree-node");

            // Fire an event that says the platform was selected
            // TODO kgomes - do I need to fire this event or just call render tracks?
            this.application.fireEvent('platformselected', node.raw);
        }
    },

    // This event is called when the user clicks on the check box next to a platform
    onTreepanelCheckChange: function (node, checked, eOpts) {
        // When a check changes on the tree, we want to fire
        // the correct application event
        if (checked === true) {
            this.application.fireEvent("platformselected", node.raw);
        } else if (checked === false) {
            this.application.fireEvent("platformdeselected", node.raw);
        }
    },

    // This method gets called when the user clicks on an item in the tree, but not on the checkbox
    onItemClickHandler: function (scope, record, item, index, e, eOpts) {
        // Send the node to the map controller to see if it can pan an zoom to that item
        if (record && e && e.target && e.target.textContent && e.target.textContent.length > 0) {
            Odss.app.getController('LeafletBoxController').zoomToPlatform(record.raw);
        }
    },

    handlePlatformGridTreePanelToolbarSelectAllButtonClick: function (scope, e, eOpts) {
        this.getPlatformsGridTreePanel().getRootNode().cascadeBy(function (node) {
            // Check to see if it's a leaf
            if (node.get('leaf') && !node.get('checked')) {
                node.set('checked', true);
                Odss.app.fireEvent("platformselected", node.raw);
            }
        });
    },

    handlePlatformGridTreePanelToolbarDeselectAllButtonClick: function (scope, e, eOpts) {
        this.getPlatformsGridTreePanel().getRootNode().cascadeBy(function (node) {
            // Check to see if it's a leaf
            node.get('leaf');
            if (node.get('leaf') && node.get('checked')) {
                node.set('checked', false);
                Odss.app.fireEvent("platformdeselected", node.raw);
            }
        });
    },

    // This event gets fired when the map says it's ready
    handleMapReady: function (source) {
        // Call the method to refresh platforms and tracks
        this.refreshPlatformsAndTracks();
    },

    // This method is called when the view selection is changed
    handleViewSelection: function (source, viewName) {
        // Call the method to refresh platforms and tracks
        this.refreshPlatformsAndTracks();
    },

    // This method is called when something about the platform view changes
    handlePlatformViewUpdated: function () {
        // Call the method to refresh platforms and tracks
        this.refreshPlatformsAndTracks();
    },

    // This method clears and refreshes all the tracks on the map
    refreshPlatformsAndTracks: function () {
        // Grab the stores we need
        var trackStore = Ext.getStore('TrackStore');
        var platformStore = Ext.getStore('PlatformTreeStore');

        // Iterate over all the tracks in the store and remove from the OpenLayers display
        trackStore.data.each(function (track, index, totalItems) {
            Odss.app.getController('LeafletBoxController').removeTrack(track);
        });

        // Now clear the data from the track store
        trackStore.loadData([], false);

        // Grab the PlatformTreeStore and set the view name
        platformStore.getProxy().extraParams = {
            viewName: Odss.app.getLastView()['name'],
            treeView: true,
            groupBy: "type"
        };

        // Now reload the tree store
        platformStore.load();
    }
});
