/*
 * File: app/controller/LayerViewSelectionWindowController.js
 *
 * This is the controller class for handling the logic in the window
 * where the user can select layers and which folders they go in
 */
Ext.define('Odss.controller.LayerViewSelectionWindowController', {
    extend: 'Ext.app.Controller',

    // Grab needed references
    refs: [
        {
            ref: 'layerViewSelectionGridPanel',
            selector: '#layer-view-selection-grid-panel'
        }
    ],

    // This method is called when the controller is initialized
    init: function (application) {
        // Link up some handler functions
        this.control({
            "#layer-tree-panel-edit-button": {
                click: this.handleLayerTreePanelEditButtonClick
            },
            "#layer-view-selection-grid-panel": {
                edit: this.onGridpanelEdit
            },
            "#layer-view-selection-window": {
                close: this.handleWindowClose
            }
        });
    },

    // This method is called after the controller is initialized
    onLaunch: function () {
        // Clear the current view name
        this.currentViewName = null;
    },

    // This event handler is called when the user selects the button to edit
    // the list of layers that are in a the current view
    handleLayerTreePanelEditButtonClick: function (button, e, eOpts) {
        // Grab the current view name
        this.currentViewName = Odss.app.getLastView()['name'];

        // Check to see if the window has even been created yet.
        if (typeof this.layerViewSelectionWindow === 'undefined' ||
            this.layerViewSelectionWindow === null) {
            this.layerViewSelectionWindow = new Odss.view.LayerViewSelectionWindow();
        }

        // Set the title on the window
        this.layerViewSelectionWindow.setTitle("Layer Selection for " + this.currentViewName);

        // A handle to this instance for call backs
        var me = this;

        // Make sure the layer store is loaded
        Ext.getStore('LayerStore').load({
            callback: function (records, operation, success) {
                // After the layer data has been loaded, the view needs the 'folder'
                // attribute set based on the view the user is currently looking at

                // Loop over the records in the store
                for (var i = 0; i < records.length; i++) {
                    // Loop over the views and find the matching view so we can
                    // properly set the folder for the current view
                    var recordViews = records[i].get('views');
                    if (recordViews && recordViews.length > 0) {
                        for (var j = 0; j < recordViews.length; j++) {
                            if (recordViews[j].name === me.currentViewName) {
                                records[i].set('folder', recordViews[j].folder);
                            }
                        }
                    }
                }
            }
        });

        // Show the window if not visible
        if (this.layerViewSelectionWindow.isVisible() === false) {
            this.layerViewSelectionWindow.show();
        }
    },

    // This method is called after the information in the grid is edited
    onGridpanelEdit: function (editor, e, eOpts) {
        // Grab a reference to this for scoping in callbacks
        var me = this;

        // This handler gets fired when the user edits the folder name associated with
        // a layer.  This action will update the 'folder' property on the layer, but
        // this is not persistent.  We need to update the folder in the views arrays.

        // Two things can happen based on the new value.  If the value is empty, the
        // layer should be removed from the view. Otherwise the value should be added
        // or updated
        if (e.value === "") {
            // This means the layer is cleared, run through the views and make sure
            // the view entry is removed
            var viewIndex = -1;
            var counter = 0;
            e.record.get('views').forEach(function (view) {
                if (view.name === me.currentViewName)
                    viewIndex = counter;
                counter++;
            });
            if (viewIndex > -1) {
                e.record.get('views').splice(viewIndex, 1);

                // Remove view on server
                Ext.Ajax.request({
                    url: 'layers/' + e.record.get('_id') + '?removeViewName=' + me.currentViewName,
                    dataType: 'json',
                    method: 'PUT',
                    success: function (data, status) {
                        // Make sure there was a response
                        if (data && data.responseText) {
                        } else {
                        }
                    },
                    error: function (error) {
                        console.log("Error on AJAX call", error);
                    }
                });
            }
        } else {
            // This means the folder value needs to be set on the view
            var viewIndex = -1;
            var counter = 0;
            e.record.get('views').forEach(function (view) {
                if (view.name === me.currentViewName) {
                    viewIndex = counter;
                }
                counter++;
            });
            if (viewIndex > -1) {
                e.record.get('views')[viewIndex].folder = e.value;
            } else {
                e.record.get('views').push({
                    name: me.getCurrentViewName,
                    folder: e.value
                });
            }
            // Update view on server
            Ext.Ajax.request({
                url: 'layers/' + e.record.get('_id') + '?addOrUpdateViewName=' + me.currentViewName + "&folderName=" + e.value,
                dataType: 'json',
                method: 'PUT',
                success: function (data, status) {
                    // Make sure there was a response
                    if (data && data.responseText) {
                    } else {
                    }
                },
                error: function (error) {
                    console.log("Error on AJAX call", error);
                }
            });
        }

        // So, grab the views array
        var viewsArray = e.record.get('views');

        // Loop over the array
        viewsArray.forEach(function (view) {

        });
    },

    // This event is called when the window is closed
    handleWindowClose: function (panel, eOpts) {
        // Send an event that layer view was most likely updated
        this.application.fireEvent("layerupdated", this);
    },

});
