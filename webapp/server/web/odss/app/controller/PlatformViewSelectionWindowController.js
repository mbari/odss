/*
 * File: app/controller/PlatformViewSelectionWindowController.js
 *
 * This controller handles the activity on the window the user use to
 * select which platforms are in which view
 */
Ext.define("Odss.controller.PlatformViewSelectionWindowController", {
  extend: "Ext.app.Controller",

  // This function is called when the controller is initialized
  init: function (application) {
    // Link event handlers
    this.control({
      "#platform-grid-tree-panel-toolbar-edit-button": {
        click: this.handlePlatformGridTreePanelToolbarEditButtonClick,
      },
      "#platform-view-selection-window-tree-panel": {
        itemappend: this.onTreepanelItemAppend,
        checkchange: this.onTreepanelCheckChange,
      },
      "#platform-view-selection-window": {
        close: this.onWindowClose,
      },
    });

    // Clear the view name
    this.currentViewName = null;
  },

  // This function is called when the user clicks on the button to open the platform editor
  handlePlatformGridTreePanelToolbarEditButtonClick: function (
    button,
    e,
    eOpts
  ) {
    // Load the platform store
    Ext.getStore("PlatformTreeStoreFull").load();

    // First check to see if the window has even been created yet.
    if (
      typeof this.platformViewSelectionWindow === "undefined" ||
      this.platformViewSelectionWindow === null
    ) {
      this.platformViewSelectionWindow =
        new Odss.view.PlatformViewSelectionWindow();
    }

    // Grab the current view name
    this.currentViewName = Odss.app.getLastView()["name"];

    // Set the title on the window
    this.platformViewSelectionWindow.setTitle(
      "Platform Selection for " + this.currentViewName
    );

    // Show the window
    if (this.platformViewSelectionWindow.isVisible() === false) {
      this.platformViewSelectionWindow.show();
    }
  },

  // This function is called when a platform is added to the editor window
  onTreepanelItemAppend: function (nodeinterface, node, index, eOpts) {
    // First just set the text based on the name of the node
    node.set("text", node.raw.name);

    // If the node that's being appended is a leaf node, then we can
    // assume it's one of our Platform model instances that's been "dressed
    // up" as a node
    if (node.raw._id) {
      // Set that it is a leaf
      node.set("leaf", true);

      // Write the text as the name and the abbreviation
      node.set("text", node.raw.name + " (" + node.raw.abbreviation + ")");

      // The node is a Platform model instance with NodeInterface
      // properties and methods added. We want to customize those
      // node properties to control how it appears in the TreePanel.

      // Initialize style sheet if it doesn't exist
      if (typeof this.styleText === "undefined") this.styleText = "";

      // Create the local style text
      this.styleText +=
        ".platform-" +
        node.raw._id +
        "-tree-node {\n" +
        "width:12px;\n" +
        "height:12px;\n" +
        "background-color:" +
        node.raw.color +
        ";\n" +
        "background-image:url('');" +
        "}\n";

      // Remove the previous stylesheet
      Ext.util.CSS.removeStyleSheet("platform-tree-node-styles");

      // Create a new style sheet with all the styles stacked up
      Ext.util.CSS.createStyleSheet(
        this.styleText,
        "platform-tree-node-styles"
      );

      // Change the icon's class to show the color of the track
      node.set(
        "iconCls",
        "x-tree-node-icon platform-" + node.raw._id + "-tree-node"
      );

      // Now I have to examine the node to see if the view name is the same as the
      // current view so I can set the check mark appropriately
      var inView = false;
      if (node.raw.views && node.raw.views.length > 0) {
        // Loop over the views
        for (var i = 0; i < node.raw.views.length; i++) {
          if (node.raw.views[i] === this.currentViewName) {
            inView = true;
          }
        }
      }

      // Set the check mark correctly
      if (inView) {
        node.set("checked", true);
      } else {
        node.set("checked", false);
      }
    }
  },

  // This method is called when a user clicks on a check box in the editor window
  onTreepanelCheckChange: function (node, checked, eOpts) {
    // When a check changes on the tree, we need to figure out if the user
    // is removing from or adding to the view
    if (checked === true) {
      // This means the user actually added the platform
      Ext.Ajax.request({
        url:
          "platforms/" + node.raw._id + "?addViewName=" + this.currentViewName,
        dataType: "json",
        method: "PUT",
        success: function (data, status) {
          // Make sure there was a response
          if (data && data.responseText) {
          } else {
          }
        },
        error: function (error) {
          console.log("Error on AJAX call", error);
        },
      });
    } else if (checked === false) {
      // This means the user removed the platform
      Ext.Ajax.request({
        url:
          "platforms/" +
          node.raw._id +
          "?removeViewName=" +
          this.currentViewName,
        dataType: "json",
        method: "PUT",
        success: function (data, status) {
          // Make sure there was a response
          if (data && data.responseText) {
          } else {
          }
        },
        error: function (error) {
          console.log("Error on AJAX call", error);
        },
      });
    }
  },

  // This is called when the editor window is closed
  onWindowClose: function (panel, eOpts) {
    // Send an event that platform view was most likely updated
    this.application.fireEvent("platformViewUpdated", this);
  },
});
