/*
 * File: app/controller/ViewController.js
 *
 * This is the controller class for the view drop down box
 */
Ext.define('Odss.controller.ViewController', {
    extend: 'Ext.app.Controller',

    // Grab references
    refs: [
        {
            ref: 'odssViewComboBox',
            selector: '#odss-view-combo-box'
        }
    ],

    // This is the function that is called when the controller is initialized
    init: function (application) {
        // Link event handler
        this.control({
            "#odss-view-combo-box": {
                select: this.onViewSelect
            }
        });
    },

    // This method is called when the controller is done initializing
    onLaunch: function () {
        // Grab the current view from the application and set it on the ComboBox
        this.getOdssViewComboBox().select(Odss.app.getLastView()['name']);
    },

    // This is the method that is called when the view selection box is changed
    onViewSelect: function (combo, records, eOpts) {
        // Grab the view with that name from the store and set as current view
        Odss.app.setLastView(records[0].raw.name, records[0].raw.repoBasePath);
    }
});
