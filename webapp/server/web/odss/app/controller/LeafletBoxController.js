/*
 * File: app/controller/LeafletBoxController.js
 *
 * This is the file that is the controller for all the activity that is happening
 * on the main leaflet map
 */
Ext.define("Odss.controller.LeafletBoxController", {
    extend: "Ext.app.Controller",

    id: "leaflet-box-controller",

    // Configuration properties
    minZoom: 2,
    maxZoom: 25,

    // This function is called by the ExtJS framework when the component
    // is created
    init: function (application) {
        // A placeholder for the map
        this.map = null;

        // Create an object to keep track of the tracks that are on the map
        this.mapTracks = {};

        // Create an object that tracks the most recent icon for tracks
        this.platformLocationIcons = {};
        this.platformIcons = {};

        // These are the layers that are added to the map
        this.mapLayers = {};

        // Create an object that will track mouse position and give lat, lon and depth
        L.Control.MousePosition = L.Control.extend({
            options: {
                position: "bottomleft",
                separator: " : ",
                emptyString: "Unavailable",
                lngFirst: false,
                numDigits: 5,
                lngFormatter: undefined,
                latFormatter: undefined,
                prefix: "",
            },

            onAdd: function (map) {
                this.timer;
                this._container = L.DomUtil.create(
                    "div",
                    "leaflet-control-mouseposition"
                );
                L.DomEvent.disableClickPropagation(this._container);
                map.on("mousemove", this._onMouseMove, this);
                this._container.innerHTML = this.options.emptyString;
                return this._container;
            },

            onRemove: function (map) {
                map.off("mousemove", this._onMouseMove);
            },

            _onMouseMove: function (e) {
                var lng = this.options.lngFormatter
                    ? this.options.lngFormatter(e.latlng.lng)
                    : L.Util.formatNum(e.latlng.lng, this.options.numDigits);
                var lat = this.options.latFormatter
                    ? this.options.latFormatter(e.latlng.lat)
                    : L.Util.formatNum(e.latlng.lat, this.options.numDigits);
                var value = this.options.lngFirst
                    ? lng + this.options.separator + lat
                    : lat + this.options.separator + lng;
                var prefixAndValue = this.options.prefix + " " + value;
                this._container.innerHTML = prefixAndValue;
                clearTimeout(this.timer);
                var me = this;
                this.timer = setTimeout(function () {
                    me.mouseStopped(e);
                }, 500);
            },

            mouseStopped: function (e) {
                // Grab a handle to this object
                var me = this;

                // Now make a call out to the elevation service to get the elevation (depth)
                Ext.Ajax.request({
                    method: "GET",
                    url: "elevation",
                    params: {
                        latitude: e.latlng.lat,
                        longitude: e.latlng.lng,
                    },
                    success: function (response, options) {
                        // Make sure we have a response
                        if (response && response.responseText) {
                            var responseJSON = JSON.parse(
                                response.responseText
                            );
                            // Make sure we have data
                            if (
                                responseJSON.data &&
                                responseJSON.data.elevation
                            ) {
                                var depth = Math.round(
                                    Math.abs(
                                        Number(responseJSON.data.elevation)
                                    )
                                );
                                me._container.innerHTML += " (" + depth + " m)";
                            }
                        }
                    },
                    failure: function (response, options) {
                        console.log("Failed to get elevation");
                        console.log(response);
                    },
                });
            },
        });

        // Do not merge options from position controls (?)
        L.Map.mergeOptions({
            positionControl: false,
        });

        // This gets called during init
        L.Map.addInitHook(function () {
            if (this.options.positionControl) {
                this.positionControl = new L.Control.MousePosition();
                this.addControl(this.positionControl);
            }
        });

        // The method to build the new mouse positon control
        L.control.mousePosition = function (options) {
            return new L.Control.MousePosition(options);
        };

        // Connect up any event handlers
        this.control({
            "#set-home-button": {
                click: this.onSetHomeButtonClick,
            },
            "#go-home-button": {
                click: this.userClickedHomeButton,
            },
        });

        // Subscribe to time window changes
        // TODO kgomes - fix this before turning on
        //application.on('timewindowupdated', this.handleTimeWindowUpdated, this);
    },

    // This function is called by the ExtJS framework during application launch
    onLaunch: function () {
        var me = this;
        // First grab the ESRI API key from the session services
        Ext.Ajax.request({
            method: "GET",
            url: "/odss/session",
            success: function (response, options) {
                // Make sure we have a response
                if (response && response.responseText) {
                    var responseJSON = JSON.parse(response.responseText);
                    // Add a layer that is the ESRI ocean basemap
                    L.esri.Vector.vectorBasemapLayer("ArcGIS:Oceans:Base", {
                        apikey: responseJSON.esriApiKey,
                        pane: "tilePane",
                    }).addTo(me.map);
                }
            },
            failure: function (response, options) {
                console.log("Failed to get response from session service");
                console.log(response);
            },
        });

        // Create the Leaflet map and attach to the leaflet box component
        // The base SRS will be 3857 as that is the most common
        this.map = L.map("leaflet-box", {
            minZoom: this.minZoom,
            maxZoom: this.maxZoom,
            attributionControl: false
        }).setView([36.7852, -122.4], 10);

        // Add a scale
        L.control.scale().addTo(this.map);

        // Add the mouse tracking control
        L.control.mousePosition().addTo(this.map);

        // Change the cursor to a cross hair
        L.DomUtil.addClass(this.map._container, "crosshair-cursor-enabled");

        // Create the measurement tool
        let polylineMeasure = L.control.polylineMeasure({
            position: "topleft",
            unit: "metres",
            showBearings: true,
            clearMeasurementsOnStop: false,
            showClearControl: true,
            showUnitControl: true,
        });

        // Add it to the map
        polylineMeasure.addTo(this.map);

        // Add the zoom and move event handlers
        this.map.on("moveend", this.mapMoveOrZoomEndHandler);
        this.map.on("zoomend", this.mapMoveOrZoomEndHandler);

        // Go to the user's stored home location
        this.goToMostRecentLocation();

        // Tell the application the map is ready
        Odss.app.fireEvent("mapready");
        //     // At this point, the OpenLayersBox view has been rendered (simply a)
        //     // <div> with id of 'open-layers-box'.

        //     // Grab a reference for callbacks
        //     var me = this;

        //     // Initialize the map
        //     this.initMap();

        //     // Set the base map based on where the app is served from
        //     if (window.location.hostname === 'odss.mbari.org' ||
        //         window.location.hostname === 'normandy.mbari.org' ||
        //         window.location.hostname === 'localhost' ||
        //         window.location.hostname === 'odss-test.shore.mbari.org') {
        //         this.setBaseMap('google', window.location.hostname, function (err) {
        //             // Fire the map ready event
        //             me.application.fireEvent('mapready');
        //         });
        //     } else {
        //         this.setBaseMap('local', window.location.hostname, function (err) {
        //             // Fire the map ready event
        //             me.application.fireEvent('mapready');
        //         });
        //     }
    },

    // This is the method that handles changes in the visible time window
    handleTimeWindowUpdated: function (source) {
        // Grab a handle to this for scoping
        var me = this;

        // Loop over all the layers
        this.map.eachLayer(function (mapLayer) {
            // If it's a WMS layer, we need to update the timestamp to max sure it's before the
            // maximum
            if (mapLayer.wmsParams) {
                // Grab the layer from the store
                var storeLayer = Ext.getStore("LayerStore").findRecord(
                    "_id",
                    mapLayer.odssId
                );

                // Calculate the maximum date that should be allowed on requests
                var maxRequestDate = null;
                if (
                    storeLayer &&
                    storeLayer.data &&
                    storeLayer.data.options &&
                    storeLayer.data.options.dateMask
                ) {
                    maxRequestDate = me.calculateLayerMaxRequestDate(
                        storeLayer.data.options.dateMask
                    );
                }

                // Now check to see if that has changed since the last time
                var hasMaxRequestDateChanged = false;
                if (
                    maxRequestDate &&
                    (!mapLayer.maxRequestDate ||
                        mapLayer.maxRequestDate.getTime() !=
                            maxRequestDate.getTime())
                ) {
                    hasMaxRequestDateChanged = true;
                    mapLayer.maxRequestDate = maxRequestDate;
                }

                // Check to see if the time controller is not in live mode
                if (!Odss.model.SpaceTime.playbackMode) {
                    // We need to clear the time on the WMS request
                    delete mapLayer.wmsParams.time;

                    // And if the max request date changed on the layer, refresh it
                    if (hasMaxRequestDateChanged) mapLayer.redraw();
                } else {
                    // OK, since we are not in live mode, that means we need to add a time to
                    // the WMS request.  But, we need to make sure the time and date on the
                    // playback control is not beyond the max request date
                    var currentStartDate = source.getStartDateField().value;
                    if (currentStartDate.getTime() > maxRequestDate.getTime()) {
                        mapLayer.wmsParams.time = maxRequestDate.toISOString();
                    } else {
                        mapLayer.wmsParams.time =
                            currentStartDate.toISOString();
                    }
                    mapLayer.redraw();
                }
            }
        });
    },

    // This function handles any map moves or zooms and sets a location on the app (and
    // in cookies) of the last place the user was located
    mapMoveOrZoomEndHandler: function (e) {
        if (e && e.sourceTarget) {
            // Set the most recent location and zoom
            Odss.app.setMostRecentLocationAndZoom(
                e.sourceTarget.getCenter().lat,
                e.sourceTarget.getCenter().lng,
                e.sourceTarget.getZoom()
            );
        }
    },

    // This method handles the event when the user clicks on the 'pin' to save the current location as
    // the 'home' location (as a cookied in the browse)
    onSetHomeButtonClick: function (button, e, eOpts) {
        // Grab the center from the map
        var currentCenterLatLng = this.map.getCenter();

        // Grab the zoom
        var currentZoom = this.map.getZoom();

        // If both were found, set them on the cookie storage
        if (currentCenterLatLng !== null && currentZoom !== null) {
            // Store it in the application object
            this.application.setHomeLocationAndZoom(
                currentCenterLatLng.lat,
                currentCenterLatLng.lng,
                currentZoom
            );
        }
    },

    // This handles the event when the user clicks on the home button
    userClickedHomeButton: function (button, e, eOpts) {
        this.goToStoredHomeLocation();
    },

    // This method moves the view to the location stored as a cookie in the browser
    goToStoredHomeLocation: function () {
        // Grab the user's home location from the application object
        var homeLocation = this.application.getHomeLocationAndZoom();

        // Move the map to the home location
        this.map.setView(
            L.latLng(homeLocation.lat, homeLocation.lon),
            homeLocation.zoom
        );
    },

    // This moves the map to the user's most recent lat, lng, and zoom level
    goToMostRecentLocation: function () {
        // Grab the user's most recent location from the application object
        var location = this.application.getMostRecentLocationAndZoom();

        // Move the map to the home location
        this.map.setView(L.latLng(location.lat, location.lon), location.zoom);
    },

    // This method is used to remove a track from the map
    removeTrack: function (track) {
        // Grab the polyline from the list of lines on the map and remove it from the map
        if (this.mapTracks[track.id]) {
            this.mapTracks[track.id].remove(this.map);
            delete this.mapTracks[track.id];
        }
        // The same for the location and platform icons
        if (this.platformLocationIcons[track.id]) {
            this.platformLocationIcons[track.id].remove(this.map);
            delete this.platformLocationIcons[track.id];
        }
        if (this.platformIcons[track.id]) {
            this.platformIcons[track.id].remove(this.map);
            delete this.platformIcons[track.id];
        }
    },

    // This method is called to add a track to the map
    renderTrack: function (track) {
        // First make sure the track coming in exists
        if (track) {
            // First remove the old one
            this.removeTrack(track);

            // Make sure there is a geometry attached
            if (track.geoJSON) {
                // Create a polyline and add it to the map, and add it to the tracking object
                this.mapTracks[track.id] = L.polyline(
                    track.geoJSON.coordinates,
                    {
                        color: track.color,
                    }
                ).addTo(this.map);

                // Loop through the timestamps to find the most recent
                var mostRecentTimestamp = 0;
                var endLatLon = null;
                for (var i = 0; i < track.geoJSON.timestamps.length; i++) {
                    if (track.geoJSON.timestamps[i] > mostRecentTimestamp) {
                        mostRecentTimestamp = track.geoJSON.timestamps[i];
                        endLatLon = track.geoJSON.coordinates[i];
                    }
                }

                // If an end lat lon is found, create an icon to place there
                if (endLatLon) {
                    // Create an icon for the exact location crosshairs
                    var locationIcon = L.icon({
                        iconUrl: "resources/images/crosshairs.png",
                        iconSize: [24, 24],
                    });
                    // Create the marker
                    var locationMarker = L.marker(endLatLon, {
                        icon: locationIcon,
                    }).addTo(this.map);

                    // Calculate how old the latest fix is in minutes
                    var timeDiff = "";
                    var currentTimestamp = new Date().getTime();
                    if (track.latestTimestamp) {
                        var millisDiff =
                            currentTimestamp - track.latestTimestamp;
                        if (millisDiff > 0) {
                            timeDiff = Math.round(millisDiff / 60000) + " min";
                        }
                    }

                    // Create the label using inline styling to match platform color
                    var tooltipText =
                        track.platformAbbreviationFK +
                        " <span style='font-size: 9px;'>" +
                        timeDiff +
                        "</span>";
                    locationMarker.bindTooltip(tooltipText, {
                        permanent: true,
                        direction: "right",
                        offset: [5, 10],
                        className: "platform-name-tooltip",
                    });
                    this.platformLocationIcons[track.id] = locationMarker;

                    // If there is a platform icon specified, create one
                    if (
                        track.platformIconUrl &&
                        track.platformIconHeight &&
                        track.platformIconWidth
                    ) {
                        var platformIcon = L.icon({
                            iconUrl: track.platformIconUrl,
                            iconSize: [
                                track.platformIconWidth,
                                track.platformIconHeight,
                            ],
                            iconAnchor: [0, track.platformIconHeight],
                        });
                        this.platformIcons[track.id] = L.marker(endLatLon, {
                            icon: platformIcon,
                        }).addTo(this.map);
                    }
                }
            }
        }
    },

    // This is the function that takes in a platform and then zooms to the current extents of the track
    // for that platform
    zoomToPlatform: function (platform) {
        if (
            this.mapTracks[platform._id] &&
            this.mapTracks[platform._id].getBounds() &&
            this.mapTracks[platform._id].getBounds()["_northEast"] &&
            this.mapTracks[platform._id].getBounds()["_southWest"]
        ) {
            this.map.flyToBounds(this.mapTracks[platform._id].getBounds());
        } else {
            var noPlatformPopup = L.popup()
                .setLatLng(this.map.getCenter())
                .setContent("No track found for " + platform.name)
                .openOn(this.map);
            var me = this;
            setTimeout(function () {
                me.map.closePopup(noPlatformPopup);
            }, 3000);
        }
    },

    // This method is called to remove a layer from the map
    removeLayer: function (layer) {
        // Remove from the map
        this.mapLayers[layer._id].remove(this.map);
        delete this.mapLayers[layer._id];
    },

    // This functions removes any layers added to the map
    removeAllLayers: function () {
        // Iterate over the map layers
        for (const layerId in this.mapLayers) {
            this.mapLayers[layerId].remove(this.map);
            delete this.mapLayers[layerId];
        }
    },

    // This method is called to add a layer to the map
    addLayer: function (node, layer) {
        if (layer.type === "vector") {
            this.addVectorLayer(node, layer);
        } else if (layer.type === "wms") {
            this.addWMSLayer(node, layer);
        }

        //     // Now we need to make sure the vector and
        //     // marker layers are still on top
        //     this.map.raiseLayer(this.vectorLayer, 2);
        //     this.map.raiseLayer(this.markerLayer, 2);
    },

    addVectorLayer: function (node, layer) {
        console.log("LeafletBoxController: addVectorLayer called");
        //     // TODO kgomes, I don't think this is working (KML)

        //     // If the layer is KML format
        //     if (layer.format === "kml") {

        //         // Create the new KML layer
        //         var kml_layer = new OpenLayers.Layer.Vector(layer.name, {
        //             projection: new OpenLayers.Projection(layer.srs),
        //             protocol: new OpenLayers.Protocol.HTTP({
        //                 url: layer.url,
        //                 format: new OpenLayers.Format.KML({
        //                     extractStyles: true,
        //                     extractAttributes: true,
        //                     maxDepth: 2
        //                 })
        //             }),
        //             strategies: [new OpenLayers.Strategy.Fixed()]
        //         });

        //         // Set the ID of the layer
        //         kml_layer.id = layer.id;

        //         // Add it to the map
        //         this.map.addLayer(kml_layer);
        //     }
    },

    // This method takes in a layer from the layer store and creates a WMS layer on the map
    // using the information from the layer store.
    addWMSLayer: function (node, layer) {
        // Check to see if the layer exists
        if (layer) {
            // Now make sure we have the URL
            if (layer.url && layer.url != "") {
                // Make a copy of the layer parameters
                var layerParams = Object.assign({}, layer.params);

                // If the SRS/CRS is in text form, convert it to the object Leaflet is expecting
                if (
                    layerParams.srs == "EPSG:4326" ||
                    layerParams.crs == "EPSG:4326"
                ) {
                    delete layerParams.srs;
                    delete layerParams.crs;
                    layerParams.crs = L.CRS.EPSG4326;
                }

                // If no opacity is defined, set a default
                if (!layerParams.opacity) {
                    layerParams.opacity = 0.5;
                }
                // And same with min and max zoom
                if (!layerParams.minZoom) layerParams.minZoom = this.minZoom;
                if (!layerParams.maxZoom) layerParams.maxZoom = this.maxZoom;

                // Create the new layer using the
                var newLayer = L.tileLayer.wms(layer.url, layerParams);
                newLayer.odssId = layer._id;

                // Add the max request date
                if (layer.options && layer.options.dateMask) {
                    newLayer.maxRequestDate = this.calculateLayerMaxRequestDate(
                        layer.options.dateMask
                    );
                }

                // Set handlers to turn on and off spinners
                newLayer.on("loading", function (ev) {
                    node.set("iconCls", "x-tree-node-icon layerTreeBusy");
                });
                newLayer.on("load", function (ev) {
                    node.set("iconCls", "x-tree-node-icon");
                });

                // Add the layer to the map
                newLayer.addTo(this.map);

                // Add the layer to the tracking object
                this.mapLayers[layer._id] = newLayer;
            }
        }
    },

    // This method takes in a layer and if it can figure out the extents of the layer, it
    // will pan and zoom to fit that layer
    zoomToLayer: function (layer) {
        // Make sure we got something
        if (layer && layer.url) {
            // Now see if it's a WMS layer
            if (layer.type && layer.type.toLowerCase() == "wms") {
                // Check to see if the layer already has a bbox on it
                if (!layer.bbox) {
                    // Since the layer could have multiple wms layers defined,
                    // let's make sure we have at least one layer
                    if (layer.params && layer.params.layers) {
                        // Split into list of layers
                        var layerArray = layer.params.layers.split(",");

                        // Make sure there are some layer names to iterate over
                        if (layerArray && layerArray.length > 0) {
                            // And handle to this to use in the AJAX callback
                            var me = this;

                            // Let's make the GetCapabilities call to see if have something
                            Ext.Ajax.request({
                                method: "GET",
                                url: layer.url,
                                params: {
                                    service: "wms",
                                    request: "GetCapabilities",
                                    version: "1.1.0",
                                },
                                success: function (response, options) {
                                    // Make sure we have a response
                                    if (response && response.responseText) {
                                        // Parse the incoming XML
                                        var oParser = new DOMParser();
                                        var oDOM = oParser.parseFromString(
                                            response.responseText,
                                            "application/xml"
                                        );

                                        // Grab all the tags that are 'Name'
                                        var nameElementArray =
                                            oDOM.getElementsByTagName("Name");

                                        // Now I have all the layer nodes and all the names of the layers being
                                        // requested, we need to loop through all of them to find the largest
                                        // bounding box for this layer
                                        for (
                                            var i = 0;
                                            i < layerArray.length;
                                            i++
                                        ) {
                                            for (
                                                var j = 0;
                                                j < nameElementArray.length;
                                                j++
                                            ) {
                                                if (
                                                    nameElementArray[j]
                                                        .innerHTML ==
                                                    layerArray[i]
                                                ) {
                                                    // Now that I have the layer node with the right name, I need to walk up the parent
                                                    // chain to look for a bounding box
                                                    var parentElement =
                                                        nameElementArray[i]
                                                            .parentElement;
                                                    while (parentElement) {
                                                        // Grab the parallel bounding box tags
                                                        var boundingBoxTags =
                                                            nameElementArray[
                                                                j
                                                            ].parentElement.getElementsByTagName(
                                                                "BoundingBox"
                                                            );
                                                        // Create some variables to track max and min x's and y's
                                                        for (
                                                            var k = 0;
                                                            k <
                                                            boundingBoxTags.length;
                                                            k++
                                                        ) {
                                                            if (
                                                                boundingBoxTags[
                                                                    k
                                                                ].getAttribute(
                                                                    "minx"
                                                                )
                                                            ) {
                                                                if (!layer.bbox)
                                                                    layer.bbox =
                                                                        {};
                                                                if (
                                                                    !layer.bbox
                                                                        .minx ||
                                                                    layer.bbox
                                                                        .minx >
                                                                        Number(
                                                                            boundingBoxTags[
                                                                                k
                                                                            ].getAttribute(
                                                                                "minx"
                                                                            )
                                                                        )
                                                                ) {
                                                                    layer.bbox.minx =
                                                                        Number(
                                                                            boundingBoxTags[
                                                                                k
                                                                            ].getAttribute(
                                                                                "minx"
                                                                            )
                                                                        );
                                                                }
                                                            }
                                                            if (
                                                                boundingBoxTags[
                                                                    k
                                                                ].getAttribute(
                                                                    "miny"
                                                                )
                                                            ) {
                                                                if (!layer.bbox)
                                                                    layer.bbox =
                                                                        {};
                                                                if (
                                                                    !layer.bbox
                                                                        .miny ||
                                                                    layer.bbox
                                                                        .miny >
                                                                        Number(
                                                                            boundingBoxTags[
                                                                                k
                                                                            ].getAttribute(
                                                                                "miny"
                                                                            )
                                                                        )
                                                                ) {
                                                                    layer.bbox.miny =
                                                                        Number(
                                                                            boundingBoxTags[
                                                                                k
                                                                            ].getAttribute(
                                                                                "miny"
                                                                            )
                                                                        );
                                                                }
                                                            }
                                                            if (
                                                                boundingBoxTags[
                                                                    k
                                                                ].getAttribute(
                                                                    "maxx"
                                                                )
                                                            ) {
                                                                if (!layer.bbox)
                                                                    layer.bbox =
                                                                        {};
                                                                if (
                                                                    !layer.bbox
                                                                        .maxx ||
                                                                    layer.bbox
                                                                        .maxx <
                                                                        Number(
                                                                            boundingBoxTags[
                                                                                k
                                                                            ].getAttribute(
                                                                                "maxx"
                                                                            )
                                                                        )
                                                                ) {
                                                                    layer.bbox.maxx =
                                                                        Number(
                                                                            boundingBoxTags[
                                                                                k
                                                                            ].getAttribute(
                                                                                "maxx"
                                                                            )
                                                                        );
                                                                }
                                                            }
                                                            if (
                                                                boundingBoxTags[
                                                                    k
                                                                ].getAttribute(
                                                                    "maxy"
                                                                )
                                                            ) {
                                                                if (!layer.bbox)
                                                                    layer.bbox =
                                                                        {};
                                                                if (
                                                                    !layer.bbox
                                                                        .maxy ||
                                                                    layer.bbox
                                                                        .maxy <
                                                                        Number(
                                                                            boundingBoxTags[
                                                                                k
                                                                            ].getAttribute(
                                                                                "maxy"
                                                                            )
                                                                        )
                                                                ) {
                                                                    layer.bbox.maxy =
                                                                        Number(
                                                                            boundingBoxTags[
                                                                                k
                                                                            ].getAttribute(
                                                                                "maxy"
                                                                            )
                                                                        );
                                                                }
                                                            }
                                                        }
                                                        // If bounding box was found, bail out
                                                        if (layer.bbox) {
                                                            parentElement =
                                                                null;
                                                            break;
                                                        } else {
                                                            // No bbox was found, go up a parent
                                                            parentElement =
                                                                parentElement.parentElement;
                                                        }
                                                    }

                                                    // Make sure we have the bounds
                                                    if (
                                                        layer.bbox &&
                                                        layer.bbox.minx &&
                                                        layer.bbox.miny &&
                                                        layer.bbox.maxx &&
                                                        layer.bbox.maxy
                                                    ) {
                                                        // Create bounds object
                                                        var minLatLng =
                                                            L.latLng(
                                                                layer.bbox.miny,
                                                                layer.bbox.minx
                                                            );
                                                        var maxLatLng =
                                                            L.latLng(
                                                                layer.bbox.maxy,
                                                                layer.bbox.maxx
                                                            );
                                                        var bounds =
                                                            L.latLngBounds(
                                                                minLatLng,
                                                                maxLatLng
                                                            );
                                                        me.map.flyToBounds(
                                                            bounds
                                                        );
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                },
                                failure: function (response, options) {
                                    console.log(
                                        "Failed to retrieve GetCapabilites from " +
                                            layer.url
                                    );
                                    console.log(response);
                                    console.log(options);
                                },
                            });
                        }
                    }
                } else {
                    // Since the bbox was already defined, make sure all the maxs and mins are there.
                    if (
                        layer.bbox &&
                        layer.bbox.minx &&
                        layer.bbox.miny &&
                        layer.bbox.maxx &&
                        layer.bbox.maxy
                    ) {
                        // Create bounds object
                        var minLatLng = L.latLng(
                            layer.bbox.miny,
                            layer.bbox.minx
                        );
                        var maxLatLng = L.latLng(
                            layer.bbox.maxy,
                            layer.bbox.maxx
                        );
                        var bounds = L.latLngBounds(minLatLng, maxLatLng);
                        this.map.flyToBounds(bounds);
                    }
                }
            }
        }
    },

    // This function takes in a layer from the layer store and calculates the max date that should
    // be allowed in requests
    calculateLayerMaxRequestDate: function (dateMask) {
        // Grab the current time as the max layer date to start
        var layerMaxDate = new Date();
        // Set millis to zero
        layerMaxDate.setMilliseconds(0);

        // Check to see if there is a date mask attached to the layer
        if (dateMask) {
            // Use a regular expression to pull the all the fields out of the mask into an array
            var maskRE = /(.*)-(.*)-(.*)T(.*):(.*):(.*)Z/;
            var maskArray = maskRE.exec(dateMask);

            // Make sure it parsed OK and if so, replace all the fields in the current date with those
            // from the mask and figure out the smallest granularity of changes from the mask
            if (maskArray && maskArray.length > 0) {
                // Granularity (yearly|monthly|daily|hourly|minutely|secondly)
                var granularity = null;

                // Now looking through the array, let's figure out which fields need to be replaced
                if (maskArray[1].toLowerCase() == "yyyy") {
                    granularity = "yearly";
                } else {
                    layerMaxDate.setUTCFullYear(Number(maskArray[1]));
                }
                if (maskArray[2].toLowerCase() == "mm") {
                    granularity = "monthly";
                } else {
                    layerMaxDate.setUTCMonth(Number(maskArray[2]) - 1);
                }
                if (maskArray[3].toLowerCase() == "dd") {
                    granularity = "daily";
                } else {
                    layerMaxDate.setUTCDate(Number(maskArray[3]));
                }
                if (maskArray[4].toLowerCase() == "hh") {
                    granularity = "hourly";
                } else {
                    layerMaxDate.setUTCHours(Number(maskArray[4]));
                }
                if (maskArray[5].toLowerCase() == "mm") {
                    granularity = "minutely";
                } else {
                    layerMaxDate.setUTCMinutes(Number(maskArray[5]));
                }
                if (maskArray[6].toLowerCase() == "ss") {
                    granularity = "secondly";
                } else {
                    layerMaxDate.setUTCSeconds(Number(maskArray[6]));
                }

                // Now we need to see if the layer max date is in the future and if so, we need to subtract by the smallest granularity
                var now = new Date();
                if (layerMaxDate.getTime() > now.getTime()) {
                    // Make sure there is some granularity defined
                    if (granularity) {
                        while (layerMaxDate.getTime() > now.getTime()) {
                            if (granularity == "yearly") {
                                layerMaxDate.setUTCFullYear(
                                    layerMaxDate.getUTCFullYear - 1
                                );
                            }
                            if (granularity == "monthly") {
                                layerMaxDate.setUTCMonth(
                                    layerMaxDate.getUTCMonth() - 1
                                );
                            }
                            if (granularity == "daily") {
                                layerMaxDate.setUTCDate(
                                    layerMaxDate.getUTCDate() - 1
                                );
                            }
                            if (granularity == "hourly") {
                                layerMaxDate.setUTCHours(
                                    layerMaxDate.getUTCHours() - 1
                                );
                            }
                            if (granularity == "minutely") {
                                layerMaxDate.setUTCMinutes(
                                    layerMaxDate.getUTCMinutes() - 1
                                );
                            }
                            if (granularity == "secondly") {
                                layerMaxDate.setUTCSeconds(
                                    layerMaxDate.getUTCSeconds() - 1
                                );
                            }
                        }
                    } else {
                        // I guess just set to the current date time
                        layerMaxDate = new Date();
                    }
                }
            }
        }

        // Return the calculated date
        return layerMaxDate;
    },
});
