/*
 * File: app/controller/DataSourceController.js
 *
 * This class controls the inteaction betwen the data tree grid
 * panel and the rest of the application.
 */
Ext.define('Odss.controller.DataSourceController', {
    extend: 'Ext.app.Controller',

    refs: [
        {
            ref: 'dataSourceTreeGrid',
            selector: 'datasourcetreegrid',
            xtype: 'treepanel'
        },
        {
            ref: 'mainTabPanel',
            selector: '#maintabpanel',
            xtype: 'Ext.tab.Panel'
        }
    ],

    // This function gets called when the controller is initialized
    init: function (application) {
        // Attach a handler to the application event 'viewselected'
        Odss.app.on('viewselected', this.handleViewSelection, this);

        // Attach handlers to items on the data source tree grid
        this.control({
            "#refresh-datasource-button": {
                click: this.onRefreshDataSourceButtonClick
            },
            "datasourcetreegrid": {
                itemappend: this.onTreepanelItemAppend,
                itemclick: this.onTreepanelItemClick
            }
        });
    },

    // This method is called after the controller is initialized and ready to go
    onLaunch: function () {
        // The application has started and the current view was initialized
        // so let's set it on the DataSource
        this.setDataSourceView(
            Odss.app.getLastView()['name'],
            Odss.app.getLastView()['repoBasePath']
        );
    },

    // This method gets called when the user clicks on the refresh button
    onRefreshDataSourceButtonClick: function (button, e, eOpts) {
        this.getDataSourceTreeGrid().getStore().load();
    },

    // This method is called when an item is added to the tree grid and simply adds a tooltip
    onTreepanelItemAppend: function (nodeinterface, node, index, eOpts) {
        node.set('qtip', 'Click to open ' + node.raw.name + ' in new browser window');
    },

    // This method is called when the user clicks on an item in the data tree grid
    onTreepanelItemClick: function (dataview, record, item, index, e, eOpts) {
        // Just open the URL link in a new window
        window.open(record.raw.url);
    },

    // This method is called when the app signals a view was selected
    handleViewSelection: function (source, viewName) {
        // Call the method to set the view name for the DataSource
        this.setDataSourceView(
            Odss.app.getLastView()['name'],
            Odss.app.getLastView()['repoBasePath']
        );
    },

    // This method sets the name and repo base path on the data store and loads it
    setDataSourceView: function (viewName, repoBasePath) {
        // Now if the repo base path exists, set it on the store and load it
        if (repoBasePath) {
            Ext.getStore('DataSourceStore').getProxy().extraParams = {
                repoBasePath: repoBasePath
            };
            Ext.getStore('DataSourceStore').load();
        }
    }
});
