/*
 * File: app/controller/TrackController.js
 *
 * This is the controller for handling logic for the tracks
 */
Ext.define('Odss.controller.TrackController', {
    extend: 'Ext.app.Controller',

    // Set the default number of points in each track
    numPointsPerTrack: '500',

    // Grab model object that will be needed
    models: [
        'Track',
        'SpaceTime'
    ],

    // Grab the track store
    stores: [
        'TrackStore'
    ],

    // This method is called when the controller is initialized
    init: function (application) {
        // Subscribe to time window changes
        application.on('timewindowupdated', this.handleTimeWindowUpdated, this);

        // Subscribe to platform selection events
        application.on('platformselected', this.handlePlatformSelected, this);

        // Subscribe to platform deselection events
        application.on('platformdeselected', this.handlePlatformDeselected, this);
    },

    // This method gets called when the window of time changes
    handleTimeWindowUpdated: function (source) {
        this.refreshTracksFromServer();
    },

    // This method is called when a platform is selected in the tree
    handlePlatformSelected: function (platform) {

        // Look for the track by it's ID
        var track = this.getTrackStoreStore().data.getByKey(platform._id);

        // If a track was not found, create a new one and stuff it in the store
        if (typeof track === 'undefined' || track === null) {

            // We need to create a new track
            track = Ext.create('Odss.model.Track');
            track.id = platform._id;
            track.platformIdFK = platform._id;
            track.platformNameFK = platform.name;
            track.platformAbbreviationFK = platform.abbreviation;
            track.platformTypeName = platform.typeName;
            track.platformIconUrl = platform.iconUrl;
            track.platformIconHeight = platform.iconHeight;
            track.platformIconWidth = platform.iconWidth;
            track.platformDescription = platform.description;
            track.color = platform.color;

            // Stuff it in the store
            this.getTrackStoreStore().data.add(track.id, track);
        }

        // Now makes sure it's track is up to date and rendered
        this.getTrackFromServerAndRender(track);
    },

    // This method is called when a track is deselected in the tree
    handlePlatformDeselected: function (platform) {

        // Look for the track in the store by it's ID
        var track = this.getTrackStoreStore().data.getByKey(platform._id);

        // If a track was found, remove it from the store
        if (typeof track !== 'undefined') {
            Odss.app.getController('LeafletBoxController').removeTrack(track);
            this.getTrackStoreStore().data.removeAtKey(track.id);
        }
    },

    // This method is used to retrieve the track geometry from the server and
    // then to add it to the map
    getTrackFromServerAndRender: function (trackToGet) {
        // Make sure the track is there
        if (typeof trackToGet !== "undefined" && trackToGet !== null) {

            // Create the request object
            var requestParams = {
                platformID: trackToGet.id,
                startDate: Odss.model.SpaceTime.timeWindowStartDatetime.toISOString(),
                endDate: Odss.model.SpaceTime.timeWindowEndDatetime.toISOString(),
                sortOrder: 'desc',
                numberOfPoints: this.numPointsPerTrack,
                taperAccuracy: true
            };

            // Add the request to the AjaxController
            Odss.app.getController('AjaxController').addRequestToQueue('GET', "tracks", requestParams, this);
        }
    },

    // This method is called to refresh all the track in the store
    refreshTracksFromServer: function () {
        // Grab all the tracks from the store
        var tracks = this.getTrackStoreStore().data;

        // For each track, call the getTrackFromServer method
        tracks.each(this.getTrackFromServerAndRender, this);
    },

    // This is the method that is called when the queued request to get the track
    // geometry from the server is successful
    successHandler: function (response, request) {
        // This is the geometry that will be created from response from server.
        // This will end up being a OpenLayers object so it creates a dependency
        // on OpenLayers
        //var geometry = null;

        // The latest timestamp on the track
        var latestTrackTimestamp = null;

        // First let's parse the reponse text into a JSON object
        var parsedResponse = JSON.parse(response.responseText);

        // Make sure the response text parsed OK
        if (typeof parsedResponse !== 'undefined' && parsedResponse !== null) {
            // Now check to see if there is any data associated with it
            if (typeof parsedResponse.data !== 'undefined' && parsedResponse.data !== null) {
                // This is an object that will hold the raw GeoJSON. The 'geometry'
                // object is one that is parsed using OpenLayers and is dependent on
                // OpenLayers
                var geoJSON = {
                    type: parsedResponse.data.type.slice(),
                    timestamps: parsedResponse.data.timestamps.slice(),
                    coordinates: []
                };

                // There is a funky mismatch between the /tracks call returns and the GeoJSON
                // Leaflet is expecting so I have to do a bit of work to support the Leaflet
                // map. First grab the array of coordinates
                for (var i = 0; i < parsedResponse.data.coordinates.length; i++) {
                    var tx = proj4('EPSG:3857', 'EPSG:4326').forward(parsedResponse.data.coordinates[i]);
                    geoJSON.coordinates.push([tx[1], tx[0]]);
                }

                // Now convert it to an OpenLayers GeoJSON object
                //geometry = this.geoJSONParser.read(parsedResponse.data, "Geometry");

                // Grab the latest timestamp
                if (parsedResponse.data.timestamps.length > 0) {
                    latestTrackTimestamp = parsedResponse.data.timestamps[0];
                }
                // Grab the track object from the store
                var matchingTrack = this.getTrackStoreStore().data.getByKey(request.params.platformID);

                // There can be removals that happen after this call, just make sure it exists
                if (typeof matchingTrack !== "undefined" && matchingTrack !== null) {

                    // Delete the existing raw GeoJSON and add the new
                    delete matchingTrack.geoJSON;
                    matchingTrack.geoJSON = geoJSON;

                    // Delete any existing geometry and add the OpenLayers object
                    // delete matchingTrack.trackGeometry;
                    // matchingTrack.trackGeometry = geometry;

                    // Set the latest timestamp
                    matchingTrack.latestTimestamp = latestTrackTimestamp;

                    // Now render it
                    //Odss.app.getController('OpenLayersBoxController').renderTrack(matchingTrack);
                    Odss.app.getController('LeafletBoxController').renderTrack(matchingTrack);
                } else {
                    console.log("Look like a track was removed from under me for platform " + request.params.platformID);
                }
            }
        }
    },

    failureHandler: function (response) {
        console.log("Track could not be retrieved for platform");
    }

});
