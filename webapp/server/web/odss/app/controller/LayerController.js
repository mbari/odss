/*
 * File: app/controller/LayerController.js
 *
 * This is the controller that handles the business logic of
 * the layer list box in the ODSS.
 */
Ext.define('Odss.controller.LayerController', {
    extend: 'Ext.app.Controller',

    // Grab a handles to components needed in the code
    refs: [
        {
            ref: 'layerTreePanelAddButton',
            selector: '#layer-tree-panel-add-button'
        }
    ],

    // This is the function that is called when the controller is initialed
    init: function (application) {
        // Link handlers to components in the layer panel
        this.control({
            "layerstreepanel": {
                itemappend: this.onTreepanelItemAppend,
                checkchange: this.onTreepanelCheckChange,
                itemclick: this.onItemClickHandler
            },
            "#layer-tree-panel-add-button": {
                click: this.onLayerAddButtonClick
            },
            "#layer-cancel-button": {
                click: this.onLayerCancelButtonClick
            },
            "#layer-save-button": {
                click: this.onLayerSaveButtonClick
            }
        });

        // Subscribe to time window changes
        application.on('timewindowupdated', this.handleTimeWindowUpdated, this);

        // Subscribe to layer updates
        application.on('layerupdated', this.handleLayerUpdates, this);

        // Subscribe to view selections
        application.on('viewselected', this.handleViewSelection, this);
    },

    // This method gets called when the controller is fully launched
    onLaunch: function() {
        // Set the view to the last view the user had
        this.setViewName();

        // Load all layers in the layer store
        Ext.getStore('LayerStore').load();
        
    },

    // This method gets called when an item gets added to the layer panel
    onTreepanelItemAppend: function (nodeinterface, node, index, eOpts) {
        // If the node that is being appended has an _id field, it is a layer
        if (node.raw._id) {
            // Turn off the check
            node.set('checked', false);

            // Set that it is a leaf
            node.set('leaf', true);

            // Add a tooltip target that should be the url of the legend
            if (typeof node.raw.legendurl !== "undefined" && node.raw.legendurl !== null && node.raw.legendurl !== "") {
                // Create the new tooltip
                node.set('qtitle', 'Legend for ' + node.raw.name);
                node.set('qtip', "<img width='400' height='100' src='" + node.raw.legendurl + "'/>");
            } else {
                node.set('qtip', "No legend available");
            }
        }
    },

    // This method is called when the user clicks on a check box
    onTreepanelCheckChange: function (node, checked, eOpts) {
        // When a check changes on the tree, we want to fire
        // the correct application event
        if (checked === true) {
            // Now add the layer
            Odss.app.getController('LeafletBoxController').addLayer(node, node.raw);
        } else if (checked === false) {
            Odss.app.getController('LeafletBoxController').removeLayer(node.raw);
        }
    },

    // This method gets called when the user click on an item in the tree, but not on the checkbox
    onItemClickHandler: function (scope, record, item, index, e, eOpts) {
        // Send the node to the map controller to see if it can pan an zoom to that item
        if (record && e && e.target && e.target.textContent && e.target.textContent.length > 0) {
            Odss.app.getController('LeafletBoxController').zoomToLayer(record.raw);
        }
    },

    // This method is called when the user clicks on the button to add a new layer
    onLayerAddButtonClick: function (button, e, eOpts) {
        // Check to see if the layer form window already exists
        if (typeof this.layerFormWindow === 'undefined' || this.layerFormWindow === null) {
            this.layerFormWindow = new Odss.view.LayerFormWindow();
        }

        // Now open the window
        if (this.layerFormWindow.isVisible() === false) {
            this.layerFormWindow.show();
        }
    },

    // This method is called when the user cancels the form to add a new layer
    onLayerCancelButtonClick: function (button, e, eOpts) {
        // Dismiss the window if it exists
        if (typeof this.layerFormWindow !== 'undefined' && this.layerFormWindow !== null) {
            this.layerFormWindow.hide();
        }
    },

    // This method is called when the user clicks on the button to save the new layer
    onLayerSaveButtonClick: function (button, e, eOpts) {
        // Grab the layer store
        var layerStore = Ext.getStore('LayerStore');

        // Make sure the layer form window exists
        if (this.layerFormWindow !== null) {

            // Grab the values from the form
            var formValues = this.layerFormWindow.down('form').getValues();

            // The record that will be saved
            var recordToSave = null;

            // Check to see if the ID hidden field was defined
            if (formValues.id && formValues.id !== '') {
                // If there is a value here, we are updating a record
                var indexOfRecord = layerStore.find('_id', formValues.id);
                if (indexOfRecord > -1) {
                    recordToSave = layerStore.getAt(indexOfRecord);
                }
            } else {
                // Create a new record
                recordToSave = Ext.create('Odss.model.Layer');
                recordToSave.set('params', {});
                recordToSave.set('options', {});
                recordToSave.set('views', []);

                // Set some defaults
                var defaultValues = {
                    params: {
                        exceptions: 'application/vnd.ogc.se_iniimage',
                        bgcolor: '0x808080',
                        format: 'image/png'
                    }
                };
                recordToSave.set(defaultValues);
            }

            // Create an object to hold the form values in the correct structure
            var valuesObject = {
                name: formValues.name,
                type: formValues.type,
                url: formValues.url,
                legendUrl: formValues.legendUrl,
                params: {
                    map: formValues.map,
                    service: formValues.service,
                    version: formValues.version,
                    request: formValues.request,
                    srs: formValues.srs,
                    layers: formValues.layers,
                    elevation: formValues.elevation,
                    transparent: formValues.transparent
                },
                options: {
                    timeformat: formValues.timeFormat,
                    hoursbetween: formValues.hoursBetween,
                    minhoursback: formValues.minHoursBack
                }
            };

            // Check the public box
            if (formValues['public'] && formValues['public'] === 'on') {
                // This means the layer should be in the view 'Public' so
                // loop over the views and try to find the Public view
                var publicFound = false;
                recordToSave.get('views').forEach(function (view) {
                    if (view.name === 'Public') {
                        publicFound = true;
                    }
                });
                // Check to see if it needs to be added
                if (!publicFound) {
                    recordToSave.get('views').push({
                        name: 'Public',
                        folder: formValues.publicFolder
                    });
                }
            } else {
                // We need to make sure it is not in the 'Public' view
            }

            // Now set those values on the layer object
            recordToSave.set(valuesObject);

            // If the record has an ID, it exists and needs to be marked dirty
            if (recordToSave._id && recordToSave._id !== '') {
                recordToSave.dirty = true;
            } else {
                layerStore.add(recordToSave);
            }

            // Now sync the store
            layerStore.sync();

            // Fire an event that a layer was updated
            this.application.fireEvent('layerupdated', this, recordToSave);
        }
    },

    // This is a helper method for date formatting
    convertDateUsingOptions: function (dateToFormat, options) {
        // Make sure parameter exists
        if (typeof dateToFormat != 'undefined' && dateToFormat !== null) {

            // Make a copy of the incoming date so as not to change the original
            var dateToReturn = new Date();
            dateToReturn.setTime(dateToFormat.getTime());

            // Let's check to make sure the date is far enough in the past if
            // the minhoursback is specified
            if (options && options.minHoursBack && typeof options.minhoursback != 'undefined' && options.minhoursback !== null) {

                // Get a difference between now and the start time specified
                var hoursdiff = (Date.now().getTime() - dateToReturn.getTime()) / (1000 * 60 * 60);

                // Check to see if it needs adjusting
                if (hoursdiff < options.minhoursback) {

                    // Calculate how many hours back we need to go
                    var hoursToGoBack = options.minhoursback - hoursdiff;

                    // Substract enough hours to get us to the minimum
                    dateToReturn.setTime(dateToReturn.getTime() - hoursToGoBack * 1000 * 60 * 60);
                }
            }

            // Check to see if the options specify a time format
            if (options && options.timeformat && typeof options.timeformat != 'undefined' && options.timeformat !== null) {
                return dateToReturn.format(options.timeformat);
            } else {
                // If not, just use the ODSS standard UTC format
                return this.application.convertDateToISO(dateToReturn);
            }
        } // End check incoming date
    },

    // This method gets called when the time window in the space time controller changes
    handleTimeWindowUpdated: function (source) {
        // Grab the list of layers from the map that are WMS layers
        // var wms_layers =
        //     Odss.app.getController('OpenLayersBoxController').map.getLayersByClass('OpenLayers.Layer.WMS');
        // TODO kgomes - need to add the parallel function for the LeafletBoxController

        // Loop over the list and merge the udpated start time if we are in playback mode
        // if (Odss.model.SpaceTime.playbackMode) {
        //     for (var i in wms_layers) {

        //         // Grab the layer
        //         var layer_to_update = wms_layers[i];

        //         // Merge the time if we are in playback mode
        //         var convertedDate =
        //             this.convertDateUsingOptions(Odss.model.SpaceTime.timeWindowStartDatetime, layer_to_update.options);
        //         layer_to_update.mergeNewParams({ time: convertedDate });
        //     }
        // }
    },

    // This method gets called when layers are updated
    handleLayerUpdates: function (source, layer) {
        // Grab the layer and layer tree store and reload it
        Ext.getStore('LayerStore').load();
        Ext.getStore('LayerTreeStore').load();
    },

    // This method handles the event when the view is changed
    handleViewSelection: function (source, viewName) {
        // Set the view name
        this.setViewName();
    },


    setViewName: function () {
        // Remove all the layers that are on now
        Odss.app.getController('LeafletBoxController').removeAllLayers();

        // Grab the view name from the application
        var viewName = Odss.app.getLastView()['name'];

        // Grab the LayerTreeStore and set the view name
        Ext.getStore('LayerTreeStore').getProxy().extraParams = {
            viewName: viewName,
            treeView: true,
            groupByFolder: true
        };

        // Now reload the tree store
        Ext.getStore('LayerTreeStore').load();
    }
});
