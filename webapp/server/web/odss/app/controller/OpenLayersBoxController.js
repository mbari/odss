/*
 * File: app/controller/OpenLayersBoxController.js
 *
 * This is the controller for the OpenLayers box
 */
Ext.define('Odss.controller.OpenLayersBoxController', {
    extend: 'Ext.app.Controller',

    id: 'open-layers-box-controller',

    // onContainerMove: function (component, x, y, eOpts) {
    //     console.log('OpenLayersBoxController: onContainerMove called');
    //     // Make sure the map is not null
    //     if (component !== null) {

    //         // Make sure it is the open layers box
    //         if (component.getId() === 'open-layers-box') {
    //             // Grab the bounding box
    //             var bbox = this.getBBox();

    //             // Make sure it is there
    //             if (bbox !== null) {

    //                 // A flag to indicate if the values are different that what is in the current space time model
    //                 var valuesChanged = false;

    //                 // Compare the BBox to make sure it actually changed
    //                 if (bbox.top !== Odss.model.SpaceTime.bBoxTop) {
    //                     valuesChanged = true;
    //                 }
    //                 if (bbox.bottom != Odss.model.SpaceTime.bBoxBottom) {
    //                     valuesChanged = true;
    //                 }
    //                 if (bbox.left != Odss.model.SpaceTime.bBoxLeft) {
    //                     valuesChanged = true;
    //                 }
    //                 if (bbox.right != Odss.model.SpaceTime.bBoxRight) {
    //                     valuesChanged = true;
    //                 }

    //                 // Fire an event if extents changed
    //                 if (valuesChanged) {
    //                     this.application.fireEvent('bboxupdated');
    //                 }
    //             }
    //         }
    //     }
    // },

    // onContainerResize: function (component, width, height, oldWidth, oldHeight, eOpts) {
    //     console.log('OpenLayersBoxController: onContainerResize called');
    //     // Make sure the map resizes too
    //     this.resizeMap();
    // },

    onSetHomeButtonClick: function (button, e, eOpts) {
        console.log('OpenLayersBox set home called');
        // Let's first see if the open layers box exists
        if (Ext.getCmp('open-layers-box')) {
            // Now make sure it's the first child of the map panel (i.e. it's the visible one)
            if (Ext.getCmp('odss-map-panel').items.keys.indexOf('open-layers-box') == 0) {
                // Grab the center from the map
                var currentCenter = this.getCenter();

                // Grab the zoom
                var currentZoom = this.getZoom();

                // If both were found, set them on the application
                if (currentCenter !== null && currentZoom !== null) {
                    Odss.app.setHomeLocationAndZoom(
                        currentCenter.lat,
                        currentCenter.lon,
                        currentZoom
                    );
                }
            }
        }
    },

    userClickedHomeButton: function (button, e, eOpts) {
        // Grab the home location from the cookie and return an MBARI biased default
        var homeLocation = Odss.app.getHomeLocationAndZoom();

        // Move the map to the home location
        this.setCenterAndZoom(homeLocation.lat, homeLocation.lon, homeLocation.zoom);
    },

    onLaunch: function () {
        // At this point, the OpenLayersBox view has been rendered (simply a)
        // <div> with id of 'open-layers-box'.

        // Grab a reference for callbacks
        var me = this;

        // Initialize the map
        this.initMap();

        // Set the base map based on where the app is served from
        if (window.location.hostname === 'odss.mbari.org' ||
            window.location.hostname === 'normandy.mbari.org' ||
            window.location.hostname === 'localhost' ||
            window.location.hostname === 'odss-test.shore.mbari.org') {
            this.setBaseMap('google', window.location.hostname, function (err) {
                // Fire the map ready event
                me.application.fireEvent('mapready');
            });
        } else {
            this.setBaseMap('local', window.location.hostname, function (err) {
                // Fire the map ready event
                me.application.fireEvent('mapready');
            });
        }
    },

    addFeature: function (id, geometry, color) {
        // Now create the feature
        var vectorFeature = new OpenLayers.Feature.Vector(geometry, {
            fillcolor: color
        });

        // Make sure was constructed
        if (vectorFeature !== null) {

            // Set the ID
            vectorFeature.id = id;

            // Add it to the map
            this.vectorLayer.addFeatures([vectorFeature]);
        }

    },

    addLayer: function (node, layer) {
        if (layer.type === 'vector') {
            this.addVectorLayer(node, layer);
        } else if (layer.type === 'wms') {
            this.addWMSLayer(node, layer);
        }

        // Now we need to make sure the vector and
        // marker layers are still on top
        this.map.raiseLayer(this.vectorLayer, 2);
        this.map.raiseLayer(this.markerLayer, 2);

    },

    addMarker: function (longitude, latitude, onMouseDownCallback) {
        // Create the open layers latitude and longitude using decimal degrees
        var olLonLat = new OpenLayers.LonLat(longitude, latitude);

        // Transform it to the correct projection
        var location = olLonLat.transform(this.proj4326, this.proj900913);

        // The size of the marker
        var size = new OpenLayers.Size(21, 25);

        // The offset from the actual location
        var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);

        // The icon to be used
        var icon = new OpenLayers.Icon('resources/images/location.png', size, offset);

        // Create the new marker
        var newMarker = new OpenLayers.Marker(location, icon);

        // Add callback function if it exists
        if (typeof onMouseDownCallback != 'undefined' && onMouseDownCallback !== null) {
            newMarker.events.register('mousedown', newMarker, onMouseDownCallback);
        }
        // Create the marker at the given location
        this.markerLayer.addMarker(newMarker);

        // Return the newly created marker
        return newMarker;

    },

    addVectorLayer: function (node, layer) {
        // TODO kgomes, I don't think this is working (KML)

        // If the layer is KML format
        if (layer.format === "kml") {

            // Create the new KML layer
            var kml_layer = new OpenLayers.Layer.Vector(layer.name, {
                projection: new OpenLayers.Projection(layer.srs),
                protocol: new OpenLayers.Protocol.HTTP({
                    url: layer.url,
                    format: new OpenLayers.Format.KML({
                        extractStyles: true,
                        extractAttributes: true,
                        maxDepth: 2
                    })
                }),
                strategies: [new OpenLayers.Strategy.Fixed()]
            });

            // Set the ID of the layer
            kml_layer.id = layer.id;

            // Add it to the map
            this.map.addLayer(kml_layer);
        }
    },

    addWMSLayer: function (node, layer) {
        // Create the new layer using the properties from the incoming layer object
        var wms_layer = new OpenLayers.Layer.WMS(layer.name, layer.url, layer.params, layer.options);

        // Attach the node
        wms_layer.node = node;

        // Set an ID
        wms_layer.id = layer._id;

        // Make sure it is not a base layer
        wms_layer.isBaseLayer = false;

        // Set the opacity
        var opacity = layer.params.opacity || 0.5;
        wms_layer.setOpacity(opacity);

        // Register events (I commented these out and moved them to the LeafletBoxController)
        // wms_layer.events.register("loadstart", wms_layer, function () {
        //     this.node.set('iconCls', 'x-tree-node-icon layerTreeBusy');
        // });
        // wms_layer.events.register("loadend", wms_layer, function () {
        //     this.node.set('iconCls', 'x-tree-node-icon');
        // });

        // Add the layer
        this.map.addLayer(wms_layer);
    },

    // getBBox: function () {
    //     if ((typeof this.map !== "undefined") && this.map !== null) {
    //         return this.map.getExtent();
    //     } else {
    //         return null;
    //     }
    // },

    getCenter: function () {
        // Make sure the map exists
        if ((typeof this.map !== "undefined") && this.map !== null && this.map.getCenter() !== null) {
            // Grab the center location
            var center900913 = this.map.getCenter();

            // Reproject into 4326 for convenience
            var center4326 = center900913.transform(this.proj900913, this.proj4326);

            // Now return it
            return center4326;
        } else {
            return null;
        }
    },

    getFeatureById: function (id) {
        return this.vectorLayer.getFeatureById(id);
    },

    // getLRCorner: function (in4326, scale) {
    //     // The point to return
    //     var lrCornerPoint = null;

    //     // Make sure map exists
    //     if ((typeof this.map !== "undefined") && this.map !== null) {
    //         // Grab the viewbox
    //         var viewbox = this.map.getExtent();

    //         // Make sure we actually have a viewbox
    //         if (typeof viewbox !== "undefined" && viewbox !== null) {
    //             // Check to see if the user wants to scale the box
    //             if (typeof scale !== 'undefined' && scale !== null && scale !== 1.0) {
    //                 viewbox = viewbox.scale(scale);
    //             }
    //             // Create a point using the bounding box
    //             lrCornerPoint = new OpenLayers.LonLat(viewbox.right, viewbox.bottom);

    //             // Now if it is to be transformed to lat/lon do that
    //             if (in4326 === true) {
    //                 lrCornerPoint = lrCornerPoint.transform(this.proj900913, this.proj4326);
    //             }
    //         }
    //     }

    //     // Return it
    //     return lrCornerPoint;
    // },

    // getULCorner: function (in4326, scale) {
    //     // The point to return
    //     var ulCornerPoint = null;

    //     // Make sure the map exists
    //     if ((typeof this.map !== "undefined") && this.map !== null) {
    //         // Grab the viewbox
    //         var viewbox = this.map.getExtent();

    //         // Make sure we actually have a viewbox
    //         if (typeof viewbox !== "undefined" && viewbox !== null) {
    //             // Check to see if the user wants to scale the box
    //             if (typeof scale !== 'undefined' && scale !== null && scale !== 1.0) {
    //                 viewbox = viewbox.scale(scale);
    //             }
    //             // Create a point using the bounding box
    //             ulCornerPoint = new OpenLayers.LonLat(viewbox.left, viewbox.top);

    //             // Now if it is to be transformed to lat/lon do that
    //             if (in4326 === true) {
    //                 ulCornerPoint = ulCornerPoint.transform(this.proj900913, this.proj4326);
    //             }
    //         }
    //     }
    //     // Return it
    //     return ulCornerPoint;

    // },

    getZoom: function () {
        // Make sure the map exists
        if ((typeof this.map !== "undefined") && this.map !== null) {
            return this.map.getZoom();
        } else {
            return null;
        }
    },

    initMap: function () {
        // Grab a reference locally
        var me = this;

        // Create the placeholder for the map
        this.map = null;

        // The base map
        this.baseMapLayer = null;

        // Create the placeholder for the vector layer to display tracks
        this.vectorLayer = null;

        // Create a placeholder for the marker layer for observations
        this.markerLayer = null;

        // An array that will keep track of the markers that are on the map
        this.markers = {};

        // An array that will keep track of the popups
        this.popups = {};

        // Grab a renderer for later use
        var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
        renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

        // Create a Google projection as it will be used later
        this.proj900913 = new OpenLayers.Projection("EPSG:900913");

        // Create the normal lat/long projection
        this.proj4326 = new OpenLayers.Projection("EPSG:4326");

        // Create the OpenLayers Map using the same projection as Google
        this.map = new OpenLayers.Map('open-layers-box', {
            projection: 'EPSG:900913',
            maxExtent: new OpenLayers.Bounds(-20037508, -20037508, 20037508, 20037508),
            maxResolution: 156543.0339,
            units: 'm',
            controls: []
        });

        // Create a Navigation control
        var navigationControl = new OpenLayers.Control.Navigation({});

        // Add it to the map
        this.map.addControl(navigationControl);

        // Create the scale line control for the map
        var scaleLineControl = new OpenLayers.Control.ScaleLine();

        // And add it
        this.map.addControl(scaleLineControl);

        // Build the MousePosition control
        var mousePositionControl = new OpenLayers.Control.MousePosition({
            'position': new OpenLayers.Pixel(100, 0),
            formatOutput: function (lonlat) {
                return lonlat.lat.toFixed(4) + ", " + lonlat.lon.toFixed(4);
            }
        });

        // Set the proper projection
        mousePositionControl.displayProjection = this.proj4326;

        // Set an empty string
        mousePositionControl.emptyString = "lat,lon";

        // And add it to the map
        this.map.addControl(mousePositionControl);

        // Zoom panel control
        var zoomPanel = new OpenLayers.Control.ZoomPanel();

        // Add it to the map
        this.map.addControl(zoomPanel);

        // Create a generic control to serve as the hover to get depth
        var hoverControl = new OpenLayers.Control();

        // Create a new div that will be the depth box
        this.depthBox = document.createElement('div');
        this.depthBox.id = 'depth-box';
        this.map.viewPortDiv.appendChild(me.depthBox);

        // ******************************************************** //
        // Create necessary components for the measurement feature  //
        // ******************************************************** //

        // Create the vector layer for measurement labels
        this.measurementVectorLayer = new OpenLayers.Layer.Vector("Measurement Labels", {
            styleMap: new OpenLayers.StyleMap({
                'default': {
                    strokeColor: "#FF0000",
                    strokeOpacity: 1,
                    strokeWidth: 3,
                    fillColor: "#FF5500",
                    fillOpacity: 0.5,
                    pointRadius: 6,
                    pointerEvents: "visiblePainted",
                    // label with \n linebreaks
                    label: "${measure} ${units} (bearing: ${bearing})",
                    fontColor: "${fontColor}",
                    fontSize: "12px",
                    fontFamily: "Courier New, monospace",
                    fontWeight: "bold",
                    labelAlign: "${align}",
                    labelXOffset: "${xOffset}",
                    labelYOffset: "${yOffset}",
                    labelOutlineColor: "black",
                    labelOutlineWidth: 4
                }
            }),
            renderers: renderer
        });

        // Create a Point Feature that will serve as a label on the measure
        this.measurementLabelPoint = new OpenLayers.Geometry.Point(0, 0);
        this.measurementLabelFeature = new OpenLayers.Feature.Vector(this.measurementLabelPoint);
        this.measurementLabelFeature.id = 'measurement-label-feature';
        this.measurementLabelFeature.attributes = {
            measure: 0,
            units: "m",
            bearing: 0,
            fontColor: "#FFFFFF",
            // positive value moves the label to the right
            xOffset: 10,
            // negative value moves the label down
            yOffset: 15
        };

        // Add the layer to the map
        this.map.addLayer(this.measurementVectorLayer);
        this.measurementVectorLayer.setZIndex(999);

        // Create a button that will be used to latch measuring
        this.measureButton = document.createElement('button');
        this.measureButton.id = 'measure-button';
        this.measureButton.innerHTML = 'Measure: OFF';
        this.measureButton.onclick = function () {
            // Toggle the measure active class on the button
            this.classList.toggle('measure-active');

            // Check to see if the button is latched or not
            if (this.innerHTML === 'Measure: ON') {
                // Change button text
                this.innerHTML = 'Measure: OFF';

                // Remove the measure label
                me.measurementVectorLayer.removeFeatures([me.measurementLabelFeature]);

                // And deactivate the measure control
                me.measureControl.deactivate();
            } else {
                // Change button text
                this.innerHTML = 'Measure: ON';

                // Move the measure label feature to 0,0 and clear label
                me.measurementLabelFeature.attributes.measure = 0.0;
                me.measurementLabelFeature.attributes.units = 'm';

                // Add the measurement label to the measurement layer
                me.measurementVectorLayer.addFeatures([me.measurementLabelFeature]);
                me.measurementLabelFeature.move(new OpenLayers.LonLat(0, 0));

                // And activate measurement control
                me.measureControl.activate();
            }
        };

        // Add the button to the view port
        this.map.viewPortDiv.appendChild(me.measureButton);

        // Create the styles for the measurement points and lines
        var measurementStyles = {
            "Point": {
                pointRadius: 3,
                graphicName: "circle",
                fillColor: "white",
                fillOpacity: 1,
                strokeWidth: 1,
                strokeOpacity: 1,
                strokeColor: "#333333"
            },
            "Line": {
                strokeWidth: 2,
                strokeOpacity: 1,
                strokeColor: "#777777"
            }
        };

        // Create the OpenLayers Style for the measure points and lines
        var measurementStyle = new OpenLayers.Style();

        // Add the styles created above
        measurementStyle.addRules([
            new OpenLayers.Rule({
                symbolizer: measurementStyles
            })
        ]);

        // Now create the default style map for the measurement control
        var measurementStyleMap = new OpenLayers.StyleMap({
            "default": measurementStyle
        });

        // Create a measure control
        this.measureControl = new OpenLayers.Control.Measure(
            OpenLayers.Handler.Path, {
            persist: true,
            geodesic: true,
            handlerOptions: {
                layerOptions: {
                    renderers: renderer,
                    styleMap: measurementStyleMap
                }
            }
        }
        );

        // Set so measuring is constant
        this.measureControl.setImmediate(true);

        // The function to handle measure events
        this.handleMeasure = function (event) {
            // Set the measure
            me.measurementLabelFeature.attributes.measure = event.measure.toFixed(2);
            me.measurementLabelFeature.attributes.units = event.units;

            // Grab the second to last point in the array
            var secondToLastPoint = event.geometry.components[event.geometry.components.length - 2];
            var secondToLastLonLat = new OpenLayers.LonLat(secondToLastPoint.x, secondToLastPoint.y);

            // Grab the last point in the array of components
            var lastPoint = event.geometry.components[event.geometry.components.length - 1];
            var lastLonLat = new OpenLayers.LonLat(lastPoint.x, lastPoint.y);

            // Create a new LatLon and move the feature to it
            me.measurementLabelFeature.move(lastLonLat);

            // Now convert lat lons to 4326 so we can calculate bearing
            var secondToLastLonLat4326 = secondToLastLonLat.transform(me.proj900913, me.proj4326);
            var lastLonLat4326 = lastLonLat.transform(me.proj900913, me.proj4326);

            // Calculate bearing
            var startLat = secondToLastLonLat4326.lat * (Math.PI / 180);
            var startLon = secondToLastLonLat4326.lon * (Math.PI / 180);
            var endLat = lastLonLat4326.lat * (Math.PI / 180);
            var endLon = lastLonLat4326.lon * (Math.PI / 180);

            var deltaLon = endLon - startLon;

            var dPhi = Math.log(Math.tan(endLat / 2.0 + Math.PI / 4.0) / Math.tan(startLat / 2.0 + Math.PI / 4.0));
            if (Math.abs(deltaLon) > Math.PI) {
                if (deltaLon > 0.0)
                    deltaLon = -(2.0 * Math.PI - deltaLon);
                else
                    deltaLon = (2.0 * Math.PI + deltaLon);
            }

            me.measurementLabelFeature.attributes.bearing = ((Math.atan2(deltaLon, dPhi) * (180 / Math.PI) + 360.0) % 360.0).toFixed(2);

        };

        // Add event handler for measuring
        this.measureControl.events.on({
            "measure": this.handleMeasure,
            "measurepartial": this.handleMeasure
        });


        // Add it to the map
        this.map.addControl(this.measureControl);

        // ******************************************************** //
        // End measurement feature                                  //
        // ******************************************************** //

        // Extend it to handle mouse hovers
        OpenLayers.Util.extend(hoverControl, {
            draw: function () {
                // When the draw function is called, set up a Hover handler to look for the elevation
                this.hover = new OpenLayers.Handler.Hover(hoverControl, {
                    // Handle the pause event
                    'pause': function (evt) {
                        // Grab the xy of the mouse
                        var mouseLonLat = me.map.getLonLatFromPixel(evt.xy);
                        var mouseLonLat4326 = me.map.getLonLatFromPixel(evt.xy).transform(me.proj900913, me.proj4326);

                        // Now make a call out to the elevation service to get the elevation (depth)
                        Ext.Ajax.request({
                            method: 'GET',
                            url: 'elevation',
                            params: {
                                'latitude': mouseLonLat4326.lat,
                                'longitude': mouseLonLat4326.lon
                            },
                            success: function (response, options) {
                                // Convert response to JSON object
                                var responseJSON = JSON.parse(response.responseText);
                                var depth = Math.round(Math.abs(Number(responseJSON.data.elevation)));
                                me.depthBox.innerHTML = depth + 'm';
                            },
                            failure: function (response, options) {
                                console.log("failure");
                                console.log(response);
                            }
                        });
                    },
                    'move': function (evt) {
                        // Clear the depth box while the mouse is moving
                        if (me.depthBox) {
                            if (me.depthBox.innerHTML != '') {
                                me.depthBox.innerHTML = '';
                            }
                        }

                    }
                });

                this.hover.activate();
            }
        });

        // Add it to the map
        this.map.addControl(hoverControl);

        // Create the vector layer for various features
        this.vectorLayer = new OpenLayers.Layer.Vector('ODSS Vector Data Layer', {
            styleMap: new OpenLayers.StyleMap({
                'default': {
                    strokeColor: "${trackcolor}",
                    fill: true,
                    fillColor: "${fillcolor}",
                    fillOpacity: 0.6,
                    pointRadius: 5
                }
            })
        });

        // Add it to the map
        this.map.addLayer(this.vectorLayer);

        // Create the marker layer
        this.markerLayer = new OpenLayers.Layer.Markers("Markers");

        // Add it
        this.map.addLayer(this.markerLayer);

        // Add a select feature control to the vector layer
        //var select = new OpenLayers.Control.SelectFeature([this.vectorLayer]);
        //this.map.addControl(select);
        //select.activate();


        //var dialog;
        //this.vectorLayer.events.on({
        //    featureselected: function(event) {
        //        console.log("Feature selected");
        //        var feature = event.feature;
        //        console.log(feature.geometry);
        //        var area = feature.geometry.getArea();
        //        var id = feature.attributes.key;
        //        var output = "Building: " + id + " Area: " + area.toFixed(2);
        //        dialog = $("<div title='Feature Info'>" + output + "</div>").dialog();
        //    },
        //    featureunselected: function() {
        //        console.log("Feature unselected");
        //        dialog.dialog("destroy").remove();
        //    }
        //});

        // Add an event handler that fires when the user drags the map
        // this.map.events.register('moveend', me, function () {
        //     // Fire an event in the parent container
        //     this.fireEvent('move', this);
        // });
    },

    removeAllLayers: function () {
        // Grab the map
        var numLayers = this.map.getNumLayers();

        // Now iterate over them and destroy them
        for (var i = numLayers - 1; i >= 0; i--) {
            var layerToRemove = this.map.layers[i];
            // If the layer has a 'node' associated, it is a data layer and can be removed
            if (layerToRemove.node) {
                this.map.removeLayer(layerToRemove);
                layerToRemove.destroy();
            }
        }
    },

    removeFeature: function (id) {
        // Remove from the vector layer
        var featureToRemove = this.vectorLayer.getFeatureById(id);
        this.vectorLayer.removeFeatures([featureToRemove]);
    },

    removeLayer: function (layer) {
        // Grab the layer
        var layerToRemove = this.map.getLayer(layer._id);

        // Grab the ID and remove the layer from the map
        this.map.removeLayer(layerToRemove);

        // Destroy it
        layerToRemove.destroy();
    },

    removeMarker: function (marker) {
        if (marker !== null) {
            this.markerLayer.removeMarker(marker);
        }
    },

    removeTrack: function (track) {
        // A local reference
        var me = this;

        // Create the feature ID
        var featureId = "odss-platform-" + track.id;

        // Remove the previous one from the map if it exists
        var currentFeature = me.vectorLayer.getFeatureById(featureId);
        if (currentFeature !== null) {
            me.vectorLayer.removeFeatures([currentFeature]);
            try {
                currentFeature.destroy();
            } catch (e) {
                console.log("Exception trying to destroy track");
                console.log(e);
            }
        }

        // Same thing with any markers associated with the track
        var trackEndMarker = me.markers[featureId];
        if (typeof trackEndMarker !== "undefined" && trackEndMarker !== null) {
            me.markerLayer.removeMarker(trackEndMarker);
            try {
                trackEndMarker.destroy();
            } catch (e) {
                console.log("Exception trying to destroy marker");

                console.log(e);
                console.log(trackEndMarker);
            }
            delete me.markers[featureId];
        }

        // Also look for any platformIcon markers
        var platformIconMarker = me.markers[featureId + '-platform-icon'];
        if (typeof platformIconMarker !== "undefined" && platformIconMarker !== null) {
            me.markerLayer.removeMarker(platformIconMarker);
            try {
                platformIconMarker.destroy();
            } catch (e) {
                console.log("Exception trying to destroy marker");

                console.log(e);
                console.log(platformIconMarker);
            }
            delete me.markers[featureId + '-platform-icon'];
        }


        // Same thing with any popups
        var trackPopup = me.popups[featureId];
        if (typeof trackPopup !== "undefined" && trackPopup !== null) {
            me.map.removePopup(trackPopup);
            try {
                trackPopup.destroy();
            } catch (e) {
                console.log("Exception trying to destroy popup");

                console.log(e);
                console.log(trackPopup);
            }
            delete me.popups[featureId];
        }
    },

    renderTrack: function (track) {
        // Create a variable to point to this
        var me = this;

        // Create the feature ID
        var featureId = "odss-platform-" + track.id;

        // Make sure there is some geometry
        if (typeof track !== "undefined") {

            // Remove the previous one from the map if it exists
            me.removeTrack(track);

            // Make sure it is not null either
            if (track !== null && track.trackGeometry !== null && track.trackGeometry.getVertices().length > 0) {

                // Grab the end points
                var endpoints = track.trackGeometry.getVertices(true);

                // Make sure there are two points and at latest one is defined
                if (typeof endpoints !== "undefined" && endpoints.length === 2 && (typeof endpoints[0] !== "undefined")) {
                    // Grab the end point and convert to LonLat
                    var endPointLonLat = new OpenLayers.LonLat(endpoints[0].x, endpoints[0].y);

                    // Create an icon that will display the exact last known position
                    var locationIcon = new OpenLayers.Icon('resources/images/crosshairs.png');

                    // Create a Marker that is located at the endpoint and has the crosshairs
                    var locationMarker = new OpenLayers.Marker(endPointLonLat, locationIcon);

                    // Add it to the array of markers associated with the map
                    me.markers[featureId] = locationMarker;

                    // Add it the marker layer
                    me.markerLayer.addMarker(locationMarker);

                    // Grab the current time
                    var timeDiff = "";
                    var currentTimestamp = (new Date()).getTime();

                    if (track.latestTimestamp) {
                        var millisDiff = currentTimestamp - track.latestTimestamp;
                        if (millisDiff > 0) {
                            var minsDiff = Math.round(millisDiff / 60000);
                            timeDiff = "(" + minsDiff + " mins old)";
                        }
                    }
                    var htmlLabel = '<span style="color:' + track.color +
                        '; ">&nbsp;' + track.platformAbbreviationFK + '<span class="fixage">' + timeDiff + '</span></span>';

                    // Create a popup and add it
                    var trackPopup = new OpenLayers.Popup(featureId, endPointLonLat, null, htmlLabel);
                    trackPopup.autoSize = true;
                    trackPopup.backgroundColor = 'transparent';

                    // Add it to the array of popups
                    me.popups[featureId] = trackPopup;

                    // Add it to the map
                    me.map.addPopup(trackPopup);

                    // Now check to see if an icon and it's size is specified by the platform
                    if (track.platformIconUrl && track.platformIconHeight && track.platformIconWidth) {
                        // Since there is one, we should create a new Marker, but offset the icon so it does
                        // not block the crosshairs

                        // Create an OpenLayers size and offset for the icon
                        var size = new OpenLayers.Size(track.platformIconWidth, track.platformIconHeight);
                        var offset = new OpenLayers.Pixel(3, -size.h);

                        // Now create the OpenLayers Icon
                        var platformIcon = new OpenLayers.Icon(track.platformIconUrl, size, offset);

                        // Now create a Marker
                        var platformMarker = new OpenLayers.Marker(endPointLonLat, platformIcon);

                        // Add it to the array of markers
                        me.markers[featureId + '-platform-icon'] = platformMarker;

                        // Add it to the marker layer
                        me.markerLayer.addMarker(platformMarker);
                    }
                }

                // Now create the feature
                var platformFeature = new OpenLayers.Feature.Vector(track.trackGeometry, {
                    trackcolor: track.color
                });

                // Make sure was constructed
                if (platformFeature !== null) {

                    // Set the ID
                    platformFeature.id = featureId;

                    // Add it to the map
                    me.vectorLayer.addFeatures(platformFeature);
                }
            }
        }
    },

    // resizeMap: function () {
    //     // Update the size of the map if it exists
    //     if ((typeof this.map !== "undefined") && this.map !== null) {
    //         //this.map.updateSize();
    //     }
    // },

    setBaseMap: function (typeOfMap, hostname, callback) {
        if (typeof typeOfMap !== "undefined" && typeOfMap !== null && typeOfMap === "google") {
            // Create a handle to myself
            var me = this;

            // Add a callback method to the window for the google library
            window.googleMapsReady = function () {
                // Check to see if previous base layer existed
                var baseMapExists = true;

                // Check to see if previous base layer existed
                if (typeof me.baseMapLayer === "undefined" || me.baseMapLayer === null) {
                    baseMapExists = false;
                } else {
                    // Remove the previous base map
                    me.map.removeLayer(me.baseMapLayer);
                }
                // Create the base layer using a Google Satellite layer
                me.baseMapLayer = new OpenLayers.Layer.Google('Google Map Layer', {
                    type: google.maps.MapTypeId.SATELLITE,
                    sphericalMercator: true,
                    maxExtent: new OpenLayers.Bounds(-20037508.34, -20037508.34, 20037508.34, 20037508.34),
                    maxResolution: 156543.0339,
                    isBaseLayer: true,
                    units: 'm',
                    numZoomLevels: 21
                });

                me.baseMapLayer = new OpenLayers.Layer.XYZ("ESRI Ocean Basemap",
                    "http://services.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/${z}/${y}/${x}",
                    {
                        sphericalMercator: true,
                        isBaseLayer: true,
                        numZoomLevels: 21
                    }
                );

                // Add the new base map layer
                me.map.addLayer(me.baseMapLayer);

                // If none existed, go ahead and set the user's home location
                if (!baseMapExists) {
                    // Grab the home location from the cookie store
                    var homeLocation = Odss.app.getHomeLocationAndZoom();

                    // Grab the current mapbox, and set center and zoom
                    me.setCenterAndZoom(homeLocation.lat, homeLocation.lon, homeLocation.zoom);

                    // Now fire an event that the bbox changed
                    me.fireEvent('bboxupdated', me);
                }

                // If there is a callback, call it
                if (callback) {
                    callback(null);
                }
            };

            // Add a script element to the document
            var script = document.createElement("script");

            // Set the source to the google maps API
            script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCVcUEyjC_7hXHWQwiT9Az4mxJwZCOK21E&callback=googleMapsReady";

            // Set the type to JavaScript
            script.type = "text/javascript";

            // Add it to the document
            document.getElementsByTagName("head")[0].appendChild(script);

        } else {
            // Check to see if previous base layer existed
            var baseMapExists = true;

            // Check to see if previous base layer existed
            if (typeof this.baseMapLayer === "undefined" || this.baseMapLayer === null) {
                baseMapExists = false;
            } else {
                // Remove the previous base map
                this.map.removeLayer(this.baseMapLayer);
            }

            // Creat the new layer
            this.baseMapLayer = new OpenLayers.Layer.XYZ("GEBCO NOAA Tiles",
                ["http://" + hostname + "/gebco_noaa/tiles/${z}/${x}/${y}.png"],
                {
                    attribution: "Sources: General Bathymetric Chart of the Oceans (<a href='http://www.gebco.net/'>GEBCO</a>) &amp; NOAA National Geophysical Data Center (<a href='http://www.ngdc.noaa.gov/mgg/dem/demportal.html'>NGDC</a>)",
                    sphericalMercator: true,
                    wrapDateLine: true,
                    transitionEffect: "resize",
                    isBaseLayer: true,
                    buffer: 1,
                    numZoomLevels: 21
                }
            );

            // Add it to the OpenLayer Map
            this.map.addLayer(this.baseMapLayer);

            // If no base map existed, set the location to the user's preference
            if (!baseMapExists) {
                // Grab the home location from the cookie store
                var homeLocation = Odss.app.getHomeLocationAndZoom();

                // Grab the current mapbox, and set center and zoom
                this.setCenterAndZoom(homeLocation.lat, homeLocation.lon, homeLocation.zoom);
            }

            // If there is a callback, call it
            if (callback) {
                callback(null);
            }
        }
    },

    // This method takes in a layer and if it can figure out the extents of the layer, it
    // will pan and zoom to fit that layer
    zoomToLayer: function (layer) {
        // Make sure we got something
        if (layer && layer.url) {

            // Now see if it's a WMS layer
            if (layer.type && layer.type.toLowerCase() == 'wms') {

                // Check to see if the layer already has a bbox on it
                if (!layer.bbox) {

                    // Since the layer could have multiple wms layers defined, 
                    // let's make sure we have at least one layer
                    if (layer.params && layer.params.layers) {

                        // Split into list of layers
                        var layerArray = layer.params.layers.split(',');

                        // Make sure there are some layer names to iterate over
                        if (layerArray && layerArray.length > 0) {

                            // And handle to this to use in the AJAX callback
                            var me = this;

                            // Let's make the GetCapabilities call to see if have something
                            Ext.Ajax.request({
                                method: 'GET',
                                url: layer.url,
                                params: {
                                    'service': 'wms',
                                    'request': 'GetCapabilities',
                                    'version': '1.1.0'
                                },
                                success: function (response, options) {

                                    // Make sure we have a response
                                    if (response && response.responseText) {
                                        // Parse the incoming XML
                                        var oParser = new DOMParser();
                                        var oDOM = oParser.parseFromString(response.responseText, "application/xml");

                                        // Grab all the tags that are 'Name'
                                        var nameElementArray = oDOM.getElementsByTagName('Name');

                                        // Now I have all the layer nodes and all the names of the layers being 
                                        // requested, we need to loop through all of them to find the largest
                                        // bounding box for this layer
                                        for (var i = 0; i < layerArray.length; i++) {
                                            for (var j = 0; j < nameElementArray.length; j++) {
                                                if (nameElementArray[j].innerHTML == layerArray[i]) {
                                                    // Now that I have the layer node with the right name, I need to walk up the parent
                                                    // chain to look for a bounding box
                                                    var parentElement = nameElementArray[i].parentElement;
                                                    while (parentElement) {
                                                        // Grab the parallel bounding box tags
                                                        var boundingBoxTags = nameElementArray[j].parentElement.getElementsByTagName('BoundingBox');
                                                        // Create some variables to track max and min x's and y's
                                                        for (var k = 0; k < boundingBoxTags.length; k++) {
                                                            if (boundingBoxTags[k].getAttribute('minx')) {
                                                                if (!layer.bbox) layer.bbox = {};
                                                                if (!layer.bbox.minx || layer.bbox.minx > Number(boundingBoxTags[k].getAttribute('minx'))) {
                                                                    layer.bbox.minx = Number(boundingBoxTags[k].getAttribute('minx'));
                                                                }
                                                            }
                                                            if (boundingBoxTags[k].getAttribute('miny')) {
                                                                if (!layer.bbox) layer.bbox = {};
                                                                if (!layer.bbox.miny || layer.bbox.miny > Number(boundingBoxTags[k].getAttribute('miny'))) {
                                                                    layer.bbox.miny = Number(boundingBoxTags[k].getAttribute('miny'));
                                                                }
                                                            }
                                                            if (boundingBoxTags[k].getAttribute('maxx')) {
                                                                if (!layer.bbox) layer.bbox = {};
                                                                if (!layer.bbox.maxx || layer.bbox.maxx < Number(boundingBoxTags[k].getAttribute('maxx'))) {
                                                                    layer.bbox.maxx = Number(boundingBoxTags[k].getAttribute('maxx'));
                                                                }
                                                            }
                                                            if (boundingBoxTags[k].getAttribute('maxy')) {
                                                                if (!layer.bbox) layer.bbox = {};
                                                                if (!layer.bbox.maxy || layer.bbox.maxy < Number(boundingBoxTags[k].getAttribute('maxy'))) {
                                                                    layer.bbox.maxy = Number(boundingBoxTags[k].getAttribute('maxy'));
                                                                }
                                                            }
                                                        }
                                                        // If bounding box was found, bail out
                                                        if (layer.bbox) {
                                                            parentElement = null;
                                                            break;
                                                        } else {
                                                            // No bbox was found, go up a parent
                                                            parentElement = parentElement.parentElement;
                                                        }
                                                    }

                                                    // Make sure we have the bounds
                                                    if (layer.bbox && layer.bbox.minx && layer.bbox.miny && layer.bbox.maxx && layer.bbox.maxy) {

                                                        // Create lat lon for lower left
                                                        var bboxLL = new OpenLayers.LonLat(layer.bbox.minx, layer.bbox.miny);
                                                        var bboxLLReProj = bboxLL.transform(me.proj4326, me.proj900913);

                                                        // And for upper right
                                                        var bboxUR = new OpenLayers.LonLat(layer.bbox.maxx, layer.bbox.maxy);
                                                        var bboxURReProj = bboxUR.transform(me.proj4326, me.proj900913);

                                                        // Now create the bounds object
                                                        var bounds = new OpenLayers.Bounds();
                                                        bounds.extend(bboxLLReProj);
                                                        bounds.extend(bboxURReProj);
                                                        me.map.zoomToExtent(bounds, true);
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                },
                                failure: function (response, options) {
                                    console.log("Failed to retrieve GetCapabilites from " + layer.url);
                                    console.log(response);
                                    console.log(options);
                                }
                            });
                        }
                    }
                } else {
                    // Since the bbox was already defined, make sure all the maxs and mins are there.
                    if (layer.bbox && layer.bbox.minx && layer.bbox.miny && layer.bbox.maxx && layer.bbox.maxy) {
                        // Create lat lon for lower left
                        var bboxLL = new OpenLayers.LonLat(layer.bbox.minx, layer.bbox.miny);
                        var bboxLLReProj = bboxLL.transform(this.proj4326, this.proj900913);

                        // And for upper right
                        var bboxUR = new OpenLayers.LonLat(layer.bbox.maxx, layer.bbox.maxy);
                        var bboxURReProj = bboxUR.transform(this.proj4326, this.proj900913);

                        // Now create the bounds object
                        var bounds = new OpenLayers.Bounds();
                        bounds.extend(bboxLLReProj);
                        bounds.extend(bboxURReProj);
                        this.map.zoomToExtent(bounds, true);
                    }
                }
            }
        }
    },

    setCenterAndZoom: function (latitude, longitude, zoom) {
        // If map is defined and so are parameters, set the location and zoom level
        if ((typeof this.map !== "undefined") &&
            (this.map !== null) &&
            (typeof this.baseMapLayer !== 'undefined') &&
            (this.baseMapLayer !== null) &&
            (latitude !== null) &&
            (longitude !== null)) {

            // Create the open layers latitude and longitude using decimal degrees
            var olLonLat = new OpenLayers.LonLat(longitude, latitude);

            // Transform it to the correct projection
            var location = olLonLat.transform(this.proj4326, this.proj900913);

            // Center map on given location
            this.map.setCenter(location);

            // Now set zoom level
            if (zoom !== null) {
                this.map.zoomTo(zoom);
            }

            // And pan to the correct location
            this.map.panTo(location);
        }
    },

    init: function (application) {
        this.control({
            // "#open-layers-box": {
            //     move: this.onContainerMove,
            //     resize: this.onContainerResize
            // },
            "#set-home-button": {
                click: this.onSetHomeButtonClick
            },
            "#go-home-button": {
                click: this.userClickedHomeButton
            }
        });
    }

});
