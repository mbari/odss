/**
 * This Object builds the Platform Mongoose model
 * @type {exports}
 */

// Import any dependencies
var log4js = require('log4js');

// The constructor
function PlatformFactory(mongoose) {

    // Grab reference to self for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');
    me.logger.debug("Creating Platform Schema and Model...");

    // This is the Platform model
    var Platform = null;

    // Create the Platform schema
    var platformSchema = new mongoose.Schema({
        _ver: Number,
        name: String,
        color: String,
        abbreviation: String,
        typeName: String,
        iconUrl: String,
        iconHeight: Number,
        iconWidth: Number,
        description: String,
        views: [String]
    });

    // Add the method to persist a JSON object that should contain platform fields
    platformSchema.statics.persistPlatformJSON = function (platformJSONToPersist, callback) {

        // First thing to do is check for an ID. If it does not exist, we assume it's a new platform
        if (platformJSONToPersist._id && platformJSONToPersist._id !== '') {

            // Since we have an ID, we want to validate if it is a valid ObjectID, we
            // can create a regular expression to look for a 24 character hex pattern.
            // That should at least weed out any weird IDs
            var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
            var matchArray = checkForHexRegExp.exec(platformJSONToPersist._id);
            if (matchArray) {
                me.logger.debug("ID appears to be valid, will try an update");

                // Try to find the Platform document by it's ID
                Platform.findById(platformJSONToPersist._id, function (err, platformToUpdate) {
                    // Check for an error
                    if (err) {
                        // Send the error back
                        me.logger.error("Error trying to find Platform by ID " + platformJSONToPersist._id);
                        if (callback)
                            callback(err);
                    } else {
                        // Check to see if a platform was found
                        if (platformToUpdate) {

                            // First we want to check the version number to make sure it matches what is
                            // in the persistent store so we are only saving changes to the most recent
                            // version of the platform
                            if (platformJSONToPersist._ver && platformToUpdate._ver &&
                                platformJSONToPersist._ver === platformToUpdate._ver) {

                                // A boolean to check to see if something changed
                                var platformChanged = false;

                                // We are good to go with the update
                                if (platformJSONToPersist.name && (!platformToUpdate.name ||
                                    platformToUpdate.name !== platformJSONToPersist.name)) {
                                    // Update the platform
                                    platformToUpdate.name = platformJSONToPersist.name;
                                    // Set the flag
                                    platformChanged = true;
                                }
                                if (platformJSONToPersist.color && (!platformToUpdate.color ||
                                    platformToUpdate.color !== platformJSONToPersist.color)) {
                                    // Update the platform
                                    platformToUpdate.color = platformJSONToPersist.color;
                                    // Set the flag
                                    platformChanged = true;
                                }
                                if (platformJSONToPersist.abbreviation && (!platformToUpdate.abbreviation ||
                                    platformToUpdate.abbreviation !== platformJSONToPersist.abbreviation)) {
                                    // Update the platform
                                    platformToUpdate.abbreviation = platformJSONToPersist.abbreviation;
                                    // Set the flag
                                    platformChanged = true;
                                }
                                if (platformJSONToPersist.typeName && (!platformToUpdate.typeName ||
                                    // Update the platform
                                    platformToUpdate.typeName !== platformJSONToPersist.typeName)) {
                                    platformToUpdate.typeName = platformJSONToPersist.typeName;
                                    // Set the flag
                                    platformChanged = true;
                                }

                                // Make sure something actually changed
                                if (platformChanged) {
                                    // Bump the version number
                                    platformToUpdate._ver = platformToUpdate._ver + 1;

                                    // Now save it
                                    platformToUpdate.save(function (errLocal, savedPlatform) {
                                        // Check for the error first
                                        if (errLocal) {
                                            me.logger.error("Error trying to update the existing platform:", errLocal);
                                            if (callback)
                                                callback(errLocal);
                                        } else {
                                            me.logger.debug("Platform updated successfully", savedPlatform);
                                            if (callback)
                                                callback(null, savedPlatform);
                                        }
                                    });
                                }
                            } else {
                                me.logger.warn("Update attempted on platform with _id " + platformJSONToPersist._id +
                                    "and _ver " + platformJSONToPersist._ver +
                                    " but the existing platform with that _id did not have that version, it had version " +
                                    platformToUpdate._ver);
                                if (callback)
                                    callback(new Error("Update attempted on platform with _id " + platformJSONToPersist._id +
                                        "and _ver " + platformJSONToPersist._ver +
                                        " but the existing platform with that _id did not have that version, it had version " +
                                        platformToUpdate._ver));
                            }
                        } else {
                            // No platform was found with that ID, create a new one
                            me.logger.warn("Tried to update Platform with ID " + platformJSONToPersist._id +
                                " but no Platform was found, will just create a new one");
                            Platform.createAndSavePlatformFromJSON(platformJSONToPersist, callback);
                        }
                    }
                });
            } else {
                Platform.createAndSavePlatformFromJSON(platformJSONToPersist, callback);
            }
        } else {
            // If it doesn't have an _id, it could still have a name, which is an alternate primary key
            if (platformJSONToPersist.name && platformJSONToPersist.name !== '') {
                // Look it up by the tracking DB ID
                Platform.find({name: platformJSONToPersist.name}, function (err, platformsWithName) {
                    // Check for an error
                    if (err) {
                        // Send the error back
                        me.logger.error("Error trying to find Platform by name " +
                            platformJSONToPersist.name);
                        if (callback)
                            callback(err);
                    } else {
                        // Check the returned array to see if there is anything in it and assume that the first
                        // object is the one we want
                        if (platformsWithName.length > 0) {
                            // Grab the first one
                            var platformToUpdate = platformsWithName[0];

                            // Check to see if a platform was found
                            if (platformToUpdate) {

                                // Note that this is bit of blind faith.  If we are searching by trackingDBID, there is
                                // most likely no incoming version number, we just have to assume we are OK to update
                                // A boolean to check to see if something changed
                                var platformChanged = false;

                                // We are good to go with the update
                                if (platformJSONToPersist.color && (!platformToUpdate.color ||
                                    platformToUpdate.color !== platformJSONToPersist.color)) {
                                    // Update the platform
                                    platformToUpdate.color = platformJSONToPersist.color;
                                    // Set the flag
                                    platformChanged = true;
                                }
                                if (platformJSONToPersist.abbreviation && (!platformToUpdate.abbreviation ||
                                    platformToUpdate.abbreviation !== platformJSONToPersist.abbreviation)) {
                                    // Update the platform
                                    platformToUpdate.abbreviation = platformJSONToPersist.abbreviation;
                                    // Set the flag
                                    platformChanged = true;
                                }
                                if (platformJSONToPersist.typeName && (!platformToUpdate.typeName ||
                                    // Update the platform
                                    platformToUpdate.typeName !== platformJSONToPersist.typeName)) {
                                    platformToUpdate.typeName = platformJSONToPersist.typeName;
                                    // Set the flag
                                    platformChanged = true;
                                }

                                // Make sure something actually changed
                                if (platformChanged) {
                                    // Bump the version number
                                    if (platformToUpdate._ver) {
                                        platformToUpdate._ver = platformToUpdate._ver + 1;
                                    } else {
                                        platformToUpdate._ver = 1;
                                    }

                                    // Now save it
                                    platformToUpdate.save(function (errLocal, savedPlatform) {
                                        // Check for the error first
                                        if (errLocal) {
                                            me.logger.error("Error trying to update the existing platform:", errLocal);
                                            if (callback)
                                                callback(errLocal);
                                        } else {
                                            me.logger.debug("Platform updated successfully", savedPlatform);
                                            if (callback)
                                                callback(null, savedPlatform);
                                        }
                                    });
                                }
                            }
                        } else {
                            // No platform was found with that name, create a new one
                            me.logger.debug("Tried to update Platform with name " +
                                platformJSONToPersist.name +
                                " but no Platform was found, will just create a new one");
                            Platform.createAndSavePlatformFromJSON(platformJSONToPersist, callback);
                        }
                    }
                });
            } else {
                // No identifiers could be found, let's just create and save a new platform
                Platform.createAndSavePlatformFromJSON(platformJSONToPersist, callback);
            }
        }
    };

    // Helper method to create and save a new platform
    platformSchema.statics.createAndSavePlatformFromJSON = function (platformJSON, callback) {
        me.logger.debug("Going to create a new platform from JSON:", platformJSON);

        // Remove any possible ID
        delete platformJSON._id;

        // Create the new platform
        var newPlatform = new Platform(platformJSON);

        // If there are no views defined
        if (!platformJSON.views || platformJSON.views.length === 0) {
            newPlatform.views = ["Public"];
        }

        // Set the version to 1
        newPlatform._ver = 1;

        // Save it
        me.logger.debug("New platform just before save:", newPlatform);
        newPlatform.save(function (err, savedPlatform) {
            // Check for any errors
            if (err) {
                me.logger.error("Error saving new platform:", err);
                if (callback) {
                    callback(err);
                }
            } else {
                // Send the new platform to the callback
                me.logger.debug("Platform saved OK:", savedPlatform);
                if (callback)
                    callback(null, savedPlatform);
            }
        });
    };

    // The method to add a view name to the array of views for the given platform _id.  This method
    // assumes that the user wants to add the view to the lastest version
    platformSchema.statics.addViewName = function (_id, viewName, callback) {
        me.logger.info("Going to add view " + viewName + " to platform with _id " + _id);
        // Find the platform in the persistent store
        Platform.findById(_id, function (err, platformToUpdate) {
            // Check for error first
            if (err) {
                me.logger.error("Error trying to find platform with _id " + _id);
                if (callback)
                    callback(err);
            } else {
                // Make sure a platform was found
                if (platformToUpdate) {
                    // If the view name is not in the array of view names, push and save, else do nothing
                    if (!platformToUpdate.views.viewName) {
                        // Now add it
                        platformToUpdate.views.push(viewName);
                        // Bump the version
                        platformToUpdate._ver = platformToUpdate._ver + 1;

                        // Now update it
                        platformToUpdate.save(function (errLocal, savedPlatform) {
                            if (errLocal) {
                                me.logger.error("Error trying to save platform after adding viewName ", errLocal);
                                if (callback)
                                    callback(errLocal);
                            } else {
                                // Send the updated platform
                                if (callback)
                                    callback(null, savedPlatform);
                            }
                        });
                    }
                } else {
                    me.logger.warn("Trying to add view name " + viewName + " to platform with _id " +
                        _id + " but no platform with that _id was found");
                    if (callback)
                        callback(new Error("No platform with _id " + _id + " was found"));
                }
            }
        });
    };

    // The method to remove a view name from the array of views for the given platform _id
    platformSchema.statics.removeViewName = function (_id, viewName, callback) {
        me.logger.info("Going to remove view " + viewName + " from platform with _id " + _id);
        // Find the platform in the persistent store
        Platform.findById(_id, function (err, platformToUpdate) {
            // Check for error first
            if (err) {
                me.logger.error("Error trying to find platform with _id " + _id);
                if (callback)
                    callback(err);
            } else {
                // Make sure a platform was found
                if (platformToUpdate) {
                    me.logger.info("platformToUpdate", platformToUpdate);
                    // If the view name is in the array of view names, remove it and save, else do nothing
                    var indexOfViewName = platformToUpdate.views.indexOf(viewName);
                    me.logger.info("Index to remove is " + indexOfViewName);
                    if (indexOfViewName > -1) {
                        // Remove it
                        platformToUpdate.views.splice(indexOfViewName, 1);

                        // Bump the version
                        platformToUpdate._ver = platformToUpdate._ver + 1;

                        // Now update it
                        platformToUpdate.save(function (errLocal, savedPlatform) {
                            if (errLocal) {
                                me.logger.error("Error trying to save platform after removing viewName ", errLocal);
                                if (callback)
                                    callback(errLocal);
                            } else {
                                // Send the updated platform
                                if (callback)
                                    callback(null, savedPlatform);
                            }
                        });
                    }
                } else {
                    me.logger.warn("Trying to remove view name " + viewName + " from platform with _id " +
                        _id + " but no platform with that _id was found");
                    if (callback)
                        callback(new Error("No platform with _id " + _id + " was found"));
                }
            }
        });
    };

    // Create the model
    Platform = mongoose.model('Platform', platformSchema);

    // The method to return the model for clients to use
    this.getPlatform = function () {
        return Platform;
    };

}

// The factory method for constructing the Platform service
exports.createPlatformFactory = function (mongoose) {
    // Create the new Platform model
    return new PlatformFactory(mongoose);
};