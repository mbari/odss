// Import logging module
var log4js = require('log4js');

// The constructor
function Sample(mongoose) {

    // Create a reference to this for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');
    me.logger.debug("Creating Sample object");

    // Grab the Schema from the mongoose connection
    var Schema = mongoose.Schema;

    // Create the sample schema
    this.sampleSchema = new Schema({
        parentSampleIDFK: String,
        project: String,
        cruise: String,
        platform: String,
        organization: String,
        contactIDFK: String,
        contactName: String,
        contactEmail: String,
        cast: String,
        stationName: String,
        owner: String,
        bottle: Number,
        depth: Number,
        analysis: String,
        plannedStartDate: Date,
        plannedEndDate: Date,
        acquired: Boolean,
        actualStartDate: Date,
        actualEndDate: Date,
        location: Schema.Types.Mixed,
        howPreserved: String,
        whereStored: String,
        whereDataResides: String,
        modEpochSeconds: Number,
        modByName: String,
        modByEmail: String,
        views: [String]
    });

    // Add the method to persist a JSON object that should contain sample fields
    this.sampleSchema.statics.persistSampleJSON = function (sampleJSONToPersist, callback) {
        me.logger.debug("Going to persist sample:", sampleJSONToPersist);

        // First thing to do is check for an ID. If it does not exist, we assume it's a new sample
        if (sampleJSONToPersist._id && sampleJSONToPersist._id !== '') {

            // Since we have an ID, we want to validate if it is a valid ObjectID, we
            // can create a regular expression to look for a 24 character hex pattern.
            // That should at least weed out any weird IDs
            var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
            var matchArray = checkForHexRegExp.exec(sampleJSONToPersist._id);
            if (matchArray) {
                me.logger.debug("ID appears to be valid, will try an update");

                // Try to find the Sample document by it's ID
                this.findById(sampleJSONToPersist._id, function (err, sampleToUpdate) {
                    // Check for an error
                    if (err) {
                        // Send the error back
                        me.logger.error("Error trying to find Sample by ID " + sampleJSONToPersist._id);
                        if (callback)
                            callback(err);
                    } else {
                        // Check to see if a document was found
                        if (sampleToUpdate) {
                            if (sampleJSONToPersist.parentSampleIDFK)
                                sampleToUpdate.parentSampleIDFK = sampleJSONToPersist.parentSampleIDFK;
                            if (sampleJSONToPersist.project)
                                sampleToUpdate.project = sampleJSONToPersist.project;
                            if (sampleJSONToPersist.cruise)
                                sampleToUpdate.cruise = sampleJSONToPersist.cruise;
                            if (sampleJSONToPersist.platform)
                                sampleToUpdate.platform = sampleJSONToPersist.platform;
                            if (sampleJSONToPersist.organization)
                                sampleToUpdate.organization = sampleJSONToPersist.organization;
                            if (sampleJSONToPersist.contactIDFK)
                                sampleToUpdate.contactIDFK = sampleJSONToPersist.contactIDFK;
                            if (sampleJSONToPersist.contactName)
                                sampleToUpdate.contactName = sampleJSONToPersist.contactName;
                            if (sampleJSONToPersist.contactEmail)
                                sampleToUpdate.contactEmail = sampleJSONToPersist.contactEmail;
                            if (sampleJSONToPersist.cast)
                                sampleToUpdate.cast = sampleJSONToPersist.cast;
                            if (sampleJSONToPersist.stationName)
                                sampleToUpdate.stationName = sampleJSONToPersist.stationName;
                            if (sampleJSONToPersist.owner)
                                sampleToUpdate.owner = sampleJSONToPersist.owner;
                            if (sampleJSONToPersist.bottle)
                                sampleToUpdate.bottle = sampleJSONToPersist.bottle;
                            if (sampleJSONToPersist.depth)
                                sampleToUpdate.depth = sampleJSONToPersist.depth;
                            if (sampleJSONToPersist.analysis)
                                sampleToUpdate.analysis = sampleJSONToPersist.analysis;

                            // Dates are in epoch seconds so they must be converted
                            if (sampleJSONToPersist.plannedStartDate && sampleJSONToPersist.plannedStartDate !== '') {
                                sampleToUpdate.plannedStartDate = new Date(sampleJSONToPersist.plannedStartDate);
                            }
                            if (sampleJSONToPersist.plannedEndDate && sampleJSONToPersist.plannedEndDate !== '') {
                                sampleToUpdate.plannedEndDate = new Date(sampleJSONToPersist.plannedEndDate);
                            }


                            if (typeof sampleJSONToPersist.acquired !== 'undefined')
                                sampleToUpdate.acquired = sampleJSONToPersist.acquired;
                            // Dates are in epoch seconds so they must be converted
                            if (sampleJSONToPersist.actualStartDate && sampleJSONToPersist.actualStartDate !== '') {
                                sampleToUpdate.actualStartDate = new Date(sampleJSONToPersist.actualStartDate);
                            }
                            if (sampleJSONToPersist.actualEndDate && sampleJSONToPersist.actualEndDate !== '') {
                                sampleToUpdate.actualEndDate = new Date(sampleJSONToPersist.actualEndDate);
                            }

                            if (sampleJSONToPersist.location)
                                sampleToUpdate.location = sampleJSONToPersist.location;
                            if (sampleJSONToPersist.howPreserved)
                                sampleToUpdate.howPreserved = sampleJSONToPersist.howPreserved;
                            if (sampleJSONToPersist.whereStored)
                                sampleToUpdate.whereStored = sampleJSONToPersist.whereStored;
                            if (sampleJSONToPersist.whereDataResides)
                                sampleToUpdate.whereDataResides = sampleJSONToPersist.whereDataResides;

                            if (sampleJSONToPersist.modEpochSeconds)
                                sampleToUpdate.modEpochSeconds = sampleJSONToPersist.modEpochSeconds;
                            if (sampleJSONToPersist.modByName)
                                sampleToUpdate.modByName = sampleJSONToPersist.modByName;
                            if (sampleJSONToPersist.modByEmail)
                                sampleToUpdate.modByEmail = sampleJSONToPersist.modByEmail;

                            // TODO kgomes get view names working

                            // Now save it
                            sampleToUpdate.save(function (errLocal, savedSample) {
                                // Check for the error first
                                if (errLocal) {
                                    me.logger.error("Error trying to update the existing sample:", errLocal);
                                    if (callback)
                                        callback(errLocal);
                                } else {
                                    me.logger.debug("Sample updated successfully", savedSample);
                                    if (callback)
                                        callback(null, savedSample);
                                }
                            });
                        } else {
                            // No sample was found with that ID, create a new one
                            me.logger.warn("Tried to update Sample with ID " + sampleJSONToPersist._id +
                                " but no Sample was found, will just create a new one");
                            this.createAndSaveSampleFromJSON(sampleJSONToPersist, callback);
                        }
                    }
                });
            } else {
                this.createAndSaveSampleFromJSON(sampleJSONToPersist, callback);
            }
        } else {
            // Create and save a new sample
            this.createAndSaveSampleFromJSON(sampleJSONToPersist, callback);
        }
    };

    // Helper method to create and save a new sample
    this.sampleSchema.statics.createAndSaveSampleFromJSON = function (sampleJSON, callback) {
        me.logger.debug("Going to create a new sample from JSON:", sampleJSON);

        // Remove any possible ID
        delete sampleJSON._id;

        // Create the new sample
        var newSample = new this(sampleJSON);

        // The dates don't quite work so replace it with the correct ones
        if (sampleJSON.plannedStartDate && sampleJSON.plannedStartDate !== '') {
            newSample.plannedStartDate = new Date(sampleJSON.plannedStartDate);
        }
        if (sampleJSON.plannedEndDate && sampleJSON.plannedEndDate !== '') {
            newSample.plannedEndDate = new Date(sampleJSON.plannedEndDate);
        }
        if (sampleJSON.actualStartDate && sampleJSON.actualStartDate !== '') {
            newSample.actualStartDate = new Date(sampleJSON.actualStartDate);
        }
        if (sampleJSON.actualEndDate && sampleJSON.actualEndDate !== '') {
            newSample.actualEndDate = new Date(sampleJSON.actualEndDate);
        }

        // Save it
        me.logger.debug("New sample just before save:", newSample);
        newSample.save(function (err, savedSample) {
            // Check for any errors
            if (err) {
                me.logger.error("Error saving new sample:", err);
                if (callback) {
                    callback(err);
                }
            } else {
                // Send the new sample to the callback
                me.logger.debug("Sample saved OK:", savedSample);
                if (callback)
                    callback(null, savedSample);
            }
        });
    };

    // Create the model
    this.Sample = mongoose.model('Sample', this.sampleSchema);

    // This is the method to return the sample model object
    this.getSample = function () {
        return me.Sample;
    };
}

// The factory method for constructing the Sample service
exports.createSample = function (mongoose) {
    // Create the new Sample model
    return new Sample(mongoose);
};