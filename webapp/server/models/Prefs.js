// Import logging module
var log4js = require('log4js');

function Prefs(mongoose) {
    // Grab reference to self for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');
    me.logger.debug("Creating Prefs Schema and Model");

    /*
     * NOTE: preliminary schema; besides the associated user (via 'uid'), only with the
     * 'planning' section (to "transfer" the currently associated handling in planning editor)
     *
     * TODO For now declaring an explicit 'uid'
     */
    me.prefsSchema = new mongoose.Schema({
        uid: String,

        planning: {
            selectedPlatforms:  Array
        }
    });

    me.Prefs = mongoose.model('Prefs', me.prefsSchema);

    me.getPrefs = function() {
        return me.Prefs;
    };
}

exports.createPrefs = function (mongoose) {
    return new Prefs(mongoose);
};
