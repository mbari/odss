// Import logging library
var log4js = require('log4js');

// The constructor
function User(mongoose) {
    // Create a reference to this for callbacks
    var me = this;

    // Grab logger defined in app.js
    me.logger = log4js.getLogger('app');
    me.logger.debug("Creating User service object");

    // Grab the Schema from the mongoose connection
    var Schema = mongoose.Schema;

    // Create the user schema
    this.userSchema = new Schema({
        local: {
            name: String,
            email: String,
            password: String
        },
        openid: {
            identifier: String,
            token: String,
            email: String,
            name: String
        },
        facebook: {
            identifier: String,
            token: String,
            email: String,
            name: String
        },
        twitter: {
            identifier: String,
            token: String,
            displayName: String,
            username: String
        },
        google: {
            identifier: String,
            token: String,
            email: String,
            name: String
        }

    });

    // Create the model
    this.User = mongoose.model('User', this.userSchema);

    // The method to return the model
    this.getUser = function () {
        return me.User;
    };
}

// The factory method for constructing the User service
exports.createUser = function (mongoose) {
    // Create the new User model
    return new User(mongoose);
};