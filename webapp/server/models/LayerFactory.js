/**
 * This class provides all the model and function related to Layer objects
 * @type {exports}
 */

// Import logging module
var log4js = require('log4js');

// The constructor
function LayerFactory(mongoose) {
    // Grab reference to self for callback
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');
    me.logger.debug("Creating Layer Schema and Model ...");

    // This is the Layer model
    var Layer = null;

    // Create the layer schema
    var layerSchema = new mongoose.Schema({
        _ver: Number,
        name: String,
        type: String,
        url: String,
        legendUrl: String,
        layerParams: {
            format: String,
            bgcolor: String,
            transparent: String,
            elevation: String,
            layers: String,
            exceptions: String,
            srs: String,
            request: String,
            version: String,
            service: String,
            map: String
        },
        layerOptions: {
            minhoursback: Number,
            hoursbetween: Number,
            timeformat: String
        },
        views: [
            {
                name: String,
                folder: String
            }
        ]
    });

    // Add the method to persist a JSON object that should contain layer fields
    layerSchema.statics.persistLayerJSON = function (layerJSONToPersist, callback) {
        me.logger.debug("Going to persist layer:", layerJSONToPersist);

        // First thing to do is check for an ID. If it does not exist, we assume it's a new layer
        if (layerJSONToPersist._id && layerJSONToPersist._id !== '') {

            // Since we have an ID, we want to validate if it is a valid ObjectID, we
            // can create a regular expression to look for a 24 character hex pattern.
            // That should at least weed out any weird IDs
            var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
            var matchArray = checkForHexRegExp.exec(layerJSONToPersist._id);
            if (matchArray) {
                me.logger.debug("ID appears to be valid, will try an update");

                // Try to find the Layer document by it's ID
                Layer.findById(layerJSONToPersist._id, function (err, layerToUpdate) {
                    // Check for an error
                    if (err) {
                        // Send the error back
                        me.logger.error("Error trying to find Layer by ID " + layerJSONToPersist._id);
                        if (callback)
                            callback(err);
                    } else {
                        // Check to see if a document was found
                        if (layerToUpdate) {

                            // First we want to check the version number to make sure it matches what is
                            // in the persistent store so we are only saving changes to the most recent
                            // version of the layer
                            if (layerJSONToPersist._ver && layerToUpdate._ver &&
                                layerJSONToPersist._ver === layerToUpdate._ver) {

                                // Create a flag to track if changes were actually made
                                var layerChanged = false;

                                // OK, we are good to go with the update
                                if (layerJSONToPersist.name && (!layerToUpdate.name ||
                                    layerToUpdate.name !== layerJSONToPersist.name)) {
                                    layerToUpdate.name = layerJSONToPersist.name;
                                    // Set changed flag
                                    layerChanged = true;
                                }
                                if (layerJSONToPersist.type && (!layerToUpdate.type ||
                                    layerToUpdate.type !== layerJSONToPersist.type)) {
                                    layerToUpdate.type = layerJSONToPersist.type;
                                    layerChanged = true;
                                }
                                if (layerJSONToPersist.url && (!layerToUpdate.url ||
                                    layerToUpdate.url !== layerJSONToPersist.url)) {
                                    layerToUpdate.url = layerJSONToPersist.url;
                                    layerChanged = true;
                                }
                                if (layerJSONToPersist.legendUrl && (!layerToUpdate.legendUrl ||
                                    layerToUpdate.legendUrl !== layerJSONToPersist.legendUrl)) {
                                    layerToUpdate.legendUrl = layerJSONToPersist.legendUrl;
                                    layerChanged = true;
                                }
                                if (layerJSONToPersist.layerParams) {
                                    // Make sure the layer to Update has layerParams
                                    if (!layerToUpdate.layerParams) layerToUpdate.layerParams = {};

                                    if (layerJSONToPersist.layerParams.format && (!layerToUpdate.layerParams.format ||
                                        layerToUpdate.layerParams.format !== layerJSONToPersist.layerParams.format)) {
                                        layerToUpdate.layerParams.format = layerJSONToPersist.layerParams.format;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.bgcolor && (!layerToUpdate.layerParams.bgcolor ||
                                        layerToUpdate.layerParams.bgcolor !== layerJSONToPersist.layerParams.bgcolor)) {
                                        layerToUpdate.layerParams.bgcolor = layerJSONToPersist.layerParams.bgcolor;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.transparent && (!layerToUpdate.layerParams.transparent ||
                                        layerToUpdate.layerParams.transparent !== layerJSONToPersist.layerParams.transparent)) {
                                        layerToUpdate.layerParams.transparent = layerJSONToPersist.layerParams.transparent;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.elevation && (!layerToUpdate.layerParams.elevation ||
                                        layerToUpdate.layerParams.elevation !== layerJSONToPersist.layerParams.elevation)) {
                                        layerToUpdate.layerParams.elevation = layerJSONToPersist.layerParams.elevation;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.layers && (!layerToUpdate.layerParams.layers ||
                                        layerToUpdate.layerParams.layers !== layerJSONToPersist.layerParams.layers)) {
                                        layerToUpdate.layerParams.layers = layerJSONToPersist.layerParams.layers;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.exceptions && (!layerToUpdate.layerParams.exceptions ||
                                        layerToUpdate.layerParams.exceptions !== layerJSONToPersist.layerParams.exceptions)) {
                                        layerToUpdate.layerParams.exceptions = layerJSONToPersist.layerParams.exceptions;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.srs && (!layerToUpdate.layerParams.srs ||
                                        layerToUpdate.layerParams.srs !== layerJSONToPersist.layerParams.srs)) {
                                        layerToUpdate.layerParams.srs = layerJSONToPersist.layerParams.srs;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.request && (!layerToUpdate.layerParams.request ||
                                        layerToUpdate.layerParams.request !== layerJSONToPersist.layerParams.request)) {
                                        layerToUpdate.layerParams.request = layerJSONToPersist.layerParams.request;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.version && (!layerToUpdate.layerParams.version ||
                                        layerToUpdate.layerParams.version !== layerJSONToPersist.layerParams.version)) {
                                        layerToUpdate.layerParams.version = layerJSONToPersist.layerParams.version;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.service && (!layerToUpdate.layerParams.service ||
                                        layerToUpdate.layerParams.service !== layerJSONToPersist.layerParams.service)) {
                                        layerToUpdate.layerParams.service = layerJSONToPersist.layerParams.service;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerParams.map && (!layerToUpdate.layerParams.map ||
                                        layerToUpdate.layerParams.map !== layerJSONToPersist.layerParams.map)) {
                                        layerToUpdate.layerParams.map = layerJSONToPersist.layerParams.map;
                                        layerChanged = true;
                                    }
                                }
                                if (layerJSONToPersist.layerOptions) {
                                    // Make sure the layer to Update has layerOptions
                                    if (!layerToUpdate.layerOptions) layerToUpdate.layerOptions = {};

                                    if (layerJSONToPersist.layerOptions.minhoursback && (!layerToUpdate.layerOptions.minhoursback ||
                                        layerToUpdate.layerOptions.minhoursback !== layerJSONToPersist.layerOptions.minhoursback)) {
                                        layerToUpdate.layerOptions.minhoursback = layerJSONToPersist.layerOptions.minhoursback;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerOptions.hoursbetween && (!layerToUpdate.layerOptions.hoursbetween ||
                                        layerToUpdate.layerOptions.hoursbetween !== layerJSONToPersist.layerOptions.hoursbetween)) {
                                        layerToUpdate.layerOptions.hoursbetween = layerJSONToPersist.layerOptions.hoursbetween;
                                        layerChanged = true;
                                    }

                                    if (layerJSONToPersist.layerOptions.timeformat && (!layerToUpdate.layerOptions.timeformat ||
                                        layerToUpdate.layerOptions.timeformat !== layerJSONToPersist.layerOptions.timeformat)) {
                                        layerToUpdate.layerOptions.timeformat = layerJSONToPersist.layerOptions.timeformat;
                                        layerChanged = true;
                                    }
                                }

                                if (layerChanged) {
                                    // Bump the version number
                                    layerToUpdate._ver = layerToUpdate._ver + 1;

                                    // Now save it
                                    layerToUpdate.save(function (errLocal, savedLayer) {
                                        // Check for the error first
                                        if (errLocal) {
                                            me.logger.error("Error trying to update the existing layer:", errLocal);
                                            if (callback)
                                                callback(errLocal);
                                        } else {
                                            me.logger.debug("Layer updated successfully", savedLayer);
                                            if (callback)
                                                callback(null, savedLayer);
                                        }
                                    });
                                }
                            } else {
                                me.logger.warn("Update attempted on layer with _id " + layerJSONToPersist._id +
                                    "and _ver " + layerJSONToPersist._ver +
                                    " but the existing layer with that _id did not have that version, it had version " +
                                    layerToUpdate._ver);
                                if (callback)
                                    callback(new Error("Update attempted on layer with _id " + layerJSONToPersist._id +
                                        "and _ver " + layerJSONToPersist._ver +
                                        " but the existing layer with that _id did not have that version, it had version " +
                                        layerToUpdate._ver));
                            }
                        } else {
                            // No layer was found with that ID, create a new one
                            me.logger.warn("Tried to update Layer with ID " + layerJSONToPersist._id +
                                " but no Layer was found, will just create a new one");
                            Layer.createAndSaveLayerFromJSON(layerJSONToPersist, callback);
                        }
                    }
                });
            } else {
                Layer.createAndSaveLayerFromJSON(layerJSONToPersist, callback);
            }
        } else {
            // Create and save a new layer
            Layer.createAndSaveLayerFromJSON(layerJSONToPersist, callback);
        }
    };

    // Helper method to create and save a new layer
    layerSchema.statics.createAndSaveLayerFromJSON = function (layerJSON, callback) {
        me.logger.debug("Going to create a new layer from JSON:", layerJSON);

        // Remove any possible ID
        delete layerJSON._id;

        // Create the new layer
        var newLayer = new Layer(layerJSON);

        // If there are no views defined
        if (!layerJSON.views || layerJSON.views.length === 0) {
            newLayer.views = [
                {
                    name: "Public",
                    folder: "Other"
                }
            ];
        }

        // Set the version to 1
        newLayer._ver = 1;

        // Save it
        me.logger.debug("New layer just before save:", newLayer);
        newLayer.save(function (err, savedLayer) {
            // Check for any errors
            if (err) {
                me.logger.error("Error saving new layer:", err);
                if (callback) {
                    callback(err);
                }
            } else {
                // Send the new layer to the callback
                me.logger.debug("Layer saved OK:", savedLayer);
                if (callback)
                    callback(null, savedLayer);
            }
        });
    };

    // The method to add a view name to the array of views for the given layer _id and _ver
    layerSchema.statics.addOrUpdateViewName = function (_id, viewName, folderName, callback) {
        me.logger.debug("Going to add (or update) view " + viewName + " in folder " +
            folderName + " to layer with _id " + _id);
        // Find the layer in the persistent store
        Layer.findById(_id, function (err, layerToUpdate) {
            // Check for error first
            if (err) {
                me.logger.error("Error trying to find layer with _id " + _id);
                if (callback)
                    callback(err);
            } else {
                // Make sure a layer was found
                if (layerToUpdate) {
                    // Iterate over the existing views array to look for view with the proper name
                    var viewFound = false;
                    var viewIndex = -1;
                    for (var i = 0; i < layerToUpdate.views.length; i++) {
                        if (layerToUpdate.views[i].name === viewName) {
                            viewFound = true;
                            viewIndex = i;
                        }
                    }
                    // If the view name is not in the array of view names, push and save, else do nothing
                    if (!viewFound) {
                        // Now add it
                        layerToUpdate.views.push({
                            name: viewName,
                            folder: folderName
                        });

                        // Bump the version
                        if (layerToUpdate._ver) {
                            layerToUpdate._ver = layerToUpdate._ver + 1;
                        } else {
                            layerToUpdate._ver = 1;
                        }

                        // Now update it
                        layerToUpdate.save(function (errLocal, savedLayer) {
                            if (errLocal) {
                                me.logger.error("Error trying to save layer after adding viewName ", errLocal);
                                if (callback)
                                    callback(errLocal);
                            } else {
                                // Send the updated layer
                                if (callback)
                                    callback(null, savedLayer);
                            }
                        });
                    } else {
                        // This means the view was found and may need to be updated, first check folder against
                        // incoming folder name
                        if (folderName !== layerToUpdate.views[viewIndex].folder) {
                            layerToUpdate.views[viewIndex].folder = folderName;

                            // Bump the version
                            if (layerToUpdate._ver) {
                                layerToUpdate._ver = layerToUpdate._ver + 1;
                            } else {
                                layerToUpdate._ver = 1;
                            }


                            // Now update it
                            layerToUpdate.save(function (errLocal, savedLayer) {
                                if (errLocal) {
                                    me.logger.error("Error trying to save layer after updating views folder name ", errLocal);
                                    if (callback)
                                        callback(errLocal);
                                } else {
                                    // Send the updated layer
                                    if (callback)
                                        callback(null, savedLayer);
                                }
                            });
                        }
                    }
                } else {
                    me.logger.warn("Trying to add or update a view name " + viewName + " to layer with _id " +
                        _id + " but no layer with that _id was found");
                    if (callback)
                        callback(new Error("No layer with _id " + _id + " was found"));
                }
            }
        });
    };

    // The method to remove a view name from the array of views for the given layer _id
    layerSchema.statics.removeViewName = function (_id, viewName, callback) {
        me.logger.debug("Going to remove view " + viewName + " from layer with _id " + _id);
        // Find the layer in the persistent store
        Layer.findById(_id, function (err, layerToUpdate) {
            // Check for error first
            if (err) {
                me.logger.error("Error trying to find layer with _id " + _id);
                if (callback)
                    callback(err);
            } else {
                // Make sure a layer was found
                if (layerToUpdate) {
                    // Loop over the array of views to find the existing view with matching name
                    var indexOfViewName = -1;
                    for (var i = 0; i < layerToUpdate.views.length; i++) {
                        if (layerToUpdate.views[i].name === viewName) {
                            indexOfViewName = i;
                        }
                    }
                    if (indexOfViewName > -1) {
                        // Remove it
                        layerToUpdate.views.splice(indexOfViewName, 1);

                        // Bump the version
                        if (layerToUpdate._ver) {
                            layerToUpdate._ver = layerToUpdate._ver + 1;
                        } else {
                            layerToUpdate._ver = 1;
                        }

                        // Now update it
                        layerToUpdate.save(function (errLocal, savedLayer) {
                            if (errLocal) {
                                me.logger.error("Error trying to save layer after removing viewName ", errLocal);
                                if (callback)
                                    callback(errLocal);
                            } else {
                                // Send the updated layer
                                if (callback)
                                    callback(null, savedLayer);
                            }
                        });
                    }
                } else {
                    me.logger.warn("Trying to remove view name " + viewName + " from layer with _id " +
                        _id + " but no layer with that _id was found");
                    if (callback)
                        callback(new Error("No layer with _id " + _id + " was found"));
                }
            }
        });
    };

    // Create the model
    Layer = mongoose.model('Layer', layerSchema);

    // This is the method to return the layer model object
    this.getLayer = function () {
        return Layer;
    };

}

// The factory method for constructing the Layer service
exports.createLayerFactory = function (mongoose) {
    // Create the new Layer model
    return new LayerFactory(mongoose);
};