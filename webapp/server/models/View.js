// Grab logging module
var log4js = require('log4js');

// The constructor
function View(mongoose) {
    // Create a reference to this for callbacks
    var me = this;

    // Grab the logger defined in app.js
    me.logger = log4js.getLogger('app');
    me.logger.debug("Creating View Schema and Model");

    // Grab the Schema from mongoose
    var Schema = mongoose.Schema;

    // Create the view schema
    this.viewSchema = new Schema({
        name: {type: String},
        repoBasePath: {type: String}

        // ODSS-132:
        ,startDate: {type: Date}
        ,endDate:   {type: Date}
    });

    // Create the model
    this.View = mongoose.model('View', this.viewSchema);

    // This is the method to return the model object
    this.getView = function (){
        return me.View;
    };
}

// The factory method for constructing the View service
exports.createView = function (mongoose) {
    // Create the new View model
    return new View(mongoose);
};
