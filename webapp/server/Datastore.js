// Import logging
var log4js = require('log4js');

// Import the Mongoose dependency
var mongoose = require('mongoose');

// Grab the postgres library and create a connection pool
var Pool = require('pg').Pool;

// The moment library for dealing with dates
var moment = require('moment');

// The constructor
function Datastore(opts) {

    // Grab a reference to this for callback
    var me = this;

    // Grab the logger defined in app.js
    this.logger = log4js.getLogger('app');

    // *************************************************************************** //
    // Set up the MongoDB connection
    // *************************************************************************** //

    // Verify the options specify the MongoDB host and database name
    if (!opts.mongodbHost || !opts.mongodbName) {
        me.logger.error("MongoDB host name or database not specified.");
        return new Error("MongoDB host and database not specified.");
    }
    me.logger.debug("Connection to MongoDB on host " + opts.mongodbHost +
        " using database " + opts.mongodbName);

    // Grab the Mongoose connection object
    this.db = mongoose.connection;

    // Set up event handlers for MongoDB connection
    this.db.on('connecting', function () {
        me.logger.info('connecting to MongoDB...');
    });
    this.db.on('error', function (error) {
        me.logger.error('Error in MongoDb connection: ' + error);
        mongoose.disconnect();
    });
    this.db.on('connected', function () {
        me.logger.info('MongoDB connected!');
    });
    this.db.on('reconnected', function () {
        me.logger.info('MongoDB reconnected!');
    });
    this.db.on('disconnected', function () {
        me.logger.warn('MongoDB disconnected!');
        // Wait for a bit and try again
        setTimeout(function () {
            mongoose.connect("mongodb://" + opts.mongodbUsername + ":" + opts.mongodbPassword + "@" + opts.mongodbHost +
                ":" + opts.mongodbPort + "/" + opts.mongodbName, { server: { auto_reconnect: true } });
        }, 10000);
    });

    // Connect using the string given
    mongoose.connect("mongodb://" + opts.mongodbUsername + ":" + opts.mongodbPassword + "@" + opts.mongodbHost +
        ":" + opts.mongodbPort + "/" + opts.mongodbName, { server: { auto_reconnect: true } });

    // *************************************************************************** //
    // Set up the PostgreSQL connection
    // *************************************************************************** //

    // Let's instantiate a new PostgreSQL connection pool
    this.pool = new Pool({
        user: opts.trackingDBUsername,
        password: opts.trackingDBPassword,
        host: opts.trackingDBHostname,
        database: opts.trackingDBDatabaseName,
        max: 10,
        idleTimeoutMillis: 1000
    });

    // Create the error handler for the Postgres connection pool
    this.pool.on('error', function (error, client) {
        me.logger.error('An error was caught by one of the postgres connections in the pool');
        me.logger.error(error);
    });

    // *************************************************************************** //
    // Now define the methods that can be called on the data store object
    // *************************************************************************** //

    // This is a method that returns the mongoose connection
    this.getMongoose = function () {
        return mongoose;
    };

    // Return the db connection
    this.getDB = function () {
        return me.db;
    };

    // Now create all the Mongoose models that the application needs
    this.layerFactory = require('./models/LayerFactory').createLayerFactory(mongoose);
    this.Layer = this.layerFactory.getLayer();
    this.platformFactory = require('./models/PlatformFactory').createPlatformFactory(mongoose);
    this.Platform = this.platformFactory.getPlatform();
    this.Prefs = require('./models/Prefs').createPrefs(mongoose).getPrefs();
    this.sampleSchema = require('./models/Sample').createSample(mongoose);
    this.Sample = this.sampleSchema.getSample();
    this.User = require('./models/User').createUser(mongoose).getUser();
    this.View = require('./models/View').createView(mongoose).getView();

    // Now the methods so that other clients can utilize them
    this.getLayer = function () {
        return this.Layer;
    };
    this.getPlatform = function () {
        return this.Platform;
    };
    this.getSample = function () {
        return this.Sample;
    };
    this.getUser = function () {
        return this.User;
    };
    this.getPrefs = function () {
        return this.Prefs;
    };
    this.getView = function () {
        return this.View;
    };

    // This is a method to get a listing of all the platforms in the tracking database (that are not AIS entries)
    this.getTrackingPlatforms = function (callback) {
        me.logger.debug('getTrackingPlatforms called');
        // Create the response
        var response = [];

        // Query for the platforms
        me.pool.query("select platform.id, platform_type.name as type, platform.name, platform.mmsi, " +
            "platform.imei, platform.source, platform.color, platform.abbr from platform inner join platform_type on " +
            "platform.platformtype_id = platform_type.id where platform_type.name != 'ais'", function (err, result) {

                // Check for errors
                if (err) {
                    me.logger.error('Error running query');
                    me.logger.error(err);
                    if (callback)
                        callback(err);
                } else {
                    me.logger.debug('Query came back with ' + result.rows.length + ' platforms');
                    if (result && result.rows && result.rows.length > 0) {
                        // Create an array to see if the platform name was already added
                        var platformNames = [];

                        // Loop over the results
                        for (var i = 0; i < result.rows.length; i++) {
                            // Check to see if we have added one with this name yet
                            if (platformNames.indexOf(result.rows[i].name) === -1) {
                                // Add it to the tracking array
                                platformNames.push(result.rows[i].name);

                                // Define a default color
                                var color = "#FF0000";

                                // If there is a color already, use that one instead of the default
                                if (result.rows[i].color && result.rows[i].color !== null && result.rows[i].color !== '')
                                    color = result.rows[i].color;

                                // Create a default abbreviation based on short name
                                var abbreviation = result.rows[i].name;
                                if (abbreviation.length > 3) {
                                    abbreviation = abbreviation.substring(0, 2);
                                }

                                // Now see if a abbr was already in the DB and use it if there
                                if (result.rows[i].abbr && result.rows[i].abbr !== null && result.rows[i].abbr !== '')
                                    abbreviation = result.rows[i].abbr;

                                // Add the platform to the response array
                                response.push(
                                    {
                                        typeName: result.rows[i].type,
                                        name: result.rows[i].name,
                                        color: color,
                                        abbreviation: abbreviation
                                    }
                                );
                            }
                        }
                    }
                    if (callback)
                        callback(null, response);
                }
            });

    };

    // This is a method to return the tracking information for a specific platform
    this.getTrack = function (platformID, startDate, endDate, lastNumberOfFixes, returnSRS, ulCorner, lrCorner,
        sortOrder, returnFormat, numberOfPoints, taperAccuracy, callback) {
        me.logger.debug("Get track called");

        // Assign a default return SRS
        var localReturnSRS = 4326;

        // Make sure the platform ID is specified
        if (typeof platformID === 'undefined' || platformID === null || platformID === '') {
            if (callback)
                callback(new Error("PlatformID not specified"));
        } else {

            // Check for specified return SRS
            if (typeof returnSRS !== 'undefined' || returnSRS !== null || returnSRS !== '') {
                // TODO validate the SRS number is a supported number
                // TODO should I check to see if SRS is an integer?
                localReturnSRS = returnSRS;
            }

            // Now we have the platform ID, we need to get the name for the SQL query
            this.getPlatform().findById(platformID, function (err, platform) {

                if (err) {
                    me.logger.error("Error looking for platform with ID " + platformID);
                    if (callback)
                        callback(new Error("PlatformID " + platformID + " was not recognized"));
                } else {
                    // Make sure a platform was found
                    if (platform) {
                        // TODO kgomes: might have to query for list of IDs first, then use those in the second query. I had all
                        // kinds of problems combining the queries.
                        var findIdsQuery = 'SELECT id from platform where name =\'' + platform.name + '\'';

                        me.pool.query(findIdsQuery, function (err, idresult) {
                            if (err) {
                                me.logger.error('Error looking for platform IDs: ' + selectPointsText);
                                me.logger.error(err);
                                if (callback)
                                    callback(err);
                            } else {
                                // We first want to make sure there were some IDs found. If the MongoDB and
                                // PostgreSQL DBs get out of sync (manual editing of either), it can happen
                                // that there are no matching platforms in the tracking DB
                                if (!idresult || !idresult.rows || idresult.rows.length <= 0) {
                                    var errorMessage = "No IDs were returned from the tracking DB for platform named " + platform.name +
                                    ", this means MongoDB platforms and tracking DB platforms are out of sync.";
                                    me.logger.error(errorMessage);
                                    callback(new Error(errorMessage));
                                } else {

                                    // Now read back any IDs
                                    var idClause = '('
                                    for (var idCounter = 0; idCounter < idresult.rows.length; idCounter++) {
                                        if (idCounter > 0) idClause += ',';
                                        idClause += idresult.rows[idCounter].id;
                                    }
                                    idClause += ')';
                                    me.logger.debug('ID clause will be: ' + idClause);

                                    // Create the select part of the query first
                                    var selectPointsText = 'SELECT datetime, ST_AsText(ST_Transform(geom,' + localReturnSRS + ')) ' +
                                        'FROM position WHERE platform_id in ' + idClause;

                                    // First look for last number of fixes as this would override any date selection
                                    if (typeof lastNumberOfFixes !== 'undefined' && lastNumberOfFixes !== null && !isNaN(parseInt(lastNumberOfFixes)) && parseInt(lastNumberOfFixes) > 0) {
                                        selectPointsText += ' ORDER BY datetime DESC, platform_id LIMIT ' + lastNumberOfFixes;
                                    } else {
                                        // Now if there is a startDateTime
                                        if (typeof startDate !== 'undefined' && startDate !== null) {
                                            // Add the start date clause
                                            selectPointsText += ' AND datetime > \'' + startDate.toISOString() + '\'';
                                        }

                                        // Now if there is a endDateTime
                                        if (typeof endDate !== 'undefined' && endDate !== null) {
                                            // Add the end date clause
                                            selectPointsText += ' AND datetime < \'' + endDate.toISOString() + '\'';
                                        }

                                        // Also add geospatial constraint if applicable
                                        if (typeof lrCorner !== 'undefined' && lrCorner !== null && lrCorner !== '' &&
                                            typeof ulCorner !== 'undefined' && ulCorner !== null && ulCorner !== '') {
                                            // OK, looks like the corners are specified, so let's try to extract the coordinates from
                                            // the strings
                                            var lrCornerLonLat = lrCorner.split(',');
                                            var lrCornerLon;
                                            var lrCornerLat;
                                            var ulCornerLonLat = ulCorner.split(',');
                                            var ulCornerLon;
                                            var ulCornerLat;

                                            // Make sure the arrays have two elements
                                            if (lrCornerLonLat.length === 2) {
                                                lrCornerLon = parseFloat(lrCornerLonLat[0]);
                                                lrCornerLat = parseFloat(lrCornerLonLat[1]);
                                            } // TODO put error handling here

                                            if (ulCornerLonLat.length === 2) {
                                                ulCornerLon = parseFloat(ulCornerLonLat[0]);
                                                ulCornerLat = parseFloat(ulCornerLonLat[1]);
                                            } // TODO put error handling here

                                            // Now make sure they are all numbers
                                            if (!isNaN(lrCornerLon) && !isNaN(lrCornerLat) && !isNaN(ulCornerLon) && !isNaN(ulCornerLat)) {
                                                selectPointsText += ' AND ST_Within(geom, GeometryFromText(\'POLYGON((' + ulCornerLon + ' ' +
                                                    ulCornerLat + ',' + lrCornerLon + ' ' + ulCornerLat + ',' + lrCornerLon + ' ' +
                                                    lrCornerLat + ',' + ulCornerLon + ' ' + lrCornerLat + ',' + ulCornerLon + ' ' + ulCornerLat +
                                                    '))\',4326))';
                                            }
                                        }
                                        // TODO check to make sure start is before end times if they both exist

                                        // Add the sort criterion
                                        if (typeof sortOrder !== 'undefined' && sortOrder !== null && (sortOrder === 'asc' || sortOrder === 'ASC' || sortOrder === 'desc' || sortOrder === 'DESC')) {
                                            selectPointsText += ' ORDER BY datetime ' + sortOrder;
                                        } else {
                                            selectPointsText += ' ORDER BY datetime DESC';
                                        }
                                    }

                                    // TODO we should really make sure that either the lastNumberOfFixes is specified, or some date range is specified.  Never want full data set I don't think!!!

                                    // Set a default return format
                                    var localReturnFormat = 'json';
                                    if (typeof returnFormat !== 'undefined' && returnFormat !== null) {
                                        if (returnFormat === 'CSV' || returnFormat === 'csv') {
                                            localReturnFormat = 'csv';
                                        } else if (returnFormat === 'HTML' || returnFormat === 'html') {
                                            localReturnFormat = 'html';
                                        } else if (returnFormat === 'KML' || returnFormat === 'kml') {
                                            localReturnFormat = 'kml';
                                        } else if (returnFormat === 'KMLTrack' || returnFormat === 'kml-track' || returnFormat === 'kmltrack') {
                                            localReturnFormat = 'kmltrack';
                                        }
                                    }

                                    // Grab local copies of the numberOfPoints and taperAccuracy for the callback
                                    var localNumberOfPoints = numberOfPoints;
                                    var localTaperAccuracy = taperAccuracy;

                                    // Connect to the tracking database
                                    me.logger.debug("Query = " + selectPointsText);

                                    // Run the query
                                    me.pool.query(selectPointsText, function (err, result) {
                                        if (err) {
                                            me.logger.error('Error running query: ' + selectPointsText);
                                            me.logger.error(err);
                                            if (callback)
                                                callback(err);
                                        } else {
                                            if (result.rows.length > 0) {
                                                me.logger.debug("P: " + platformID + "->" + result.rows.length + " rows; "
                                                    + numberOfPoints + " points;taper=" + taperAccuracy);
                                            }

                                            me.generateResponse(platform, localReturnFormat, result, localNumberOfPoints,
                                                localTaperAccuracy, function (err, returnObject) {
                                                    if (err) {
                                                        if (callback) {
                                                            callback(err);
                                                        }
                                                    } else {
                                                        if (callback) {
                                                            callback(null, returnObject);
                                                        }
                                                    }
                                                }
                                            );
                                        }
                                    }
                                    );
                                }
                            }
                        });
                    } else {
                        if (callback) {
                            callback(new Error("No platform with ID " + platformID + " was found"));
                        }
                    }
                }
            });
        }
    };

    // This function takes in a resultset from the database call and then constructs the proper response object
    this.generateResponse = function (platform, returnFormat, resultset, numberOfPoints, taperAccuracy, callback) {
        me.logger.debug("Generate Reponse:platform->" + platform + ",returnFormat->" + returnFormat + ',numberOfPoints->' +
            numberOfPoints + ',taperAccuracy->' + taperAccuracy);
        // The object to return
        var returnObject = {
            status: 'success'
        };

        // A placeholder for any error messages
        var errorMessage = '';

        // Make sure a format was specified
        if (typeof returnFormat !== 'undefined') {
            // Write the header of the data
            if (returnFormat === 'csv') {
                returnObject.data = 'Date(GMT),longitude,latitude\n';
            } else if (returnFormat === 'html') {
                returnObject.data = '<!DOCTYPE html>\n';
                returnObject.data += '<html>\n';
                returnObject.data += '<head><title>' + platform.name + ' positions</title></head>\n';
                returnObject.data += '<body>\n';
                returnObject.data += '<script>\n';
                returnObject.data += '     var time = new Date().getTime();\n';
                returnObject.data += '     function refresh() {\n';
                returnObject.data += '         if(new Date().getTime() - time >= 60000) \n';
                returnObject.data += '             window.location.reload(true);\n';
                returnObject.data += '         else \n';
                returnObject.data += '             setTimeout(refresh, 10000);\n';
                returnObject.data += '     }\n';
                returnObject.data += '     setTimeout(refresh, 10000);\n';
                returnObject.data += '</script>\n'
                returnObject.data += '<table border=\'1\'>\n<tr><th>Date(GMT)</th><th>longitude</th><th>latitude</th></tr>\n';
            } else if (returnFormat === 'json') {
                returnObject.data = {
                    type: 'LineString',
                    timestamps: [],
                    coordinates: []
                };
            } else if (returnFormat === 'kml') {
                returnObject.data = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n' +
                    '<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n' +
                    '<Document>\n' + '<name>' + platform.name + ' Track</name>\n' +
                    '<Style id="platform-pos-point"><IconStyle><Icon><href>resources/images/map_pin.png</href></Icon></IconStyle></Style>' +
                    '<Folder id="platform-placemarks">\n<name>' + platform.name + ' Track</name>\n';
            } else if (returnFormat === 'kmltrack') {
                returnObject.data = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n' +
                    '<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\">\n' +
                    '<Folder id="' + platform.name + '-track">\n' +
                    '  <Placemark>\n    <gx:Track>\n';
            } else {
                returnObject.status = 'fail';
                errorMessage = 'Return format requested (' + returnFormat + ') was not understood';
            }
        } else {
            returnObject.status = 'fail';
            errorMessage = 'No return format supplied';
        }

        // Make sure the resultset is there
        if (typeof resultset === 'undefined' || resultset === null) {
            returnObject.status = 'fail';
            errorMessage = 'No result set supplied';
        } else {
            if (resultset && resultset.rows && resultset.rows.length > 0) {
                me.logger.debug("There are " + resultset.rows.length + " rows");

                // A variable to hold the number of points to skip as we are looping through
                var numPointsToSkip = 0;

                // Calculate one-third of the number of points requested in case a taper was also requested
                var oneThirdNumberOfPoints = 0;

                // And a counter to keep track of points as we are skipping
                var skipCounter = 1;

                // Now check to see if we need to skip any points
                if (typeof numberOfPoints !== 'undefined' &&
                    numberOfPoints !== null &&
                    numberOfPoints > 0 &&
                    resultset.rows.length > numberOfPoints) {
                    // First let's see if we are supposed to taper the accuracy
                    if (taperAccuracy) {
                        // This means that the first 1/3 of the points are at full resolution and then we fill
                        // in the last 2/3 by skipping rows.  So calculate the value of 1/3 of the points requested
                        oneThirdNumberOfPoints = Math.floor(numberOfPoints / 3);

                        // Now we take the last 2/3 and figure out how many to skip in that group to come up
                        // with the proper number of points
                        var twoThirdsNumberOfPoints = numberOfPoints - oneThirdNumberOfPoints;

                        numPointsToSkip = Math.floor((resultset.rows.length - oneThirdNumberOfPoints - 1) / (twoThirdsNumberOfPoints - 1));
                    } else {
                        // Calculate the number of points to skip by removing two because we will always add
                        // the start and end points
                        numPointsToSkip = Math.floor((resultset.rows.length - 2) / (numberOfPoints - 2));
                        me.logger.debug(numberOfPoints + " points were requested");
                        me.logger.debug("Number of points to skip = " + numPointsToSkip);

                    }
                }
                if (returnFormat === 'csv') {
                    for (var icsv = 0; icsv < resultset.rows.length; icsv++) {
                        if (icsv === 0 || icsv === resultset.rows.length - 1 || numPointsToSkip === 0 ||
                            (taperAccuracy && icsv < oneThirdNumberOfPoints) ||
                            (numPointsToSkip > 0 && skipCounter === numPointsToSkip)) {
                            skipCounter = 1;
                            returnObject.data += resultset.rows[icsv].datetime.toISOString() + "," +
                                resultset.rows[icsv].st_astext.split('(')[1].split(' ')[0] + ',' +
                                resultset.rows[icsv].st_astext.split('(')[1].split(' ')[1].split(')')[0] + '\n';
                        } else {
                            skipCounter++;
                        }
                    }
                } else if (returnFormat === 'html') {
                    for (var ihtml = 0; ihtml < resultset.rows.length; ihtml++) {
                        if (ihtml === 0 || ihtml === resultset.rows.length - 1 || numPointsToSkip === 0 ||
                            (taperAccuracy && ihtml < oneThirdNumberOfPoints) ||
                            (numPointsToSkip > 0 && skipCounter === numPointsToSkip)) {
                            skipCounter = 1;
                            returnObject.data += '<tr><td>' + resultset.rows[ihtml].datetime.toISOString() + "</td><td>" +
                                resultset.rows[ihtml].st_astext.split('(')[1].split(' ')[0] + '</td><td>' +
                                resultset.rows[ihtml].st_astext.split('(')[1].split(' ')[1].split(')')[0] + '</td></tr>\n';
                        } else {
                            skipCounter++;
                        }
                    }
                } else if (returnFormat === 'json') {
                    for (var ijson = 0; ijson < resultset.rows.length; ijson++) {
                        if (ijson === 0 || ijson === resultset.rows.length - 1 || numPointsToSkip === 0 ||
                            (taperAccuracy && ijson < oneThirdNumberOfPoints) ||
                            (numPointsToSkip > 0 && skipCounter === numPointsToSkip)) {
                            skipCounter = 1;
                            // Add the timestamp
                            returnObject.data.timestamps.push(resultset.rows[ijson].datetime.getTime());
                            returnObject.data.coordinates.push([parseFloat(resultset.rows[ijson].st_astext.split('(')[1].split(' ')[0]),
                            parseFloat(resultset.rows[ijson].st_astext.split('(')[1].split(' ')[1].split(')')[0])]);
                        } else {
                            skipCounter++;
                        }
                    }
                } else if (returnFormat === 'kml') {
                    // First create a variable to hold the line string data
                    var lineStringCoordinates = '';
                    for (var ikml = 0; ikml < resultset.rows.length; ikml++) {
                        if (ikml === 0 || ikml === resultset.rows.length - 1 || numPointsToSkip === 0 ||
                            (taperAccuracy && ikml < oneThirdNumberOfPoints) ||
                            (numPointsToSkip > 0 && skipCounter === numPointsToSkip)) {
                            // Reset the counter
                            skipCounter = 1;

                            // Grab the timestamp
                            var timestamp_moment = moment(resultset.rows[ikml].datetime);
                            var lon = resultset.rows[ikml].st_astext.split('(')[1].split(' ')[0];
                            var lat = resultset.rows[ikml].st_astext.split('(')[1].split(' ')[1].split(')')[0];
                            // Add a placemark with the point data
                            returnObject.data += '<Placemark>\n<TimeStamp>\n<when>' + timestamp_moment.toISOString() +
                                '</when>\n</TimeStamp>\n<styleUrl>#platform-pos-point</styleUrl><Point><coordinates>' + lon + ',' + lat +
                                '</coordinates></Point></Placemark>'
                            // Append the LineString information
                            lineStringCoordinates += resultset.rows[ikml].st_astext.split('(')[1].split(' ')[0] + ',' +
                                resultset.rows[ikml].st_astext.split('(')[1].split(' ')[1].split(')')[0] + ',0\n';
                        } else {
                            skipCounter++;
                        }
                    }
                    // Now add the linestring placemark
                    returnObject.data += '<Placemark><name>' + platform.name + ' Track' + '</name>' +
                        '<Style><LineStyle><width>2</width><color>ff' + platform.color.substring(5) + platform.color.substring(3, 5) +
                        platform.color.substring(1, 3) + '</color></LineStyle></Style>' +
                        '<LineString><coordinates>' + lineStringCoordinates + '</coordinates></LineString></Placemark>';
                } else if (returnFormat === 'kmltrack') {
                    // For the KML track object, we need to print all the timestamp first, then all the coordinates so
                    // let's create an array to hold the coordinates until later
                    let coords = [];
                    for (var ikmltrack = 0; ikmltrack < resultset.rows.length; ikmltrack++) {
                        if (ikmltrack === 0 || ikmltrack === resultset.rows.length - 1 || numPointsToSkip === 0 ||
                            (taperAccuracy && ikmltrack < oneThirdNumberOfPoints) ||
                            (numPointsToSkip > 0 && skipCounter === numPointsToSkip)) {
                            // Reset the counter
                            skipCounter = 1;

                            // Grab the timestamp
                            var timestamp_moment = moment(resultset.rows[ikmltrack].datetime);
                            var lon = resultset.rows[ikmltrack].st_astext.split('(')[1].split(' ')[0];
                            var lat = resultset.rows[ikmltrack].st_astext.split('(')[1].split(' ')[1].split(')')[0];
                            // Add a when entry for the time and date
                            returnObject.data += '      <when>' + timestamp_moment.toISOString() + '</when>\n';
                            // Add the coordinate to the array
                            coords.push('      <gx:coord>' + lon + ' ' + lat + ' 0</gx:coord>\n      <gx:angles>0 0 0</gx:angles>\n');
                        } else {
                            skipCounter++;
                        }
                    }
                    // Now add the coordinates
                    coords.forEach(function (coord) {
                        returnObject.data += coord;
                    });
                }
            }
        }
        // Make sure a format was specified
        if (typeof returnFormat !== 'undefined') {
            // Write the footer of the data (if needed)
            if (returnFormat === 'csv') {
                // Do nothing
            } else if (returnFormat === 'html') {
                returnObject.data += '</table>\n</body>\n</html>';
            } else if (returnFormat === 'json') {
                // Do nothing
            } else if (returnFormat === 'kml') {
                returnObject.data += '</Folder>\n</Document>\n</kml>';
            } else if (returnFormat === 'kmltrack') {
                returnObject.data += '    </gx:Track>\n  </Placemark>\n</Folder>\n</kml>';
            }
        }

        // Now make the callback
        if (callback) {
            // If there was an error, send an error message
            if (returnObject.status === 'fail') {
                callback(new Error(errorMessage));
            } else {
                callback(null, returnObject);
            }
        }
    };
}

// The factory method for constructing the Datastore
exports.createDatastore = function (opts) {
    // Create the new Datastore
    return new Datastore(opts);
};
