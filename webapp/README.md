# Webapp Build

This directory contains the source for the web application server and client app that the Ant build file (build.xml) deploys.  The default target for the Ant build is 'deploy'.  There should be a .properties file in the ../servers directory that has a name that you use in the build call to specify which properties to load before the build.  If you have a server named 'myserver', there should be a file named '../servers/myserver/myserver.properties' and then you would call Ant passing in this as a property named 'server'.  For example, you might run:

ant -Dserver=myserver deploy

Here are the targets and what they do:

* create-node-config:
  * copies the server/config.js.template to server/config.js and replaces variables in the file with properties from your server.properites file.  These properites configure the connection to the MongoDB instance, the connection to tracking database, the Node.js server, and the  connection to the RabbitMQ server.
* deploy:
  * has a dependency on create-node-config, so runs that first
  * runs the 'npm install' command in the server/web/odss/odssplatim-ui directory to install the node packages that are necessary for the resource calendar
  * runs the 'gulp' command in the server/web/odss/odssplatim-ui directory to build the resource calendar component
  * copies the server and web application files to the deployment directory where the ODSS will be served from
  * runs the 'npm install' command in the server directory to install the node packages necessary for the odss node.js server
  * creates the logs directory in the server deployment location.  This is necessary or the ODSS node server will fail when trying to write log files.
  * copies the odss-proxy-pass.conf file to the apache configuration directory specified in the server.properties file to configure the appropriate URL mappings to reach the node server port.
* undeploy:
  * removes the odss server deployment directory.
  * removes the apache proxy pass configuration file to remove the URL to port mapping.
