# Oceanographic Decision Support System (ODSS)

## License

Copyright (C) 2022 Kevin Gomes - Monterey Bay Aquarium Research Institute

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Introduction

This software in this project is designed to provide operational and experiment support for oceanographic research. It is a web application with a NodeJS server backend that utilizes a MongoDB database to store information about experiments and the assets involved. It has several dependencies that it needs to operate. The required dependencies are a MongoDB database and a PostGIS database that conforms to the schema defined in another MBARI project called the Tracking database. This repository contains different Docker related resources to help you both deploy and develop the ODSS. You may choose to deploy the various components of the ODSS independently, but the docker resources in this project can help illustrate how you might do that.

## Development

### Dependencies

A docker-compose file has been created to help you bring up a development environment that will allow you to more easily develop the ODSS web application. There are quite a few dependencies as the ODSS is a complex set of related components. In order to start up the development environment, there are some steps that need to be taken.

1. Make sure Docker is installed on your local machine.
1. In the ```resources/dev``` folder, make a copy of the .env-dev.template and rename the file .env
1. Edit the .env file and define the location on your development machine where you would like all the relevant files from the various services to be placed. The property in the .env file is ODSS_BASEDIR_HOST. For our example, we will use ```/path/to/odss``` which would make the property in the .env file look like:

        ODSS_BASEDIR_HOST=/path/to/odss

1. Also add properties for any of the variables that start with ```ODSS_``` and make sure to use double quotes if variable value has any spaces. All these properties are necessary so the services will run correctly.
1. Note there are a lot of other properties below the ```ODSS_``` ones in the .env file that you can edit if you feel the need, but they will work as is just to spin up a development environment. Two variables you might want to change are the ```ERDDAP_baseUrl``` and the ```ERDDAP_baseHttpsUrl``` variables. They point to ```localhost``` which is fine for development, but if you want to reach the server from another machine, you could change ```localhost``` to be the name of your machine on the network and then other machines would be able to see all the services.
1. Once you have defined the necessary properties in the .env, open a terminal window and change into the ```resources/dev``` folder where you can run the ```setup.sh``` script which will create the appropriate directory structure and copy the correct files into those directories to prepare your machine to run the required services. Note that if you haven't run it before, you will be asked questions to generate a SSL certificate (you only need to do this once unless you delete the generated certificates). Here is what the script actually does:
    1. Reads the variables in the .env file
    1. Checks to make sure the host basedir exists
    1. If no ```httpd``` directory exists, it creates one
    1. If no ssl key/cert files exist, it kicks off a process to create them and puts them in the httpd directory
    1. If no ```httpd.conf``` file exists, it copies the one from the httpd directory in source code to that location
    1. If no ```rabbitmq``` directory exists, it creates one
    1. If no ```rabbitmq/data``` directory exists, it creates one
    1. If no ```rabbitmq/logs``` directory exists, it creates one
    1. If no ```erddap``` directory exists, it creates one
    1. If no ```erddap/erddapData``` directory exists, it creates one
    1. If no ```erddap/datasets.xml``` file exist, it copies the one from the correct version of erddap in local source to that location.
    1. If no ```thredds``` directory exists, it creates one
    1. If no ```thredds/content``` directory exists, it creates one
    1. If no ```thredds/data``` directory exists, it creates one and then copies in an example NetCDF file.
    1. If no ```thredds/logs``` directory exists, it creates one.
    1. If no ```thredds/tomcat``` directory exists, it creates one.
    1. If no ```thredds/tomcat/logs``` directory exists, it creates one.
    1. If no ```mapserver``` directory exists, it creates one.
    1. If no ```mapserver/logs``` directory exists, it creates one.
    1. If no ```mapserver/mapfiles``` directory exists, it creates one.
    1. If the ```mapserver/mapfiles/odss.map``` file does not exist, it copies one from the resources/dev/mapserver directory.
    1. If no ```mapserver/mapfiles/charts``` directory exists, it creates one and copies in some example data files.
    1. If no ```mapserver/mapfiles/stations``` directory exists, it creates one and copies in some example data files.
    1. If no ```mongodb``` directory exists, it creates one
    1. If no ```mbaritracking``` directory exists, it creates one
    1. If no ```mbaritracking/postgis``` directory exists, it creates one
    1. If no ```mbaritracking/client``` directory exists, it creates one
    1. If no ```mbaritracking/client/logs``` directory exists, it creates one
    1. It makes sure the resources/dev/trackingdb/tracking-init.sh script has executable permissions
    1. If no ```utils``` directory exists, it creates one
    1. If no ```utils/logs``` directory exists, it creates one
    1. If no ```utils/mapdatasets.json``` file exists, it copies the one from resources/dev/utils directory
    1. If no ```repo``` directory exists, it creates one
    1. If no ```repo/activities``` directory exists, it creates one
    1. If no ```repo/public``` directory exists, it creates one
    1. If no ```repo/platforms``` directory exists, it creates one
    1. If no ```repo/routine-products``` directory exists, it creates one
    1. If no ```server``` directory exists, it creates one
    1. If no ```server/logs``` directory exists, it creates one
1. Once the setup.sh is complete, you can start all the services by running the following in the ```resources/dev``` folder:

        docker-compose up

1. It can take a bit for all the services to start, but you can now visit the following URLs:
   1. [RabbitMQ Management Console https://localhost/rabbitmq/](https://localhost/rabbitmq/)
   1. [ERDDAP Server https://localhost/erddap/](https://localhost/erddap/)
   1. [THREDDS Server https://localhost/thredds/](https://localhost/thredds/)
   1. [Mapserver CGI-BIN executable https://localhost/cgi-bin/mapserv](https://localhost/cgi-bin/mapserv): This should just generate the API response: ```loadParams(): Web application error. No query information to decode. QUERY_STRING is set, but empty.``` which tells you it's working.
1. After all the services have started, there is a handy NodeJS script that will publish simulated platform locations to your local ODSS to start populating the tracking database. Once you have NodeJS installed on your local machine, you can then run:

        cd ./trackingdb
        npm install
        node sendTestAMQPMessages.js

This will start publishing simulated locations and will keep doing so until you kill the script by issuing a Ctrl-C.

#### M1 Macs

In order to run the docker compose services on an M1 Mac, you need to uncomment the following line at the end of the ```httpd/httpd.conf``` file:

    Mutex posixsem

also, you need to uncomment any ```platform: linux/arm64/v8``` lines in the docker-compose.yml file as well as commenting out the ```image: camptocamp/mapserver:8.0``` line and uncommenting the ```image: camptocamp/mapserver:8.0-arm64``` line. Also, be aware that, as of the time of this writing, ERDDAP and THREDDS do not have images for ARM architectures and sometime the Java processes inside AMD containers running on ARM hang so you might have issues (especially with THREDDS it seems) hanging.

### Workflow

At this point all the pieces are in place for you to start the ODSS on your host locally and begin development. **One thing you have to do before starting the application server is get an API key from ESRI for the default ESRI base map**. This is beyond the scope of these instructions, but once you have the ESRI key, you will want to make a copy of the config.js.template file and rename it config.js. The config.js file is where the server will look for information about the services it needs and to find out where it will write it's log files. You can edit the config.js file directly and change the default settings for the various parameters or you can override those variables by defining environment variables. At the bottom of the file is a property for the ESRI API key and you can paste in the key you received from ESRI. Another way is to use environment variables. The way you define the environment variables depends on how you start the services and how you want to develop. You can run the server in Visual Studio Code or you can run it from the command line.

#### Visual Studio Code

You can run the ODSS server component in VS Code so that you can set breakpoints, step through, etc. the server side code. While beyond the scope of the instructions here, you need Visual Studio Code installed and have it set up so that it can [debug NodeJS code](https://code.visualstudio.com/docs/nodejs/nodejs-debugging). You can create a configuration that runs the webapp/server/app.js file, but you need to make sure you have the proper environment variables set up so that the server will know where to look for the supported services. Here is an example launch configuration for the services started by the docker compose method (note you need to edit these settings to point to the right locations, i.e. the /path/to/directory will be different):

    {
      "type": "node",
      "request": "launch",
      "name": "Launch app.js",
      "skipFiles": ["<node_internals>/**"],
      "program": "${workspaceFolder}/webapp/server/app.js",
      "cwd": "${workspaceFolder}/webapp/server",
      "env": {
        "ODSS_LOG_FILE_LOCATION": "/path/to/directory/odss/server/logs",
        "ODSS_REPO_ROOT_DIR": "/path/to/directory/odss/server/repo",
        "ODSS_REPO_ROOT_DIR_URL": "/odss/data",
        "ODSS_TRACKING_UPDATE_INTERVAL": "30000",
        "ODSS_APP_LOGGER_LEVEL": "debug",
        "ODSS_MONGODB_HOST": "localhost",
        "ODSS_MONGODB_PORT": "27017",
        "ODSS_MONGODB_DATABASE_NAME": "odss",
        "ODSS_MONGODB_USERNAME": "odssuser",
        "ODSS_MONGODB_PASSWORD": "odsspassword",
        "ODSS_TRACKING_DATABASE_PROTOCOL": "postgres",
        "ODSS_TRACKING_DATABASE_HOST": "localhost",
        "ODSS_TRACKING_DATABASE_PORT": "5432",
        "ODSS_TRACKING_DATABASE_NAME": "mbaritracking",
        "ODSS_TRACKING_DATABASE_USERNAME": "odssuser",
        "ODSS_TRACKING_DATABASE_PASSWORD": "odsspassword",
        "ODSS_APP_SERVER_HOST_BASE_URL": "/",
        "ODSS_APP_SERVER_PORT": "3000",
        "ODSS_APP_SERVER_EXPRESS_COOKIE_PARSER_SECRET": "mysecret",
        "ODSS_APP_SERVER_EXPRESS_SESSION_SECRET": "mysessionsecret"
        "ODSS_APP_SERVER_ESRI_API_KEY": "xxxxxxxxxx"
      }
    }

#### Command line

To run the ODSS server from the command line, you will need to install NodeJS and a utility named nodemon and then configure the environment so that the server knows where to connect to the underlying services. See the section above about running the server in VS Code for the list of environment variables you will need to set. Once they are set, you can run the server by:

    cd webapp/server
    nodemon --ignore './node_modules/' --ignore './web/' app.js

The ignore arguments will prevent nodemon from monitoring all the library files that ODSS depends on as well as ignore changes to the files in the web application itself as it does not need to restart the server if the client side code changes. When running from the command line, you can either define environment variables or use the config.js method. Basically, when the server starts up, it will load the config.js file and will first look for environment variables and if none exist, it will use the values defined in the config.js file.

### Environment Layout

If you follow the development environment setup above, you will end up with the following mapping of files and directories. Note that the ```Host Location``` is relative to the directory that is associated with the ```ODSS_BASEDIR_HOST``` environment variable.

| Host Location | Associated Service | Purpose | Container Location |
| ------------- | ------------------ | ------- | ------------------ |
| erddap/erddapData | ERDDAP | Directory where the ERDDAP services writes all it's files, logs, caches, indexes, etc. | /erddapData |
| erddap/datasets.xml | ERDDAP | File that defines the files and directories that will be shared by ERDDAP and how they are to be shared. This file is originally copied from the source files in the ```resources/dev/v2.18``` directory to the host location during the run of setup.sh. It is mounted into the container to override the default configuration file. | /usr/local/tomcat/content/erddap/datasets.xml |

## Debug Web Application

Once you have started the ODSS server in one of the two methods above, you can load the application into your browser by going to:

    http://localhost:3000/odss

and then you can develop the server side code and/or the web application itself. Debugging of the web application should be done in the browser using the developer tools for the browser you are using and are beyond the scope of these instructions.

## Build Docker Image and Deploy to Repo

Once you have completed your development, you will want to commit the changes to the remote Git repo, then build and deploy the ODSS Docker image.

1. Come up with a version number that makes sense for the scope of changes you have made to this codebase. If they are bug fixes, you typically increment the third number in the version, if they are feature enhancements or additions that are not breaking changes for clients of this code, you can increment the second number in the version. If the code changes are substantial and/or will break client usage, you should increment the first digit in the version number.
1. Add a line at the bottom of this file describing the changes that are associated with this new version number.
1. Commit your changes into your local Git repository.
1. Create a Git tag in your local repository with the number you generated in the first step.
1. Push the HEAD to the remote repository
1. Push the tag you created to the remote repository
1. Next, you should generate a new Docker image that can be pushed to the remote DockerHub repository. You can build this by running the following (where x.x.x is the version number you generated in step one.):

        docker build -t mbari/odss:x.x.x .

1. Once the image is done, push it to DockerHub using:

        docker push mbari/odss:x.x.x

where x.x.x is the version number you generated in step one.

## Production

While we do push docker images for the core odss web application, we have often found it useful to simply run a git clone on the production server for the application itself so updates are much easier. So, in essence it is very similar to the development deployment, but the ODSS server needs to be started automatically along with the Docker containers that provide the supporting services.

## Upgrading

## Version History

1.0.0 - Initial checking of complete overhaul and move to Docker. Should have done a 1.0.0 a LONG time ago and have just been working on master.
1.0.1 - Changed the LIMIT query for tracking API calls to use new, more efficient indexes in MBARITracking DB.
1.0.2 - Fixed a bug in ordering that I introduced in 1.0.1 and added the trackingdb API to the httpd.conf file which was introduced in 1.0.4 version of mbaritracking database container. I updated the docker-compose.yml to use that version too.
1.0.3 - Another attempt to fix the PostgreSQL bug. It's really hard to debug, because it's only happening on the production machine and there is no rhyme or reason to it at all.
1.0.4 - Enabled CORS on all routes on server and refactored logging so that it's just one log file that rolls daily and only keeps the last 30 days.
1.1.0 - Moved from ExtJS commercial to open source version
1.1.2 - Pointed help window to docs.mbari.org
1.1.3 - Fix bug with platform selection window that surfaced with move to open source ExtJS
1.1.4 - I just needed to bump the number to allow for another docker push where I am pushing multiple architectures
1.1.5 - Changed ESRI base layer as they deprecated their old one
